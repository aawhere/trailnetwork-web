#!/bin/bash
#!/bin/sh
die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "1 argument required (demo build number), $# provided"
echo $1 | grep -E -q '^(\-?[0-9a-zA-Z])+$' || die "Numeric argument required, $1 provided"

mvn clean appengine:update -Pdemo -Dapiserver=tn-api-demo.appspot.com
mvn clean appengine:update -Pdemo -Dgae.application.version=demo-$1 -Dapiserver=$1.tn-api-demo.appspot.com
