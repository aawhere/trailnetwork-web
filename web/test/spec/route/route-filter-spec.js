define(['../../mock/route/route-fixtures.js', 'underscore', 'common/entities', 'route/route-module'], function(routeFixtures, _, Entities) {
	'use strict';

	describe('routeFilter', function() {
		var routeFilter, defaultRoute, defaultAndAlternateRoutes, defaultRoutes, alternateRoute, alternateRoutes, defaultRouteElevationOverDistance, alternateRouteElevationOverDistance, delta;

		beforeEach(module('tn.route'));

		beforeEach(function() {
			inject(function($injector) {
				var RouteFilter;
				RouteFilter = $injector.get('TnRouteFilter');
				expect(RouteFilter).toBeDefined();
				routeFilter = new RouteFilter();
				expect(routeFilter).toBeDefined();
			});
			defaultRoute = routeFixtures.routes.birslauf;

			defaultRoutes = new Entities();
			defaultRoutes.add(defaultRoute);

			alternateRoute = routeFixtures.routes.firmenlauf;

			alternateRoutes = new Entities();
			alternateRoutes.add(alternateRoute);

			defaultAndAlternateRoutes = new Entities();
			defaultAndAlternateRoutes.add(defaultRoute);
			defaultAndAlternateRoutes.add(alternateRoute);
		});

		beforeEach(function() {
			defaultRouteElevationOverDistance = (defaultRoute.course.track.elevationGain.value / defaultRoute.distance.value);
			alternateRouteElevationOverDistance = (alternateRoute.course.track.elevationGain.value / alternateRoute.distance.value);
			delta = 0.01;
		});

		describe('predicateByDistance', function() {

			it('should filter by distance', function() {
				assertDistancePredicate(defaultRoute, true, {
					min: 0,
					max: defaultRoute.distance.value + delta
				});
			});

			it('should not filter the upper bounds (it is exclusive)', function() {
				assertDistancePredicate(defaultRoute, false, {
					min: 0,
					max: defaultRoute.distance.value
				});
			});

			it('should filter the upper bounds (it is inclusive)', function() {
				assertDistancePredicate(defaultRoute, true, {
					min: defaultRoute.distance.value,
					max: defaultRoute.distance.value + delta
				});
			});

			it('should return all if no filters are present', function() {
				assertDistancePredicate(defaultRoute, true, []);
			});

		});

		describe('predicateByElevation', function() {

			it('should filter by elevation gain', function() {
				assertElevationPredicate(defaultRoute, true, {
					min: 0,
					max: defaultRouteElevationOverDistance + delta
				});
			});

			//Route elevation gain is not available. It will be added in a future version.
			xit('should not filter the upper bounds (it is exclusive)', function() {
				assertElevationPredicate(defaultRoute, false, {
					min: 0,
					max: defaultRouteElevationOverDistance
				});
			});

			it('should filter the upper bounds (it is inclusive)', function() {
				assertElevationPredicate(defaultRoute, true, {
					min: defaultRouteElevationOverDistance,
					max: defaultRouteElevationOverDistance + delta
				});
			});

			it('should return all if no filters are present', function() {
				assertElevationPredicate(defaultRoute, true, []);
			});
		});

		describe('predicateTrailheads', function() {
			it('should limit to the list of trailheads (one)', function() {
				var thIds;
				thIds = [defaultRoute.start.id];
				assertTrailheadsPredicate(defaultRoute, true, thIds);
				assertTrailheadsPredicate(alternateRoute, false, thIds);
			});

			it('should limit to the list of trailheads (multiple)', function() {
				var thIds;
				thIds = [defaultRoute.start.id, alternateRoute.start.id];
				assertTrailheadsPredicate(defaultRoute, true, thIds);
				assertTrailheadsPredicate(alternateRoute, true, thIds);
				assertTrailheadsPredicate(routeFixtures.routes.ffHillRun, false, thIds);
			});

			it('should return all results if trailhedIds empty', function() {
				var result;
				routeFilter.conditions.trailheadIds = [];
				result = routeFilter.predicateTrailheads(defaultRoute);
				expect(result).toBeTruthy();
				result = routeFilter.predicateTrailheads(alternateRoute);
				expect(result).toBeTruthy();
			});
		});

		describe('predicateRoutes', function() {
			it('should limit to the list of routes (one)', function() {
				var routeIds;
				routeIds = [defaultRoute.id];
				assertRoutesPredicate(defaultRoute, true, routeIds);
				assertRoutesPredicate(alternateRoute, false, routeIds);
			});

			it('should limit to the list of routes (multiple)', function() {
				var routeIds;
				routeIds = [defaultRoute.id, alternateRoute.id];
				assertRoutesPredicate(defaultRoute, true, routeIds);
				assertRoutesPredicate(alternateRoute, true, routeIds);
				assertRoutesPredicate(routeFixtures.routes.ffHillRun, false, routeIds);
			});

			it('should return all results if routeIds empty', function() {
				var result;
				routeFilter.conditions.routeIds = [];
				result = routeFilter.predicateRoutes(defaultRoute);
				expect(result).toBeTruthy();
				result = routeFilter.predicateRoutes(alternateRoute);
				expect(result).toBeTruthy();
			});
		});

		describe('predicateAll', function() {

			it('should filter by distance and trailheads predicates', function() {
				assertAllPredicate(defaultRoute, true, {
					min: 0,
					max: defaultRoute.distance.value + delta
				}, [defaultRoute.start.id]);
			});

			it('should filter by routes predicates', function() {
				assertAllPredicate(defaultRoute, true, undefined, undefined, undefined, [defaultRoute.id]);
			});

			it('should filter by distance and trailheads predicates and not match', function() {
				assertAllPredicate(defaultRoute, false, {
					min: 0,
					max: defaultRoute.distance.value + delta
				}, [alternateRoute.start.id]);
			});

			it('should filter by distance and elevation predicates', function() {
				assertAllPredicate(defaultRoute, true, {
					min: 0,
					max: defaultRoute.distance.value + delta
				}, [], {
					min: 0,
					max: defaultRouteElevationOverDistance + delta
				});
			});

			it('should filter by distance, elevation, and trailheadIds predicates', function() {
				assertAllPredicate(defaultRoute, true, {
					min: 0,
					max: defaultRoute.distance.value + delta
				}, [defaultRoute.start.id], {
					min: 0,
					max: defaultRouteElevationOverDistance + delta
				});
			});

		});

		describe('filterAll', function() {

			it('should filter the routes (one)', function() {
				assertAllFilter(defaultAndAlternateRoutes, defaultRoutes, {
					min: 9000,
					max: 11000
				}, [defaultRoute.start.id]);
			});

			it('should filter the routes and preserve score ordering', function() {
				assertAllFilter(defaultAndAlternateRoutes, defaultAndAlternateRoutes, {
					min: 0,
					max: 11000
				}, [defaultRoute.start.id, alternateRoute.start.id]);
			});

			it('should filter the routes and elevation gain for default route', function() {
				assertAllFilter(defaultAndAlternateRoutes, defaultAndAlternateRoutes, {
					min: 0,
					max: 11000
				}, [], {
					min: 0,
					max: defaultRouteElevationOverDistance + 0.01
				});
			});

			//Elevation gain has been removed, it will be added to route in a future release.
			xit('should filter the routes and elevation gain for alternative route', function() {
				assertAllFilter(defaultAndAlternateRoutes, alternateRoutes, {
					min: 0,
					max: 11000
				}, [], {
					min: defaultRouteElevationOverDistance + 0.02,
					max: alternateRouteElevationOverDistance + 0.01
				});
			});

			//Elevation gain has been removed, it will be added to route in a future release.
			xit('should filter the routes, elevation, and trailheadIds', function() {
				assertAllFilter(defaultAndAlternateRoutes, defaultRoutes, {
					min: 0,
					max: 11000
				}, [defaultRoute.startTrailheadId.value], {
					min: 0,
					max: defaultRouteElevationOverDistance + 0.01
				});
			});
		});

		function assertAllFilter(routes, expectedValue, distConditions, trailheadIds, elevationConditions) {
			//Set initial score high so others can be compared (which are in decending order).
			var result, prevScore = 10000000;
			configureAll(distConditions, trailheadIds, elevationConditions);
			result = routeFilter.filterAll(routes);
			expect(result.entities.length).toEqual(expectedValue.entities.length);
			expect(result.get().length).toBe(expectedValue.get().length);
			expect(result.get()).toEqual(expectedValue.get());

			_.each(result.entities, function(r) {
				expect(r.score).toBeLessThan(prevScore);
				prevScore = r.score;
			});
		}

		function assertAllPredicate(route, expectedValue, distConditions, trailheadIds, elevationConditions, routeIds) {
			var result;
			configureAll(distConditions, trailheadIds, elevationConditions, routeIds);
			result = routeFilter.predicateAll(route);
			expect(result).toEqual(expectedValue);
		}

		function assertDistancePredicate(route, expectedValue, conditions) {
			var result;
			configureDistance(conditions);
			result = routeFilter.predicateDistance(route);
			expect(result).toEqual(expectedValue);
		}

		function assertElevationPredicate(route, expectedValue, conditions) {
			var result;
			configureElevation(conditions);
			result = routeFilter.predicateElevation(route);
			expect(result).toEqual(expectedValue);
		}

		function assertTrailheadsPredicate(route, expectedValue, trailheadIds) {
			var result;
			configureTrailheads(trailheadIds);
			result = routeFilter.predicateTrailheads(route);
			expect(result).toBe(expectedValue);
		}

		function assertRoutesPredicate(route, expectedValue, routeIds) {
			var result;
			configureRoutes(routeIds);
			result = routeFilter.predicateRoutes(route);
			expect(result).toBe(expectedValue);
		}

		function configureAll(conditions, trailheadIds, elevationConditions, routeIds) {
			configureDistance(conditions);
			configureTrailheads(trailheadIds);
			configureElevation(elevationConditions);
			configureRoutes(routeIds);
		}

		function configureTrailheads(trailheadIds) {
			routeFilter.conditions.trailheadIds = configurePrepare(trailheadIds);
		}

		function configureRoutes(routeIds) {
			routeFilter.conditions.routeIds = configurePrepare(routeIds);
		}

		function configureDistance(conditions) {
			routeFilter.conditions.distance = configurePrepare(conditions);
		}

		function configureElevation(conditions) {
			routeFilter.conditions.elevation = configurePrepare(conditions);
		}

		function configurePrepare(conditions) {
			if (_.isUndefined(conditions)) {
				conditions = [];
			} else {
				conditions = _.isArray(conditions) ? conditions : [conditions];
			}
			return conditions;
		}

	});
});