define(['angular-mocks', '../../mock/route/route-fixtures.js', 'route/route-service', 'app'],
	function(angular, fixtures) {
		'use strict';

		var standardHeaders = {
			"Accept": "application/json, text/plain, */*"
		};

		describe('tnRouteService', function() {
			var routeService, $httpBackend, trailheadId = '8e6854b89c7';

			beforeEach(module('tn'));
			beforeEach(module('tn.route'));

			beforeEach(inject(function($injector) {
				var trailheadId = '8e6854b89c7';
				$httpBackend = $injector.get("$httpBackend");
				$httpBackend.whenGET(/.*\/rest\/trailheads\/.*\/routes\/mains.json/)
					.respond(fixtures.trailheads[trailheadId].routes);
				$httpBackend.whenGET(/.*\/rest\/routes.*/)
					.respond(fixtures.trailheads[trailheadId].routes);

				routeService = $injector.get('tnRouteService');
			}));

			afterEach(function() {
				$httpBackend.verifyNoOutstandingExpectation();
				$httpBackend.verifyNoOutstandingRequest();
			});

			describe('route mains', function() {

				it('gets main routes from trailhead', function() {
					var mains;

					expect(routeService).toBeDefined();
					routeService.getMainsFromTrailhead(trailheadId).then(function(data) {
						mains = data;
					});
					$httpBackend.flush();
					expect(mains.hasOwnProperty('mains')).toBe(true);

				});

				it('gets main routes with track', function() {
					var mains;

					expect(routeService).toBeDefined();
					routeService.getMainsFromTrailhead(trailheadId).then(function(data) {
						mains = data;
					});
					$httpBackend.expectGET(/.*\/rest\/trailheads\/.*\/routes\/mains.json/,
						angular.extend(standardHeaders));
					$httpBackend.flush(1);

				});
			});

			describe('removeRoutesNotMatchingTrailheads', function() {
				it('should remove the routes', function() {
					var ths, routes;
					routes = [fixtures.routes.birslauf, fixtures.routes.firmenlauf];
					ths = [fixtures.routes.firmenlauf.start];
					expect(routes.length).toBe(2);
					routeService.removeRoutesNotMatchingTrailheads(routes, ths);
					expect(routes[0]).toBe(fixtures.routes.birslauf);
				});
			});

			describe('getRoutesMainsWithCoursesAndTrailheadsAndGpolylineWithinBounds', function() {
				it('should retrive some routes', function() {
					var mains;
					var bounds = {
						northEast: {
							latitude: {
								value: 40
							},
							longitude: {
								value: -100
							}
						},
						southWest: {
							latitude: {
								value: 30
							},
							longitude: {
								value: -130
							}
						}
					};

					expect(routeService).toBeDefined();
					routeService.getMainsWithinBounds(bounds, {
						limit: 2,
						page: 1
					}).then(function(data) {
						mains = data;
					});
					$httpBackend.expectGET(/.*\/rest\/routes.json?limit=2&page=1&options=track-gpolyline&options=route-courses&options=route-start-inline&options=route-finish-inline&bounds=30,-130|40,-100/, standardHeaders);
					$httpBackend.flush(1);
				});
			});
		});
	});