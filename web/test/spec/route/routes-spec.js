/*
define(['route/routes', '../../mock/route/route-fixtures.js', 'route/route-categories', 'underscore', 'route/route-filter'],
	function(Routes, routeFixtures, routeCategories, _, RouteFilter) {
		'use strict';

		xdescribe('Routes', function() {
			var routes, routeFilter, generic, generic2;


			beforeEach(function() {
				routeFilter = new RouteFilter();
				routes = new Routes(routeFilter);
				generic = routeFixtures.routes.birslauf;
				generic2 = routeFixtures.routes.firmenlauf;
			});

			describe('add', function() {
				it('should add a route to its collection', function() {
					expect(function() {
						routes.add(generic);
					}).not.toThrow();
				});

				it('should add a route to its collection with category', function() {
					expect(function() {
						routes.add(generic, routeCategories.prominent);
					}).not.toThrow();
				});
			});

			describe('get', function() {
				it('should get the values added to the collection', function() {
					var result;
					routes.add(generic);
					routes.add(generic2, routeCategories.prominent);
					result = routes.get();
					expect(_.isArray(result)).toBeTruthy();
					expect(result.length).toBe(1);
					expect(result[0].id).toBe(generic.id);

					result = routes.get('notACategory');
					expect(result.length).toBe(0);
				});

				it('should get the values added to the collection with category', function() {
					var result, category;
					category = routeCategories.prominent;
					expect(routes.get().length).toBe(0);
					routes.add(generic, category);
					routes.add(generic2);
					result = routes.get(category);
					expect(_.isArray(result)).toBeTruthy();
					expect(result.length).toBe(1);
					expect(result[0].id).toBe(generic.id);

					result = routes.get('notACategory');
					expect(result.length).toBe(0);
				});
			});

			describe('filter', function() {

				it('should filter the result', function() {
					var result;
					expect(routes.get().length).toBe(0);
					routes.add(generic);
					routes.add(generic2);
					routeFilter.conditions.distance = [{
						min: 9000,
						max: 11000
					}];

					result = routes.filter();
					expect(result).toBeDefined();
					expect(_.isArray(result)).toBeTruthy();
					expect(result.length).toBe(1);
					expect(result[0].id).toBe(generic.id);
				});
			});

			describe('clearAll', function() {
				it('should clear all routes, including all categories', function() {
					var category;
					category = routeCategories.prominent;
					routes.add(generic, category);
					routes.add(generic2);
					expect(routes.get(category).length).toBe(1);
					expect(routes.get().length).toBe(1);
					routes.clearAll();
					expect(routes.get(category).length).toBe(0);
					expect(routes.get().length).toBe(0);
				});
			});

			describe('find', function() {
				it('should find the route in the default category', function() {
					var result;
					routes.add(generic2);
					routes.add(generic);
					result = routes.find(generic.id);
					expect(result.id).toBe(generic.id);
				});
			});

		});

	});
	*/