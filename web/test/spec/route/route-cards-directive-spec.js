define(['../../mock/route/route-fixtures.js', 'common/entities', 'jquery', 'explore/explore-events', 'route/route-cards-directive',  ],
	function(/*routeFixtures, Entities, jQuery, exploreEvents*/) {
		'use strict';

/*
		describe('tnRouteCards', function() {
			var el, scope, elScope, rootScope, id, thId, birslauf;
			birslauf = routeFixtures.routes.birslauf;
			id = birslauf.id;
			thId = birslauf.start.id;

			beforeEach(module('tn'));

			beforeEach(module('views/route/route-cards-directive.html'));

			beforeEach(inject(function($rootScope, $compile, $injector, $httpBackend) {
				var routes;
				routes = new Entities();

				el = angular.element(
					'<tn-route-cards routes="routes" ' +
					'trailheads="trailheads" ' +
					'></tn-route-cards>'
				);
*/
//				$httpBackend.whenGET(/\/views\/.*/).respond('');
/*
				rootScope = $rootScope;
				scope = $rootScope.$new();
				scope.routes = routes;
				scope.routes.add(birslauf);

				scope.trailheads = [birslauf.start];

				el = $compile(el)(scope);
				scope.$digest();
				elScope = el.scope();

				spyOn(rootScope, '$broadcast');
			}));

			it('should respond to a mouse route hover event', function() {
				jQuery(el[0]).mouseover();
				//elScope.mouseoverRoute(id);
				elScope.$digest();
				assertRouteHighlighted(id);
			});

			it('should respond to a mouse route unhover event', function() {
				elScope.mouseoverRoute(id);
				elScope.mouseleaveRoute(id);
				scope.$digest();
				assertRouteHighlighted(undefined);
			});

			it('should respond to a mouse th hover event', function() {
				elScope.mouseoverTh(thId);
				scope.$digest();
				assertThHighlighted(thId);
			});

			it('should respond to a mouse th unhover event', function() {
				elScope.mouseoverTh(thId);
				elScope.mouseleaveTh(thId);
				scope.$digest();
				assertThHighlighted(undefined);
			});

			describe('focusRoute', function() {
				it('should focus the route', function() {
					elScope.focusRoute(id);
					scope.$digest();
					assertFocusRouteId(id);
				});

				it('should unfocus the route if clicked twice', function() {
					elScope.focusRoute(id);
					scope.$digest();
					assertFocusRouteId(id);
					elScope.focusRoute(id);
					scope.$digest();
					assertFocusRouteId(undefined);
				});
			});

			function assertRouteHighlighted(id) {
				expect(rootScope.$broadcast).toHaveBeenCalledWith(exploreEvents.routeHighlighted, id);
				//expect(idFound).toBe(id);
			}

			function assertThHighlighted(id) {
				var idFound;
				if (scope.highlightedTh) {
					idFound = scope.highlightedTh.id;
				}
				expect(idFound).toBe(id);
			}

			function assertFocusRouteId(id) {
				var idFound;
				if (scope.selected) {
					idFound = scope.selected.id;
				}
				expect(idFound).toBe(id);
			}

		});
*/

	});