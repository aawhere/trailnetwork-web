define(['trailhead/trailhead-filter', '../../mock/trailhead/trailhead-fixtures.js', 'trailhead/trailhead-util', 'map/map-util'],
	function(TrailheadFilter, thFixtures, thUtil, mapUtil) {
		'use strict';

		describe('trailhead-filter', function() {
			var ths = [],
				filter;

			beforeEach(function() {
				ths = [];
				ths.push(thFixtures.trailheads.firmenlauf);
				ths.push(thFixtures.trailheads.cupMuttenz);
				filter = new TrailheadFilter();
				filter.conditions.zoomLevel = {
					4: {
						geoCellResolution: 5,
						prominence: {
							prominent: 5,
							prevalent: 25
						}
					},
					3: {
						geoCellResolution: 4,
						prominence: {
							prominent: 3,
							prevalent: 10
						}
					},
					2: {
						geoCellResolution: 3,
						prominence: {
							prominent: 2,
							prevalent: 5
						}
					},
					1: {
						geoCellResolution: 2,
						prominence: {
							prominent: 1,
							prevalent: 2
						}
					}
				};
			});

			describe('filterToCell', function() {
				it('should filter to the provided geocell', function() {
					var result;
					result = filter.filterToCell(ths, '1');
					expect(result.length).toBe(0);
					result = filter.filterToCell(ths, 'e');
					expect(result.length).toBe(2);
				});
			});

			describe('filterProminence', function() {
				it('should filter the trailheads to the provided geocell, zoomLevel, and prominence', function() {
					var result;
					result = filter.filterProminence(ths, thUtil.bounds(ths), 1, 'prominent');
					expect(result.length).toBe(1);
					result = filter.filterProminence(ths, thUtil.bounds(ths), 1, 'prevalent');
					expect(result.length).toBe(2);
				});

				it('trailheads outside the bounds should not be included', function() {
					var result;
					var bounds = mapUtil.boundingBox(1, 1, 0, 0);
					result = filter.filterProminence(ths, bounds, 1, 'prevalent');
					expect(result.length).toBe(0);
				});

				it('trailheads outside the bounds should bot be included take 2', function() {
					var result;
					ths.push(thFixtures.trailheads.chinaCamp);
					var bounds = thUtil.bounds(ths);
					bounds.southWest.longitude.value = 0;
					result = filter.filterProminence(ths, bounds, 4, 'prevalent');
					expect(result.length).toBe(2);
				});
			});

			describe('filterPrevalent', function() {
				it('should filter to prevalent levels', function() {
					var result;
					result = filter.filterPrevalent(ths, thUtil.bounds(ths), 1);
					expect(result.length).toBe(2);
				});
			});
		});
	});