define(['trailhead/trailhead-util', '../../mock/trailhead/trailhead-fixtures.js'], function(thUtil, thFixtures) {
	'use strict';

	describe('trailhead-util', function() {
		var ths;

		beforeEach(function() {
			ths = [thFixtures.trailheads.cupMuttenz, thFixtures.trailheads.firmenlauf];
		});

		describe('sortByScoreDescending', function() {
			it('should sort the trailheads by score decending', function() {
				var result;
				result = thUtil.sortByScoreDescending(ths);
				expect(result[0]).toBe(thFixtures.trailheads.firmenlauf);
				expect(result[1]).toBe(thFixtures.trailheads.cupMuttenz);
			});

		});

		describe('findTrailheadWithId', function() {
			it('should find the trailhead', function() {
				var result, expectedId;
				expectedId = thFixtures.trailheads.cupMuttenz.id;
				result = thUtil.findTrailheadWithId(ths, expectedId);
				expect(result.id).toBe(expectedId);
			});
		});

		describe('bounds', function() {
			it('should find the bounds of the supplied trailheads', function() {
				var result;
				result = thUtil.bounds(ths);
				expect(result.northEast.latitude.value).toBe(47.5371);
				expect(result.northEast.longitude.value).toBe(7.62271);
				expect(result.southWest.latitude.value).toBe(47.53685);
				expect(result.southWest.longitude.value).toBe(7.6167);
			});
		});

	});

});