define(['trailhead/trailhead-repository', '../../mock/trailhead/trailhead-fixtures.js'
	],
	function(ExploreTrailheadRepository, thFixtures) {
		'use strict';

		describe('explore-trailhead-repository', function() {
			var ths = [],
				repo;

			beforeEach(function() {
				ths = [];
				ths.push(thFixtures.trailheads.firmenlauf);
				ths.push(thFixtures.trailheads.cupMuttenz);
				repo = new ExploreTrailheadRepository();
			});

			it('should add a trailhead to the repository', function() {
				repo.add(ths[0]);
				expect(repo.get().length).toBe(1);
			});

			it('should add an array of trailheads to the repository', function() {
				repo.add(ths);
				expect(repo.get().length).toBe(ths.length);
			});

			it('should not allow duplicates to be added trailheads to the repository', function() {
				repo.add(ths);
				expect(repo.get().length).toBe(ths.length);
				repo.add(ths);
				expect(repo.get().length).toBe(ths.length);
			});

			it('should get trailheads by geocell added to the repository', function() {
				var result;
				ths.push(thFixtures.trailheads.chinaCamp);
				repo.add(ths);
				result = repo.getByGeoCell(ths[0].id.substring(0, 5));
				expect(result.get().length).toBe(2);
				result = repo.getByGeoCell(ths[2].id.substring(0, 1));
				expect(result.get().length).toBe(1);
			});

			it('should throw exception if exceeding the max resolution', function() {
				expect(repo.getByGeoCell, ths[0].id.substring(0, repo.MAX_RESOLUTION + 'a')).toThrow();
			});

			it('should return empty result if none found', function() {
				var result;
				result = repo.getByGeoCell('11111');
				expect(result.get().length).toBe(0);
			});

			describe('find', function() {
				it('should find a trailhead', function() {
					var result, expected, trailheads;
					trailheads = thFixtures.requests.sixtyThree.trailhead;
					expected = trailheads[0];
					repo.add(trailheads);
					result = repo.find(expected.id);
					expect(result.id).toBe(expected.id);
				});
			});
		});
	});