define(['angular-mocks', 'trailhead/trailhead-service'], function() {
	'use strict';

	describe('trailhead service using a live network connection', function() {
		//var thService;

		beforeEach(module('tn'));
		beforeEach(module('tn.trailhead'));
		//beforeEach(inject(function(tnTrailheadService) {
		//	console.log('trailheadService' + service);
		//		thService = service;
		//	}));

		it('get trailheads', function() {
			var finished = false;

			//Runs the following code async
			runs(function() {
				//			thService.getTrailheads().then(function(data) {
				//				expect(data.hasOwnProperty('trailhead').toBe(true);
				finished = true;
				//				console.log(data);
				//			});
			});

			//Waits for the value to be true (or timesout)
			waitsFor(function() {
				return finished;
			}, "Timed out waiting for trailheads to be returned", 2000);
		});
	});

	describe('trailhead service using a mock', function() {
		var $httpBackend;
		beforeEach(module('tn.trailhead'));

		beforeEach(inject(function($injector) {
			var trailheads;
			// Our fixture (with inconsequential info deleted for brevity)
			trailheads = {};
			trailheads.all = {
				"filter": {
					"pageTotal": 2
				},
				"trailhead": [{
					"score": 1708,
					"id": "e01b96ce976",
					"location": {
						"value": "47.53685474395752,7.616701126098633",
						"latitude": {
							"value": 47.53685
						},
						"longitude": {
							"value": 7.6167
						}
					},
					"name": {
						"$": "Firmenlauf"
					}
				}, {
					"score": 41,
					"id": "8e630edb20d",
					"location": {
						"value": "37.42862820625305,-122.0963591337204",
						"latitude": {
							"value": 37.42863
						},
						"longitude": {
							"value": -122.09636
						}
					},
					"name": {
						"$": "Unknown"
					}
				}, ]
			};

			$httpBackend = $injector.get("$httpBackend");

			$httpBackend.when("HEAD", "/trailheads").respond();
			$httpBackend.when("TRACE", "/trailheads").respond();
			$httpBackend.when("OPTIONS", "/trailheads").respond();

			$httpBackend.whenGET(/\/trailheads\?.*/).respond(trailheads.all);
			$httpBackend.whenGET("/trailheads/1").respond(trailheads.all[0]);

		}));

		afterEach(function() {
			$httpBackend.verifyNoOutstandingExpectation();
			$httpBackend.verifyNoOutstandingRequest();
		});

		it("should mock the http call", function() {
			var trailheadService, q;
			inject(function(tnTrailheadService) {
				trailheadService = tnTrailheadService;
			});

			q = trailheadService.getTrailheads().$object;
			expect(q).toBeDefined();
			expect(q[0]).toBeUndefined();
			$httpBackend.flush();
			expect(q.length).toBe(2);
			expect(q.trailhead[0].location.latitude.value).toBeGreaterThan(1);
		});
	});
});