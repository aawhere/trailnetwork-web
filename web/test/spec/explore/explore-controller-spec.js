define(['../../mock/trailhead/trailhead-fixtures.js', 'underscore',
		'../../spec/map/map-test-util.js', '../../mock/route/route-fixtures.js', 'explore/explore-controller', 'explore/explore-routes-controller'
	],
	function(thFixtures, _, mapTestUtil, routeFixtures) {
		'use strict';

		describe('explore-controller', function() {
			var $scope, exploreController, thService, exploreTrailheadService, ths,
				httpBackend, routeFilter, exploreRoutesController;

			//module 'tn' has the restangular config info.
			//TODO: factor our the config into so we don't need to rely on tn module
			beforeEach(module('tn'));
			beforeEach(module('tn.explore'));

			beforeEach(inject(function($rootScope, $controller, tnTrailheadService,
				tnRouteService, $location, tnMapUtil, tnExploreTrailheadService, $q, $timeout, $httpBackend, tnLocationService) {

				exploreTrailheadService = tnExploreTrailheadService;
				httpBackend = $httpBackend;

				//httpBackend.whenGET(/.*\/trailheads\/.*\/routes\/mains\?*/)
				//	.respond(routeFixtures.requests.th_e01b96ce976);

				httpBackend.whenGET(/.*\/routes\/mains\?*/)
					.respond(routeFixtures.requests.th_e01b96ce976);

				httpBackend.whenGET(/\/views\/.*/).respond('');

				ths = thFixtures.requests.two;
				thService = tnTrailheadService;
				thService.getTrailheads = function() {
					var deferred = $q.defer();
					$timeout(function() {
						$scope.$apply(function() {
							deferred.resolve(ths);
						});
					}, 0);
					return deferred.promise;
				};

				routeFilter = {};

				$scope = $rootScope.$new();

				spyOn(thService, 'getTrailheads').andCallThrough();

				exploreController = $controller('tnExploreController', {
					$scope: $scope,
					thService: thService,
					routeService: tnRouteService,
					$location: $location,
					mapUtil: tnMapUtil,
					exploreTrailheadService: exploreTrailheadService
					//routeFilter,
					//locationService
				});

				exploreRoutesController = $controller('tnExploreRoutesController', {
					$scope: $scope,
					thService: thService,
					routeService: tnRouteService,
					mapUtil: tnMapUtil,
					exploreTrailheadService: exploreTrailheadService,
					routeFilter: routeFilter,
					locationService: tnLocationService,
					$location: $location
				});

				$scope.map.bounds = mapTestUtil.boundsFittingTrailheads(ths.trailhead);
				$timeout.flush();

				$httpBackend.flush();

			}));

			afterEach(function() {
				httpBackend.verifyNoOutstandingExpectation();
				httpBackend.verifyNoOutstandingRequest();
			});

			xit('should initialize the controller', function() {
				expect(exploreController).toBeDefined();
			});

			xdescribe('ExploreTrailheadsView', function() {
				it('should get the trailheads when initialized', function() {
					var ths;
					ths = exploreTrailheadService.get('e');
					expect(thService.getTrailheads).toHaveBeenCalled();
					expect(ths).toBeDefined();
					expect(_.keys(ths).length).toBe(2);
				});

				it('should get set the trailheads in scope', function() {
					var thsWithProminence;
					thsWithProminence = $scope.trailheads;
					expect(thsWithProminence).toBeDefined();
					expect(thsWithProminence.prominent.length).toBe(2);
					expect(thsWithProminence.prevalent.length).toBe(0);
				});

				xit('should get update the prominent routes', function() {
					var routes;
					routes = $scope.routes.get();
					expect(routes).toBeDefined();
					expect(routes.length).toBeGreaterThan(0);
				});

			});

		});
	});