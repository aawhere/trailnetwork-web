define(['../../mock/trailhead/trailhead-fixtures.js', 'underscore',
		'../../spec/map/map-test-util.js', '../../mock/route/route-fixtures.js', 'explore/explore-controller',
		'explore/explore-trailhead-controller'
	],
	function(/*thFixtures, _, mapTestUtil, routeFixtures*/) {
		'use strict';

/*
		describe('explore-trailhead-controller', function() {
			var $scope, exploreController, exploreTrailheadController, thService, exploreTrailheadService, ths, th,
				httpBackend;

			//module 'tn' has the restangular config info.
			//TODO: factor our the config into so we don't need to rely on tn module
			beforeEach(module('tn'));
			beforeEach(module('tn.explore'));

			beforeEach(inject(function($rootScope, $controller, tnTrailheadService,
				tnRouteService, $location, tnMapUtil, tnExploreTrailheadService, $q, $timeout, $httpBackend) {

				exploreTrailheadService = tnExploreTrailheadService;
				httpBackend = $httpBackend;
*/
//				httpBackend.whenGET(/.*\/trailheads\/routes.json\?*/)
//					.respond(routeFixtures.requests.th_e01b96ce976);
//				httpBackend.whenGET(/.*\/views\?*/)
/*					.respond("");

				th = thFixtures.trailheads.chinaCamp;
				thService = tnTrailheadService;
				thService.getTrailhead = function() {
					var deferred = $q.defer();
					$timeout(function() {
						$scope.$apply(function() {
							deferred.resolve(th);
						});
					}, 0);
					return deferred.promise;
				};

				ths = thFixtures.requests.two;
				thService.getTrailheads = function() {
					var deferred = $q.defer();
					$timeout(function() {
						$scope.$apply(function() {
							deferred.resolve(ths);
						});
					}, 0);
					return deferred.promise;
				};

				$scope = $rootScope.$new();

				spyOn(thService, 'getTrailheads').andCallThrough();

				exploreController = $controller('tnExploreController', {
					$scope: $scope,
					thService: thService,
					routeService: tnRouteService,
					$location: $location,
					mapUtil: tnMapUtil,
					exploreTrailheadService: exploreTrailheadService
					//routeFilter,
					//locationService
				});

				exploreTrailheadController = $controller('tnExploreTrailheadController', {
					$scope: $scope,
					thService: thService,
					routeService: tnRouteService,
					$location: $location,
					//$stateParams,
					//locationService
				});

				$scope.map.bounds = mapTestUtil.boundsFittingTrailheads(ths.trailhead);
				$timeout.flush();

				$httpBackend.flush();

			}));

			afterEach(function() {
				httpBackend.verifyNoOutstandingExpectation();
				httpBackend.verifyNoOutstandingRequest();
			});

			it('should initialize the controller', function() {
				expect(exploreController).toBeDefined();
			});

			describe('ExploreTrailheadsView', function() {

				it('should get update the prominent routes', function() {
					var routes;
					routes = $scope.routes.get();
					expect(routes).toBeDefined();
					expect(routes.length).toBeGreaterThan(0);
				});

			});

		});
*/
	});