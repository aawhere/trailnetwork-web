define(['common/entities', '../../mock/route/route-fixtures.js'], function(Entities, routeFixtures) {
	'use strict';

	describe('Entities', function() {
		var entities,
			route,
			route2;

		beforeEach(function() {
			entities = new Entities();
			route = routeFixtures.routes.birslauf;
			route2 = routeFixtures.routes.firmenlauf;
		});

		describe('add', function() {
			it('should add a route', function() {
				entities.add(route);
				expect(entities.entities.length).toBe(1);
				expect(entities.entities[0].id).toBe(route.id);
				expect(entities.version).toBe(1);
			});

			it('should not add another identical route', function() {
				entities.add(route);
				expect(entities.version).toBe(1);
				expect(entities.entities.length).toBe(1);
				entities.add(route);
				expect(entities.version).toBe(2);
				expect(entities.entities.length).toBe(1);
				expect(entities.entities[0].id).toBe(route.id);
			});

			it('should add an array of routes', function() {
				entities.add([route, route2]);
				expect(entities.version).toBe(1);
				expect(entities.get().length).toBe(2);
			});
		});

		describe('remove', function() {
			it('should remove a route', function() {
				expect(entities.entities.length).toBe(0);
				entities.add(route);
				expect(entities.version).toBe(1);
				expect(entities.entities.length).toBe(1);
				entities.remove(route.id);
				expect(entities.version).toBe(2);
				expect(entities.entities.length).toBe(0);
			});
		});

		describe('get', function() {
			it('should get entities', function() {
				var got;
				entities.add(route);
				expect(entities.version).toBe(1);
				expect(entities.entities.length).toBe(1);
				got = entities.get();
				expect(entities.version).toBe(1);
				expect(got.length).toBe(1);
				expect(got[0].id).toBe(route.id);
			});
		});

		describe('find', function() {

			it('should find the entity', function() {
				var got;
				entities.add(route);
				expect(entities.version).toBe(1);
				expect(entities.entities.length).toBe(1);
				got = entities.find(route.id);
				expect(entities.version).toBe(1);
				expect(got.id).toBe(route.id);
			});

			it('should not find the entity', function() {
				var got;
				expect(entities.entities.length).toBe(0);
				got = entities.find(route.id);
				expect(got).toBe(undefined);
			});
		});

		describe('has', function() {

			it('should determine if it has the entity', function() {
				var got;
				entities.add(route);
				expect(entities.version).toBe(1);
				expect(entities.entities.length).toBe(1);
				got = entities.has(route.id);
				expect(entities.version).toBe(1);
				expect(got).toBeTruthy();
			});

			it('should determine if it does not have the entity', function() {
				var got;
				expect(entities.entities.length).toBe(0);
				got = entities.has(route.id);
				expect(got).toBeFalsy();
			});
		});
	});
});