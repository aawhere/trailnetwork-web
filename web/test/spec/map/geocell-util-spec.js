define(['../../mock/measure-fixtures.js', 'map/geocell-util'], function(measureFixtures, geocellUtil) {
	'use strict';

	describe('geocellUtil', function() {

		function matcherEqualFloat(actual, expected, delta, context) {
			var low, high;
			delta = (delta > 0) ? delta : 0;
			low = expected - delta;
			high = expected + delta;

			var notText = context.isNot ? " not" : "";
			context.message = function() {
				return "Expected " + actual + notText + " to be less than " + high + " and greater than " + low;
			};

			return actual >= low && actual <= high;
		}

		//TODO: can these custom matchers be moved into it's own linbrary?
		beforeEach(function() {
			this.addMatchers({
				toEqualFloat: function(expected, delta) {
					return matcherEqualFloat(this.actual, expected, delta, this);
				}
			});

		});

		describe('geoModelBoundsFromBoundingBox', function() {

			it('should convert the BoundingBox into a format understood by GeoModel', function() {
				var bounds, result;
				bounds = measureFixtures.boundingBox.sample1;
				result = geocellUtil.geoModelBoundsFromBoundingBox(bounds);
				expect(result).toBeDefined();
				expect(result.getNorth()).toBeDefined();
				expect(result.getSouth()).toBeDefined();
				expect(result.getEast()).toBeDefined();
				expect(result.getWest()).toBeDefined();
				expect(result.northEast.lat).toEqualFloat(bounds.northEast.latitude.value);
				expect(result.northEast.lon).toEqualFloat(bounds.northEast.longitude.value);
				expect(result.southWest.lat).toEqualFloat(bounds.southWest.latitude.value);
				expect(result.southWest.lon).toEqualFloat(bounds.southWest.longitude.value);
			});
		});

		describe('geoCellsFromBounds', function() {
			var bounds;

			beforeEach(function() {
				bounds = measureFixtures.boundingBox.sample1;
			});

			it('should get GeoCells with single resolution', function() {
				var geoCells;
				geoCells = geocellUtil.geoCellsFromBounds(bounds);
				expect(geoCells).toEqual(['e01b94e0', 'e01b94e1', 'e01b94e2', 'e01b94e3', 'e01b94e4', 'e01b94e5', 'e01b94e6', 'e01b94e7', 'e01b94e8', 'e01b94e9', 'e01b94ea', 'e01b94eb', 'e01b94ec', 'e01b94ed', 'e01b94ee', 'e01b94ef', 'e01b9640', 'e01b9641', 'e01b9642', 'e01b9643', 'e01b9644', 'e01b9645', 'e01b9646', 'e01b9647', 'e01b9648', 'e01b9649', 'e01b964a', 'e01b964b', 'e01b964c', 'e01b964d', 'e01b964e', 'e01b964f', 'e01b9660', 'e01b9661', 'e01b9662', 'e01b9663', 'e01b9664', 'e01b9665', 'e01b9666', 'e01b9667', 'e01b9668', 'e01b9669', 'e01b966a', 'e01b966b', 'e01b966c', 'e01b966d', 'e01b966e', 'e01b966f', 'e01b96c0', 'e01b96c1', 'e01b96c2', 'e01b96c3', 'e01b96c4', 'e01b96c5', 'e01b96c6', 'e01b96c7', 'e01b96c8', 'e01b96c9', 'e01b96ca', 'e01b96cb', 'e01b96cc', 'e01b96cd', 'e01b96ce', 'e01b96cf']);
			});

			it('should get GeoCells with single resolution of 2', function() {
				var geoCells, resolution = 6;
				geoCells = geocellUtil.geoCellsFromBounds(bounds, resolution);
				expect(geoCells).toEqual(['e01b94', 'e01b96']);
			});
		});

		describe('explode', function() {
			it('should explode the provided cell', function() {
				var cell, result;
				cell = 'a';
				result = geocellUtil.explode(cell);
				expect(result.length).toBe(geocellUtil.NUM_OF_CHILD_CELLS_IN_CELL);
			});
		});

		describe('toSingleResolution', function() {
			it('should limit to a single resolution', function() {
				var cells, result;
				cells = ['ab', 'abcd', 'abc', 'a'];
				result = geocellUtil.toSingleResolution(cells, 3);
				expect(result.length).toBe(256);
			});

			it('should maintain a list of single resolution cells', function() {
				var cells, result;
				cells = ['c00', 'c01', 'caa', 'cab', 'e00', 'e01'];
				result = geocellUtil.toSingleResolution(cells, 3);
				expect(result.length).toBe(6);
			});
		});

		describe('boundingBoxFromGeoModelBounds', function() {
			it('should convert a geoModelBounds to a BoundingBox', function(){
				var bounds, result;
				bounds = measureFixtures.boundingBox.sample1;
				var geomodelBounds = geocellUtil.geoModelBoundsFromBoundingBox(bounds);
				result = geocellUtil.boundingBoxFromGeoModelBounds(geomodelBounds);
				expect(result.northEast.latitude.value).toEqual(bounds.northEast.latitude.value);
				expect(result.northEast.longitude.value).toEqual(bounds.northEast.longitude.value);
				expect(result.southWest.latitude.value).toEqual(bounds.southWest.latitude.value);
				expect(result.southWest.longitude.value).toEqual(bounds.southWest.longitude.value);
			});
		});

		describe('boundsFromCell', function() {
			it('should convert a cell to its bounds', function() {
				var bounds;
				bounds = geocellUtil.boundsFromCell('8');
				expect(bounds.northEast.latitude.value).toEqual(45);
				expect(bounds.northEast.longitude.value).toEqual(-90);
				expect(bounds.southWest.latitude.value).toEqual(0);
				expect(bounds.southWest.longitude.value).toEqual(-180);
			});
		});
	});
});