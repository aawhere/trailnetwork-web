define(['underscore', 'map/map-util'], function(_, mapUtil) {
	'use strict';

	var mapTestUtil = {};

	/**
	 * Creates the minimum bounds that fits the provided trailheads.
	 *
	 * Warning: doesn't consider the 180 maridian.
	 *
	 * @param  {[type]} ths [description]
	 * @return {[type]}     [description]
	 */
	mapTestUtil.boundsFittingTrailheads = function(ths) {
		var north, east, south, west;
		_.each(ths, function(th) {
			var lat = th.location.latitude.value;
			if(_.isUndefined(north) || lat > north) {
				north = lat;
			}
			if (_.isUndefined(south) || lat < south) {
				south = lat;
			}
			var lon = th.location.longitude.value;
			if(_.isUndefined(east) || lon > east) {
				east = lon;
			}
			if (_.isUndefined(west) || lon < west) {
				west = lon;
			}
		});
		return mapUtil.stringToBoundingBox(mapTestUtil.boundingBoxString(north,east,south,west));
	};

	mapTestUtil.boundingBoxString = function(north, east, south, west) {
		return south+','+west+'|'+north+','+east;
	};

	mapTestUtil.boundingBox = function(north, east, south, west) {
		return mapUtil.stringToBoundingBox(mapTestUtil.boundingBoxString(north, east, south, west));
	};

	return mapTestUtil;

});