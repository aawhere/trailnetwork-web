define(['../../mock/route/route-fixtures.js', '../../mock/measure-fixtures.js', 'map/map-util', '../../mock/trailhead/trailhead-fixtures.js'],
       function(routeFixtures, measureFixtures, mapUtil, trailheadFixtures) {
	'use strict';

	describe('mapUtil', function() {
		var tolerance = 0.0001;

		function matcherEqualFloat(actual, expected, delta, context) {
			var low, high;
			delta = (delta > 0) ? delta : 0;
			low = expected - delta;
			high = expected + delta;

			var notText = context.isNot ? " not" : "";
			context.message = function() {
				return "Expected " + actual + notText + " to be less than " + high + " and greater than " + low;
			};

			return actual >= low && actual <= high;
		}

		//TODO: can these custom matchers be moved into it's own linbrary?
		beforeEach(function() {
			this.addMatchers({
				toEqualFloat: function(expected, delta) {
					return matcherEqualFloat(this.actual, expected, delta, this);
				},
				toEqualFloatBox: function(expected, delta) {
					var actual;
					actual = this.actual;

					var notText = this.isNot ? " not" : "";
					this.message = function() {
						return "Expected BoundingBox " + mapUtil.boundingBoxToString(actual) + notText + " to be equal to " +
							mapUtil.boundingBoxToString(expected) + " with a tolerance of " + delta;
					};

					return matcherEqualFloat(actual.northEast.latitude.value, expected.northEast.latitude.value, delta, this) &&
						matcherEqualFloat(actual.northEast.longitude.value, expected.northEast.longitude.value, delta, this) &&
						matcherEqualFloat(actual.southWest.latitude.value, expected.southWest.latitude.value, delta, this) &&
						matcherEqualFloat(actual.southWest.longitude.value, expected.southWest.longitude.value, delta, this);
				}
			});

		});

		describe('points', function() {
			//Google maps has a max decimal of 5 on coordinates.
			var location;

			beforeEach(function() {
				location = measureFixtures.geoCoordinate.sample1;
			});

			describe('createLatLng', function() {
				it('should create a google.maps.LatLng', function() {
					var result;
					result = mapUtil.createLatLng(location.latitude.value, location.longitude.value);
					expect(result).toBeDefined();
					expect(result.lat()).toEqualFloat(location.latitude.value, tolerance);
					expect(result.lng()).toEqualFloat(location.longitude.value, tolerance);
				});
			});

			describe('createLatLngFromLocation', function() {

				it('should create a google.maps.LatLng', function() {

					var result;
					location = trailheadFixtures.trailheads.firmenlauf.location;
					result = mapUtil.latLngFromLocation(location);
					expect(result.lat()).toEqualFloat(location.latitude.value, tolerance);
					expect(result.lng()).toEqualFloat(location.longitude.value, tolerance);
				});
			});
		});

		describe('bounding boxes', function() {
			var boundingBox;

			beforeEach(function() {
				boundingBox = measureFixtures.boundingBox.sample1;
			});

			describe('latLngBoundsFromBoundingBox and boundingBoxFromLatLngBounds', function() {
				it('should convert the tn.measure.BoundingBox to a google.maps.LatLngBounds', function() {
					var latLngBounds, result;
					latLngBounds = mapUtil.latLngBoundsFromBoundingBox(boundingBox);
					expect(latLngBounds).toBeDefined();
					result = mapUtil.boundingBoxFromLatLngBounds(latLngBounds);
					expect(result).toEqualFloatBox(boundingBox, tolerance);
				});
			});

			describe('boundingBoxToString and stringToBoundingBox', function() {
				it('should convert a BoundingBox into a String representation and back', function() {
					var resultString, result;
					resultString = mapUtil.boundingBoxToString(boundingBox);
					expect(resultString).toBe('47.49664,7.60254|47.53784,7.62451');
					result = mapUtil.stringToBoundingBox(resultString);
					expect(result).toEqualFloatBox(boundingBox, tolerance);
				});
			});

			describe('boundingBox', function() {
				it('should create a BoundingBox', function() {
					var north = 1;
					var east = 2;
					var south = -1;
					var west = -2;
					var result = mapUtil.boundingBox(north, east, south, west);
					expect(result.northEast.latitude.value).toBe(north);
					expect(result.northEast.longitude.value).toBe(east);
					expect(result.southWest.latitude.value).toBe(south);
					expect(result.southWest.longitude.value).toBe(west);
				});
			});

			describe('growBoundingBox', function() {
				it('should grow a BoundingBox', function() {
					var north = 1;
					var east = 10;
					var south = -1;
					var west = -2;
					var boundingBox = mapUtil.boundingBox(north, east, south, west);
					var grownBox = mapUtil.growBoundingBox(boundingBox, 0.5);
					expect(grownBox.northEast.latitude.value).toBe(1.50);
					expect(grownBox.northEast.longitude.value).toBe(13);
					expect(grownBox.southWest.latitude.value).toBe(-1.50);
					expect(grownBox.southWest.longitude.value).toBe(-5);
				});
			});

			describe('boundsContainsBounds', function() {
				it('should contain the bounds', function() {
					var north = 1;
					var east = 10;
					var south = -1;
					var west = -2;
					var boundingBox = mapUtil.boundingBox(north, east, south, west);
					var grownBox = mapUtil.growBoundingBox(boundingBox, 0.5);
					var result = mapUtil.boundsContainsBounds(boundingBox, grownBox);
					expect(result).toBe(true);
				});
			});
		});

		describe("geometry math", function() {
			describe("boundsContainsPoint", function() {
				it("should indicate that the point is contained in the box", function() {
					var point, bounds, result;
					bounds = measureFixtures.boundingBox.sample1;
					point = measureFixtures.boundingBox.pointWithinSample1;
					result = mapUtil.boundsContainsPoint(bounds, point);
					expect(result).toBeTruthy();
				});

				it('should indicate that a point on the boundary is contained in the box', function() {
					var point, bounds, result;
					bounds = measureFixtures.boundingBox.sample1;
					point = measureFixtures.boundingBox.pointOnEdgeOfSample1;
					result = mapUtil.boundsContainsPoint(bounds, point);
					expect(result).toBeTruthy();
				});
			});
		});
	});
});