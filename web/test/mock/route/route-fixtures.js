define(function() {
	'use strict';

	return {
		routes: {
			birslauf: {
				"score": 3477,
				"contributingActivities": 69,
				"alternateCount": 0,
				"main": true,
				"id": 4901975695032320,
				"version": 1698,
				"dateCreated": "2013-11-04T08:59:39.770Z",
				"dateUpdated": "2014-05-23T15:24:23.285Z",
				"startTrailheadId": {
					"value": "e01b96ccb42"
				},
				"finishTrailheadId": {
					"value": "e01b96cec68"
				},
				"courseId": {
					"value": "gc-2939038",
					"app": "gc",
					"appId": "2939038"
				},
				"boundingBox": {
					"bounds": "47.49664306640625,7.6025390625|47.537841796875,7.62451171875",
					"northEast": {
						"value": "47.537841796875,7.62451171875",
						"latitude": {
							"type": "latitude",
							"unit": "°",
							"value": 47.53784,
							"display": {
								"type": "latitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "47.537840 °",
								"formattedValue": "47.537840",
								"value": 47.53784
							}
						},
						"longitude": {
							"type": "longitude",
							"unit": "°",
							"value": 7.62451,
							"display": {
								"type": "longitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "7.624510 °",
								"formattedValue": "7.624510",
								"value": 7.62451
							}
						}
					},
					"southWest": {
						"value": "47.49664306640625,7.6025390625",
						"latitude": {
							"type": "latitude",
							"unit": "°",
							"value": 47.49664,
							"display": {
								"type": "latitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "47.496640 °",
								"formattedValue": "47.496640",
								"value": 47.49664
							}
						},
						"longitude": {
							"type": "longitude",
							"unit": "°",
							"value": 7.60254,
							"display": {
								"type": "longitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "7.602540 °",
								"formattedValue": "7.602540",
								"value": 7.60254
							}
						}
					},
					"center": {
						"value": "47.517242431640625,7.613525390625",
						"latitude": {
							"type": "latitude",
							"unit": "°",
							"value": 47.51724,
							"display": {
								"type": "latitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "47.517240 °",
								"formattedValue": "47.517240",
								"value": 47.51724
							}
						},
						"longitude": {
							"type": "longitude",
							"unit": "°",
							"value": 7.61353,
							"display": {
								"type": "longitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "7.613530 °",
								"formattedValue": "7.613530",
								"value": 7.61353
							}
						}
					},
					"diagonalLength": {
						"type": "length",
						"unit": "m",
						"value": 4874.76611,
						"display": {
							"type": "length",
							"lang": "en_US",
							"abbreviatedUnit": "mi",
							"formatted": "3.03 mi",
							"formattedValue": "3.03",
							"value": 3.02904
						}
					}
				},
				"distance": {
					"type": "length",
					"unit": "m",
					"value": 10199.54371,
					"display": {
						"type": "length",
						"lang": "en_US",
						"abbreviatedUnit": "mi",
						"formatted": "6.34 mi",
						"formattedValue": "6.34",
						"value": 6.3377
					}
				},
				"activityTypeStats": [{
					"count": 69,
					"total": 69,
					"percent": {
						"type": "ratio",
						"unit": "ratio",
						"value": 1,
						"display": {
							"type": "ratio",
							"lang": "en_US",
							"abbreviatedUnit": "%",
							"formatted": "100 %",
							"formattedValue": "100",
							"value": 100
						}
					},
					"activityType": {
						"lang": "en-US",
						"type": "ActivityType",
						"key": "RUN",
						"$": "Running"
					}
				}],
				"applicationStats": [{
					"count": 69,
					"total": 69,
					"percent": {
						"type": "ratio",
						"unit": "ratio",
						"value": 1,
						"display": {
							"type": "ratio",
							"lang": "en_US",
							"abbreviatedUnit": "%",
							"formatted": "100 %",
							"formattedValue": "100",
							"value": 100
						}
					},
					"application": {
						"name": {
							"lang": "en-US",
							"type": "Name",
							"key": "GARMIN_CONNECT",
							"$": "Garmin Connect"
						},
						"key": {
							"value": "gc"
						}
					}
				}],
				"mainId": {
					"value": 4901975695032320
				},
				"name": {
					"lang": "en-US",
					"$": "Birslauf"
				},
				"start": {
					"score": 3531,
					"numberOfRoutes": 2,
					"numberOfMains": 2,
					"numberOfAlternates": 0,
					"id": "e01b96ccb42",
					"version": 1787,
					"dateCreated": "2013-11-04T08:59:39.493Z",
					"dateUpdated": "2014-05-23T15:17:18.471Z",
					"location": {
						"value": "47.5344692,7.6155876",
						"latitude": {
							"type": "latitude",
							"unit": "°",
							"value": 47.53447,
							"display": {
								"type": "latitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "47.534470 °",
								"formattedValue": "47.534470",
								"value": 47.53447
							}
						},
						"longitude": {
							"type": "longitude",
							"unit": "°",
							"value": 7.61559,
							"display": {
								"type": "longitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "7.615590 °",
								"formattedValue": "7.615590",
								"value": 7.61559
							}
						}
					},
					"name": {
						"lang": "en-US",
						"$": "Birslauf"
					}
				},
				"course": {
					"id": "gc-2939038",
					"track": {
						"elevationGain": {
							"type": "elevationChange",
							"unit": "m",
							"value": 355,
							"display": {
								"type": "elevationChange",
								"lang": "en_US",
								"abbreviatedUnit": "ft",
								"formatted": "1,165 ft",
								"formattedValue": "1,165",
								"value": 1164.69816
							}
						},
						"startTime": "2009-04-04T16:00:02.000Z"
					}
				}
			},
			firmenlauf: {
				"score": 1836,
				"contributingActivities": 60,
				"alternateCount": 1,
				"main": true,
				"id": 4781114778451968,
				"version": 865,
				"dateCreated": "2013-11-04T09:02:44.941Z",
				"dateUpdated": "2014-05-23T15:25:58.540Z",
				"startTrailheadId": {
					"value": "e01b96cec68"
				},
				"finishTrailheadId": {
					"value": "e01b96cec68"
				},
				"courseId": {
					"value": "gc-198356466",
					"app": "gc",
					"appId": "198356466"
				},
				"boundingBox": {
					"bounds": "47.515869140625,7.613525390625|47.537841796875,7.62451171875",
					"northEast": {
						"value": "47.537841796875,7.62451171875",
						"latitude": {
							"type": "latitude",
							"unit": "°",
							"value": 47.53784,
							"display": {
								"type": "latitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "47.537840 °",
								"formattedValue": "47.537840",
								"value": 47.53784
							}
						},
						"longitude": {
							"type": "longitude",
							"unit": "°",
							"value": 7.62451,
							"display": {
								"type": "longitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "7.624510 °",
								"formattedValue": "7.624510",
								"value": 7.62451
							}
						}
					},
					"southWest": {
						"value": "47.515869140625,7.613525390625",
						"latitude": {
							"type": "latitude",
							"unit": "°",
							"value": 47.51587,
							"display": {
								"type": "latitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "47.515870 °",
								"formattedValue": "47.515870",
								"value": 47.51587
							}
						},
						"longitude": {
							"type": "longitude",
							"unit": "°",
							"value": 7.61353,
							"display": {
								"type": "longitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "7.613530 °",
								"formattedValue": "7.613530",
								"value": 7.61353
							}
						}
					},
					"center": {
						"value": "47.52685546875,7.6190185546875",
						"latitude": {
							"type": "latitude",
							"unit": "°",
							"value": 47.52686,
							"display": {
								"type": "latitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "47.526860 °",
								"formattedValue": "47.526860",
								"value": 47.52686
							}
						},
						"longitude": {
							"type": "longitude",
							"unit": "°",
							"value": 7.61902,
							"display": {
								"type": "longitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "7.619020 °",
								"formattedValue": "7.619020",
								"value": 7.61902
							}
						}
					},
					"diagonalLength": {
						"type": "length",
						"unit": "m",
						"value": 2581.63843,
						"display": {
							"type": "length",
							"lang": "en_US",
							"abbreviatedUnit": "mi",
							"formatted": "1.60 mi",
							"formattedValue": "1.60",
							"value": 1.60416
						}
					}
				},
				"distance": {
					"type": "length",
					"unit": "m",
					"value": 6577.29029,
					"display": {
						"type": "length",
						"lang": "en_US",
						"abbreviatedUnit": "mi",
						"formatted": "4.09 mi",
						"formattedValue": "4.09",
						"value": 4.08694
					}
				},
				"activityTypeStats": [{
					"count": 60,
					"total": 60,
					"percent": {
						"type": "ratio",
						"unit": "ratio",
						"value": 1,
						"display": {
							"type": "ratio",
							"lang": "en_US",
							"abbreviatedUnit": "%",
							"formatted": "100 %",
							"formattedValue": "100",
							"value": 100
						}
					},
					"activityType": {
						"lang": "en-US",
						"type": "ActivityType",
						"key": "RUN",
						"$": "Running"
					}
				}],
				"applicationStats": [{
					"count": 60,
					"total": 60,
					"percent": {
						"type": "ratio",
						"unit": "ratio",
						"value": 1,
						"display": {
							"type": "ratio",
							"lang": "en_US",
							"abbreviatedUnit": "%",
							"formatted": "100 %",
							"formattedValue": "100",
							"value": 100
						}
					},
					"application": {
						"name": {
							"lang": "en-US",
							"type": "Name",
							"key": "GARMIN_CONNECT",
							"$": "Garmin Connect"
						},
						"key": {
							"value": "gc"
						}
					}
				}],
				"mainId": {
					"value": 4781114778451968
				},
				"name": {
					"lang": "en-US",
					"$": "Firmenlauf"
				},
				"start": {
					"score": 1856,
					"numberOfRoutes": 2,
					"numberOfMains": 1,
					"numberOfAlternates": 1,
					"id": "e01b96cec68",
					"version": 3232,
					"dateCreated": "2013-11-04T08:59:39.685Z",
					"dateUpdated": "2014-05-23T15:21:30.971Z",
					"location": {
						"value": "47.536898399221485,7.617071825882484",
						"latitude": {
							"type": "latitude",
							"unit": "°",
							"value": 47.5369,
							"display": {
								"type": "latitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "47.536900 °",
								"formattedValue": "47.536900",
								"value": 47.5369
							}
						},
						"longitude": {
							"type": "longitude",
							"unit": "°",
							"value": 7.61707,
							"display": {
								"type": "longitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "7.617070 °",
								"formattedValue": "7.617070",
								"value": 7.61707
							}
						}
					},
					"name": {
						"lang": "en-US",
						"$": "Firmenlauf"
					}
				},
				"course": {
					"id": "gc-198356466",
					"track": {
						"elevationGain": {
							"type": "elevationChange",
							"unit": "m",
							"value": 684,
							"display": {
								"type": "elevationChange",
								"lang": "en_US",
								"abbreviatedUnit": "ft",
								"formatted": "2,244 ft",
								"formattedValue": "2,244",
								"value": 2244.09449
							}
						},
						"startTime": "2012-06-29T17:00:32.000Z"
					},
					"accountId": {
						"value": "gc-3359350",
						"app": "gc",
						"appId": "3359350"
					}
				}
			},
			ffHillRun: {
				"score": 72,
				"main": true,
				"id": 6193420284461056,
				"version": 37,
				"dateCreated": "2013-11-15T08:58:50.663Z",
				"dateUpdated": "2013-11-26T19:20:13.017Z",
				"start": {
					"score": 485,
					"id": "8e6391b8f67",
					"version": 1370,
					"dateCreated": "2013-11-15T08:53:07.279Z",
					"dateUpdated": "2013-12-03T05:30:28.883Z",
					"location": {
						"value": "37.65810513496399,-121.87729561328888",
						"latitude": {
							"type": "latitude",
							"unit": "°",
							"value": 37.65811,
							"display": {
								"type": "latitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "37.658110 °",
								"formattedValue": "37.658110",
								"value": 37.65811
							}
						},
						"longitude": {
							"type": "longitude",
							"unit": "°",
							"value": -121.8773,
							"display": {
								"type": "longitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "-121.877300 °",
								"formattedValue": "-121.877300",
								"value": -121.8773
							}
						}
					},
					"name": {
						"lang": "en-US",
						"$": "FF HM Training Run Reverse Happy Valley Via Laguna"
					}
				},
				"finish": {
					"score": 485,
					"id": "8e6391b8f67",
					"version": 1370,
					"dateCreated": "2013-11-15T08:53:07.279Z",
					"dateUpdated": "2013-12-03T05:30:28.883Z",
					"location": {
						"value": "37.65810513496399,-121.87729561328888",
						"latitude": {
							"type": "latitude",
							"unit": "°",
							"value": 37.65811,
							"display": {
								"type": "latitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "37.658110 °",
								"formattedValue": "37.658110",
								"value": 37.65811
							}
						},
						"longitude": {
							"type": "longitude",
							"unit": "°",
							"value": -121.8773,
							"display": {
								"type": "longitude",
								"lang": "en_US",
								"abbreviatedUnit": "°",
								"formatted": "-121.877300 °",
								"formattedValue": "-121.877300",
								"value": -121.8773
							}
						}
					},
					"name": {
						"lang": "en-US",
						"$": "FF HM Training Run Reverse Happy Valley Via Laguna"
					}
				},
				"courseId": {
					"value": "gc-262609363",
					"app": "gc",
					"appId": "262609363"
				},
				"course": {
					"id": "gc-262609363",
					"version": 2,
					"dateCreated": "2013-11-15T08:51:17.866Z",
					"dateUpdated": "2013-11-15T08:51:17.908Z",
					"applicationKey": {
						"value": "gc"
					},
					"description": "",
					"name": {
						"lang": "en-US",
						"type": "ActivityMessage",
						"key": "UNKNOWN",
						"$": "Unknown"
					},
					"activityType": {
						"lang": "en-US",
						"type": "ActivityType",
						"key": "RUN",
						"$": "Running"
					},
					"track": {
						"numberOfTrackpoints": 519,
						"elapsedTime": {
							"duration": 3291000,
							"display": {
								"lang": "en_US",
								"type": "duration",
								"compact": "54:51",
								"iso": "PT54M51S",
								"writtenOut": "54 minutes and 51 seconds"
							}
						},
						"start": {
							"elevation": {
								"type": "elevation",
								"unit": "m",
								"value": 104,
								"display": {
									"type": "elevation",
									"lang": "en_US",
									"abbreviatedUnit": "ft",
									"formatted": "341 ft",
									"formattedValue": "341",
									"value": 341.20735
								}
							},
							"timestamp": {
								"date": "2013-01-16T01:38:14.000Z",
								"display": {
									"lang": "en_US",
									"type": "datetime",
									"shortDate": "1/16/13",
									"shortDateTime": "1/16/13 1:38 AM",
									"mediumDate": "Jan 16, 2013",
									"mediumDateTime": "Jan 16, 2013 1:38:14 AM",
									"fullTime": "1:38:14 AM UTC",
									"mediumTime": "1:38:14 AM",
									"sortableDateTime": "2013-01-16T01:38:14.000"
								}
							},
							"location": {
								"value": "37.6580883,-121.8771183",
								"latitude": {
									"type": "latitude",
									"unit": "°",
									"value": 37.65809,
									"display": {
										"type": "latitude",
										"lang": "en_US",
										"abbreviatedUnit": "°",
										"formatted": "37.658090 °",
										"formattedValue": "37.658090",
										"value": 37.65809
									}
								},
								"longitude": {
									"type": "longitude",
									"unit": "°",
									"value": -121.87712,
									"display": {
										"type": "longitude",
										"lang": "en_US",
										"abbreviatedUnit": "°",
										"formatted": "-121.877120 °",
										"formattedValue": "-121.877120",
										"value": -121.87712
									}
								}
							}
						},
						"finish": {
							"elevation": {
								"type": "elevation",
								"unit": "m",
								"value": 97,
								"display": {
									"type": "elevation",
									"lang": "en_US",
									"abbreviatedUnit": "ft",
									"formatted": "318 ft",
									"formattedValue": "318",
									"value": 318.24147
								}
							},
							"timestamp": {
								"date": "2013-01-16T02:33:05.000Z",
								"display": {
									"lang": "en_US",
									"type": "datetime",
									"shortDate": "1/16/13",
									"shortDateTime": "1/16/13 2:33 AM",
									"mediumDate": "Jan 16, 2013",
									"mediumDateTime": "Jan 16, 2013 2:33:05 AM",
									"fullTime": "2:33:05 AM UTC",
									"mediumTime": "2:33:05 AM",
									"sortableDateTime": "2013-01-16T02:33:05.000"
								}
							},
							"location": {
								"value": "37.6576903,-121.8773295",
								"latitude": {
									"type": "latitude",
									"unit": "°",
									"value": 37.65769,
									"display": {
										"type": "latitude",
										"lang": "en_US",
										"abbreviatedUnit": "°",
										"formatted": "37.657690 °",
										"formattedValue": "37.657690",
										"value": 37.65769
									}
								},
								"longitude": {
									"type": "longitude",
									"unit": "°",
									"value": -121.87733,
									"display": {
										"type": "longitude",
										"lang": "en_US",
										"abbreviatedUnit": "°",
										"formatted": "-121.877330 °",
										"formattedValue": "-121.877330",
										"value": -121.87733
									}
								}
							}
						},
						"elevationGain": {
							"type": "elevationChange",
							"unit": "m",
							"value": 84,
							"display": {
								"type": "elevationChange",
								"lang": "en_US",
								"abbreviatedUnit": "ft",
								"formatted": "276 ft",
								"formattedValue": "276",
								"value": 275.59055
							}
						},
						"distance": {
							"type": "length",
							"unit": "m",
							"value": 6256.67776,
							"display": {
								"type": "length",
								"lang": "en_US",
								"abbreviatedUnit": "mi",
								"formatted": "3.89 mi",
								"formattedValue": "3.89",
								"value": 3.88772
							}
						},
						"spatialBuffer": {
							"maxResolution": 8,
							"$": "8 8e 8e6 8e63 8e639 8e6391 8e6391b 8e6391b2 8e6391b3 8e6391b4 8e6391b5 8e6391b6 8e6391b7 8e6391b8 8e6391b9 8e6391ba 8e6391bb 8e6391bd 8e6391be 8e6391bf 8e6391e 8e6391e0 8e6391e1 8e6391e2 8e6391e3 8e6391e8 8e6391e9 8e6391ea 8e6393 8e63931 8e639314"
						},
						"averageSpeed": {
							"type": "velocity",
							"unit": "m/s",
							"value": 1.90115,
							"display": {
								"type": "velocity",
								"lang": "en_US",
								"abbreviatedUnit": "mph",
								"formatted": "4.3 mph",
								"formattedValue": "4.3",
								"value": 4.25275
							}
						},
						"meanDistanceBetweenTrackpoints": {
							"type": "length",
							"unit": "m",
							"value": 12.07853,
							"display": {
								"type": "length",
								"lang": "en_US",
								"abbreviatedUnit": "mi",
								"formatted": "0.01 mi",
								"formattedValue": "0.01",
								"value": 0.00751
							}
						},
						"meanDurationBetweenTrackpoints": {
							"duration": 6353,
							"display": {
								"lang": "en_US",
								"type": "duration",
								"compact": "00:06",
								"iso": "PT6.353S",
								"writtenOut": "6 seconds and 353 milliseconds"
							}
						},
						"boundingBox": {
							"bounds": "37.650146484375,-121.88232421875|37.66387939453125,-121.849365234375",
							"northEast": {
								"value": "37.66387939453125,-121.849365234375",
								"latitude": {
									"type": "latitude",
									"unit": "°",
									"value": 37.66388,
									"display": {
										"type": "latitude",
										"lang": "en_US",
										"abbreviatedUnit": "°",
										"formatted": "37.663880 °",
										"formattedValue": "37.663880",
										"value": 37.66388
									}
								},
								"longitude": {
									"type": "longitude",
									"unit": "°",
									"value": -121.84937,
									"display": {
										"type": "longitude",
										"lang": "en_US",
										"abbreviatedUnit": "°",
										"formatted": "-121.849370 °",
										"formattedValue": "-121.849370",
										"value": -121.84937
									}
								}
							},
							"southWest": {
								"value": "37.650146484375,-121.88232421875",
								"latitude": {
									"type": "latitude",
									"unit": "°",
									"value": 37.65015,
									"display": {
										"type": "latitude",
										"lang": "en_US",
										"abbreviatedUnit": "°",
										"formatted": "37.650150 °",
										"formattedValue": "37.650150",
										"value": 37.65015
									}
								},
								"longitude": {
									"type": "longitude",
									"unit": "°",
									"value": -121.88232,
									"display": {
										"type": "longitude",
										"lang": "en_US",
										"abbreviatedUnit": "°",
										"formatted": "-121.882320 °",
										"formattedValue": "-121.882320",
										"value": -121.88232
									}
								}
							}
						},
						"startTime": "2013-01-16T01:38:14.000Z"
					},
					"account": {
						"remoteId": "3936085",
						"id": "gc-3936085",
						"version": 1,
						"dateCreated": "2013-11-15T08:33:42.205Z",
						"dateUpdated": "2013-11-15T08:33:42.205Z",
						"application": {
							"value": "gc"
						},
						"aliases": [
							"cherycejohnson"
						]
					}
				},
				"mainId": {
					"value": 6193420284461056
				},
				"name": {
					"lang": "en-US",
					"$": "Hill Run"
				}
			}
		},
		requests: {
			"th_e01b96ce976": {
				"filter": {
					"limit": 20,
					"pageTotal": 1,
					"pageCursor": "E-ABAIICImoMc350bi1hcGktZGV2chILEgVSb3V0ZRiAgICAuZmxCgwU",
					"end": 1,
					"finalPage": true,
					"start": 1,
					"condition": [{
						"field": "route-main",
						"operator": "EQUALS",
						"value": "true",
						"expression": "route-main = true"
					}, {
						"field": "route-start",
						"operator": "EQUALS",
						"value": "e01b96ce976",
						"expression": "route-start = e01b96ce976"
					}]
				},
				"route": [{
					"score": 1700,
					"main": true,
					"id": 5845878107537408,
					"version": 233,
					"dateCreated": "2013-11-02T13:03:53.353Z",
					"dateUpdated": "2013-11-26T19:20:06.858Z",
					"start": {
						"score": 1708,
						"id": "e01b96ce976",
						"version": 588,
						"dateCreated": "2013-11-02T13:01:16.835Z",
						"dateUpdated": "2013-11-26T19:20:25.245Z",
						"location": {
							"value": "47.53685474395752,7.616701126098633",
							"latitude": {
								"type": "latitude",
								"unit": "°",
								"value": 47.53685,
								"display": {
									"type": "latitude",
									"lang": "en_US",
									"abbreviatedUnit": "°",
									"formatted": "47.536850 °",
									"formattedValue": "47.536850",
									"value": 47.53685
								}
							},
							"longitude": {
								"type": "longitude",
								"unit": "°",
								"value": 7.6167,
								"display": {
									"type": "longitude",
									"lang": "en_US",
									"abbreviatedUnit": "°",
									"formatted": "7.616700 °",
									"formattedValue": "7.616700",
									"value": 7.6167
								}
							}
						},
						"name": {
							"lang": "en-US",
							"$": "Firmenlauf"
						}
					},
					"finish": {
						"score": 1708,
						"id": "e01b96ce976",
						"version": 588,
						"dateCreated": "2013-11-02T13:01:16.835Z",
						"dateUpdated": "2013-11-26T19:20:25.245Z",
						"location": {
							"value": "47.53685474395752,7.616701126098633",
							"latitude": {
								"type": "latitude",
								"unit": "°",
								"value": 47.53685,
								"display": {
									"type": "latitude",
									"lang": "en_US",
									"abbreviatedUnit": "°",
									"formatted": "47.536850 °",
									"formattedValue": "47.536850",
									"value": 47.53685
								}
							},
							"longitude": {
								"type": "longitude",
								"unit": "°",
								"value": 7.6167,
								"display": {
									"type": "longitude",
									"lang": "en_US",
									"abbreviatedUnit": "°",
									"formatted": "7.616700 °",
									"formattedValue": "7.616700",
									"value": 7.6167
								}
							}
						},
						"name": {
							"lang": "en-US",
							"$": "Firmenlauf"
						}
					},
					"course": {
						"id": "gc-194762888",
						"version": 2,
						"dateCreated": "2013-10-09T10:05:04.379Z",
						"dateUpdated": "2013-10-09T10:05:04.479Z",
						"applicationKey": {
							"value": "gc"
						},
						"description": "",
						"name": {
							"lang": "en-US",
							"$": "Schweizer Firmenlauf"
						},
						"activityType": {
							"lang": "en-US",
							"type": "ActivityType",
							"key": "RUN",
							"$": "Running"
						},
						"track": {
							"numberOfTrackpoints": 453,
							"elapsedTime": {
								"duration": 1854000,
								"display": {
									"lang": "en_US",
									"type": "duration",
									"compact": "30:54",
									"iso": "PT30M54S",
									"writtenOut": "30 minutes and 54 seconds"
								}
							},
							"start": {
								"elevation": {
									"type": "elevation",
									"unit": "m",
									"value": 274,
									"display": {
										"type": "elevation",
										"lang": "en_US",
										"abbreviatedUnit": "ft",
										"formatted": "899 ft",
										"formattedValue": "899",
										"value": 898.95013
									}
								},
								"timestamp": {
									"date": "2012-06-29T17:00:18.000Z",
									"display": {
										"lang": "en_US",
										"type": "datetime",
										"shortDate": "6/29/12",
										"shortDateTime": "6/29/12 5:00 PM",
										"mediumDate": "Jun 29, 2012",
										"mediumDateTime": "Jun 29, 2012 5:00:18 PM",
										"fullTime": "5:00:18 PM UTC",
										"mediumTime": "5:00:18 PM",
										"sortableDateTime": "2012-06-29T17:00:18.000"
									}
								},
								"location": {
									"value": "47.5366996,7.6170493",
									"latitude": {
										"type": "latitude",
										"unit": "°",
										"value": 47.5367,
										"display": {
											"type": "latitude",
											"lang": "en_US",
											"abbreviatedUnit": "°",
											"formatted": "47.536700 °",
											"formattedValue": "47.536700",
											"value": 47.5367
										}
									},
									"longitude": {
										"type": "longitude",
										"unit": "°",
										"value": 7.61705,
										"display": {
											"type": "longitude",
											"lang": "en_US",
											"abbreviatedUnit": "°",
											"formatted": "7.617050 °",
											"formattedValue": "7.617050",
											"value": 7.61705
										}
									}
								}
							},
							"finish": {
								"elevation": {
									"type": "elevation",
									"unit": "m",
									"value": 268,
									"display": {
										"type": "elevation",
										"lang": "en_US",
										"abbreviatedUnit": "ft",
										"formatted": "879 ft",
										"formattedValue": "879",
										"value": 879.26509
									}
								},
								"timestamp": {
									"date": "2012-06-29T17:31:12.000Z",
									"display": {
										"lang": "en_US",
										"type": "datetime",
										"shortDate": "6/29/12",
										"shortDateTime": "6/29/12 5:31 PM",
										"mediumDate": "Jun 29, 2012",
										"mediumDateTime": "Jun 29, 2012 5:31:12 PM",
										"fullTime": "5:31:12 PM UTC",
										"mediumTime": "5:31:12 PM",
										"sortableDateTime": "2012-06-29T17:31:12.000"
									}
								},
								"location": {
									"value": "47.5367515,7.6169092",
									"latitude": {
										"type": "latitude",
										"unit": "°",
										"value": 47.53675,
										"display": {
											"type": "latitude",
											"lang": "en_US",
											"abbreviatedUnit": "°",
											"formatted": "47.536750 °",
											"formattedValue": "47.536750",
											"value": 47.53675
										}
									},
									"longitude": {
										"type": "longitude",
										"unit": "°",
										"value": 7.61691,
										"display": {
											"type": "longitude",
											"lang": "en_US",
											"abbreviatedUnit": "°",
											"formatted": "7.616910 °",
											"formattedValue": "7.616910",
											"value": 7.61691
										}
									}
								}
							},
							"elevationGain": {
								"type": "elevationChange",
								"unit": "m",
								"value": 83,
								"display": {
									"type": "elevationChange",
									"lang": "en_US",
									"abbreviatedUnit": "ft",
									"formatted": "272 ft",
									"formattedValue": "272",
									"value": 272.30971
								}
							},
							"distance": {
								"type": "length",
								"unit": "m",
								"value": 6190.67774,
								"display": {
									"type": "length",
									"lang": "en_US",
									"abbreviatedUnit": "mi",
									"formatted": "3.85 mi",
									"formattedValue": "3.85",
									"value": 3.84671
								}
							},
							"spatialBuffer": {
								"maxResolution": 8,
								"$": "e e0 e01 e01b e01b9 e01b96 e01b966 e01b9665 e01b9667 e01b966d e01b966f e01b96c e01b96c5 e01b96c6 e01b96c7 e01b96cc e01b96cd e01b96ce"
							},
							"averageSpeed": {
								"type": "velocity",
								"unit": "m/s",
								"value": 3.33909,
								"display": {
									"type": "velocity",
									"lang": "en_US",
									"abbreviatedUnit": "mph",
									"formatted": "7.5 mph",
									"formattedValue": "7.5",
									"value": 7.46934
								}
							},
							"meanDistanceBetweenTrackpoints": {
								"type": "length",
								"unit": "m",
								"value": 13.69619,
								"display": {
									"type": "length",
									"lang": "en_US",
									"abbreviatedUnit": "mi",
									"formatted": "0.01 mi",
									"formattedValue": "0.01",
									"value": 0.00851
								}
							},
							"meanDurationBetweenTrackpoints": {
								"duration": 4101,
								"display": {
									"lang": "en_US",
									"type": "duration",
									"compact": "00:04",
									"iso": "PT4.101S",
									"writtenOut": "4 seconds and 101 milliseconds"
								}
							},
							"boundingBox": {
								"bounds": "47.515869140625,7.613525390625|47.537841796875,7.62451171875",
								"northEast": {
									"value": "47.537841796875,7.62451171875",
									"latitude": {
										"type": "latitude",
										"unit": "°",
										"value": 47.53784,
										"display": {
											"type": "latitude",
											"lang": "en_US",
											"abbreviatedUnit": "°",
											"formatted": "47.537840 °",
											"formattedValue": "47.537840",
											"value": 47.53784
										}
									},
									"longitude": {
										"type": "longitude",
										"unit": "°",
										"value": 7.62451,
										"display": {
											"type": "longitude",
											"lang": "en_US",
											"abbreviatedUnit": "°",
											"formatted": "7.624510 °",
											"formattedValue": "7.624510",
											"value": 7.62451
										}
									}
								},
								"southWest": {
									"value": "47.515869140625,7.613525390625",
									"latitude": {
										"type": "latitude",
										"unit": "°",
										"value": 47.51587,
										"display": {
											"type": "latitude",
											"lang": "en_US",
											"abbreviatedUnit": "°",
											"formatted": "47.515870 °",
											"formattedValue": "47.515870",
											"value": 47.51587
										}
									},
									"longitude": {
										"type": "longitude",
										"unit": "°",
										"value": 7.61353,
										"display": {
											"type": "longitude",
											"lang": "en_US",
											"abbreviatedUnit": "°",
											"formatted": "7.613530 °",
											"formattedValue": "7.613530",
											"value": 7.61353
										}
									}
								}
							},
							"startTime": "2012-06-29T17:00:18.000Z"
						},
						"account": {
							"remoteId": "2809255",
							"id": "gc-2809255",
							"version": 1,
							"dateCreated": "2013-10-09T10:05:04.278Z",
							"dateUpdated": "2013-10-09T10:05:04.278Z",
							"application": {
								"value": "gc"
							},
							"aliases": [
								"sitzmanj"
							]
						}
					},
					"mainId": {
						"value": 5845878107537408
					},
					"name": {
						"lang": "en-US",
						"$": "FirmenLauf"
					}
				}]
			}
		},
		trailheads: {
			'8e6854b89c7': {
				'routes': {
					'mains': {
						"route": [{
							"score": 6464,
							"main": true,
							"id": 5044856841830400,
							"version": 19873,
							"dateCreated": "2013-11-11T09:52:51.570Z",
							"dateUpdated": "2013-12-08T16:09:45.997Z",
							"start": {
								"score": 0,
								"id": "8e6854b89c7",
								"version": 0
							},
							"finish": {
								"score": 0,
								"id": "8e6854b89c7",
								"version": 0
							},
							"courseId": {
								"value": "gc-332340341"
							},
							"mainId": {
								"value": 5044856841830400
							},
							"aliases": [{
								"value": 4869929266315264
							}, {
								"value": 5198515739295744
							}, {
								"value": 5143820941393920
							}, {
								"value": 5192616064843776
							}],
							"name": {
								"lang": "en-US",
								"$": "CHINA CAMP"
							}
						}]
					}
				}
			}
		}
	};
});