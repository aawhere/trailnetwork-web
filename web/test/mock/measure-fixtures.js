define(function() {
	'use strict';

	return {
		"geoCoordinate": {
			"sample1": {
				"value": "47.534611,7.6159632",
				"latitude": {
					"type": "latitude",
					"unit": "°",
					"value": 47.53461,
					"display": {
						"type": "latitude",
						"lang": "en_US",
						"abbreviatedUnit": "°",
						"formatted": "47.534610 °",
						"formattedValue": "47.534610",
						"value": 47.53461
					}
				},
				"longitude": {
					"type": "longitude",
					"unit": "°",
					"value": 7.61596,
					"display": {
						"type": "longitude",
						"lang": "en_US",
						"abbreviatedUnit": "°",
						"formatted": "7.615960 °",
						"formattedValue": "7.615960",
						"value": 7.61596
					}
				}
			}
		},
		"boundingBox": {
			"sample1": {
				"bounds": "47.49664306640625,7.6025390625|47.537841796875,7.62451171875",
				"northEast": {
					"value": "47.537841796875,7.62451171875",
					"latitude": {
						"type": "latitude",
						"unit": "°",
						"value": 47.53784,
						"display": {
							"type": "latitude",
							"lang": "en_US",
							"abbreviatedUnit": "°",
							"formatted": "47.537840 °",
							"formattedValue": "47.537840",
							"value": 47.53784
						}
					},
					"longitude": {
						"type": "longitude",
						"unit": "°",
						"value": 7.62451,
						"display": {
							"type": "longitude",
							"lang": "en_US",
							"abbreviatedUnit": "°",
							"formatted": "7.624510 °",
							"formattedValue": "7.624510",
							"value": 7.62451
						}
					}
				},
				"southWest": {
					"value": "47.49664306640625,7.6025390625",
					"latitude": {
						"type": "latitude",
						"unit": "°",
						"value": 47.49664,
						"display": {
							"type": "latitude",
							"lang": "en_US",
							"abbreviatedUnit": "°",
							"formatted": "47.496640 °",
							"formattedValue": "47.496640",
							"value": 47.49664
						}
					},
					"longitude": {
						"type": "longitude",
						"unit": "°",
						"value": 7.60254,
						"display": {
							"type": "longitude",
							"lang": "en_US",
							"abbreviatedUnit": "°",
							"formatted": "7.602540 °",
							"formattedValue": "7.602540",
							"value": 7.60254
						}
					}
				}
			},
			"pointWithinSample1": {
				"latitude": {
					"value": 47.50
				},
				"longitude": {
					"value": 7.61
				}
			},
			"pointOnEdgeOfSample1": {
				"latitude": {
					"value": 47.53784
				},
				"longitude": {
					"value": 7.62451
				}
			}
		}
	};
});