//Mock out the google apis as we don't need them right away
// @See https://github.com/jrburke/requirejs/issues/724
define(function () {
    window.google = {};
    return window.google;
});