var tests = [];
tests.push('domReady!');
tests.push('angular');
tests.push('angular-mocks');
for (var file in window.__karma__.files) {
	if (window.__karma__.files.hasOwnProperty(file)) {
		if (/spec\/.*.js/.test(file)) {
			tests.push(file);
		}
	}
}


//Hack to get rid of 'There is no timestamp for' errors
// See https://github.com/karma-runner/karma-requirejs/issues/6
for (var file in window.__karma__.files) {
	window.__karma__.files[file.replace(/^\//, '')] = window.__karma__.files[file];
}
// End Hack

var appDir = 'build/dev';
var testRelativeDir = '../../../test';

require(['base/'+appDir+'/scripts/tn-config'], function() {
	'use strict';
	require.config({
		baseUrl: 'base/'+appDir+'/scripts',
		waitSeconds: 200,
		paths: {
			'angular-mocks': testRelativeDir + '/vendor/angular-mocks/1.2.3/angular-mocks',
			//create alias to plugins (not needed if plugins are on the baseUrl)
			async: '../vendor/requirejs-plugins/1.0.2/async',
			'google-maps-loader': testRelativeDir + '/vendor/google-maps/google-maps-loader',
			'google-maps-stub': testRelativeDir + '/vendor/google-maps/google-maps-stub',
		},
		shim: {
			'angular-mocks': {
				exports: 'angular'
			}
		}

	});

	require(['google-maps-loader', ''], function() {
		require(tests,
			function() {
				window.__karma__.start();
			});
	});
});