describe('tn.explore', function() {
	'use strict';

	var ExplorePage = require('./explore-page.js');
	var explorePage;

	/**
	 * Note that we keep a series of tasks together to avoid loading the trailnetwork over and over,
	 * which is slow. There may be some interaction effects that fail a test as a result, but it is a trade off
	 * between speed of test and isolation.
	 *
	 * @return {[type]} [description]
	 */
	describe('TrailNetwork', function() {

		it('should load the page', function() {
			explorePage = new ExplorePage();
			explorePage.get();
		});


		it('should have a title', function() {
			expect(browser.getTitle()).toEqual('TrailNetwork');
		});

		/*
		 * Since we leave the app when the back button is pressed, we need to revert to the webDriver instance.
		 * TODO: figure out how this is done.
		 */
		xit('should go back to the previous page', function() {
			browser.driver.navigate().back();
			browser.driver.wait(function() {
				browser.driver.getCurrentUrl().then(function(url) {
					expect(url).toEqual('');
				});
			});
		});

		it('should have the location in the url', function() {
			explorePage.searchParameters().expect.toHaveLocationParameters();
		});

		it('should change map types; url should reflect change. Should update map type to origional if back button is pressed.', function() {
			var mapTypeId = 'hybrid';
			var origionalMapTypeId = explorePage.searchParameters().mapType();
			explorePage.setMapType(mapTypeId);
			explorePage.searchParameters().expect.withMapType(mapTypeId).toMatchCurrentUrl();
			browser.navigate().back();
			origionalMapTypeId.then(function(mt) {
				explorePage.searchParameters().expect.withMapType(mt).toMatchCurrentUrl();
			});
		});

		it('should change the location search parameters if the map is panned', function() {
			var initialParameters = explorePage.searchParameters();
			explorePage.pan();
			var afterPanParameters = explorePage.searchParameters();

			expect(initialParameters.lat()).not.toEqual(afterPanParameters.lat());
			expect(initialParameters.lng()).not.toEqual(afterPanParameters.lng());
		});

		it('should change the zoom level in the location search parameters if the map zoom is changed', function() {
			var initialParameters = explorePage.searchParameters();
			explorePage.zoom(15);
			var afterZoomParameters = explorePage.searchParameters();

			expect(initialParameters.zoom()).not.toEqual(afterZoomParameters.zoom());
		});

		it('should go back to the initial location after a pan if the back button is pressed', function() {
			var page = new ExplorePage();
			page.get();
			var initialParameters = explorePage.searchParameters();
			page.pan();
			var afterPanParameters = explorePage.searchParameters();
			browser.navigate().back();
			var afterBackParameters = explorePage.searchParameters();
			expect(initialParameters.url()).toEqual(afterBackParameters.url());
			expect(afterBackParameters.url()).not.toEqual(afterPanParameters.url());
		});

		it('should set the map to the center, zoom, and map type if reloaded', function() {
			var mapTypeId = 'hybrid';
			var zoom = 15;
			var url;
			var page = new ExplorePage();

			page.get();
			page.setMapType(mapTypeId);
			page.pan();
			page.zoom(zoom);

			url = page.searchParameters().url();
			page.get(url);
			page.mapMatcher().expect.withMapTypeId(mapTypeId).withZoom(zoom).toMatchMap();

		});

	});

});