var ExploreMapSearchParameters = function() {
	'use strict';

	var currentUrl = browser.getCurrentUrl(); //promise
	var regexLocationParameter = /m=(\d+\.\d+),-?(\d+\.\d+),(\d+z),(\w+)/;
	var regexLatitudeIndex = 1;
	var regexLongitudeIndex = 2;
	var regexZoomIndex = 3;
	var regexMapTypeIdIndex = 4;
	var mapTypeId;

	this.lat = function() {
		return currentUrl.then(function(currentUrl) {
			var regexResult = regexLocationParameter.exec(currentUrl);
			return regexResult[regexLatitudeIndex];
		});
	};

	this.lng = function() {
		return currentUrl.then(function(currentUrl) {
			var regexResult = regexLocationParameter.exec(currentUrl);
			return regexResult[regexLongitudeIndex];
		});
	};

	this.zoom = function() {
		return currentUrl.then(function(currentUrl) {
			var regexResult = regexLocationParameter.exec(currentUrl);
			return regexResult[regexZoomIndex];
		});
	};

	this.mapType = function() {
		return currentUrl.then(function(currentUrl) {
			var regexResult = regexLocationParameter.exec(currentUrl);
			return regexResult[regexMapTypeIdIndex];
		});
	};

	this.url = function() {
		return currentUrl;
	};

	this.expect = {};

	this.expect.withMapType = function(id) {
		mapTypeId = id;
		return this;
	};

	this.expect.toMatchCurrentUrl = function() {
		currentUrl.then(function(currentUrl) {
			var regexResult = regexLocationParameter.exec(currentUrl);
			if (mapTypeId) {
				expect(regexResult[regexMapTypeIdIndex]).toBe(mapTypeId);
			}
		});
	};

	this.expect.toHaveLocationParameters = function() {
		expect(currentUrl).toMatch(regexLocationParameter);
	};

	this.expect.toHaveMapType = function() {
		currentUrl.then(function(currentUrl) {
			var regexResult = regexLocationParameter.exec(currentUrl);
			expect(regexResult[regexMapTypeIdIndex]).toBeTruthy();
		});
	};
};

var ExploreMap = function(mapElement) {
	'use strict';

	this.map = mapElement;
	var mapTypeId;
	var zoom;
	var lat;
	var lng;

	this.expect = {};

	this.expect.withMapTypeId = function(id) {
		mapTypeId = id;
		return this;
	};

	this.expect.withZoom = function(z) {
		zoom = z;
		return this;
	};

	this.expect.withLatitude = function(latitude) {
		lat = latitude;
		return this;
	};

	this.expect.withLongitude = function(longitude) {
		lng = longitude;
		return this;
	};

	this.expect.toMatchMap = function() {
		if (mapTypeId) {
			expect(mapElement.evaluate("explore.map.service.mapTypeId()")).toEqual(mapTypeId);
		}
		if (zoom) {
			expect(mapElement.evaluate("explore.map.service.zoom()")).toEqual(zoom);
		}
		if (lat) {
			expect(mapElement.evaluate("explore.map.service.center().latitude.value")).toEqual(lat);
		}
		if (lng) {
			expect(mapElement.evaluate("explore.map.service.center().longitude.value")).toEqual(lng);
		}
	};
};

var ExplorePage = function() {
	'use strict';

	var trailNetworkUrl = 'http://0.0.0.0:9000';
	this.map = element(by.css('.explore-map'));
	this.cardTrayItems = element.all(by.repeater('r in routes.get()'));

	this.get = function(url) {
		if (url) {
			url.then(function(resolvedUrl) {
				browser.get(resolvedUrl);
			});
		} else {
			browser.get(trailNetworkUrl);
		}
	};

	this.setMapType = function(mapTypeId) {
		this.map.evaluate("setMapTypeId('" + mapTypeId + "')");
	};

	this.searchParameters = function() {
		return new ExploreMapSearchParameters();
	};

	this.mapMatcher = function() {
		return new ExploreMap(this.map);
	};

	this.pan = function(distance) {
		distance = distance || {
			x: 40,
			y: 40
		};
		browser.actions().dragAndDrop(
			this.map, distance
		).perform();
		//time for google maps to update.
		browser.sleep(500);
	};

	this.zoom = function(zoom) {
		this.map.evaluate("explore.map.service.zoom(" + zoom + ")");
		//time for google maps to update.
		browser.sleep(1000);
	};

	this.firstRoute = function() {
		return this.cardTrayItems[0].element(by.css('.route-card'));
	};
};

module.exports = ExplorePage;