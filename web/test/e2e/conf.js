exports.config = {
	seleniumAddress: 'http://localhost:4444/wd/hub',

	suites: {
		explore: 'explore/**/*.js',
		route: 'route/**/*.js',
	},

	specs: ['**/*.js'],

	jasmineNodeOpts: {
		showColors: true,
		defaultTimeoutInterval: 30000
	}
};