var RoutePage = function(url) {
	'use strict';

	if (url) {
		this.routeUrl = url;
	} else {
		this.routeUrl = 'http://0.0.0.0:9000/route/4649255557922816';
	}

	this.get = function() {
		browser.get(this.routeUrl);
	};

};

module.exports = RoutePage;