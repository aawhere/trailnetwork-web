describe('tn.explore', function() {
	'use strict';

	var ExplorePage = require('../explore/explore-page.js');
	var explorePage;
	var RoutePage = require('./route-page.js');
	var routePage = new RoutePage();

	/**
	 * Note that we keep a series of tasks together to avoid loading the trailnetwork over and over,
	 * which is slow. There may be some interaction effects that fail a test as a result, but it is a trade off
	 * between speed of test and isolation.
	 *
	 * @return {[type]} [description]
	 */
	describe('Route', function() {

		//For some reason it will load the page if it is in a seperate test, but not when
		//other steps follow from within the same test.
		it('should load the Explore page to setup tests', function() {
			explorePage = new ExplorePage();
			explorePage.get();
			expect(browser.getTitle()).toMatch(/(.+)/);
		});

		it('should transition to a route when the card is clicked.', function() {
			explorePage.firstRoute().click();
			explorePage.searchParameters().expect.toHaveLocationParameters();
		});


		it('should have a title', function() {
			expect(browser.getTitle()).toMatch(/(.+)\sRoute\s\|\sTrailNetwork/);
		});

		//How to test???
		xit('should zoom into the route bounds', function() {

		});

		it('should have map location parameters when visited directly', function() {
			routePage.get();
			explorePage.searchParameters().expect.toHaveLocationParameters();
		});

	});

});