'use strict';

/*
 * Development server 'connect' configuration.
 */
var modRewrite = require('connect-modrewrite');
var mountFolder = function(connect, dir) {
	return connect.static(require('path').resolve(dir));
};


module.exports = function(grunt) {
	var project, pkg;

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	/*
	 * read in the package.json, which has useful values such as version.
	 */
	pkg = grunt.file.readJSON('package.json');

	/*
	 * Place constant variable names here and reference throughout gruntfile.
	 */

	project = {
		dir: 'app',
		app: 'app',
		tmp: '.tmp',
		scripts: 'scripts',
		karma: {
			configFile: 'karma.conf.js'
		},
		changelog: {
			dir: 'app/version/',
			changelogMdFile: 'app/version/CHANGELOG.md',
			changelogHtmlFile: 'app/version/CHANGELOG.html',
		},
	};

	project.scripts = {};
	project.scripts.dir = 'scripts';
	project.scripts.path = [project.dir, project.scripts.dir].join('/');

	project.styles = {};
	project.styles.dir = 'styles';
	project.styles.path = [project.app, project.styles.dir].join('/');

	project.fonts = {};
	project.fonts.dir = 'fonts';
	project.fonts.path = [project.app, project.fonts.dir].join('/');

	project.images = {};
	project.images.dir = 'images';
	project.images.path = [project.app, project.images.dir].join('/');

	project.vendor = {};
	project.vendor.dir = 'vendor';
	project.vendor.path = [project.app, project.vendor.dir].join('/');

	project.build = {
		now: grunt.template.today('yyyymmdd'), // Alternative: yyyymmddhhMMss
		version: pkg.version,
		dir: 'build',
		tmpDir: '.tmp',
		proxyServer: grunt.option('proxyServer') || '',
		gruntProxyServer: grunt.option('gruntProxyServer') || 'dev.trailnetwork.com',
		gruntProxyServerPort: grunt.option('gruntProxyServerPort') || '80',
		googleKeyDev: 'AIzaSyAbBjn3CZ_MOAVWD8l6tfVec4hoNacylBY',
		googleKeyDemo: 'AIzaSyBsFkEP0I2pxC6lv3ir2K2Ud6jAVsDHRK8',
		googleKeyLive: 'AIzaSyCyf5COPoHDzTihXktFZiRRCTee8gd7jYc',
	};
	project.build.path = project.build.dir;

	//for the template plugin
	grunt.template.addDelimiters('handlebars-like-delimiters', '{{', '}}');

	// Define the configuration for all the tasks
	grunt.initConfig({

		// Project settings
		project: project,
		pkg: pkg,

		// Add vendor prefixed styles
		autoprefixer: {
			options: {
				browsers: ['last 2 version', 'Android 4']
			},
			all: {
				files: [{
					expand: true,
					cwd: project.build.path,
					src: '{,*/}*.css',
					dest: project.build.path,
				}]
			},
		},

		bump: {
			options: {
				files: ['package.json'],
				updateConfigs: [],
				commit: true,
				commitMessage: 'Release v%VERSION%',
				commitFiles: ['package.json', project.changelog.changelogMdFile, project.changelog.changelogHtmlFile],
				createTag: true,
				tagName: 'v%VERSION%',
				tagMessage: 'Version %VERSION%',
				push: true,
				pushTo: 'origin',
				gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d'
			}
		},

		cacheBust: {
			live: {
				options: {
					encoding: 'utf8',
					algorithm: 'md5',
					length: 16,
					jsonOutput: true,
					baseDir: project.build.path,
					filters: {
						'script': [

							function() {
								return this.attribs['data-main'];
							}, // for requirejs mains.js
							function() {
								return this.attribs.src;
							}, // keep default 'src' mapper
						]
					},
				},
				files: [{
					src: [project.build.path + '/index.html']
				}]
			}
		},

		changelog: {
			options: {
				dest: project.changelog.changelogMdFile,
				commitLink: function(hash) {
					return '[' + hash + '](https://bitbucket.org/aawhere/trailnetwork-web/commits/' + hash + ')';
				},
				issueLink: function(issueId) {
					return '[' + issueId + '](https://aawhere.jira.com/browse/' + issueId + ')';
				},
				subtitle: grunt.template.today('yyyy-mm-dd'),
			}
		},

		/**
		 * Clean files and folders.
		 *
		 * @see https://github.com/gruntjs/grunt-contrib-clean
		 */
		clean: {
			options: {
				// force: true, //Allows for deletion of folders outside currat working dir (CWD). Use with caution.
			},
			all: {
				files: [{
					dot: true,
					src: [project.build.path, project.build.tmpDir]
				}]
			},
		},

		// The actual grunt server settings
		connect: {
			options: {
				port: 9000,
				// Change this to '0.0.0.0' to access the server from outside.
				hostname: '0.0.0.0',
				livereload: 35729
			},
			livereload: {
				options: {
					middleware: function(connect) {
						return [
							require('grunt-connect-proxy/lib/utils').proxyRequest,
							modRewrite([
								//'!\\.\\w+$ /index.html'
								'!(favicon.ico|favicon.png|scripts|views|views-nopreprocess|styles|images|fonts|locale|ui|vendor|version|users|rest|datasources|playground.html).*$ /index.html'
							]),
							mountFolder(connect, project.build.path),
							// function myMiddleware(req, res, next) {
							// 	if (req.url.indexOf('/users') === 0) {
							// 		res.end(grunt.file.read('scripts/user-admin.json'));
							// 	} else {
							// 		return next();
							// 	}
							// }
						];
					},
					open: false
				},
				proxies: [{
					context: ['/users/current', '/users/success', '/rest', '/datasources'],
					host: project.build.gruntProxyServer,
					port: project.build.gruntProxyServerPort,
					https: false,
					xforward: false,
					headers: {
						"host": project.build.gruntProxyServer
					}
				}]
			},
			test: {
				options: {
					port: 9001,
					middleware: function(connect) {
						return [
							mountFolder(connect, 'test'),
							mountFolder(connect, project.build.path)
						];
					}
				}
			},
		},

		// Copies remaining files to places other tasks can use
		copy: {
			dev: {
				files: [{
					expand: true,
					dot: true,
					cwd: project.dir,
					dest: project.build.path,
					src: [
						'*.{ico,png,txt,html}',
						'vendor/**/*',
						'images/**/*',
						'fonts/*',
						'scripts/**/*.js',
						'views/**/*.html',
						'views-nopreprocess/**/*.html',
						'users/**/*',
					]
				}]
			},
			demo: '<%= copy.live %>',
			live: {
				files: [{
						expand: true,
						dot: true,
						cwd: project.dir,
						dest: project.build.path,
						src: [
							'*.{ico,png,txt}',
							'vendor/**/*',
							'images/{,*/}*.{webp}',
							'fonts/*',
							'scripts/tn-config.js',
							'scripts/tn.js',
							'views/**/*.html',
							'views-nopreprocess/**/*.html',
							'users/**/*',
						]
					},
					//to support requirejs and preprocessor
					{
						expand: true,
						dot: true,
						cwd: project.dir,
						dest: project.build.tmpDir,
						src: [
							'scripts/**/*.js', 'vendor/**/*'
						]
					}
				]
			},
		},

		/**
		 * Validate files with JSHint.
		 *
		 * @see https://github.com/gruntjs/grunt-contrib-jshint
		 * @see http://www.jshint.com/docs/
		 */
		jshint: {
			options: {
				jshintrc: '.jshintrc',
				reporter: require('jshint-stylish'),
				white: false
			},
			app: {
				src: [
					'Gruntfile.js',
					project.scripts.path + '/{,*/}*.js'
				]
			},
			test: {
				options: {
					jshintrc: 'test/.jshintrc'
				},
				src: ['test/spec/{,*/}*.js']
			}
		},

		markdown: {
			all: {
				files: [{
					expand: true,
					src: project.changelog.dir + '/*.md',
					ext: '.html'
				}]
			}
		},

		preprocess: {
			options: {
				inline: true,
				context: {
					proxyServer: project.build.proxyServer,
					version: project.build.version,
				}
			},
			dev: {
				options: {
					context: {
						googleKey: project.build.googleKeyDev,
					}
				},
				src: [project.build.dir + '/' + project.scripts.dir + '/app-route-constants.js', project.build.path + '/index.html'],
			},
			demo: {
				options: {
					context: {
						googleKey: project.build.googleKeyDemo,
					},
				},
				src: [project.build.tmpDir + '/' + project.scripts.dir + '/app-route-constants.js', project.build.path + '/index.html'],
			},
			live: {
				options: {
					context: {
						googleKey: project.build.googleKeyLive,
					},
				},
				src: '<%= preprocess.demo.src %>',
			}
		},

		// Compiles Sass to CSS and generates necessary files if requested
		// compass: {
		// 	options: {
		// 		sassDir: project.styles.path,
		// 		imagesDir: project.images.path,
		// 		javascriptsDir: project.scripts.path,
		// 		fontsDir: project.fonts.path,
		// 		importPath: project.vendor.path,
		// 		httpImagesPath: '/' + project.images.dir,
		// 		httpGeneratedImagesPath: '/' + project.images.dir + '/generated',
		// 		httpFontsPath: project.fonts.path,
		// 		relativeAssets: false,
		// 		assetCacheBuster: false
		// 	},
		// 	all: {
		// 		options: {
		// 			//banner: '<%= banner.long %>',
		// 			//debugInfo: true,
		// 			//style: 'expanded', // Output style. Can be nested, compact, compressed, expanded.
		// 			//generatedImagesDir: '<%= project.build.dev.path %>/images',
		// 			cssDir: [project.build.path, project.styles.dir].join('/'),
		// 		}
		// 	},
		// },

		cssmin: {
			live: {
				files: {
					'<%= [project.build.path, project.styles.dir].join("/")%>/main.css': [
						[project.build.path, project.styles.dir].join('/') + '/main.css',
						project.styles.path + '/{,*/}*.css'
					]
				}
			}
		},

		/*
		 * Runs requirejs's optimize feature to smartly concatnate the javascript files
		 * into one file then run it through an angularjs compatible minimizer
		 */
		requirejs: {
			live: {
				options: {
					baseUrl: [project.build.tmpDir, project.scripts.dir].join('/'),
					mainConfigFile: project.scripts.path + '/tn-config.js',
					//name: 'tn-bootstrap',
					//out: '<%= project.build.live.path %>/<%= project.scripts %>/tn-bootstrap.js',
					dir: [project.build.path, project.scripts.dir].join('/'),
					removeCombined: true,
					//required for generateSourceMaps. See http://requirejs.org/docs/errors.html#sourcemapcomments
					preserveLicenseComments: false,
					generateSourceMaps: true,
					optimize: "uglify2",
					//optimize: "none",
					modules: [
						{
							name: 'vendor',
							include: ['jquery',
								'underscore',
						  		'angular',
						  		'angular-uirouter',
						  		'angular-material',
						  		'angular-translate',
						  		'restangular',
						  		'domReady',
						  		'spinner',
							]
						},
						{
							name: 'tn-bootstrap',
							exclude: ['vendor'],
						},
						{
							name: 'tn-auth-bootstrap',
							exclude: ['vendor'],
						}
					],
					// paths: {
					//We'll include these manually
					//'jquery': 'empty:',
					// 'angular': 'empty:',
					// 'angular-route': 'empty:',
					// 'angular-sanitize': 'empty:',
					// 'angular-google-maps': 'empty:',
					// 'restangular': 'empty:',
					//},
					uglify2: {
						warnings: true,
						//Required for angularjs
						mangle: false
					},
					done: function(done, output) {
						var duplicates = require('rjs-build-analysis').duplicates(output);

						if (duplicates.length > 0) {
							grunt.log.subhead('Duplicates found in requirejs build:');
							grunt.log.warn(duplicates);
							return done(new Error('r.js built duplicate modules, please check the excludes option.'));
						}

						done();
					}
				}
			}
		},

		sass: {
			options: {
				sourceMap: true
			},
			dist: {
				files: {
					'main.css': 'main.scss'
				}
			},
			dev: {
				files: {
					'<%= project.build.dir %>/<%= project.styles.dir %>/main.css': project.styles.path + '/main.scss'
				},
			},
			live: {
				files: {
					'<%= project.build.dir %>/<%= project.styles.dir %>/main.css': project.styles.path + '/main.scss'
				},
			},
		},

		/**
		 * Concatenate the html templates an pre-populate them into
		 * the $templateCache
		 * @type {Object}
		 */
		ngtemplates: {
			live: {
				options: {
					htmlmin: {
						//collapseBooleanAttributes: true,
						//collapseWhitespace: true,
						//removeAttributeQuotes: true,
						//removeComments: true, // Only if you don't use comment directives!
						//removeEmptyAttributes: true,
						//removeRedundantAttributes: true,
						//removeScriptTypeAttributes: true,
						//removeStyleLinkTypeAttributes: true
					},
					module: 'tn',
					bootstrap: function(module, script) {
						return "define(['tn-module'], function(tnModule) {" +
							"  tnModule.run(['$templateCache', function($templateCache) {" +
							"console.log('caching templates');" +
							script +
							"  }]);" +
							"});";
					},
					url: function(url) {
						return url.replace('app', '');
					},
				},
				src: project.dir + '/views/**/**.html',
				dest: project.build.tmpDir + '/' + project.scripts.dir + '/tn-templates.js'
			}
		},

		/*
		 * processes html files for build blocks, performing the directed transformation of those blocks.
		 * Here it is being used to replace the requirejs declaration useed during development
		 * with the liveuction ready script file.
		 */
		processhtml: {
			options: {
				// Task-specific options go here.
			},
			live: {
				files: [{
					expand: true,
					cwd: project.dir,
					//Note that this list's order is importaint.
					src: ['**/*.html', '!vendor/**'],
					dest: project.build.path
				}]
			},
		},

		// The following *-min tasks liveuce minified files in the dist folder
		imagemin: {
			live: {
				files: [{
					expand: true,
					cwd: '<%= project.app %>/images',
					src: '{,**/}*.{png,jpg,jpeg,gif}',
					dest: project.build.path + '/images'
				}]
			}
		},
		svgmin: {
			live: {
				files: [{
					expand: true,
					cwd: '<%= project.app %>/images',
					src: '{,*/}*.svg',
					dest: project.build.path + '/images'
				}]
			}
		},

		/*
		 * Minify html. This is performed in the dist directory as processhtml already
		 * copies the html files to the destination.
		 *
		 * Note: Most processing is currently disabled.
		 */
		htmlmin: {
			live: {
				options: {
					removeComments: false, //index.html has IE related stuff in it.
					collapseWhitespace: false,
					// Optional configurations that you can uncomment to use
					// removeCommentsFromCDATA: true,
					// collapseBooleanAttributes: true,
					// removeAttributeQuotes: true,
					// removeRedundantAttributes: true,
					// useShortDoctype: true,
					// removeEmptyAttributes: true,
					// removeOptionalTags: true*/
				},
				files: [{
					expand: true,
					cwd: project.build.path,
					//Note that this list's order is importaint.
					src: ['**/*.html', '!vendor/**'],
					dest: project.build.path,
				}]
			}
		},

		// Allow the use of non-minsafe AngularJS files. Automatically makes it
		// minsafe compatible so Uglify does not destroy the ng references
		ngAnnotate: {
			options: {
				singleQuotes: true,
				add: true,
			},
			live: {
				files: [{
					expand: true,
					dot: true,
					cwd: project.build.tmpDir,
					src: [
						project.scripts.dir + '/**/*.js'
					],
					dest: project.build.tmpDir,
				}, ],
			},
		},

		pagespeed: {
			options: {
				nokey: true,
				url: "http://dev.trailnetwork.com",
				//strategy: "mobile"
				strategy: "mobile",
				locale: "en_US",
			},
			desktop: {
				strategy: "desktop"
			},
			mobile: {
				strategy: "mobile"
			},
			google: {
				strategy: "mobile",
				url: "http://google.com"
			}
		},

		// Run some tasks in parallel to speed up the build process
		// concurrent: {
		// 	dev: [
		// 		'newer:jshint:app',
		// 		'sass:dev',
		// 		'newer:copy:dev'
		// 	],
		// 	prod: [
		// 		'copy:prod',
		// 		'sass:prod',
		// 		'imagemin:prod',
		// 		'svgmin:prod',
		// 		'processhtml:prod'
		// 	]
		// },

		// Test settings
		karma: {
			unit: {
				configFile: '<%= project.karma.configFile %>',
				singleRun: true
			},
			watch: {
				configFile: '<%= project.karma.configFile %>',
				singleRun: false
			}
		},

		/**
		 * Run predefined tasks whenever watched file patterns are added, changed
		 * or deleted.
		 *
		 * @see https://github.com/gruntjs/grunt-contrib-watch
		 */
		watch: {
			options: {
				livereload: '<%= connect.options.livereload %>',
				livereloadOnError: false,
			},
			dev: {
				files: [project.dir + '/**/*', '!' + project.vendor.path + '/**'],
				tasks: ['default'],
			},

		},
	});

	/*----------------------------------( TASKS )----------------------------------*/

	grunt.registerTask('build', function(target) {
		target = target || 'dev';
		var liveOrDev = (target === 'demo' || target === 'live') ? 'live' : 'dev';

		var tasks = [
			//			'concurrent:' + target,
		];

		if (target === 'dev') {
			tasks.push('newer:copy:dev');
			tasks.push('newer:jshint:app');
			tasks.push('sass:dev');
			tasks.push('autoprefixer');
			tasks.push('preprocess:dev');
			//tasks.push('newer:copy:dev'),
		} else if (target === 'live' || target === 'demo') {
			tasks.push('clean:');
			tasks.push('copy:' + target);
			tasks.push('sass:' + liveOrDev);
			tasks.push('imagemin:' + liveOrDev);
			tasks.push('svgmin:' + liveOrDev);
			tasks.push('processhtml:' + liveOrDev);
			tasks.push('autoprefixer');
			//'htmlmin:prod',
			tasks.push('preprocess:' + target);
			tasks.push('ngtemplates::' + liveOrDev);
			tasks.push('ngAnnotate::' + liveOrDev);
			tasks.push('requirejs:' + liveOrDev);
			tasks.push('cssmin:' + liveOrDev);
			tasks.push('cacheBust:' + liveOrDev);
		} else if (target === 'demo') {
			//Add demo tasks here.
		} else if (target === 'live') {
			//Add live tasks here.
		}

		grunt.task.run(tasks);
	});

	grunt.registerTask('default', ['build:dev', ]);

	grunt.registerTask('serve', function(target) {
		var defaultTasks = [
			'clean',
		];

		if (target === 'live') {
			defaultTasks.push('build:live');
		} else if (target === 'test') {
			defaultTasks.push('build:dev');
			defaultTasks.push('karma:watch');
		} else {
			defaultTasks.push('build:dev');

		}

		defaultTasks.push('configureProxies:livereload');
		defaultTasks.push('connect:livereload');
		defaultTasks.push('watch');
		grunt.task.run(defaultTasks);
	});

	grunt.registerTask('test', [
		'clean:dev',
		'concurrent:dev',
		'autoprefixer',
		'preprocess:dev',
		'connect:test',
		'karma:unit'
	]);

	/**
	 * Use with --setversion to set the version to the desired value, otherwise it automatically bumps the patch version.
	 *
	 * Default target is dev. Specify demo or live to build to those targets.
	 * @param  {[type]} target [description]
	 * @return {[type]}        [description]
	 */
	grunt.registerTask('release', "Release a new version, push it to bitbucket", function(target) {
		target = target ? target : 'dev';
		grunt.task.run(['bump-only', 'build:' + target, 'changelog', 'markdown', 'bump-commit']);
	});

};