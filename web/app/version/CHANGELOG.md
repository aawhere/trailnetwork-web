<a name="1.39.2"></a>
### 1.39.2 (2015-01-24)


#### Bug Fixes

* **WW-236:** add multiple people to the route request when exploring routes on the map. ([fe5640c3c5b6021d4d1aea2c1fe829fa3a66f228](https://bitbucket.org/aawhere/trailnetwork-web/commits/fe5640c3c5b6021d4d1aea2c1fe829fa3a66f228))
* **WW-254:** make search results not show incorrect results. ([7c2c59e82c68b6e115926bd66d67c47b88325964](https://bitbucket.org/aawhere/trailnetwork-web/commits/7c2c59e82c68b6e115926bd66d67c47b88325964))
* **WW-282:**
  * change default trailhead request from 1000 to 100 ([17c53c24026271ce96f3d78c49e49c6d2ed66f33](https://bitbucket.org/aawhere/trailnetwork-web/commits/17c53c24026271ce96f3d78c49e49c6d2ed66f33))
  * remove limit by score option for trailhead service ([e4ab99b85bc4e6ea5c27560232ff4df5064b120a](https://bitbucket.org/aawhere/trailnetwork-web/commits/e4ab99b85bc4e6ea5c27560232ff4df5064b120a))
* **WW-283:** #comment fix map when browser size is narrow, change title bar to be consistent  ([6393843a6ff25a86ed3702500ed0d2c45e4781b8](https://bitbucket.org/aawhere/trailnetwork-web/commits/6393843a6ff25a86ed3702500ed0d2c45e4781b8))
* **WW-284:** change "documents" to "searchDocuments" to match the API's changes ([a85ff8d5aed01b572e03cc523262520b8439a34f](https://bitbucket.org/aawhere/trailnetwork-web/commits/a85ff8d5aed01b572e03cc523262520b8439a34f))
* **WW-286:** change route and person search to use new search api ([932307d4546d217f5e4eafd633c69934e34cdabc](https://bitbucket.org/aawhere/trailnetwork-web/commits/932307d4546d217f5e4eafd633c69934e34cdabc))
* **WW-71:** fix breadcrumb layout, constrain trailhead banner text, fix more icon layout ([86e54d85002e9ea619112bbbec514c7aaafad9f7](https://bitbucket.org/aawhere/trailnetwork-web/commits/86e54d85002e9ea619112bbbec514c7aaafad9f7))
* **cardtray:** adjusted horizontal scroll speed for mac trackpads. ([954ac8ebac6d97a56b408c95c4621cb24b27dc9a](https://bitbucket.org/aawhere/trailnetwork-web/commits/954ac8ebac6d97a56b408c95c4621cb24b27dc9a))
* **ww-236:**
  * fix route completion events filter in route leaderboard view ([19a3abeed73d56f509cc675e8d28424402d68f7a](https://bitbucket.org/aawhere/trailnetwork-web/commits/19a3abeed73d56f509cc675e8d28424402d68f7a))
  * add "Filter Routes" title to filter icon ([e58e31d46a17cf6c00f1a0199bddfc86619ea805](https://bitbucket.org/aawhere/trailnetwork-web/commits/e58e31d46a17cf6c00f1a0199bddfc86619ea805))
  * fix breadcrumb link ([765bcd87687d1ccfb4b17f06bfbef0fac9784e3f](https://bitbucket.org/aawhere/trailnetwork-web/commits/765bcd87687d1ccfb4b17f06bfbef0fac9784e3f))
  * make explore-route-chart clear before fetching new data ([8a56e1720ccc1bf54c251cca5f91f1954a59f0b1](https://bitbucket.org/aawhere/trailnetwork-web/commits/8a56e1720ccc1bf54c251cca5f91f1954a59f0b1))
  * fix breadcrumbs adding trailhead when viewing a route ([1792288138306ef4d9500d70888aed936a329ab7](https://bitbucket.org/aawhere/trailnetwork-web/commits/1792288138306ef4d9500d70888aed936a329ab7))
  * fix no routes message that was falsly displaying when switching to explore-route ([6a33bfc3b6a10a88b0a6cb767825a981e9d8ee66](https://bitbucket.org/aawhere/trailnetwork-web/commits/6a33bfc3b6a10a88b0a6cb767825a981e9d8ee66))
  * fix route person filter when removing last person from filter ([51a3a03cb96fda747e4536f609b88fed0b878bb4](https://bitbucket.org/aawhere/trailnetwork-web/commits/51a3a03cb96fda747e4536f609b88fed0b878bb4))
  * fix regression where location search was not working ([84c507a3d56373c4b94d7bcbbe4cd02e5c03c92e](https://bitbucket.org/aawhere/trailnetwork-web/commits/84c507a3d56373c4b94d7bcbbe4cd02e5c03c92e))
  * fix regression where map default location was off ([76e7d555c4e916a83343268f999704c96cba1f20](https://bitbucket.org/aawhere/trailnetwork-web/commits/76e7d555c4e916a83343268f999704c96cba1f20))
  * fix regression in mouse scroll on card-tray ([20298c047693401b6e080e558a05b12132a2de38](https://bitbucket.org/aawhere/trailnetwork-web/commits/20298c047693401b6e080e558a05b12132a2de38))
  * re-position cardtray expand/collapse button. ([d9c91f00220f68b3fe80463ca28d5243b5271277](https://bitbucket.org/aawhere/trailnetwork-web/commits/d9c91f00220f68b3fe80463ca28d5243b5271277))
  * fix issue were route search results were not being displayed. ([9a8d3a79b3433457e533e7c7a98c03cdf448e765](https://bitbucket.org/aawhere/trailnetwork-web/commits/9a8d3a79b3433457e533e7c7a98c03cdf448e765))
  * cleanup relation pages, fix map centering issue ([6c30b8272884cd6f08dcf4702fac1488556048e8](https://bitbucket.org/aawhere/trailnetwork-web/commits/6c30b8272884cd6f08dcf4702fac1488556048e8))
  * add route-filter to explore route page ([75caa797cb2bd20427978a77c516bbf6e8d9ada0](https://bitbucket.org/aawhere/trailnetwork-web/commits/75caa797cb2bd20427978a77c516bbf6e8d9ada0))
  * fix trailhead route filter dialog, remove border from more button fix route rela ([1c06fe25387c08e94309175fe3f6ebcc78b74ffe](https://bitbucket.org/aawhere/trailnetwork-web/commits/1c06fe25387c08e94309175fe3f6ebcc78b74ffe))
  * add person filter to explore trailhead view, fix issue where animation starts ea ([00d4b13d043214e68243fc15057f5ebd66c729ca](https://bitbucket.org/aawhere/trailnetwork-web/commits/00d4b13d043214e68243fc15057f5ebd66c729ca))
  * switch to retreive routes from search service instead of datastore ([90ae873a52e197968ab6f9a27ff9ef515258280b](https://bitbucket.org/aawhere/trailnetwork-web/commits/90ae873a52e197968ab6f9a27ff9ef515258280b))
  * change "X" to close instead of clear filter, add clear link to bottom of form. ([401295ede65966dccdec974f7bad10e6d797deee](https://bitbucket.org/aawhere/trailnetwork-web/commits/401295ede65966dccdec974f7bad10e6d797deee))
  * show routes even when zoomed out far, fix font colors, fix route filter summary  ([6dda5bb32afba17eec694926cb607d5124e5b5f7](https://bitbucket.org/aawhere/trailnetwork-web/commits/6dda5bb32afba17eec694926cb607d5124e5b5f7))
* **ww-257:** hide activity links for private users ([b8f5418834b487987f5714f6b8c5c82b94a47185](https://bitbucket.org/aawhere/trailnetwork-web/commits/b8f5418834b487987f5714f6b8c5c82b94a47185))
* **ww-258:** add link to profile page if user sharing publicly. ([f39e96bd0c5b2a85409e1af9831287fc76bdec4c](https://bitbucket.org/aawhere/trailnetwork-web/commits/f39e96bd0c5b2a85409e1af9831287fc76bdec4c))
* **ww-268:**
  * adjust horizontal scroll for trackpads ([d652169ae7c240c31d6278e4f06fec7a2dc188bb](https://bitbucket.org/aawhere/trailnetwork-web/commits/d652169ae7c240c31d6278e4f06fec7a2dc188bb))
  * fix cardtray error message height, add no results message in route search dropdo ([f8d78e601c97ac30bc57e74b8bca6d65e5acd73d](https://bitbucket.org/aawhere/trailnetwork-web/commits/f8d78e601c97ac30bc57e74b8bca6d65e5acd73d))
  * remove right border radius from location search box ([b00f100f46b1df430306977dac2e8effa99bf42b](https://bitbucket.org/aawhere/trailnetwork-web/commits/b00f100f46b1df430306977dac2e8effa99bf42b))
  * fix regression on leaflet styling ([4b8aa4c03ce54ae9d7251ce02419dcaf84ee69b1](https://bitbucket.org/aawhere/trailnetwork-web/commits/4b8aa4c03ce54ae9d7251ce02419dcaf84ee69b1))
  * move search button to right ([375a2f4e40d6b6a287bb192037bead7c99af27ee](https://bitbucket.org/aawhere/trailnetwork-web/commits/375a2f4e40d6b6a287bb192037bead7c99af27ee))
* **ww-270:** remove mousewheel event interception for trackpad, remove auto-expand from route ([a85f78bcbbdc7dda093db04c064bb608b125be56](https://bitbucket.org/aawhere/trailnetwork-web/commits/a85f78bcbbdc7dda093db04c064bb608b125be56))
* **ww-273:** reduce number of routes returned from 20 to 10, fix route filter by activity typ ([15c6111f9fab56987ea040d8b4d833a65cff7a13](https://bitbucket.org/aawhere/trailnetwork-web/commits/15c6111f9fab56987ea040d8b4d833a65cff7a13))
* **ww-275:** found second call to initialize chart, removed call and made routeId watch state ([417fdee5880594adf3fbbaaba85083fab21bad0c](https://bitbucket.org/aawhere/trailnetwork-web/commits/417fdee5880594adf3fbbaaba85083fab21bad0c))
* **ww-276:**
  * fix regression in explore-trailhead page when removing trailhead-service. ([0de964127a27f48b5938aa1e8e8cc6c63a082e38](https://bitbucket.org/aawhere/trailnetwork-web/commits/0de964127a27f48b5938aa1e8e8cc6c63a082e38))
  * add check to see if route has a startTrailhead first before asking the API for a ([2ba388ff8756de1f3fb2fae58feeedbcf1a72726](https://bitbucket.org/aawhere/trailnetwork-web/commits/2ba388ff8756de1f3fb2fae58feeedbcf1a72726))
* **ww-71:**
  * fix issues with trailhead/route titles ([40ebf450ab1d303c9179233aeb660f8d30defba4](https://bitbucket.org/aawhere/trailnetwork-web/commits/40ebf450ab1d303c9179233aeb660f8d30defba4))
  * add route search back, fix link fonts on details page,allow detail and filter pa ([da39366ca12550b70064261542c355fe3549cc16](https://bitbucket.org/aawhere/trailnetwork-web/commits/da39366ca12550b70064261542c355fe3549cc16))


#### Features

* **WW-236:**
  * I just added a quick hack until a server description is available. "This route h ([07bd05750be35f38356d2ad077fcc19c6e89174c](https://bitbucket.org/aawhere/trailnetwork-web/commits/07bd05750be35f38356d2ad077fcc19c6e89174c))
  * change route filter collapsed content to show icons when filter is empty, make f ([654e2a4bc26420d5f50d86e5a9c4bc98318e038d](https://bitbucket.org/aawhere/trailnetwork-web/commits/654e2a4bc26420d5f50d86e5a9c4bc98318e038d))
* **WW-254:** add trailhead name and link to route search results. ([be19f33fc7f0cd605d7e47d53ba408892843027f](https://bitbucket.org/aawhere/trailnetwork-web/commits/be19f33fc7f0cd605d7e47d53ba408892843027f))
* **ww-236:**
  * add collapsable route fitler dialog to trailhead and route pages ([7b0d335f076f60fa3bd4fbe982737dac37eee5ce](https://bitbucket.org/aawhere/trailnetwork-web/commits/7b0d335f076f60fa3bd4fbe982737dac37eee5ce))
  * add clear button to route filter leaflet ([c3cb9318891fff3f49f65817ed5b55478d382e69](https://bitbucket.org/aawhere/trailnetwork-web/commits/c3cb9318891fff3f49f65817ed5b55478d382e69))
  * add names to collapsed filter ([df4453252d741238b62d4c5b409127eb041cdc09](https://bitbucket.org/aawhere/trailnetwork-web/commits/df4453252d741238b62d4c5b409127eb041cdc09))
  * add url encoding for person filter ([1ab01f7f2289dae129411a68c44e41eeb711f64c](https://bitbucket.org/aawhere/trailnetwork-web/commits/1ab01f7f2289dae129411a68c44e41eeb711f64c))
* **ww-268:**
  * change explore route page to use material design elements, add material design f ([789f5ad66fe37c28cf7346b385a69004a84f629d](https://bitbucket.org/aawhere/trailnetwork-web/commits/789f5ad66fe37c28cf7346b385a69004a84f629d))
  * imporove styling on person select list ([48f540e12b980296185bfa947b829ac3f1b920d1](https://bitbucket.org/aawhere/trailnetwork-web/commits/48f540e12b980296185bfa947b829ac3f1b920d1))
* **ww-71:**
  * add trailhead icon back to route cards, fix cardtray height to avoid resizing ma ([a15fc6f053f05552d9cd7cd9e0ad446ca4e08221](https://bitbucket.org/aawhere/trailnetwork-web/commits/a15fc6f053f05552d9cd7cd9e0ad446ca4e08221))
  * add collapse arrow to route-detail, make route-details text partly conceled when ([2687de31dc504c56566301361208e9aaa1bff080](https://bitbucket.org/aawhere/trailnetwork-web/commits/2687de31dc504c56566301361208e9aaa1bff080))
  * add close button to route filter, make route search not overlap route filter ([2f6949142d5b003e3bf3dc6207313a8bc3b95e21](https://bitbucket.org/aawhere/trailnetwork-web/commits/2f6949142d5b003e3bf3dc6207313a8bc3b95e21))
  * add fixed height to mobile card tray ([731ccbf941897ed522eda6ab2260eb377c63c951](https://bitbucket.org/aawhere/trailnetwork-web/commits/731ccbf941897ed522eda6ab2260eb377c63c951))
  * add mobile responsive web styles and markup ([6602c269608eebbc65d9b0cee5b3223fa40109b6](https://bitbucket.org/aawhere/trailnetwork-web/commits/6602c269608eebbc65d9b0cee5b3223fa40109b6))
  * add mobile support for the explore leaflets: search and filter. ([ad391c2dae964abee5c9c9958a0f6d9e470efdcf](https://bitbucket.org/aawhere/trailnetwork-web/commits/ad391c2dae964abee5c9c9958a0f6d9e470efdcf))
  * primitive mobile phone view for explore. ([776f1dfb1dd4ce2669cf8dfa9f5f6db7e1f8ec59](https://bitbucket.org/aawhere/trailnetwork-web/commits/776f1dfb1dd4ce2669cf8dfa9f5f6db7e1f8ec59))


<a name="1.38.0"></a>
## 1.38.0 (2014-12-18)


#### Bug Fixes

* **WW-256:**
  * fix trailhead view with cold reload ([606671851aece7ef511d2da066b580a2e6906360](https://bitbucket.org/aawhere/trailnetwork-web/commits/606671851aece7ef511d2da066b580a2e6906360))
  * fix trailhead view ([33b0b7c4b63299d5bbb35771997294d202cf015f](https://bitbucket.org/aawhere/trailnetwork-web/commits/33b0b7c4b63299d5bbb35771997294d202cf015f))
  * fix regression where route filter not updated on back button press ([e855b1bf2e5fbcd5fb3c3691bf78cbfd9d2cb524](https://bitbucket.org/aawhere/trailnetwork-web/commits/e855b1bf2e5fbcd5fb3c3691bf78cbfd9d2cb524))
  * fix chart animation when changing variations. ([53f25b8b18bddbeca9ab1ca7d79cd6867bcc57c3](https://bitbucket.org/aawhere/trailnetwork-web/commits/53f25b8b18bddbeca9ab1ca7d79cd6867bcc57c3))
  * Change map load order so that it is initiated after the bounds is known. ([073178a0462205c2a53281be2ab15713a526fb1c](https://bitbucket.org/aawhere/trailnetwork-web/commits/073178a0462205c2a53281be2ab15713a526fb1c))


<a name="1.35.0"></a>
## 1.35.0 (2014-12-07)


#### Bug Fixes

* **WW-239:**
  * additional fixes for broken marin message ([05766dddc59baa1ae29fc9aafa329b73dc0cad99](https://bitbucket.org/aawhere/trailnetwork-web/commits/05766dddc59baa1ae29fc9aafa329b73dc0cad99))
  * fix broken marin county link on the out of bounds message. ([c7e14ab51f28fb394f3b81bc5b8367c98b8aadc7](https://bitbucket.org/aawhere/trailnetwork-web/commits/c7e14ab51f28fb394f3b81bc5b8367c98b8aadc7))


<a name="1.35.0-rc1"></a>
### 1.35.0-rc1 (2014-11-02)


#### Bug Fixes

* **WW-239:** fix broken marin county link on the out of bounds message. ([c7e14ab51f28fb394f3b81bc5b8367c98b8aadc7](https://bitbucket.org/aawhere/trailnetwork-web/commits/c7e14ab51f28fb394f3b81bc5b8367c98b8aadc7))


<a name="1.34.2"></a>
### 1.34.2 (2014-10-31)


#### Bug Fixes

* **TN-700:** make route-summary-copy conform to new keys in route description ([93567dac965b87b040e38bbf2c87d3231381673f](https://bitbucket.org/aawhere/trailnetwork-web/commits/93567dac965b87b040e38bbf2c87d3231381673f))
* **TN-741:** expand completionStats and socialCompletionStats on client ([e3faaba77925259103bb5045346bf0e77690444e](https://bitbucket.org/aawhere/trailnetwork-web/commits/e3faaba77925259103bb5045346bf0e77690444e))
* **WW-139:** don't show no trailheads found when near area with trailheads ([e1d86a5284cb2d22572ebbae8ac27db08c3fe352](https://bitbucket.org/aawhere/trailnetwork-web/commits/e1d86a5284cb2d22572ebbae8ac27db08c3fe352))
* **WW-219:**
  * Expand error page reporting to include activities that respond with 202 ([e491acdb399ccd453767d3b5d3310a1f778fba8e](https://bitbucket.org/aawhere/trailnetwork-web/commits/e491acdb399ccd453767d3b5d3310a1f778fba8e))
  * added better notifications for explore pages ([2be630c947d5f2e5e79470d22b56d74bc2275e8d](https://bitbucket.org/aawhere/trailnetwork-web/commits/2be630c947d5f2e5e79470d22b56d74bc2275e8d))
  * update error handling on relation pages ([0c2ed94438172380e5a554f3921c6da89bad237b](https://bitbucket.org/aawhere/trailnetwork-web/commits/0c2ed94438172380e5a554f3921c6da89bad237b))
  * Add no results statement for completions pages ([2b26bc85f5dd39d221af39e3815c8a0f7c87b6b8](https://bitbucket.org/aawhere/trailnetwork-web/commits/2b26bc85f5dd39d221af39e3815c8a0f7c87b6b8))
  * update route page to use 404 when route id returns a 404 ([212998178ed60950b3152b852940bc894135e6be](https://bitbucket.org/aawhere/trailnetwork-web/commits/212998178ed60950b3152b852940bc894135e6be))
* **WW-221:** fix issue where url filter parameters where not honored ([10162285251066eb270100c8f5d04d28314de18a](https://bitbucket.org/aawhere/trailnetwork-web/commits/10162285251066eb270100c8f5d04d28314de18a))
* **WW-223:**
  * accounts page should display a 404 page. ([3b6e00d63e545fac9f370366b68dfd9d4987e5e6](https://bitbucket.org/aawhere/trailnetwork-web/commits/3b6e00d63e545fac9f370366b68dfd9d4987e5e6))
  * set a maximum zoom level for places search results ([b6263380f067facf974b376d605728c7e37646c0](https://bitbucket.org/aawhere/trailnetwork-web/commits/b6263380f067facf974b376d605728c7e37646c0))
* **WW-230:** refactor styles along lines of bootstrap-sass project ([264314e0eb22cbe059be0a750627d2d6202b6d30](https://bitbucket.org/aawhere/trailnetwork-web/commits/264314e0eb22cbe059be0a750627d2d6202b6d30))
* **WW-232:** add ROAD activityType ([2d46d0ea265499f3962e72d5de777e2cc8086784](https://bitbucket.org/aawhere/trailnetwork-web/commits/2d46d0ea265499f3962e72d5de777e2cc8086784))


#### Features

* **TN-700:** incorporate trailhead description from server ([92228ea3524409e86dc7439b6b703b9bbf6b05df](https://bitbucket.org/aawhere/trailnetwork-web/commits/92228ea3524409e86dc7439b6b703b9bbf6b05df))
* **WW-229:** add 404 page to relation pages ([0ceefdd1f3389bb6626e9948ce99b8b7f4abfe79](https://bitbucket.org/aawhere/trailnetwork-web/commits/0ceefdd1f3389bb6626e9948ce99b8b7f4abfe79))


<a name="1.34.1"></a>
### 1.34.1 (2014-10-20)


#### Bug Fixes

* **WW-135:** fix issue where debug was always enabled (https://bitbucket.org/aawhere/trailnetwork-web/commits/93fd3f2508bdef1cf092423f453b3b11a5959014)


