define(function() {
	'use strict';

	var restEndpoint = '/rest';
	var routes = {};

	routes.proxyServer = '/* @echo proxyServer */';

	if (!routes.proxyServer) {
		routes.proxyServer = window.location.host;
		routes.proxyServerBaseUrl = restEndpoint;
	}
	routes.proxyServerBaseUrl = 'http://' + routes.proxyServer;
	routes.proxyServerBaseRestUrl = 'http://' + routes.proxyServer + restEndpoint;

	routes.states = {
		account: 'account',
		activity: 'activity',
		explore: 'explore',
		exploreRoutes: 'explore.routes',
		exploreTrailhead: 'explore.trailhead',
		exploreRoute: 'explore.route',
		exploreRouteAlternate: 'explore.route.alternate',
		exploreRouteDetails: 'explore.route.details',
		exploreRouteLeaderboard: 'explore.route.leaderboard',
		leaderboard: 'leaderboard',
		person: 'person',
		personAll: 'person.all',
		personActivities: 'person.activities',
		personCompletions: 'person.completions',
		personPopularRoutes: 'person.popularRoutes',
		personLeaderboard: 'person.leaderboard',
		personRoutes: 'person.routes',
		route: 'route',
		routeAll: 'route.all',
		routeEvents: 'route.events',
		routePeople: 'route.people',
		routeCompletions: 'route.completions',
		routeLeaderboard: 'route.leaderboard',
		notFound: 'notFound',
	};

	routes.paths = {
		account: 'account',
		activity: 'activity',
		activities: 'activities',
		all: 'all',
		alternates: 'variations',
		alternate: 'variation',
		detail: 'detail',
		explore: 'explore',
		events: 'events',
		home: '/',
		leaderboard: 'race',
		person: 'person',
		persons: 'persons',
		people: 'people',
		popular: 'popular',
		route: 'route',
		routes: 'routes',
		recent: 'recent',
		seperator: '/',
		trailhead: 'th',
		//depricated
		visits: 'visits',
		completions: 'visits',
	};

	routes.params = {
		admin: 'debug',
		boxes: 'boxes',
		paramsStart: '?',
		paramsMore: '&',
		allRoutes: 'allRoutes',
	};

	routes.locationQueryParameterName = 'm';
	routes.routeFilterQueryParameterName = 'f';
	routes.routeCompletionQueryParameterName = 'c';

	return routes;

});