define(['common/common-module',
        'jquery',
    ],
    function(commonModule) {
        'use strict';

        /**
         * angular-click-outside
         * An angular directive to detect a click outside of an elements scope. Great for closing dialogues, drawers and off screen menu's etc.
         *
         * Usage
         *
         * To use this directive, ensure the element you want to detect a close outside of has an id or class assigned. Be wary of using classes as some unwanted elements may have the same class. General ID's will suffice, but instances of dynamically inserted list items may require the use of classes.
         *
         * Add the directive via the click-outside attribute, and give exceptions via the outside-if-not attribute.
         *
         * Basic example:
         *
         * <div class="menu" id="main-menu" click-outside="closeThis">
         *     ...
         * </div>
         * This is of little use though without a callback function to do something with that click:
         *
         * <div class="menu" id="main-menu" click-outside="closeThis()">
         *     ...
         * </div>
         * Where closeThis() is the function assigned to the scope via the controller such as:
         *
         * angular
         *     .module('myApp')
         *     .controller('MenuController', ['$scope', MenuController]);
         *
         * function MenuController($scope) {
         *     $scope.closeThis = function () {
         *         console.log('closing');
         *    }
         * }
         *
         * <button id="my-button">Menu Trigger Button</button>
         * <div ng-controller="MenuController">
         *     <div class="menu" id="main-menu" click-outside="closeThis()" outside-if-not="my-button">
         *         ...
         *     </div>
         * </div>
         * Adding Exceptions
         *
         * You can also add exceptions via the outside-if-not tag, which executes the callback function, but only if the id's listed wasn't clicked. In this case closeThis() will be called only if clicked outside and #my-button wasn't clicked as well. This can be great for things like slide in menus that might have a button outside of the menu scope that triggers it:
         *
         * <button id="my-button">Menu Trigger Button</button>
         * <div class="menu" id="main-menu" click-outside="closeThis()" outside-if-not="my-button">
         *     ...
         * </div>
         * You can have more than one exception by comma delimiting a list such as:
         *
         * <button id="my-button">Menu Trigger Button</button>
         * <div class="menu" id="main-menu" click-outside="closeThis()" outside-if-not="my-button, another-button">
         *     ...
         * </div>
         * <button id="another-button">A second trigger button</button>
         *
         */
        commonModule.directive('tnClickOutside', function($document) {
            return {
                restrict: 'A',
                scope: {
                    clickOutside: '&'
                },
                link: function($scope, elem, attr) {
                    var classList = (attr.outsideIfNot !== undefined) ? attr.outsideIfNot.replace(', ', ',').split(',') : [];
                    if (attr.id !== undefined) {
                        classList.push(attr.id);
                    }

                    $document.on('click', function(e) {
                        var i = 0,
                            element;

                        if (!e.target) {
                            return;
                        }

                        for (element = e.target; element; element = element.parentNode) {
                            var id = element.id;
                            var classNames = element.className;

                            if (id !== undefined) {
                                for (i = 0; i < classList.length; i++) {
                                    if (id.indexOf(classList[i]) > -1 || classNames.indexOf(classList[i]) > -1) {
                                        return;
                                    }
                                }
                            }
                        }

                        $scope.$eval($scope.clickOutside);
                    });
                }
            };
        });
    });