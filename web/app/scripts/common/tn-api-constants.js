define(['app-route-constants'], function(appRouteConstants) {
	'use strict';

	var FIELD_SEPERATOR = '-';
	var PATH_SEPERATOR = '/';

	var api = {};

	api.params = {
		bounds: 'bounds',
		conditions: 'conditions',
		expand: 'expand',
		limit: 'limit',
		page: 'page',
		orderBy: 'orderBy',
		options: 'options',
		autoLocate: 'autoLocate',
	};

	api.orderBy = {
		ascending: 'ascending',
		descending: 'descending',
		none: 'none',
	};

	api.options = {
		activity: 'activity',
		account: 'account',
		routeEvent: 'routeEvent',
		gpolyline: 'track-gpolyline',
		routeGpolyline: 'route-track-gpolyline',
		person: 'person',
		route: 'route',
		routeBounds: 'routeBounds',
	};

	api.domain = {
		account: 'account',
		activity: 'activity',
		completion: 'routeCompletion',
		person: 'person',
		routeEvent: 'routeEvent',
		route: 'route',
		trailhead: 'trailhead',
	};

	api.relativeKey = {
		activityStartTime: 'track-start-time',
		activityType: 'activityType',
		activityTypeDistanceCategory: 'activityTypeDistanceCategory',
		accountId: 'accountId',
		activityId: 'activityId',
		boundingBox: 'boundingBox',
		completionStats: 'completionStats',
		description: 'description',
		distanceCategory: 'distanceCategory',
		routeEventId: 'routeEventId',
		isCourse: 'isCourse',
		id: 'id',
		personId: 'personId',
		personalTotal: 'personalTotal',
		personalOrder: 'personalOrder',
		routeId: 'routeId',
		routeCompletionStatsActivityTypeStatsActivityType: 'relativeStarts-activityType',
		socialOrder: 'socialOrder',
		socialRaceRank: 'socialRaceRank',
		socialCountRank: 'socialCountRank',
		socialCompletionStats: 'socialCompletionStats',
		startTime: 'startTime',
		startTrailheadId: 'startTrailheadId',
		relativeCompletionStats: 'relativeCompletionStats',
		relativeCompletionStatsPersonId: 'relativeCompletionStats-personId',
		relativeCompletionStatsActivityType: 'relativeCompletionStats-activityType',
		relativeCompletionStatsDistanceCategory: 'relativeCompletionStats-distance',
		relativeStarts: 'relativeStarts',
		relativeStartsPersonId: 'relativeStarts-personId',
		routeCompletionStats: 'routeCompletionStats',
		trailheadId: 'trailheadId',
		trailheadRelativeStartsDistance: 'relativeStarts-distance',
		score: 'score',
	};

	api.fields = {
		activityAccountId: api.domain.activity + FIELD_SEPERATOR + api.relativeKey.accountId,
		activityStartTime: api.domain.activity + FIELD_SEPERATOR + api.relativeKey.activityStartTime,
		completionActivityId: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.activityId,
		completionActivityType: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.activityType,
		completionIsCourse: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.isCourse,
		completionPersonId: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.personId,
		completionPersonalTotal: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.personalTotal,
		completionPersonalOrder: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.personalOrder,
		completionRouteEventId: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.routeEventId,
		completionRouteId: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.routeId,
		completionSocialOrder: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.socialOrder,
		completionSocialRaceRank: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.socialRaceRank,
		completionSocialCountRank: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.socialCountRank,
		completionStartTime: api.domain.completion + FIELD_SEPERATOR + api.relativeKey.startTime,
		eventScore: api.domain.routeEvent + FIELD_SEPERATOR + api.relativeKey.score,
		personAccountId: api.domain.person + FIELD_SEPERATOR + api.relativeKey.accountId,
		routeActivityType: api.domain.route + FIELD_SEPERATOR + api.relativeKey.relativeCompletionStatsActivityType,
		routeBoundingBox: api.domain.route + FIELD_SEPERATOR + api.relativeKey.boundingBox,
		routeCompletionPersonId: api.domain.route + FIELD_SEPERATOR + api.relativeKey.relativeCompletionStatsPersonId,
		routeCompletionStats: api.domain.route + FIELD_SEPERATOR + api.relativeKey.completionStats,
		routeDistanceCategory: api.domain.route + FIELD_SEPERATOR + api.relativeKey.relativeCompletionStatsDistanceCategory,
		routeEventActivityType: api.domain.routeEvent + FIELD_SEPERATOR + api.relativeKey.activityType,
		routeRouteId: api.domain.route + FIELD_SEPERATOR + api.relativeKey.id,
		routeStartTrailheadId: api.domain.route + FIELD_SEPERATOR + api.relativeKey.startTrailheadId,
		routeSocialCompletionStats: api.domain.route + FIELD_SEPERATOR + api.relativeKey.socialCompletionStats,
		trailheadScore: api.domain.trailhead + FIELD_SEPERATOR + api.relativeKey.score,
		trailheadRouteCompletionStatsActivityType: api.domain.trailhead + FIELD_SEPERATOR + api.relativeKey.routeCompletionStatsActivityTypeStatsActivityType,
		trailheadDistanceCategory: api.domain.trailhead + FIELD_SEPERATOR + api.relativeKey.trailheadRelativeStartsDistance,
		trailheadRelativeStarts: api.domain.trailhead + FIELD_SEPERATOR + api.relativeKey.relativeStarts,
		trailheadRouteCompletionStats: api.domain.trailhead + FIELD_SEPERATOR + api.relativeKey.routeCompletionStats,
		trailheadRelativeStartsPersonId: api.domain.trailhead + FIELD_SEPERATOR + api.relativeKey.relativeStartsPersonId,
		trailheadId: api.domain.trailhead + FIELD_SEPERATOR + api.relativeKey.id,
		routeRelativeCompletionStats: api.domain.route + FIELD_SEPERATOR + api.relativeKey.relativeCompletionStats,

		/*
		//TODO: move to activity-tn-api-constants
				applicationReference: 'activity-applicationReference',
				gpolyline: 'track-gpolyline',

				routeCompletionsPersonId: 'routeCompletion-personId',
				routeCompletionsDuration: 'routeCompletion-duration',
				routeCompletionActivityTypeStatsActivityType: 'route-completionStats-activityTypeStats-activityType',
				routeCompletionApplicationStatsApplication: 'route-completionStats-applicationStats-application',
				routeCompletionActivityType: 'routeCompletion-activityType',
				routeCompletionStartTime: 'routeCompletion-startTime',
				routeCourse: 'route-course',
				routeCourses: 'route-courses',
				routeBounds: 'route-bounds',
				routeCourseInline: 'route-course-inline',
				routeDistanceCategory: 'route-distanceCategory',
				routeStartInline: 'route-start-inline',
				routeFinishInline: 'route-finish-inline',
				routeTrackGPolyline: 'route-track-gpolyline',
		 */
	};

	api.paths = {
		accounts: 'accounts',
		alternates: 'alternates',
		activities: 'activities',
		completions: 'completions',
		events: 'events',
		persons: 'persons',
		routes: 'routes',
		mains: 'mains',
		trailheads: 'trailheads',
		users: 'users',
		currentUser: 'users/current',
		signin: 'users/signin.html?mode=select',
	};

	api.mediaTypeExtensions = {
		png: '.png',
		svg: '.svg',
	};

	api.url = {
		routeImage: function(routeId) {
			return appRouteConstants.proxyServerBaseRestUrl + PATH_SEPERATOR + api.paths.routes + PATH_SEPERATOR + routeId + api.mediaTypeExtensions.svg;
		}
	};

	return api;
});