define(function() {
	'use strict';

	/**
	 * Events for the explore page.
	 */
	return {
		notification: 'notification',
		clearNotification: 'clear-notification',
	};

});