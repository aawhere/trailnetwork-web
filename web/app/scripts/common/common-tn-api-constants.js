define(function() {
	'use strict';

/**
 * DEPRICATED. See tn-api-constants
 */
	return {
		ascending: 'ascending',
		bounds: 'bounds',
		conditions: 'conditions',
		descending: 'descending',
		limit: 'limit',
		page: 'page',
		orderBy: 'orderBy',
		options: 'options',

		optionValues: {
			account: 'account',
			route: 'route',
			person: 'person',
			activity: 'activity',
		}
	};
});