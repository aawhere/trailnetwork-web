define(['common/common-module',
	'jquery',
	//
	 'autocomplete'
], function(commonModule, jQuery) {
	'use strict';

	/**
	 * Autocomplete directive. Extends https://github.com/JustGoscha/allmighty-autocomplete
	 * and adds a reset button to the input field.
	 */
	commonModule.directive('autocomplete', function() {
		return {
			restrict: 'E',
			//run before actual autocomplete
			priority: 0,
			compile: function(tEl) {
				var input = jQuery(tEl[0]).find('INPUT');
				input.attr('tn-reset-field', true);
			}
		};
	});
});