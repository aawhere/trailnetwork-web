define(function() {
	'use strict';

	/**
	 * Add a method to Function to allow easy inheritence of objects.
	 *
	 * @param  {Object} parentClassOrObject [description]
	 * @return {null}                     [description]
	 */
	Function.prototype.inheritsFrom = function(ParentClassOrObject) {
		if (ParentClassOrObject.constructor === Function) {
			//Normal Inheritance
			this.prototype = new ParentClassOrObject();
			this.prototype.constructor = this;
			this.prototype.parent = ParentClassOrObject.prototype;
		} else {
			//Pure Virtual Inheritance
			this.prototype = ParentClassOrObject;
			this.prototype.constructor = this;
			this.prototype.parent = ParentClassOrObject;
		}
		return this;
	};
	return null;
});