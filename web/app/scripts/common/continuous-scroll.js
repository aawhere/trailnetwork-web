define(['common/common-module'], function(commonModule) {
	'use strict';

	commonModule.directive('tnContinuousScroll', [
		'$compile',
		function($compile) {
			return {
				restrict: 'A',
				replace: false,
				terminal: true, //this setting is important, see explanation below
				priority: 1000, //this setting is important, see explanation below
				link: function($scope, el) {
					var service = el.attr('tn-continuous-scroll');
					if(!el.attr('infinite-scroll')) {
						el.attr('infinite-scroll', service+'.nextPage()');
					}
					if(!el.attr('infinite-scroll-disabled')) {
						el.attr('infinite-scroll-disabled', service+'.busy || '+service+'.finalPage || '+service+'.hasError');
					}
					if(!el.attr('infinite-scroll-distance')) {
						el.attr('infinite-scroll-distance', 1);
					}
					el.removeAttr("tn-continuous-scroll");
					el.removeAttr("data-tn-continuous-scroll");

					$compile(el)($scope);
				}
			};
		}
	]);
});

