define(function() {
	'use strict';

	// Seperator
	var sep = '-';

	var api = {};

	api.domains = {
		route: 'route',
	};

	api.paths = {
		routeSearch: 'routes/search',
		search: 'search',
	};

	api.options = {
		query: 'query',
		indexes: 'searchIndexes',
		fields: 'fields',
	};

	api.headers = {

	};

	api.fields = {
		route: {
			id: api.domains.route + sep + 'id',
			name: api.domains.route + sep + 'name',
			activityType: api.domains.route + sep + 'relativeCompletionStats-activityType',
			personId: api.domains.route + sep + 'relativeCompletionStats-personId',
			distanceCategory: api.domains.route + sep + 'distanceCategory',
			trailheadName: api.domains.route + sep + 'startTrailhead-name',
			trailheadId: api.domains.route + sep + 'startTrailheadId',
		}
	};

	api.conditions = {
		route: {
			activityType: api.fields.route.activityType,
			personId: api.fields.route.personId,
			distanceCategory: api.fields.route.distanceCategory,
		}
	};

	api.indexes = {
		person: 'person',
		route: 'route',
		trailhead: 'trailhead',
	};

	return api;

});