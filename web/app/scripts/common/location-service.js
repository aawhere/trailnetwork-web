define(['common/common-module',
	'app-route-constants',
	'underscore'
], function(commonModule, appRouteConstants, _) {
	'use strict';

	commonModule.service('locationService', function($location, $window, $state, $log) {

		var service = {};

		//Previous location including query params. Different from browser history
		//in that this stores query params even if the history doesn't.
		service.previous = appRouteConstants.paths.home;

		service.updateLocation = function(path) {
			$log.debug('updateLocation');
			service.previous = $location.url();
			return $location.url(path);
		};

		service.updateLocationSearch = function(search) {
			$log.debug('updating location', search);
			service.previous = $location.url();
			var result = $location.search(search);
			return result;
		};

		service.removeLocationSearch = function(param) {
			$log.debug('removing location', param);
			var search = $location.search();
			delete search[param];
			service.updateLocationSearch(search);
		};

		service.goBack = function() {
			//if it was the first page
			if ($window.history.length === 1) {
				service.goExplore();
			} else {
				$window.history.back();
			}
		};

		service.goExplore = function() {
			//service.updateLocation(service.previous);
			$state.go(appRouteConstants.states.exploreRoutes);
		};

		/**
		 * Similar to angular.extend, this copies the values in the search parameter
		 * to the current query parameters, overwriting values as required.
		 *
		 * @param  {[type]} search  [description]
		 * @param {String} remove remove these parameters from the location (don't extend these)
		 * @return {[type]}         [description]
		 */
		service.extendLocationSearch = function(search, remove) {
			var current = $location.search();
			var merged = _.extend(current, search);
			_.each(remove, function(toRemove) {
				if (merged[toRemove]) {
					delete merged[toRemove];
				}
			});
			return service.updateLocationSearch(merged);
		};

		service.removeLocationSearchParameter = function(parameterName) {
			var params = $location.search();
			if (params[parameterName]) {
				params[parameterName] = '';
			}
			service.updateLocationSearch(params);
		};

		return service;
	});

});