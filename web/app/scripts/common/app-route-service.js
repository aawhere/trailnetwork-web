define(['common/common-module',
	'app-route-constants', 'angular',
	'route/completions-url-encoding',
	'underscore',
	'route/route-filter-url-encoding',
	//
	'explore/explore-location-service',
], function(commonModule, rc, angular, completionsUrlEncoding, _, routeUrlEncoder) {
	'use strict';

	commonModule.service('appRouteService', function($injector, $location, exploreLocationService) {

		function state() {
			return $injector.get('$state');
		}

		function params() {
			var p = $location.search();
			var jsonParams = JSON.stringify(p);
			return JSON.parse(jsonParams);
		}

		var service = {};

		service.activity = {};

		service.activity.activityUrl = function(activityId) {
			return state().href(rc.states.activity, {
				activityId: activityId,
			});
		};

		service.explore = {};

		service.explore.routeUrl = function(routeId, follow) {
			var p = params();
			p.routeId = routeId;
			var route = rc.states.exploreRoute;
			if (follow) {
				state().go(route, p);
			} else {
				return state().href(route, p);
			}
		};

		function exploreRoutesParams(location, zoom) {
			var p = params();
			if (location && zoom) {
				p[rc.locationQueryParameterName] = exploreLocationService.encodeLocation(location, zoom);
			}
			return p;
		}

		service.explore.goToRoutes = function(location, zoom) {
			var p = exploreRoutesParams(location, zoom);
			return state().go(rc.states.exploreRoutes, p);
		};

		service.explore.routesUrl = function(location, zoom) {
			var p = exploreRoutesParams(location, zoom);
			return state().href(rc.states.exploreRoutes, p);
		};

		service.explore.goToRoute = function(routeId) {
			return state().go(rc.states.exploreRoute, {
				routeId: routeId,
			});
		};

		service.explore.trailheadUrl = function(trailheadId) {
			if(!trailheadId) {
				return;
			}
			return state().href(rc.states.exploreTrailhead, {
				trailheadId: trailheadId,
			});
		};

		service.route = {};

		service.route.routeAllUrl = function(routeId) {
			var p = params();
			p.routeId = routeId;
			return state().href(rc.states.routeAll, p);
		};

		service.route.routeCompletionsUrl = function(routeId, filterByPerson) {
			console.warn('!!!!! Depricated, use route.completionsUrl instead');
			return service.route.completionsUrl(routeId, filterByPerson);
		};

		service.route.completionsUrl = function(routeId, filterByPerson) {
			var parameters = {
				routeId: routeId,
			};
			if (filterByPerson) {
				var items = {
					people: [],
				};
				if (!_.isArray(filterByPerson)) {
					filterByPerson = [filterByPerson];
				}
				_.each(filterByPerson, function(person) {
					items.people.push({
						id: person.id,
						text: person.name,
					});
				});
				var personParams = completionsUrlEncoding.encode(items);
				angular.extend(parameters, personParams);
			}
			return state().href(rc.states.routeCompletions, parameters);
		};

		service.route.peopleUrl = function(routeId) {
			return state().href(rc.states.routePeople, {
				routeId: routeId,
			});
		};

		service.route.eventsUrl = function(routeId) {
			return state().href(rc.states.routeEvents, {
				routeId: routeId
			});
		};

		service.route.leaderboardUrl = function(routeId, eventId, activityType) {
			var parameters = {
				routeId: routeId,
			};
			if (eventId) {
				var personParams = completionsUrlEncoding.encode({
					routeEvent: eventId,
				});
				var filterParams = routeUrlEncoder.asQueryParam(routeUrlEncoder.encodeActivityType('', activityType));
				angular.extend(parameters, personParams, filterParams);
			}
			return state().href(rc.states.routeLeaderboard, parameters);
		};

		service.person = {};

		service.person.personAllUrl = function(personId) {
			return state().href(rc.states.personAll, {
				personId: personId,
			});
		};

		service.person.activitiesUrl = function(personId) {
			return state().href(rc.states.personActivities, {
				personId: personId
			});
		};

		service.person.completionsUrl = function(personId) {
			return state().href(rc.states.personCompletions, {
				personId: personId
			});
		};

		service.person.popularRoutesUrl = function(personId) {
			return state().href(rc.states.personPopularRoutes, {
				personId: personId
			});
		};

		service.person.leaderboardUrl = function(personId) {
			return state().href(rc.states.personLeaderboard, {
				personId: personId,
			});
		};

		service.trailhead = {};

		service.trailhead.trailheadUrl = function(trailheadId) {
			//depricated
			console.log('appRouteService.trailhead.trailheadUrl is depricated');
			return service.explore.trailheadUrl(trailheadId);
		};

		return service;
	});

});