define(['common/common-module', 'common/resize-listener'], function(commonModule, resizeListener) {
	'use strict';

	commonModule.directive('tnOnResize', function($timeout) {
			return {
				restrict: 'A',
				transclude: false,
				replace: false,
				scope: {
					tnOnResize: '='
				},
				link: function(scope, el) {

					var element = el[0];
					resizeListener.addResizeListener(element, function() {
						$timeout(function() {
							scope.tnOnResize();
						},0);
					});
				}
			};
		}
	);
});