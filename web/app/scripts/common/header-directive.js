define(['common/common-module'], function(commonModule) {
	'use strict';

	commonModule.directive('tnHeader', function() {

			return {
				restrict: 'EA',
				transclude: false,
				templateUrl: '/views/common/header-directive.html',
				replace: true,
				scope: {},
				link: function() {

				}
			};

		}
	);
});