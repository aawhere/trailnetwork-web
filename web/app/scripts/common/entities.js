define(['underscore'], function(_) {
	'use strict';

	var Entities = function() {

		// When creating the collection, we are going to work off
		// the core array. In order to maintain all of the native
		// array features, we need to build off a native array.
		var entities = Object.create(Array.prototype);

		// Initialize the array.
		entities = (Array.apply(entities, arguments) || entities);

		// Add all the class methods to the entities.
		Entities.injectClassMethods(entities);

		//for backwards compatibility.
		entities.entities = entities;
		entities.version = 0;

		return (entities);

	};

	//
	// Define the static methods.
	//

	Entities.injectClassMethods = function(entities) {

		// Loop over all the prototype methods and add them
		// to the new collection.
		for (var method in Entities.prototype) {

			// Make sure this is a local method.
			if (Entities.prototype.hasOwnProperty(method)) {

				// Add the method to the collection.
				entities[method] = Entities.prototype[method];

			}

		}

		// Return the updated collection.
		return (entities);

	};


	//
	// Class Methods
	//

	Entities.prototype.updateVersion = function() {
		this.version = this.version + 1;
	};

	/**
	 * Add an entity to the list. Entities must have an identifier defined by 'entity.id'.
	 *
	 * @param {tn.*.Entity} entity    [description]
	 * @param {details} additional details to store with the entity, such as polyline options.
	 */
	Entities.prototype.add = function(arrayOrEntity) {
		var context = this,
			arrayOfEntities;

		arrayOfEntities = _.isArray(arrayOrEntity) ? arrayOrEntity : [arrayOrEntity];
		_.each(arrayOfEntities, function(entity) {
			var existing = _.find(context, function(e) {
				return e.id === entity.id;
			});
			if (!existing) {
				context.push(entity);
			}
		});

		this.updateVersion();
		return this;
	};

	/**
	 * remove the entity from the list.
	 *
	 * @param  {String} entityId  [description]
	 */
	Entities.prototype.remove = function(entityId) {
		var context = this;
		var without = _.filter(context,
			function(e) {
				var select = true;
				if (e.id === entityId) {
					select = false;
				}
				return select;
			});
		this.clear();
		this.add(without);

		this.updateVersion();
		return this;
	};

	/**
	 * Finds the entity in the collection. Returns undefined if none found.
	 *
	 * @param  {[type]} id       [description]
	 * @return {[type]}          [description]
	 */
	Entities.prototype.find = function(id) {
		var found = _.find(this, function(entity) {
			//console.log('looking for ' + id + ', found ' + entityWrapper.entity.id + (entityWrapper.entity.id === id), _.isNumber(id), _.isNumber(entityWrapper.entity.id));
			return (entity.id === id);
		});
		return found;
	};

	/**
	 * Does this collection contain the provided id?
	 *
	 * @param  {[type]}  id       [description]
	 * @return {Boolean}          [description]
	 */
	Entities.prototype.has = function(id) {
		return (this.find(id)) ? true : false;
	};

	Entities.prototype.clear = function() {
		while (this.length > 0) {
			this.pop();
		}

		this.updateVersion();
	};

	Entities.prototype.isEmpty = function() {
		return this.length === 0;
	};

	return Entities;
});