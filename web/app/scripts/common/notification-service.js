define(['common/common-module', 'underscore', 'common/common-events'], function(commonModule, _, commonEvents) {
	'use strict';

	commonModule.service('notificationService', function($rootScope, $translate, $log) {
			var message = '';
			var service = {};

			service.notify = function(msg) {
				if (_.isUndefined(msg)) {
					return message;
				} else {
					message = msg;
					$rootScope.$broadcast(commonEvents.notification, message);
					return message;
				}
			};

			/**
			 * Clears all notifications.
			 *
			 * @return {[type]} [description]
			 */
			service.clear = function() {
				$log.info('clearing notifications');
				$rootScope.$broadcast(commonEvents.clearNotification);
			};

			//Depricated. Use service.notify(msg);
			service.notification = function(msg) {
				console.log('service.notification is depricated!');
				service.notify(msg);
			};

			service.networkingError = function() {
				$translate('TN_NETWORKING_ERROR').then(function(msg) {
					service.notification(msg);
				});
			};

			return service;
		}
	);
});