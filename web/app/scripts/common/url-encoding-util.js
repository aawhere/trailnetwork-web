define(['underscore'], function(_) {
	'use strict';

	var util = {};

	util.constants = {
		FILTER_SEPERATOR: '!',
		FILTER_CODE_SEPERATOR: ',',
	};

	/**
	 * Encode the value into a query parameter in the form
	 * !code,value
	 *
	 * value is treated or converted to its string form.
	 *
	 * @param  {[type]} code  code used to retreive value in the future.
	 * @param  {[type]} value the value to encode
	 * @return {[type]}       the encoded parameter
	 */
	util.encodeParam = function(code, value) {
		return util.constants.FILTER_SEPERATOR + code + util.constants.FILTER_CODE_SEPERATOR + value;
	};

	/**
	 * Uses encodeParam to encode the value and appends it to the provided param.
	 *
	 * Example encodeParams('a', 'myValue', '!b,myOtherValue') => !b,myOtherValue!a,myValue
	 *
	 * @param  {[type]} code  [description]
	 * @param  {[type]} value [description]
	 * @param  {[type]} param [description]
	 * @return {[type]}       [description]
	 */
	util.encodeParams = function(code, value, param) {
		var encodedValue = encodeURIComponent(value);
		return param + util.encodeParam(code, encodedValue);
	};

	/**
	 * Decodes the parameters.
	 *
	 * @param  {[type]} queryString [description]
	 * @return {[type]}             [description]
	 */
	util.decodeParams = function(queryString) {
		var map = {};
		if (queryString) {
			var parts = queryString.split(util.constants.FILTER_SEPERATOR);
			//Remove the blank parts due to the leading "!";
			parts = _.without(parts, '');
			if (parts.length > 0) {} else {
				throw "Invalid input";
			}
			_.each(parts, function(part) {
				var components = part.split(util.constants.FILTER_CODE_SEPERATOR);
				if (components.length > 0) {} else {
					throw "Invalid input";
				}
				var code = components[0];
				var value = decodeURIComponent(components[1]);
				map[code] = value;
			});

		}

		return map;
	};

	return util;

});