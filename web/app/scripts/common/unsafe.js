define(['common/common-module'], function(commonModule) {
	'use strict';

	commonModule.filter('unsafe', function($sce) {
			return function(val) {
				return $sce.trustAsHtml(val);
			};
		}
	);
});