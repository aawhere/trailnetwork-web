define(['common/common-module', 'explore/explore-events'], function(commonModule, exploreEvents) {
	'use strict';

	commonModule.directive('tnAreaChart', function($rootScope, $timeout, $log) {
		return {
			restrict: 'EA',
			replace: false,
			scope: {
				/*
				 * An array of objects with the following specification:
				 * {
				 *  location: tn.measure.GeoCorrdinate,
				 *  distance: tn.measure.Length,
				 *  elevation: tn.measure.Elevation,
				 * }
				 */
				data: '=',
				//Row index of selection
				selection: '=',
				xAxisTitle: '@',
				yAxisTitle: '@'
			},
			link: function(scope, el) {

				var CONTAINER_WIDTH = 330;

				el.css('cursor', 'pointer');

				//Note this assumes only one dataset on the y axis. If we support
				//more than one dataset, we will need to modify this and the logic
				//for ensuring the correct baseline is set.
				var V_AXIS_INDEX = 1;

				function setSelection(rowIndex) {
					scope.ac.setSelection([{
						row: rowIndex,
						column: null
					}]);
				}

				function clearSelection() {
					scope.ac.setSelection(null);
				}

				function setupWatches() {

					scope.$watch('data', function() {
						drawChart();
					});

					scope.$watch('selection', function() {
						if (scope.ac) {
							if (scope.selection) {
								setSelection(scope.selection);
							} else {
								clearSelection();
							}
						}
					});

					scope.$watch(function() {
						//$log.debug('parents width', el.parent().width());
						return el.parent().width();
					}, function(width) {
						$log.debug('elements parent changed size, redrawing chart', width);
						drawChart();
					});
					// el.parent().resize(function() {
					// 	drawChart();
					// });

				}

				scope.$on(exploreEvents.clearChart, function() {
					if(scope.ac) {
						scope.ac.clearChart();
					}
				});


				function drawChart() {
					if (!scope.data) {
						return;
					}

					$log.info('Drawing chart', scope.data);
					//var data = google.visualization.arrayToDataTable(scope.data);
					var data = scope.data;

					var elWidth = el.parent().width() || CONTAINER_WIDTH;

					var ac = new google.visualization.AreaChart(el[0]);
					scope.ac = ac;
					ac.draw(data, {
						width: elWidth,
						height: 50,
						focusTarget: 'category',
						hAxis: {
							//title: scope.data.getColumnLabel(0),
							textPosition: 'none',
							baselineColor: '#ffffff',
							gridlines: {
								count: 0
							}
						},
						vAxis: {
							//title: scope.data.getColumnLabel(1),
							textPosition: 'none',
							gridlines: {
								count: 0
							},
							baseline: scope.data.getColumnRange(V_AXIS_INDEX).min,
							maxValue: 300,
						},
						crosshair: {
							trigger: 'both',
							orientation: 'vertical',
						},
						legend: {
							position: 'none',
						},
						tooltip: {
							trigger: 'none',
						},
						chartArea: {
							width: '100%',
							height: '100%',
							//left: -1,
							//top: 10,
						}
					});

					function handleMouseOverChart(event) {
						//$log.log('mouseoverchart', event);
						$timeout(function() {
							$rootScope.$broadcast(exploreEvents.chartHighlighted, event);
						}, 0);
					}

					function handleMouseLeaveChart() {
						//$log.log('mouseoverchart', event);
						$timeout(function() {
							$rootScope.$broadcast(exploreEvents.chartUnHighlighted);
						}, 0);
					}

					function handleSelect() {
						var selection = ac.getSelection();
						$timeout(function() {
							//only one item is selectable at a time on this chart.
							var row;
							var column;
							if (selection.length > 0) {
								row = selection[0].row;
								column = selection[0].column;
							}
							scope.selection = row;
							$rootScope.$broadcast(exploreEvents.chartSelect, row, column);
						}, 0);
					}

					google.visualization.events.addListener(ac, 'select', handleSelect);
					google.visualization.events.addListener(ac, 'onmouseover', handleMouseOverChart);
					el.mouseleave(function() {
						$timeout(function() {
							handleMouseLeaveChart();
						}, 0);
					});

					$rootScope.$broadcast(exploreEvents.chartInitialized);
				}

				$log.info('loading Google charts');
				google.load("visualization", "1", {
					packages: ["corechart"],
					callback: setupWatches()
				});

			}
		};
	});
});