define(['common/common-module'], function(commonModule) {
	'use strict';

	commonModule.service('debugService', function($location, $parse) {
		var Service = function() {
			//console.log($location.search());
			//console.log($parse(true)());
			var rawDebugParam = $location.search().debug;
			var debugParam = (rawDebugParam === true) ? rawDebugParam : $parse(rawDebugParam)();
			if (debugParam === true) {
				this.enable();
			} else if (debugParam === false) {
				this.disable();
			}
		};

		Service.prototype.enable = function() {
			this.enabled = true;
		};

		Service.prototype.disable = function() {
			this.enabled = false;
		};

		return new Service();
	});
});