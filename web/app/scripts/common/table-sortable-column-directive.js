define(['common/common-module', 'underscore'
	//

], function(commonModule) {
	'use strict';

	/**
	 * Makes a column sortable. Note that you must provide the "sortable" class on your <th> declaration.
	 * We cannot include the <th> in this directive because the browser ejects non-table elements from the <thead> section.
	 *
	 * @param {Array(Routes)} The routes to <display></display>
	 *
	 */
	commonModule.directive('tnSortableColumn', function() {
			return {
				restrict: 'A',
				template: '<div ng-click="onClick()">' +
					'<span ng-transclude></span>' +
					'<span class="direction-symbol">{{sortDirectionSym()}}</span>' +
					'</div>',
				replace: false,
				transclude: true,
				scope: {
					order: '@',
					by: '=',
					reverse: '='
				},
				link: function($scope, el) {

					el.addClass('sortable');

					$scope.onClick = function() {
						if ($scope.order === $scope.by) {
							$scope.reverse = !$scope.reverse;
						} else {
							$scope.by = $scope.order;
							$scope.reverse = false;
						}
					};

					$scope.sortDirectionSym = function() {
						if ($scope.by ===$scope.order) {
							return ($scope.reverse) ? '▾' : '▴';
						} else {
							return ' ';
						}
					};
				}
			};
		}
	);
});