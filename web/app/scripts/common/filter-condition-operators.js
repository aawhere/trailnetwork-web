define([], function() {
	'use strict';

	return {
		equals: '=',
		in: 'in',
	};
});