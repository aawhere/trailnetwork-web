define([
  'common/common-module',
  'underscore'
], function(commonModule, _) {
  'use strict';

  /**
   * @ngdoc filter
   * @name fuzzy
   * @kind function
   *
   * @description
   * fuzzy string searching for array of strings, objects
   */

  commonModule.filter('fuzzy', function() {
    return function(items, search) {

      var matches = _.filter(items, function(item) {
        var j = 0; // remembers position of last found character

        // consider each search character one at a time
        for (var i = 0; i < search.length; i++) {
          var l = search[i];
          if (l === ' ') {
            continue; // ignore spaces
          }

          j = item.indexOf(l, j); // search for character & update position
          if (j === -1) {
            return false; // if it's not found, exclude this item
          }
        }
        return true;

      });
      return matches;
    };
  });
});