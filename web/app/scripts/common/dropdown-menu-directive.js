define(['common/common-module',
		'underscore',
		'common/browser-util',
		'angular',
		'jquery',
		//
	],
	function(commonModule,
		_,
		browserUtil,
		angular,
		jquery) {
		'use strict';

		/**
		 * Transforms the element into a dropdown menu. Requires the markup to follow the following pattern:
		 *
		 * <div tn-dropdown-menu>
		 * 		<div tn-dropdown-menu-control></div>
		 * 		<div tn-dropdown-menu-content></div>
		 * </div>
		 *
		 * @param {object}
		 *            locations: Array[tn.measure.GeoCorrdinate].
		 *
		 */
		commonModule.directive('tnDropdownMenu', function(dropdownMenuService) {
			return {
				restrict: 'A',
				link: function(scope, el, attr) {
					var control = jquery(el).find('[tn-dropdown-menu-control]');
					var content = jquery(el).find('[tn-dropdown-menu-content]');

					if (attr.hasOwnProperty('dontCloseOnClick')) {
						content.bind('click', function(event) {
							event.stopPropagation();
						});
					}

					dropdownMenuService.register(content);

					control.bind('click', function(event) {
						event.stopPropagation();
						dropdownMenuService.toggleActive(content);
					});

					scope.$on('$destroy', function() {
						dropdownMenuService.unregister(content);
					});

				}
			};
		});

		commonModule.factory('dropdownMenuService', function() {
			var service = {},
				_dropdowns = [];

			browserUtil.addWindowOnClick(function() {
				angular.forEach(_dropdowns, function(el) {
					el.removeClass('active');
				});
				//$scope.$apply();
			});

			service.register = function(ddEl) {
				_dropdowns.push(ddEl);
			};

			service.unregister = function(ddEl) {
				var index;
				index = _dropdowns.indexOf(ddEl);
				if (index > -1) {
					_dropdowns.splice(index, 1);
				}
			};

			service.toggleActive = function(ddEl) {
				angular.forEach(_dropdowns, function(el) {
					if (el !== ddEl) {
						el.removeClass('active');
					}
				});

				ddEl.toggleClass('active');
			};

			return service;
		});
	});