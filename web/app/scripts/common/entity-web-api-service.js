/**
 * 	Responsible for retreiving routes. May add to the route repository as required.
 *
 */
define(['angular',
		'common/common-module',
		'common/tn-api-constants',
		//
		'common/notification-service',
	],
	function(angular, commonModule, apiConstants) {
		'use strict';

		/**
		 * Responsible for retreiving an entity. Abstract.
		 *
		 */
		commonModule.factory('EntityWebApiService', function($q, notificationService, $translate, $log) {

				/**
				 *	Abstract object that encapsulates the logic of retreiving data from the web api,
				 *	providing error notifications and logging, pageination, and basic condition handling.
				 *
				 * @param {Object} options {
				 *     notFoundMessageKey: A key to $translate for the not found message.,
				 *     entityName: The name of the entity retreived from the web api (eg. route, trailhead, dictionary).
				 *     id: the id of the entity to retreive.
				 *     repo: an optional entity repository for caching entities.
				 *
				 * }
				 */
				var Entity = function(options) {
					if (!options) {
						options = {};
					}
					this.options = options;
					this.id = options.id;
					this.entityName = options.entityName;
					this.notFoundMessageKey = options.notFoundMessageKey;
					this.repo = options.repo;
					//depricated, use entity
					this.item = undefined;
					this.entity = undefined;
					this.busy = false;
					this.hasError = false;
					this.error = undefined;
					this.requestParameters = {};
					this.requestParameters[apiConstants.params.options] = [];
					this.requestParameters[apiConstants.params.expand] = [];
				};

				/**
				 * Get the route from the server if not already retreived.
				 *
				 * @return {[Promise]} A promise for an tn.route.Route.
				 */
				Entity.prototype.get = function() {
					var deferred = $q.defer();
					var context = this;
					var repo = this.repo;
					var repoEntity;
					if (repo) {
						repoEntity = repo.find(this.id);
					}

					if (!this.entity && !this.busy) {
						if (repoEntity) {
							deferred.resolve(repoEntity);
						} else {
							this.busy = true;
							$log.info('Retrieving ' + this.entityName + ' from web api.');
							this.getFromWebApi().then(function(entity) {
								if (entity) {
									if (context.repo) {
										context.repo.add(entity);
									}
									context.entity = entity;
									//depricated in favor or context.entity
									context.item = entity;
									deferred.resolve(entity);
								} else {
									$log.warn('Received reponse from the web api, but it was undefined', entity);
									deferred.reject();
									context.hasError = true;
								}
								context.busy = false;
							}, function(err) {
								$log.warn('Failed to retreive ' + context.entityName + context.id + '.');
								deferred.reject(err);
								context.hasError = true;
								context.busy = false;
								context.error = err;
							});
						}
					} else {
						deferred.resolve(context.entity);
					}
					return deferred.promise;
				};

				/**
				 * Calls the web api and retreives the entities.
				 *
				 * @return {$q} Promise for a response.
				 */
				Entity.prototype.getFromWebApi = function() {
					$log.warn('You should be overriding Entity.prototype.getFromWebApi()');
					//TODO, override me.
				};

				Entity.prototype.addRequestOptions = function(options) {
					if (!_.isArray(options)) {
						options = [options];
					}
					this.requestParameters[apiConstants.params.options] = _.union(this.requestParameters[apiConstants.params.options], options);
					return this;
				};

				Entity.prototype.expandFields = function(fieldKey) {
					if (!_.isArray(fieldKey)) {
						fieldKey = [fieldKey];
					}
					this.requestParameters[apiConstants.params.expand] = _.union(this.requestParameters[apiConstants.params.expand], fieldKey);
					return this;
				};


				Entity.prototype.notFoundMessage = function(key) {
					if (key) {
						$translate(key, {
							id: this.id
						}).then(function(msg) {
							$log.warn(msg);
							notificationService.notify(msg);
						});
					} else {
						$log.warn('Entity not found for ' + this.entityName + ' ' + this.id);
					}
				};

				return Entity;
			}

		);
	});