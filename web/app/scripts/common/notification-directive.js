define(['common/common-module', 'common/common-events'], function(commonModule, commonEvents) {
	'use strict';

	commonModule.directive('tnNotification', function($timeout) {

			return {
				restrict: 'EA',
				transclude: false,
				templateUrl: '/views/common/notification-directive.html',
				replace: true,
				scope: {},
				link: function(scope) {

					var concealClass = 'conceal';

					scope.visible = false;

					scope.$on(commonEvents.notification, function(event, message) {
						console.log('displaying notification', message);
						scope.displayMessage(message);
					});

					scope.$on(commonEvents.clearNotification, function() {
						console.log('clearing notification');
						scope.message = '';
						scope.classModifier = concealClass;
						scope.visible = false;
					});

					scope.displayMessage = function(message) {
						scope.message = message;
						scope.visible = true;
						scope.classModifier = 'reveal';
						$timeout(function() {
							console.log('conceling notification');
							scope.classModifier = concealClass;
							scope.hide();
						}, 10000);
					};

					scope.hide = function() {
						$timeout(function() {
							console.log('hiding notification');
							scope.visible = false;
						}, 700);
					};

				}
			};

		}
	);
});