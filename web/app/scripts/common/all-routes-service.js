define(['common/common-module'], function(commonModule) {
	'use strict';

	commonModule.service('allRoutesService', function($location, debugService, $parse) {
			var service = {};

			var rawParam = $location.search().allRoutes;
			service.enabled = (rawParam === true) ? rawParam : $parse(rawParam)();
			if (service.enabled === true && debugService.enabled !== false) {
				debugService.enable();
			}

			return service;
		}
	);
});