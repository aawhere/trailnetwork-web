define(['common/common-module', "underscore"],
	function(commonModule, _) {
		'use strict';

		/**
		 * Provides a modal dialog
		 *
		 */
		commonModule.directive('tnModal', function($state, $timeout) {
				return {
					restrict: 'EA',
					templateUrl: '/views/common/modal-directive.html',
					replace: true,
					transclude: true,
					scope: {
						modalTitle: '=',
						close: '&',
						header: '=',
					},
					link: function($scope) {
						var changeEventStarted = false;
						//$scope.isOpen = _.has(attr, 'isOpen') ? $scope.isOpen : false;

						//$scope.$on('$stateChangeSuccess', function() {
							$timeout(function() {
								$scope.isOpen = true;
							}, 150);
						//});

						if(_.isUndefined($scope.header)) {
							$scope.header = true;
						}

						$scope.$on('$stateChangeStart', function(event, toState, toStateParams) {
							//Check to make sure we arent in the middle of a $stateChangeEvent
							if (changeEventStarted) {
								return;
							}
							//Stop the state transition
							//event.preventDefault();

							//Collapse the panel
							$scope.isOpen = false;

							//Wait for the panel to collapse completely
							$timeout(function() {
								changeEventStarted = true;
								//Then transiton the state
								$state.transitionTo(toState, toStateParams);
							}, 600);
						});

					}
				};
			}
		);
	});