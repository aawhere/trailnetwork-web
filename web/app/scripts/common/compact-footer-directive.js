define(['common/common-module'], function(commonModule) {
	'use strict';

	commonModule.directive('tnCompactFooter', function() {
		return {
			restrict: 'EA',
			templateUrl: '/views/common/compact-footer-directive.html',
			replace: false,
			scope: {
			},
			link: function() {

			}
		};
	});
});