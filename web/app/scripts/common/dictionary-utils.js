define(function() {
	'use strict';

	var util = {};

	util.findByKey = function(dictionary, key) {
		var result;
		_.each(dictionary.fields.field, function(field) {
			if (field.key.value === key) {
				result = field;
			}
		});
		return result;
	};

	return util;
});