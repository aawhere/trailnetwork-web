/**
 * 	Responsible for retreiving routes. May add to the route repository as required.
 *
 */
define(['angular',
		'common/common-module',
		'common/tn-api-constants',
		'common/entities',
		'common/filter-condition-operators',
		'underscore',
		//
		'common/notification-service',
	],
	function(angular, commonModule, apiConstants, Entities, operators, _) {
		'use strict';

		/**
		 * Responsible for retreiving a collection of entities. Abstract.
		 *
		 */
		commonModule.factory('EntitiesWebApiService', function($q, notificationService, $translate, $log) {

				/**
				 *	Abstract object that encapsulates the logic of retreiving data from the web api,
				 *	providing error notifications and logging, pageination, and basic condition handling.
				 *
				 * @param {Object} options {
				 *     notFoundMessageKey: A key to $translate for the not found message.,
				 *     entitiesPropertyName: The property name of the collection of entities retreived from the web api.
				 *
				 * }
				 */
				var EntitiesWebApiService = function(options) {
					if (!options) {
						this.options = {};
					} else {
						this.options = options;
					}
					//If true, ignore the results of the xhr request.
					this.isCanceled = false;
					this.entities = new Entities();
					this.currentPageEntities = new Entities();
					this.busy = false;
					this.requestFilter = undefined;
					this.page = 0;
					this.finalPage = false;
					this.hasError = false;
					this.error = undefined;
					this.requestParameters = {};
					this.requestParameters[apiConstants.params.conditions] = [];
					this.requestParameters[apiConstants.params.options] = [];
					this.requestParameters[apiConstants.params.expand] = [];
					this.filterConditions = {};
				};

				/**
				 * Limit the number of results returned from the server.
				 *
				 * @param  {Number} limit
				 * @return {this}
				 */
				EntitiesWebApiService.prototype.setLimit = function(limit) {
					if (limit) {
						this.requestParameters[apiConstants.params.limit] = limit;
					}
					return this;
				};

				EntitiesWebApiService.prototype.makeRequest = function() {
					$log.warn('You should be overriding Entities.prototype.get()');
					//TODO, override me.
				};

				/**
				 * Get the route from the server if not already retreived.
				 *
				 * @return {[Promise]} A promise for an tn.route.Route.
				 */
				EntitiesWebApiService.prototype.nextPage = function() {
					if (this.finalPage === false && !this.busy) {
						this.page = this.page + 1;
						this.currentRequest = undefined;
					}
					return this.get();
				};

				/**
				 * Calls the web api and retreives the entities.
				 *
				 * @return {$q} Promise for a response.
				 */
				EntitiesWebApiService.prototype.get = function() {
					var context = this;
					$log.info('Retrieving page ' + context.page + ' for ' + context.options.entitiesPropertyName);


					if (!this.busy && !this.currentRequest) {
						context.busy = true;
						this.currentRequest = $q.defer();
						updateRequestParameters(context);
						this.makeRequest().then(function(response) {
							if (context.isCanceled) {
								return;
							}
							context.requestFilter = response.filter;
							context.finalPage = response.filter.finalPage;
							var entities = response[context.options.entitiesPropertyName];
							if (response && entities) {
								context.currentPageEntities.clear();
								context.currentPageEntities.add(entities);
								context.entities.add(entities);
							} else {
								context.currentPageEntities.clear();
							}
							context.currentRequest.resolve(context.currentPageEntities);
							context.busy = false;
						}, function(err, status) {
							if (status >= 500) {
								notificationService.networkingError();
							}
							context.currentRequest.reject(err);
							context.busy = false;
							context.hasError = true;
							context.error = err;
						});
					}
					return this.currentRequest.promise;
				};

				EntitiesWebApiService.prototype.cancel = function() {
					this.isCanceled = true;
				};

				EntitiesWebApiService.prototype.addRequestOptions = function(options) {
					if (!_.isArray(options)) {
						options = [options];
					}
					this.requestParameters[apiConstants.params.options] = _.union(this.requestParameters[apiConstants.params.options], options);
					return this;
				};

				EntitiesWebApiService.prototype.addRequestParam = function(key, value) {
					this.requestParameters[key] = value;
					this.filterConditions[key] = value;
				};

				EntitiesWebApiService.prototype.removeRequestParam = function(key) {
					delete this.requestParameters[key];
					delete this.filterConditions[key];
				};

				EntitiesWebApiService.prototype.addFilterCondition = function(key, operator, value) {
					if (operator === operators.equals) {
						this.addRequestParam(key, value);
					} else {
						var parsedValue;
						if (_.isArray(value)) {
							_.each(value, function(v) {
								if (parsedValue) {
									parsedValue = parsedValue + ',' + v;
								} else {
									parsedValue = v;
								}
							});
						} else {
							parsedValue = value;
						}
						this.addRequestConditions(key + ' ' + operator + ' ' + parsedValue);
					}
				};

				EntitiesWebApiService.prototype.clearFilterConditions = function() {
					_.each(this.filterConditions, function(value, key) {
						delete this.requestParameters[key];
					});
					this.requestParameters[apiConstants.params.conditions] = [];
					this.requestParameters = {};
				};

				EntitiesWebApiService.prototype.addRequestConditions = function(conditions) {
					if (!_.isArray(conditions)) {
						conditions = [conditions];
					}
					this.requestParameters[apiConstants.params.conditions] = _.union(this.requestParameters[apiConstants.params.conditions], conditions);
					return this;
				};

				EntitiesWebApiService.prototype.clearRequestConditions = function() {
					console.warn("Depricated, use clearFilterConditions");
					this.clearFilterConditions();
				};

				EntitiesWebApiService.prototype.expandFields = function(fieldKey) {
					if (!_.isArray(fieldKey)) {
						fieldKey = [fieldKey];
					}
					this.requestParameters[apiConstants.params.expand] = _.union(this.requestParameters[apiConstants.params.expand], fieldKey);
					return this;
				};

				EntitiesWebApiService.prototype.orderBy = function(relativeKey, direction) {
					var orderByString = relativeKey;
					if (direction) {
						orderByString += ' ' + direction;
					}
					this.requestParameters[apiConstants.params.orderBy] = orderByString;
					return this;
				};

				EntitiesWebApiService.prototype.clearOrderBy = function() {
					this.requestParameters[apiConstants.params.orderBy] = undefined;
				};

				EntitiesWebApiService.prototype.notFoundMessage = function(key) {
					if (key) {
						$translate(key).then(function(msg) {
							$log.warn(msg);
							notificationService.notify(msg);
						});
					} else {
						$log.warn('Entities not found');
					}
				};

				EntitiesWebApiService.prototype.isEmpty = function() {
					return this.entities.length === 0;
				};

				EntitiesWebApiService.prototype.isFirstPage = function() {
					return (this.page === 1);
				};

				function updateRequestParameters(context) {
					if (context.page > 0) {
						context.requestParameters[apiConstants.params.page] = context.page;
					}
					return context.requestParameters;
				}

				return EntitiesWebApiService;
			}

		);
	});