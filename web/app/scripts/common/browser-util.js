define(['underscore'],
	function(_) {
		'use strict';

		var util = {};

		util.getParameterByName = function(name) {
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
				results = regex.exec(location.search);
			return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		};

		util.addWindowOnClick = function(func) {
			var windowOnClick = window.onclick;
			if(_.isFunction(windowOnClick)) {
				window.onclick = _.wrap(windowOnClick, function(funct) {
					funct();
					func();
				});
			} else {
				window.onclick = func;
			}
		};

		return util;
	});