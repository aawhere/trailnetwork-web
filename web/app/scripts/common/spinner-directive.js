define(['common/common-module'], function(commonModule) {
	'use strict';

	/**
	 * A spinner control
	 *
	 */
	commonModule.directive('tnSpinner', function() {
			return {
				restrict: 'EA',
				transclude: false,
				template: '<span class="spinner-container spinner-display" us-spinner="{radius:4, width:3, length: 6, lines: 9}" spinner-start-active="1"></span>',
				replace: true,
				link: function() {

				}
			};
		}
	);
});