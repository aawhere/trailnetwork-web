define(['common/common-module'], function(commonModule) {
  'use strict';

  commonModule.directive('tnResetField', function($compile, $timeout) {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {},
      link: function(scope, el, attrs, ctrl) {
        // limit to input element of specific types
        var inputTypes = /text|search|tel|url|email|password/i;
        if (el[0].nodeName !== "INPUT") {
          throw new Error("resetField is limited to input elements");
        }
        if (!inputTypes.test(attrs.type)) {
          throw new Error("Invalid input type for resetField: " + attrs.type);
        }

        // compiled reset icon template
        var template = $compile('<div aria-label="cancel" ng-click="reset()" ng-show="enabled" class="reset-field"><span class="icon-cancel medium hover-effects"></span></div>')(scope);
        el.after(template);

        scope.reset = function() {
          ctrl.$setViewValue(null);
          ctrl.$render();
          $timeout(function() {
            el[0].focus();
          }, 0, false);
        };

        // el.bind('input', function() {
        //     scope.enabled = !ctrl.$isEmpty(el.val());
        //   })
        //   .bind('focus', function() {
        //     scope.enabled = !ctrl.$isEmpty(el.val());
        //     scope.$apply();
        //   })
        //   .bind('blur', function() {
        //     //Default used to be false
        //     console.log('blur', !ctrl.$isEmpty(el.val()), el.val());
        //     scope.enabled = !ctrl.$isEmpty(el.val());
        //     scope.$apply();
        //   });

        scope.$watch(function() {
          return ctrl.$viewValue;
        }, function(val) {
          scope.enabled = val ? true : false;
        });
      }
    };
  });

});