define(['angular'], function(angular) {
	'use strict';
	var commonModule;

	commonModule = angular.module('tn.common', []);

	// commonModule.config(function($tooltipProvider) {
	// 	$tooltipProvider.setTriggers({

	// 	});
	// });

	return commonModule;

});