define(['common/common-module', 'underscore'], function(commonModule, _) {
  'use strict';

  var defaults = {
    animation: 'am-fade',
    prefixClass: 'typeahead',
    prefixEvent: '$typeahead',
    placement: 'bottom-left',
    template: 'typeahead/typeahead.tpl.html',
    trigger: 'focus',
    container: false,
    keyboard: true,
    html: false,
    delay: 0,
    minLength: 1,
    filter: 'filter',
    limit: 6,
    comparator: ''
  };

  commonModule.directive('typeahead', function($q) {

    return {
      restrict: 'E',
      template: '<div ng-include="template"></div>',
      transclude: 'true',
      scope: {
        template: '@',
        suggest: '&',
        query: '=',
        limit: '@',
        minLength: '@',
      },
      link: {
        pre: function($scope, el, attr) {
          if (!attr.template) {
            $scope.template = defaults.template;
          }
          if (!attr.limit) {
            $scope.limit = defaults.limit;
          }
          if (!attr.minLength) {
            $scope.minLength = defaults.minLength;
          }
        },
        post: function($scope) {

          $scope.$watch('query', function(query) {
            console.log('query changed, calling callback', query);
            if (query.length >= $scope.minLength) {
              $q.when($scope.suggest(query)).then(function(matches) {
                $scope.matches = _.first(matches, $scope.limit);
              });
            } else {
              $scope.matches = [];
            }
          });

        }
      }
    };
  });
});