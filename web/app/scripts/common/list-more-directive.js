define(['common/common-module'], function(commonModule) {
	'use strict';

	commonModule.directive('tnListMore', function() {
			return {
				templateUrl: '/views/common/list-more-directive.html',
				restrict: 'EA',
				replace: false,
				scope: {
					url: '=',
				},
				link: function() {
				}
			};
		}
	);
});