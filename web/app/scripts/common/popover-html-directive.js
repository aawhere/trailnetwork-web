define(['common/common-module',
    'angular-bootstrap',
], function(commonModule) {
    'use strict';

    commonModule.directive('popoverHtmlPopup',
        function() {
            return {
                restrict: 'EA',
                replace: true,
                scope: {
                    title: '@',
                    content: '@',
                    placement: '@',
                    animation: '&',
                    isOpen: '&',
                    compileScope: '&'
                },
                templateUrl: '/views/common/popover-html-template.html',
                link: function() {

                }
            };
        });

    commonModule.directive('popoverHtml', function($tooltip) {
        return $tooltip('popoverHtml', 'popover', 'click');
    });
});