define(['underscore'], function(_) {
	'use strict';

	var util = {};

	util.toArray = function(arrayOrObject) {
		return _.isArray(arrayOrObject) ? arrayOrObject : [arrayOrObject];
	};

	util.arraysEqual = function(firstArray, secondArray) {
		// if the other array is a falsy value, return
		if (!secondArray || !firstArray) {
			return false;
		}

		// compare lengths - can save a lot of time
		if (firstArray.length !== secondArray.length) {
			return false;
		}

		for (var i = 0, l = firstArray.length; i < l; i++) {
			// Check if we have nested arrays
			if (firstArray[i] instanceof Array && secondArray[i] instanceof Array) {
				// recurse into the nested arrays
				if (!firstArray[i].compare(secondArray[i])) {
					return false;
				}
			} else if (firstArray[i] !== secondArray[i]) {
				// Warning - two different object instances will never be equal: {x:20} != {x:20}
				return false;
			}
		}
		return true;
	};

	/**
	 * Takes an array of string values known to be numbers and converts it into an array of numbers.
	 *
	 * Example: ["1", "2"] becomes [1,2]
	 *
	 * Example: ["a", "2"] throws exception
	 *
	 * @param  {Array[String]} arrayOfStrings [description]
	 * @return {Array[Number]}                [description]
	 */
	util.arrayOfStringToArrayOfNumber = function(arrayOfStrings) {
		var result = [];
		_.each(arrayOfStrings, function(s) {
			result.push(parseInt(s, 10));
		});
		return result;
	};

	/**
	 * Takes an array of string values known to be numbers and converts it into an array of numbers
	 * if they are numbers, otherwise leaves them as a string.
	 *
	 * Example: ["1", "2"] becomes [1,2]
	 *
	 * Example: ["a", "2"] becomes ["a", 2]
	 *
	 * @param  {Array[String]} arrayOfStrings [description]
	 * @return {Array[Number]}                [description]
	 */
	util.arrayOfStringToArrayOfNumberOrString = function(arrayOfStrings) {
		var result = [];
		_.each(arrayOfStrings, function(s) {
			var parsed = parseInt(s, 10);
			if (_.isNaN(parsed)) {
				result.push(s);
			} else {
				result.push(parsed);
			}
		});
		return result;
	};

	/**
	 * Performs a deep clone of an object
	 * See http://jsperf.com/cloning-an-object/2 for details on different methods.
	 * @param  {Object} object the origional
	 * @return {Object}        the clone
	 */
	util.clone = function(object) {
		return JSON.parse(JSON.stringify(object));
	};

	/**
	 * casts the Identifier as a number if it is not a number (i.e. it is a string).
	 * @param  {[type]} id [description]
	 * @return {[type]}         [description]
	 */
	util.idAsNumber = function(id) {
		var result = id;
		if (typeof id !== "number") {
			result = parseInt(id, 10);
		}
		return result;
	};

	/**
	 * casts the Identifiers as a number if it is not a number.
	 * @param  {Array[String or Number]} ids [description]
	 * @return {Array[Number]}     [description]
	 */
	util.idsAsNumbers = function(ids) {
		var result = [];
		_.each(ids, function(id) {
			result.push(util.idAsNumber(id));
		});
		return result;
	};

	/**
	 * Sets a quiet period during which the function will not be invoked.
	 * After the quiet period has elapsed, subsequent calls to the function will call the
	 * wrapped function. An optional reset function can be provided to reset the quiet period.
	 *
	 * @param  {[type]} func [description]
	 * @param {Object} context The "this" for the function
	 * @param  {[type]} wait time of the quiet period
	 * @param {Function} reset called to determine if a reset of the quiet period should be performed. Should return true to rest, false otherwise.
	 * @return {[type]}      [description]
	 */
	var quietPeriodEnds;
	util.quietPeriod = function(func, context, wait, reset) {
		return function() {
			if (!_.isUndefined(quietPeriodEnds)) {
				//console.log('checking if we need to reset');
				//Deterine if the reset period has elapsed.
				if (reset && reset.apply()) {
					console.log('resetting quiet period');
					quietPeriodEnds = undefined;
				}
			}

			if (Date.now() > quietPeriodEnds) {
				//console.log('quiet period ended, running function');
				//Good, we can run the function.
				return func.apply(context, arguments);
			}

			if (_.isUndefined(quietPeriodEnds)) {
				//console.log('setting quiet period');
				quietPeriodEnds = Date.now() + wait;
			}
		};
	};

	util.arrayToApiString = function(arrayOfIds) {
		return arrayOfIds.join(',');
	};

	return util;
});