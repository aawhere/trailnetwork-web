define(function() {
	'use strict';

	var util = {};

	util.isServerError = function(statusCode) {
		return statusCode >= 500;
	};

	util.isClientError = function(statusCode) {
		return (statusCode >= 400 && statusCode < 500);
	};

	util.isAcceptedSuccess = function(statusCode) {
		return (statusCode === 202);
	};

	return util;
});