define(['underscore'], function(_) {
	'use strict';

	return {

		/**
		 * Takes an array of tn.application.ApplicationReference and returns an
		 * object, free of duplicate applications with the following properties:
		 *
		 * {
		 * [applicationKey]: applicationReference.url
		 * }
		 *
		 * @param  {Array[tn.application.ApplicationReference]} arrayOfApplications [description]
		 * @return {Array[String]}    array of ApplicationKeys
		 */
		uniqueApplicationsFromApplicationReferences: function(arrayOfApplicationReferences) {
			if (!_.isArray(arrayOfApplicationReferences)) {
				return [];
			} else {
				var applications = {};
				_.each(arrayOfApplicationReferences, function(reference) {
					applications[reference.activityId.app] = reference.url;
				});
				return applications;
			}
		}

	};
});