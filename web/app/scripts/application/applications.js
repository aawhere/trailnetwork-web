define(function() {
	'use strict';

	var ICON_URL_PREFIX = '/images/provider/';
	var ICON_URL_SUFFIX = '/logo.png';

	return {
		'gc': {
			name: 'Garmin Connect',
			icon: ICON_URL_PREFIX + 'gc' + ICON_URL_SUFFIX,
			activityLink: function(remoteId) {
				return 'http://connect.garmin.com/activity/' + remoteId;
			}
		},
		'mm': {
			name: 'MapMyFitness',
			icon: ICON_URL_PREFIX + 'mm' + ICON_URL_SUFFIX,
			activityLink: function(remoteId) {
				return 'http://www.mapmyfitness.com/workout/' + remoteId;
			}
		},
		'et': {
			name: 'EveryTrail',
			icon: ICON_URL_PREFIX + 'et' + ICON_URL_SUFFIX,
			activityLink: function(remoteId) {
				return 'http://www.everytrail.com/view_trip.php?trip_id=' + remoteId;
			}
		}
	};
});