define(['angular'], function(angular) {
	'use strict';

	var applicationModule;

	applicationModule = angular.module('tn.application', []);

	return applicationModule;
});