define(['application/application-module',
       'underscore',
		//
		'activity/activity-util',
		'application/applications',
		'route/route-application-links-directive'
	],
	function(applicationModule, _) {
		'use strict';

		/**
		 * Provides a formatted link to a provider
		 *
		 * @param {String} The url of the provider
		 *
		 */
		applicationModule.directive('tnApplicationLink', function() {
				return {
					restrict: 'EA',
					template: '<a ng-href="{{url}}" target="_blank" ng-click="$event.stopPropagation()"><tn-application-display application-key="applicationKey" icon-only="iconOnly"></tn-application-display></a>',
					replace: false,
					scope: {
						url: '=',
						applicationKey: '=',
						iconOnly: '='
					},
					link: function(scope) {
						console.log('iconOnly', scope.iconOnly);
						if(_.isUndefined(scope.iconOnly)) {
							scope.iconOnly = false;
						}
					}
				};
			}
		);
	});