define(['application/application-module',
		'application/applications'
	],
	function(applicationModule, applications) {
		'use strict';

		/**
		 * Provides a formatted link to a provider
		 *
		 * @param {String} The url of the provider
		 *
		 */
		applicationModule.directive('tnApplicationDisplay', function() {
				return {
					restrict: 'EA',
					template: '<span class="application-display" title="{{applicationName}}"><img src="{{applicationIconLink}}" alt="{{applicationName}}"><span ng-if="!iconOnly" ng-bind="applicationName"></span></span>',
					replace: false,
					scope: {
						applicationKey: '=',
						iconOnly: '='
					},
					link: function(scope) {
						//scope.iconOnly = _.has(attr, 'iconOnly') ? true : false;
						scope.applications = applications;

						scope.$watch('applicationKey', function() {
							if (scope.applicationKey && applications[scope.applicationKey]) {
								scope.applicationIconLink = applications[scope.applicationKey].icon;
								scope.applicationName = applications[scope.applicationKey].name;
							}
						});
					}
				};
			}
		);
	});