/**
 * bootstraps angular onto the window.document node
 */
require(['angular',
	'jquery',
	'app',
	'domReady!',
	'common/function-overrides'
], function(ng, jquery, app, document) {
	'use strict';

	ng.bootstrap(document, ['tn']);
});