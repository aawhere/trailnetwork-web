define(['angular', 'underscore',
	'explore/explore-module',
	'trailhead/trailhead-util',
	'jquery',
	'route/routes',
	'common/entities',
	'explore/explore-events',
	'map/map-events',
	'app-route-constants',
	'common/common-util',
	'map/location',
	'map/bounding-box',
	//
	'common/location-service',
	'common/debug-service',
	'route/route-filter-service',
	'explore/explore-location-service',
	'map/map-util',
	'map/map-geocell-directive',
	'search/location-search-directive',
	'map/map-cardtray-directive',
	'map/map-service',
	'map/boxes-directive',
	'route/markers-directive',
	'route/route-module',
	'route/route-service',
	'route/route-cards-directive',
	'route/route-detail-directive',
	'route/route-filter-controller',
	'trailhead/trailhead-cardtray-banner-directive',
	'user/current-user-controller',
	'common/compact-footer-directive',
	'map/map-control-directive',
	'map/map-layer-control-directive',
], function(angular,
	_,
	exploreModule,
	thUtil,
	jQuery,
	Routes,
	Entities,
	exploreEvents,
	mapEvents,
	appRouteConstants,
	commonUtil,
	Location,
	BoundingBox) {
	'use strict';

	return exploreModule
		.controller('tnExploreController', function($scope, routeService, $location, mapUtil, locationService,
			$rootScope, $timeout, debugService, exploreLocationService, Map, $stateParams, $log,
			routeFilterService) {

			//Flag that the inital map search parameter has been set in history (and not just replaced).
			//Numerical because we want to set the location plus have a replacable state initially. Therefore
			//we must set the location 2 times.
			var retainMapLocation = false;
			var initialMapLocation = true;
			var defaultZoom = exploreLocationService.defaultZoom();
			var defaultCenter = exploreLocationService.defaultCenter();

			//Namespace for scope properties in this controller.
			$scope.explore = {};
			$scope.explore.routeFilterService = routeFilterService;
			$scope.explore.map = {
				service: undefined,
			};

			$scope.explore.boxes = {
				visible: false,
			};

			$scope.routes = new Entities();

			$scope.locations = [];
			$scope.invisibleLocations = [];

			$scope.trailheads = {};
			$scope.trailheads.prevalent = new Entities();
			$scope.trailheads.prominent = new Entities();

			$scope.debug = debugService.enabled;

			$scope.explore.cardTray = {
				visible: true,
				minimized: false,
			};

			// Enable the new Google Maps visuals until it gets enabled by default.
			// See http://googlegeodevelopers.blogspot.ca/2013/05/a-fresh-new-look-for-maps-api-for-all.html
			google.maps.visualRefresh = true;

			angular.extend($scope, {
				search: {
					visible: true
				},
				trailhead: {
					selectable: true,
					selectedOptionsProminent: {
						icon: '/images/thMarker-16-red.png'
					},
					selectedOptionsPrevalent: {
						icon: '/images/thMarker-6-red.png'
					},
					optionsProminent: {
						icon: '/images/thMarker-16-green.png'
					},
					optionsPrevalent: {
						icon: '/images/thMarker-6-green.png'
					},
					onClick: function(th) {
						//console.log('trailhead.onClick', Date.now());
						exploreLocationService.goToTrailhead(th.id);
					}
				},
				route: {
					selectable: true,
					selectedOptions: {},

					highlight: true,
					highlightOptions: {},
				},
				locations: {
					invisibleDefaultOptions: {
						opacity: 0.0,
						icon: {
							url: '/images/thMarker-16-green.png',
							anchor: {
								x: 8,
								y: 8
							}
						}
					}
				}
			});

			$scope.$on('$stateChangeStart', function() {
				initialMapLocation = false;
				retainMapLocation = false;
			});

			$scope.$on(mapEvents.mapClick, function() {
				if ($scope.explore.map.clickClearsSelected) {
					$rootScope.$broadcast(exploreEvents.routeUnSelectAll);
					$rootScope.$broadcast(exploreEvents.trailheadUnSelectAll);
				}
			});

			$scope.$on(mapEvents.mapSearch, function() {
				$timeout(function() {
					//console.log('setting location due to search');
					$scope.addMapLocationToHistory();
				}, 1);
			});

			$scope.$on(mapEvents.mapTypeChanged, function() {
				//Look to see if we are automatically transitioning between Roadmap and Terrain.
				//If so, don't record a history event, else replace history.
				//This aleviates both unwanted history entries while browsing, and during state changes to a route
				//that zooms into the route.
				if (!$scope.explore.map.transitionMapTypeId) {
					$scope.addMapLocationToHistory();
				} else {
					$scope.replaceMapLocationInHistory();
				}
			});

			$scope.$on(mapEvents.mapBoundsChanged, function() {
				onMapBoundsChanged();
			});

			$scope.$on(mapEvents.mapSearchBefore, function() {
				retainMapLocation = true;
			});

			$scope.$watch(function() {
				return $location.url();
			}, function() {
				handleUrlChange();
			});

			function onMapBoundsChanged() {
				transitionMapType();
				if (retainMapLocation === true) {
					$timeout(function() {
						//console.log('setting initial location for map');
						$scope.addMapLocationToHistory();
						retainMapLocation = false;
					}, 1);
				} else {
					$timeout(function() {
						//console.log('replacing location for map', mapService.bounds(), mapService.center());
						$scope.replaceMapLocationInHistory();
						if (initialMapLocation) {
							initialMapLocation = false;
							retainMapLocation = true;
						}
					}, 1);
				}
			}

			function handleUrlChange() {
				if ($scope.explore.map.service) {
					var parts = $scope.decodeMapSearchParams();
					if (parts) {
						$scope.explore.map.service.mapTypeId(parts.mapTypeId);
						$scope.setMapLocation(parts.center, parts.zoom);
						$scope.decodeBoxesSearchParams();
					}
				}
			}

			/**
			 * Add a route to the map.
			 *
			 * @param {tn.route.Route} route the Route to add.
			 */
			$scope.addRoute = function(route) {
				if (route) {
					$scope.routes.add(route);
				}
			};

			/**
			 * Add a collection of routes to the map.
			 *
			 * @param {Array(tn.route.Routes)} routes
			 */
			$scope.addRoutes = function(routes) {
				if (routes && _.isArray(routes) && routes.length > 0) {
					_.each(routes, function(route) {
						$scope.addRoute(route);
					});
				}
			};

			/**
			 * get routes
			 *
			 * @return {common.Entities(tn.route.Route)} the routes
			 */
			$scope.getRoutes = function() {
				return $scope.routes;
			};

			/**
			 * remove all routes from the map.
			 *
			 * @return {undefined} does not return anything.
			 */
			$scope.clearRoutes = function() {
				$scope.routes.clear();
			};

			/**
			 * Add a location to the map as a marker.
			 *
			 * @param {tn.measure.GeoCorrdinate} location the Location to add.
			 */
			$scope.addLocation = function(location) {
				if (mapUtil.isLocation(location)) {
					$scope.locations.push(location);
				}
			};

			/**
			 * Add a collection of locations to the map as markers.
			 *
			 * @param {Array(tn.measure.GeoCorrdinate)} locations
			 */
			$scope.addLocations = function(locations) {
				if (locations && _.isArray(locations) && locations.length > 0) {
					_.each(locations, function(location) {
						$scope.addLocation(location);
					});
				}
			};

			/**
			 * get locations
			 *
			 * @return {Array[tn.measure.GeoCorrdinate]} the locations
			 */
			$scope.getLocations = function() {
				return $scope.locations;
			};

			/**
			 * remove all location markers from the map.
			 *
			 * @return {undefined} does not return anything.
			 */
			$scope.clearLocations = function() {
				$scope.locations = [];
			};

			/**
			 * Add a location to the map as a marker.
			 *
			 * @param {tn.measure.GeoCorrdinate} location the Location to add.
			 */
			$scope.addInvisibleLocation = function(location) {
				if (mapUtil.isLocation(location)) {
					$scope.invisibleLocations.push(location);
				}
			};

			/**
			 * Add a collection of locations to the map as markers.
			 *
			 * @param {Array(tn.measure.GeoCorrdinate)} locations
			 */
			$scope.addInvisibleLocations = function(locations) {
				if (locations && _.isArray(locations) && locations.length > 0) {
					_.each(locations, function(location) {
						$scope.addInvisibleLocation(location);
					});
				}
			};

			/**
			 * get locations
			 *
			 * @return {Array[tn.measure.GeoCorrdinate]} the locations
			 */
			$scope.getInvisibleLocations = function() {
				return $scope.invisibleLocations;
			};

			/**
			 * remove all location markers from the map.
			 *
			 * @return {undefined} does not return anything.
			 */
			$scope.clearInvisibleLocations = function() {
				$scope.invisibleLocations = [];
			};

			/**
			 * add a collection of trailheads to the map
			 *
			 * @param {Array(tn.trailhead.Trailhead))} trailheads
			 */
			$scope.addPrevalentTrailheads = function(trailheads) {
				trailheads = commonUtil.toArray(trailheads);
				if (trailheads && _.isArray(trailheads)) {
					$scope.trailheads.prevalent.add(trailheads);
				}
			};

			$scope.clearPrevalentTrailheads = function() {
				$scope.trailheads.prevalent.clear();
			};

			/**
			 * add a collection of trailheads to the map
			 *
			 * @param {Array(tn.trailhead.Trailhead))} trailheads
			 */
			$scope.addProminentTrailheads = function(trailheads) {
				trailheads = commonUtil.toArray(trailheads);
				if (trailheads && _.isArray(trailheads)) {
					$scope.trailheads.prominent.add(trailheads);
				}
			};

			/**
			 * remove all trailheads from the map
			 *
			 * @return {undefined} does not return anything.
			 */
			$scope.clearTrailheads = function() {
				$scope.clearPrevalentTrailheads();
				$scope.clearProminentTrailheads();
			};

			/**
			 * remove all trailheads from the map
			 *
			 * @return {undefined} does not return anything.
			 */
			$scope.clearProminentTrailheads = function() {
				$scope.trailheads.prominent.clear();
			};

			/**
			 * remove all trailheads from the map
			 *
			 * @return {undefined} does not return anything.
			 */
			$scope.clearPrevalentTrailheads = function() {
				$scope.trailheads.prevalent.clear();
			};

			/**
			 * get the trailheads
			 *
			 * @return {common.Entities} The trailheads.
			 */
			$scope.getPrevalentTrailheads = function() {
				return $scope.trailheads.prevalent;
			};

			/**
			 * get the trailheads
			 *
			 * @return {common.Entities} The trailheads.
			 */
			$scope.getProminentTrailheads = function() {
				return $scope.trailheads.prominent;
			};

			$scope.getPrevalentTrailhead = function(trailheadId) {
				return $scope.trailheads.prevalent.find(trailheadId);
			};

			$scope.getProminentTrailhead = function(trailheadId) {
				return $scope.trailheads.prominent.find(trailheadId);
			};

			/**
			 * Encodes the maps current state into a url query parameter.
			 *
			 * @param  {boolean} replace replace the current history or not?
			 */
			function encodeMapSearchParams(replace) {
				var params = {};
				params[appRouteConstants.locationQueryParameterName] = exploreLocationService.encodeMap($scope.explore.map.service.center(), $scope.explore.map.service.zoom(), $scope.explore.map.service.mapTypeId());
				// $log.debug('encodeMapSearchParams', mapService.center(), params);
				var result = locationService.extendLocationSearch(params);
				if (replace) {
					$location.replace();
					result.replace();
					//console.log('encodeMapSearchParams and replacing browser history', result);
				}
			}

			$scope.$on('$locationChangeStart', function(event, url) {
				$log.info('$locationChangeStart', url);
			});

			/**
			 * Encoded the maps current state into a url query parameter and replaces the
			 * current browser history with this one.
			 *
			 */
			$scope.replaceMapLocationInHistory = function() {
				encodeMapSearchParams(true);
			};

			/**
			 * Encodes the maps current state into a url query parameter and
			 * adds the url plus the query parameters to the browsers history.
			 */
			$scope.addMapLocationToHistory = function() {
				//setMapSearchParameterHistory = true;
				encodeMapSearchParams(false);
			};

			$scope.decodeMapSearchParams = function() {
				var search = $location.search();
				//console.log('decodeMapSearchParams', search);
				if (search[appRouteConstants.locationQueryParameterName]) {
					try {
						var parts = exploreLocationService.decodeMap(search[appRouteConstants.locationQueryParameterName]);
						return parts;
					} catch (err) {
						//Invalid input.
						console.log('error parsing location: ' + err);
					}
				}
			};

			$scope.decodeBoxesSearchParams = function() {
				var boxes = $stateParams.boxes;
				if (boxes) {
					$scope.explore.boxes.visible = true;
				}
			};

			$scope.setDefaultMapLocation = function() {
				$scope.setMapLocation(defaultCenter, defaultZoom);
			};

			/**
			 * change the location of the map.
			 *
			 * @param {tn.measure.GeoCoordinate} center, the center of the map.
			 * @param {Integer} zoom the zoom level (1-16, 1 is the world, 16 the farthest zoom)
			 */
			$scope.setMapLocation = function(center, zoom) {
				if (center) {
					$scope.explore.map.service.center(center);
				}
				if (zoom) {
					$scope.explore.map.service.zoom(zoom);
				}
			};

			$scope.hasMapLocationSearchParams = function() {
				var search = $location.search();
				var query;
				var locationQuery = search[appRouteConstants.locationQueryParameterName];

				if (locationQuery) {
					query = locationQuery;
				}

				try {
					exploreLocationService.decodeLocation(query);
					return true;
				} catch (err) {
					return false;
				}
			};

			/**
			 * Convienence method for e2e testing.
			 *
			 * @param {[type]} mapTypeId [description]
			 */
			$scope.setMapTypeId = function(mapTypeId) {
				$scope.explore.map.service.mapTypeId(mapTypeId);
			};

			function transitionMapType() {
				if ($scope.explore.map.service.zoom() >= $scope.explore.map.service.maxZoomForMapType($scope.explore.map.service.mapTypeId())) {
					var currentMapType = $scope.explore.map.service.mapTypeId();
					if (currentMapType === 'terrain') {
						$scope.explore.map.service.mapTypeId('roadmap');
						$scope.explore.map.transitionMapTypeId = currentMapType;
					}
				} else if ($scope.explore.map.transitionMapTypeId && $scope.explore.map.service.zoom() < $scope.explore.map.service.maxZoomForMapType($scope.explore.map.transitionMapTypeId)) {
					if ($scope.explore.map.transitionMapTypeId === 'terrain' && $scope.explore.map.service.mapTypeId() === 'roadmap') {
						$scope.explore.map.service.mapTypeId($scope.explore.map.transitionMapTypeId);
					}
					$scope.explore.map.transitionMapTypeId = undefined;
				}
			}

			$scope.explore.initialize = function(center, zoom, bounds) {
				var locationParams = $scope.decodeMapSearchParams();
				var hasLocationParams = false;
				bounds = BoundingBox.createFromBoundingBox(bounds);
				if (!locationParams) {
					locationParams = {
						center: center || bounds && bounds.center() || defaultCenter,
						zoom: zoom || defaultZoom,
					};
				} else {
					hasLocationParams = true;
				}

				if (!$scope.explore.map.service) {
					$scope.explore.map.service = new Map({
						center: Location.createFromLocation(locationParams.center).googleLatLng(),
						zoom: locationParams.zoom,
						mapTypeId: locationParams.mapTypeId,
						mapTypeControl: false,
						// mapTypeControlOptions: {
						// 	position: google.maps.ControlPosition.TOP_RIGHT,
						// 	style: google.maps.MapTypeControlStyle.DEFAULT,
						// },
						streetViewControlOptions: {
							position: google.maps.ControlPosition.RIGHT_BOTTOM,
						},
						zoomControlOptions: {
							position: google.maps.ControlPosition.RIGHT_BOTTOM,
							style: google.maps.ZoomControlStyle.SMALL,
						},
						scaleControl: true,
						overviewMapControl: true,
					});
					if (bounds && !hasLocationParams) {
						//map gets initiated in the next digest.
						$timeout(function() {
							$scope.explore.map.service.bounds(bounds);
						});
					}
				} else {
					//already intialized, go to location
					if (bounds) {
						$scope.explore.map.service.bounds(bounds);
					} else if (locationParams.center && locationParams.zoom) {
						$scope.explore.map.service.center(locationParams.center);
						$scope.explore.map.service.zoom(locationParams.zoom);
					}
				}

			};

		});
});