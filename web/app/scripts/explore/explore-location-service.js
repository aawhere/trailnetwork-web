define(['explore/explore-module',
	'app-route-constants',
	'map/map-util',
	//
	'common/location-service',
	'explore/explore-routes-service',
], function(exploreModule, appRouteConstants, mapUtil) {
	'use strict';

	exploreModule.service('exploreLocationService', function($location, $state, exploreRoutesService) {
			var MAP_QUERY_PARAM_DELIMINATOR = ',';
			var LAT_LNG_LENGTH = 7;
			var service = {};
			var MAP_TYPE_REGEX = /(hybrid|roadmap|satellite|terrain)/;

			var defaultCenter = {
				latitude: {
					value: 37.86330631001689
				},
				longitude: {
					value: -122.50696332979737
				}
			};

			var defaultZoom = 14;

			service.goToTrailhead = function(trailheadId) {
				$state.go('explore.trailhead', {
					trailheadId: trailheadId
				});
			};

			service.goToRoute = function(routeId) {
				$state.go('explore.route', {
					routeId: routeId
				});
			};

			service.linkToRoute = function(routeId) {
				return appRouteConstants.pathSeperator + appRouteConstants.routePath + appRouteConstants.pathSeperator + routeId;
			};

			service.goToExplore = function() {
				$state.go(appRouteConstants.states.exploreRoutes);
			};

			//Restores the explore-routes controller to it's previous state.
			service.restoreExploreRoutes = function() {
				$state.go(appRouteConstants.states.exploreRoutes, exploreRoutesService.map.search);
			};

			service.goToDefaultLocation = function() {
				$state.go(appRouteConstants.states.exploreRoutes, {
					"@": service.encodeLocation(defaultCenter, defaultZoom)
				}, {
					reload: true
				});
			};

			service.defaultCenter = function() {
				//return a copy.
				return {
					latitude: {
						value: defaultCenter.latitude.value
					},
					longitude: {
						value: defaultCenter.longitude.value
					}
				};
			};

			service.defaultZoom = function() {
				return defaultZoom;
			};


			/**
			 * Encode the current location (map center+zoom level) as a compact query parameter
			 *
			 * @param  {tn.measure.GeoCoordinate} center [description]
			 * @param  {Integer} zoom   the google maps zoom level 1-16
			 * @return {String}		The encoded location
			 */
			service.encodeLocation = function(center, zoom) {
				var encoded = '';
				encoded += center.latitude.value.toFixed(LAT_LNG_LENGTH);
				encoded += MAP_QUERY_PARAM_DELIMINATOR;
				encoded += center.longitude.value.toFixed(LAT_LNG_LENGTH);
				encoded += MAP_QUERY_PARAM_DELIMINATOR + zoom + 'z';
				return encoded;
			};

			/**
			 * Decode the current location (map center+zoom level) from a compact query parameter
			 *
			 * @param  {tn.measure.GeoCoordinate} center [description]
			 * @param  {Integer} zoom   the google maps zoom level 1-16
			 * @return {Object}		Object with properties 'center' and zoom. If input invalid, throws exception.
			 */
			service.decodeLocation = function(encoded) {
				var decoded = {};
				var center = {};
				center.latitude = {};
				center.longitude = {};

				var regex = /([\d\.]+),(\-?[\d\.]+),([\d]+)z.*/;
				if (regex.test(encoded)) {
					var parts = regex.exec(encoded);
					center.latitude.value = parseFloat(parts[1]);
					center.longitude.value = parseFloat(parts[2]);
					decoded.center = center;
					decoded.zoom = parseInt(parts[3], 10);
				} else {
					throw "Invalid input " + encoded + ' must match ' + regex;
				}

				return decoded;
			};

			service.encodeMapType = function(mapType) {
				if (mapUtil.isValidMapTypeId(mapType)) {
					return mapType;
				} else {
					throw "Invalid input " + mapType + ' must be a valid map type';
				}
			};

			service.decodeMapType = function(encoded) {
				var decoded = {};
				var mapType = MAP_TYPE_REGEX.exec(encoded)[0];
				if (mapUtil.isValidMapTypeId(mapType)) {
					decoded.mapTypeId = mapType;
					return decoded;
				} else {
					throw "Invalid input " + encoded + ' must be a valid map type';
				}
			};

			service.encodeMap = function(center, zoom, mapType) {
				var locationEncoded = service.encodeLocation(center, zoom);
				var mapTypeEncoded = service.encodeMapType(mapType);
				return locationEncoded + MAP_QUERY_PARAM_DELIMINATOR + mapTypeEncoded;
			};

			service.decodeMap = function(encoded) {
				var locationDecoded = service.decodeLocation(encoded);
				var mapTypeDecoded = service.decodeMapType(encoded);
				return angular.extend(locationDecoded, mapTypeDecoded);
			};

			return service;
		}
	);
});