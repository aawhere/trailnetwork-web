define(['explore/explore-module',
	'search/search-events',
	//
	'common/app-route-service',
	'search/search-directive',
	'route/route-filter-service',
], function(exploreModule, searchEvents) {
	'use strict';

	exploreModule.controller('tnExploreRoutesLeafletsController', function($scope, appRouteService,
	                         routeFilterService) {

			var navigateToId = function(id, type) {
				switch (type) {
					case 'route':
						appRouteService.explore.routeUrl(id, true);
						break;
					default:
				}
			};

		$scope.routeFilterService = routeFilterService;

		$scope.$on(searchEvents.selected, function(event, id, type) {
			console.log('search result selected', id, type);
			navigateToId(id, type);
		});

	});
});