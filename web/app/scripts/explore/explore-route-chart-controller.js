define(['explore/explore-module',
		'app-route-constants',
		'explore/explore-events',
		'underscore',
		'common/common-util',
		'map/location',
		'user/user-events',
		//
		'common/area-chart-directive',
		'spinner'
	],
	function(exploreModule, routeConstants, exploreEvents, _, commonUtil, Location, userEvents) {
		'use strict';

		exploreModule.controller('tnExploreRouteChartController', function($scope, $timeout, usSpinnerService, $state) {
			var CHART_ELEVATION_GAIN_KEY = 'elevationGain';
			var CHART_ELEVATION_LOSS_KEY = 'elevationLoss';
			var CHART_DISTANCE_COLUMN_INDEX = 0;
			var CHART_ELEVATION_COLUMN_INDEX = 1;
			var CHART_LOCATION1_KEY = 'location1';
			var ROUTE_ANIMATE_DELAY = 45; //milliseconds
			var spinnerKey = 'chartSpinner';
			var stopAnimation;
			var visulizationLoaded = false;
			$scope.isRouteAnimated = false;
			$scope.chart = {};
			$scope.chart.tare = false;
			$scope.maxNumberOfTrackpoints = 300;

			function cleanupData(dt) {
				var jsonDt = JSON.parse(dt.toJSON());
				var dtColumns = jsonDt.cols;
				jsonDt.cols = [];
				jsonDt.cols.push(dtColumns[0]);
				jsonDt.cols.push(dtColumns[1]);
				var dtRows = jsonDt.rows;
				jsonDt.rows = [];
				_.each(dtRows, function(row) {
					var r = {};
					r.c = [];
					r.c.push(row.c[0]);
					r.c.push(row.c[1]);
					r.p = {};
					r.p[CHART_ELEVATION_GAIN_KEY] = row.c[2];
					r.p[CHART_ELEVATION_LOSS_KEY] = row.c[3];
					r.p[CHART_LOCATION1_KEY] = row.c[4];
					jsonDt.rows.push(r);
				});
				return new google.visualization.DataTable(jsonDt);
			}

			function loadDataTable() {
				usSpinnerService.spin(spinnerKey);
				console.log('loading datatable');
				var query = new google.visualization.Query(routeConstants.proxyServerBaseUrl +
					'/datasources/routes?routeId=' + $scope.exploreRoute.routeId + '&maxNumberOfTrackpoints=' + $scope.maxNumberOfTrackpoints);
				query.send(function(queryResponse) {
					usSpinnerService.stop(spinnerKey);
					if (queryResponse.isError()) {
						//TODO: handle error.
						console.log('error retreiving datatable:', queryResponse.getMessage(), queryResponse.getReasons(), queryResponse.getDetailedMessage());
					} else {
						console.log('received datatable');
						var dt = cleanupData(queryResponse.getDataTable());
						$timeout(function() {
							$scope.chart.dataTable = dt;
							$scope.chart.elevationLabel = dt.getColumnLabel(CHART_ELEVATION_COLUMN_INDEX);
							$scope.chart.distanceLabel = dt.getColumnLabel(CHART_DISTANCE_COLUMN_INDEX);
							$scope.chart.resetValues();
							displayInvisibleMarkers();
							$scope.chart.loaded = true;
						}, 0);
						console.log('stopping spinner');
					}
				});
			}

			function getDistanceValue(row) {
				return getColumnValue(row, CHART_DISTANCE_COLUMN_INDEX);
			}

			function getElevationGain(row) {
				return getPropertyValue(row, CHART_ELEVATION_GAIN_KEY);
			}

			function getFormattedElevationGain(row) {
				return getFormattedPropertyValue(row, CHART_ELEVATION_GAIN_KEY);
			}

			function getColumnValue(row, key) {
				return $scope.chart.dataTable.getValue(row, key);
			}

			function getFormattedColumnValue(row, key) {
				return $scope.chart.dataTable.getFormattedValue(row, key);
			}

			function getFormattedPropertyValue(row, key) {
				return $scope.chart.dataTable.getRowProperty(row, key).f;
			}

			function getPropertyValue(row, key) {
				return $scope.chart.dataTable.getRowProperty(row, key).v;
			}

			function displayInvisibleMarkers() {
				var numOfRows = $scope.chart.dataTable.getNumberOfRows();
				var i = 0;

				$scope.clearInvisibleLocations();
				while (i < numOfRows) {
					var iLocationString = getPropertyValue(i, CHART_LOCATION1_KEY);
					var iLocation = Location.createFromString(iLocationString);
					$scope.addInvisibleLocation(iLocation);
					i++;
				}
			}

			/**
			 * Find the index in the datatable of the provided location
			 *
			 * @param  {[type]} location [description]
			 * @return {[type]}          [description]
			 */
			function findLocationIndex(location) {
				var numOfRows = $scope.chart.dataTable.getNumberOfRows();
				var i = 0;
				var locationString = Location.createFromString(location);

				while (i < numOfRows) {
					var iLocationString = getPropertyValue(i, CHART_LOCATION1_KEY);
					if (iLocationString === locationString) {
						break;
					}
					i++;
				}
				return i;
			}

			function getLocationAtIndex(index) {
				var locationString = $scope.chart.dataTable.getRowProperty(index, CHART_LOCATION1_KEY).v;
				var location = Location.createFromString(locationString);
				return location;
			}

			$scope.animateRoute = function() {
				$scope.isRouteAnimated = true;
				console.log('animateRoute', $scope.isRouteAnimated);
				var numOfRows = $scope.chart.dataTable.getNumberOfRows();
				var i = 0;
				animateRouteRecursive(i, numOfRows);
			};

			$scope.stopAnimateRoute = function() {
				if ($scope.chart.initialized) {
					$scope.isRouteAnimated = false;
					$scope.chart.selection = undefined;
					clearLocations();
				}
			};

			function animateRouteRecursive(index, numRows) {
				if ($scope.isRouteAnimated) {
					$scope.clearLocations();
					$scope.addLocation(getLocationAtIndex(index));
					$scope.chart.selection = index;
					setValues(index);
					index++;
					if (index < numRows) {
						$timeout(function() {
							animateRouteRecursive(index, numRows);
						}, ROUTE_ANIMATE_DELAY);
					} else {
						$scope.chart.resetValues();
						$scope.isRouteAnimated = false;
					}
				} else {
					//$scope.chart.clearSelection();
				}
			}

			function clearLocations() {
				$scope.clearLocations();
				$scope.chart.resetValues();
			}

			function tare(row) {
				$scope.chart.tare = true;
				$scope.chart.elevationGainTare = getElevationGain(row);
				$scope.chart.elevationLossTare = getPropertyValue(row, CHART_ELEVATION_LOSS_KEY);
				$scope.chart.distanceTare = getColumnValue(row, CHART_DISTANCE_COLUMN_INDEX);
			}

			function clearTare() {
				$scope.chart.tare = false;
				$scope.chart.elevationGainTare = undefined;
				$scope.chart.elevationLossTare = undefined;
				$scope.chart.distanceTare = undefined;
			}

			function calculateTare(row) {
				setValues(row);
				$scope.chart.elevationGain = getElevationGain(row) - $scope.chart.elevationGainTare;
				$scope.chart.elevationLoss = getPropertyValue(row, CHART_ELEVATION_LOSS_KEY) - $scope.chart.elevationLossTare;
				$scope.chart.distance = getDistanceValue(row) - $scope.chart.distanceTare;

				if ($scope.chart.elevationGain < 0 || $scope.chart.elevationLoss < 0) {
					//we are to the left of the tare, invert and switch values.
					var gain, loss;
					gain = $scope.chart.elevationGain;
					loss = $scope.chart.elevationLoss;
					$scope.chart.elevationGain = Math.abs(loss);
					$scope.chart.elevationLoss = Math.abs(gain);
				}

				//format values
				$scope.chart.elevationGain = $scope.chart.elevationGain.toFixed(0);
				$scope.chart.elevationLoss = $scope.chart.elevationLoss.toFixed(0);
				$scope.chart.distance = Math.abs($scope.chart.distance.toFixed(1));

				$scope.addLocation(getLocationAtIndex($scope.chart.selection));
			}

			function setSelection(location) {
				var locationIndex = findLocationIndex(location);
				$scope.addLocation(location);
				$scope.chart.selection = locationIndex;
				$scope.chart.resetValues();
			}

			$scope.chart.clearSelection = function() {
				$scope.chart.selection = undefined;
				$scope.chart.resetValues();
			};

			function loadVisulizationCallback() {
				visulizationLoaded = true;
			}

			console.log('loading Google charts');
			google.load("visualization", "1", {
				packages: ["corechart"],
				callback: loadVisulizationCallback
			});

			function setValues(row) {
				$scope.chart.elevationGain = getFormattedElevationGain(row);
				$scope.chart.elevationLoss = getFormattedPropertyValue(row, CHART_ELEVATION_LOSS_KEY);
				$scope.chart.elevation = getFormattedColumnValue(row, CHART_ELEVATION_COLUMN_INDEX);
				$scope.chart.distance = getFormattedColumnValue(row, CHART_DISTANCE_COLUMN_INDEX);
			}

			$scope.chart.resetValues = function() {
				if ($scope.exploreRoute.route) {
					$scope.chart.distance = $scope.exploreRoute.route.distance.display.formatted;
				}
				$scope.chart.elevationGain = getFormattedElevationGain($scope.chart.dataTable.getNumberOfRows() - 1);
				$scope.chart.elevationLoss = getFormattedPropertyValue($scope.chart.dataTable.getNumberOfRows() - 1, CHART_ELEVATION_LOSS_KEY);
				if ($scope.chart.selection) {
					$scope.chart.elevation = getFormattedColumnValue($scope.chart.selection, CHART_ELEVATION_COLUMN_INDEX);
				} else {
					$scope.chart.elevation = '';
				}
			};

			$scope.chart.entityValueClass = function(value) {
				if (value && (value.length > 8)) {
					return 'narrow-text';
				} else {
					return '';
				}
			};

			stopAnimation = function(indexes) {
				if ($scope.chart.initialized) {
					if ($scope.isRouteAnimated) {
						$scope.stopAnimateRoute();
					}
					clearLocations();
					if ($scope.chart.selection && !$scope.chart.tare) {
						tare($scope.chart.selection);
					}
					if (indexes) {
						var row = indexes.row;
						if ($scope.chart.tare) {
							calculateTare(row);
						} else {
							setValues(row);
						}
						$scope.addLocation(getLocationAtIndex(row));
					}
				}
			};

			var resetChartQuietPeriod = false;
			$scope.$on(exploreEvents.chartHighlighted, commonUtil.quietPeriod(function(event, indexes) {
				stopAnimation(indexes);
			}, this, 300, function() {
				if (resetChartQuietPeriod) {
					resetChartQuietPeriod = false;
					return true;
				} else {
					return false;
				}
			}));

			$scope.$on(exploreEvents.chartUnHighlighted, function() {
				clearLocations();
				clearTare();
				$scope.chart.clearSelection();
				resetChartQuietPeriod = true;
			});

			$scope.$on(exploreEvents.chartStopAnimation, function() {
				stopAnimation();
			});

			$scope.$on(exploreEvents.chartInitialized, function() {
				$scope.chart.initialized = true;
			});

			$scope.$on(exploreEvents.chartSelect, function(event, row, column) {
				console.log(exploreEvents.chartSelect, row, column);
				if (row) {
					tare(row);
				} else {
					clearTare();
				}
			});

			$scope.$watch('chart.initialized', function(value) {
				if (value && !$scope.isRouteAnimated) {
					$scope.animateRoute();
				}
			});

			$scope.$on(exploreEvents.markerSelected, function(event, location) {
				$scope.stopAnimateRoute();
				setSelection(location);
			});

			$scope.$on(exploreEvents.markerUnSelected, function() {
				clearLocations();
			});

			$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
				if ((fromState.name === $state.current.name)) {
					$scope.stopAnimateRoute();
				}
			});

			var initialize = function() {
				if (visulizationLoaded === true) {
					$scope.chart.initialized = false;
					$scope.chart.dataTable = undefined;
					$scope.$broadcast(exploreEvents.clearChart);
					loadDataTable();
				}
			};

			$scope.$watch(function() {
				return $scope.exploreRoute.routeId + visulizationLoaded;
			}, function(routeId) {
				if (routeId && visulizationLoaded) {
					initialize();
				}
			});

			$scope.$on(userEvents.unitOfMeasurePreferenceChanged, function() {
				initialize();
			});
		});
	});