define(['explore/explore-module',
	'explore/explore-events',
	'user/user-events',
	//
	'common/browser-service',
	'common/notification-service',
	'common/location-service',
	'explore/explore-controller',
	'explore/breadcrumbs-directive',
	'explore/explore-location-service',
	'map/geo-service',
	'route/route-filter-controller',
	'route/route-filter-service',
	'trailhead/trailhead-detail-directive',
	'trailhead/trailhead-web-api-service',
	'route/routes-web-api-service',
], function(exploreModule, exploreEvents, userEvents) {
	'use strict';

	return exploreModule
		.controller('tnExploreTrailheadCardsController',
			function($scope,
				TrailheadWebApiService,
				$stateParams,
				locationService,
				exploreLocationService,
				$rootScope,
				$timeout,
				$state,
				routeFilterService,
				notificationService,
				$translate,
				RoutesWebApiService) {

				$scope.exploreTrailheads = {
					trailheadId: $stateParams.trailheadId,
				};

				angular.extend($scope, {
					cardTray: {
						visible: true,
						minimized: false,
						showNoRoutesMessage: false,
						showTrailheadNotFoundMessage: false,
						clear: function() {
							$scope.cardTray.showNoRoutesMessage = false;
							$scope.cardTray.showTrailheadNotFoundMessage = false;
						},
					},
					banner: {
						visible: true,
						onClose: function() {
							exploreLocationService.restoreExploreRoutes();
						},
						text: ""
					},
					leafletContainerVisible: false,
					routeFilterService: routeFilterService,
				});

				$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
					if (fromState.name === $state.current.name) {
						$scope.explore.initializing = false;
						$rootScope.$broadcast(exploreEvents.routeUnSelectAll);
						$scope.clearTrailheads();
						$scope.clearRoutes();
					}
				});

				$scope.$on(exploreEvents.routeSelected, function(event, routeId) {
					//console.log('explore-controller.$on(' + exploreEvents.trailheadSelected + ')', trailheadId);
					exploreLocationService.goToRoute(routeId);
				});

				$scope.$on(exploreEvents.routeFilterUpdated, function() {
					console.log('onRouteFitlerUpdated');
					getRoutes($scope.exploreTrailheads.trailheadId);
				});

				$scope.$on(userEvents.unitOfMeasurePreferenceChanged, function() {
					getRoutes($scope.exploreTrailheads.trailheadId);
				});

				function updateTrailhead(th) {
					//remove all but one trailhead
					$scope.clearTrailheads();
					$scope.addProminentTrailheads(th);
					$scope.selectedTrailhead = th;
					if (th.hasOwnProperty('name')) {
						$translate('TH_TRAILHEAD_BANNER', {
							name: th.name.$
						}).then(function(banner) {
							$scope.banner.text = banner;
						});
					}
					//The trailhead was previously selected when it was clicked on. We want it to be unselected (green).
					$rootScope.$broadcast(exploreEvents.trailheadUnSelected, th.id);
				}

				/**
				 * get the trailhead and routes
				 *
				 * @param  {trailhead} th The trailhead of interest
				 */
				function getTrailhead(thId) {
					new TrailheadWebApiService(thId).get().then(function(th) {
						updateTrailhead(th);
					}, function(error) {
						console.log('ERROR: Trailhead ' + thId + ' was not found. ', error);
					});
				}

				function displayRoutes(routes) {
					$scope.cardTray.clear();
					$scope.leafletContainerVisible = true;
					//zoom to extent of mains.
					if (_.isEmpty(routes)) {
						$scope.cardTray.showNoRoutesMessage = true;
					} else {
						$scope.clearRoutes();
						$scope.addRoutes(routes);

						$timeout(function() {
							onFirstPage();
							$scope.explore.map.service.onInitialized().then(function() {
								//The map is initially hidden.
								$scope.explore.map.visible = true;
								$scope.search.visible = false;
								$scope.route.selectable = true;
								$scope.route.highlight = true;
								$scope.trailhead.selectable = false;
							});
						}, 50);
					}
				}

				function onFirstPage() {
					if ($scope.exploreTrailheads.routesWebApiService.page === 1) {
						$scope.explore.initialize(null, null, $scope.exploreTrailheads.routesWebApiService.bounds);
					}
				}

				function getRoutes(thId) {
					var routesWebApiService = new RoutesWebApiService()
						.limitToTrailheadIds(thId)
						.filteredByRouteFilter(routeFilterService);

					$scope.exploreTrailheads.routesWebApiService = routesWebApiService;

					$scope.nextPage();
				}

				function redirectToHome() {
					//Route was not found, redirect to home
					exploreLocationService.restoreExploreRoutes();
				}

				$scope.nextPage = function() {
					console.log('retreiving next page of results', $scope.exploreTrailheads.trailheadId, $scope.filter ? $scope.filter.page + 1 : 1);
					$scope.exploreTrailheads.routesWebApiService.nextPage().then(function() {
						displayRoutes($scope.exploreTrailheads.routesWebApiService.entities);
					}, function() {
						$translate('TN_NETWORKING_ERROR').then(function(msg) {
							notificationService.notification(msg);
						});
						redirectToHome();
					});
				};

				getTrailhead($scope.exploreTrailheads.trailheadId);
				getRoutes($scope.exploreTrailheads.trailheadId);
			}
		);
});