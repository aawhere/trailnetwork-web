define(['angular', 'underscore',
	'explore/explore-module',
	'explore/explore-events',
	'map/zoom-levels',
	'trailhead/trailhead-filter',
	'common/entities',
	'trailhead/trailhead-util',
	'common/common-util',
	'map/map-events',
	'route/route-util',
	'user/user-events',
	'map/location',
	//
	/*'map/geocell-util',*/
	/*'trailhead/trailhead-util',*/
	'common/browser-service',
	'common/location-service',
	'common/notification-service',
	'explore/explore-controller',
	'explore/explore-location-service',
	'route/route-filter-service',
	'explore/explore-routes-service',
	'search/location-search-directive',
	'map/map-cardtray-directive',
	'route/route-module',
	'route/routes-web-api-service',
	'route/route-cards-directive',
	'route/route-detail-directive',
	'route/route-filter-controller',
	'trailhead/trailheads-web-api-service',
	'trailhead/trailhead-web-api-service',
	'trailhead/trailhead-cardtray-banner-directive',
	'spinner',
], function(angular, _, exploreModule, exploreEvents, zoomLevelMap, TrailheadFilter, Entities, trailheadUtil, commonUtil,
	mapEvents, routeUtil, userEvents) { //, Location) {
	'use strict';

	return exploreModule
		.controller('tnExploreRoutesCardsController', function($scope,
			TrailheadsWebApiService,
			RoutesWebApiService,
			locationService,
			$location,
			$state,
			exploreLocationService,
			usSpinnerService,
			$q,
			routeFilterService,
			exploreRoutesService,
			browserService,
			notificationService,
			$log,
			routeCardsSpinnerKey,
			$timeout) {

			var MAX_TRAILHEADS = 100;
			var MAX_TRAILHEADS_IN_VIEW = 15;
			var ROUTES_LIMIT = 10;
			//Flag to indicate that the view is being restored.
			var restoringView = false;
			var trailheadsWebApiService = new TrailheadsWebApiService();

			angular.extend($scope, {
				prominentRoute: {
					//greater than or equal to this value
					zoomLevelToDisplay: 4
				},
			});

			$scope.cardTray = {};
			$scope.cardTray.showNoRoutesMessage = false;
			$scope.cardTray.showErrorMessage = false;
			$scope.cardTray.showOutOfBoundsMessage = false;
			$scope.cardTray.showCards = true;
			$scope.cardTray.clear = function() {
				$scope.cardTray.showNoRoutesMessage = false;
				$scope.cardTray.showErrorMessage = false;
				$scope.cardTray.showOutOfBoundsMessage = false;
				$scope.cardTray.showCards = true;
			};
			$scope.cardTray.displayOutOfBoundsMessage = function() {
				$scope.cardTray.clear();
				$scope.explore.cardTray.visible = true;
				$scope.cardTray.showCards = false;
				$scope.cardTray.showOutOfBoundsMessage = true;
			};
			$scope.cardTray.displayErrorMessage = function() {
				$scope.cardTray.clear();
				$scope.explore.cardTray.visible = true;
				$scope.cardTray.showCards = false;
				$scope.cardTray.showErrorMessage = true;
			};
			$scope.cardTray.displayNoRoutesMessage = function() {
				$scope.cardTray.clear();
				$scope.explore.cardTray.visible = true;
				$scope.cardTray.showCards = false;
				$scope.cardTray.showNoRoutesMessage = true;
			};

			$log.info('initializing tnExploreRoutesController');

			$scope.route.selectable = true;
			$scope.route.highlight = true;
			$scope.trailhead.selectable = true;
			browserService.useDefaultTitle();
			$scope.leafletContainerVisible = true;

			$scope.service = exploreRoutesService;

			$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
				if (fromState.name === $state.current.name) {
					$scope.explore.initializing = false;
					$scope.clearTrailheads();
					$scope.clearRoutes();
				}
			});

			$scope.$watch(function() {
				return $location.url();
			}, function() {
				if (!restoringView) {
					$log.debug('storing url', $location.url(), $scope.explore.map.service.center(), $scope.explore.map.service.bounds());
					$scope.service.map.url = $location.url();
					$scope.service.map.center = $scope.explore.map.service.center();
					$scope.service.map.zoom = $scope.explore.map.service.zoom();
					$scope.service.map.zoomLevel = currentZoomLevel();
					$scope.service.map.bounds = $scope.explore.map.service.bounds();
					$scope.service.map.search = $location.search();
				}
			});

			$scope.$on(mapEvents.mapBoundsChanged, function(event, bounds) {
				console.log('on ' + mapEvents.mapBoundsChanged, bounds, $scope.explore.map.service.center().latitude.value);
				if (restoringView) {
					$log.debug('restoring view');
					restoringView = false;
					$log.debug('center', $scope.explore.map.service.center());
				} else {
					update();
				}
			});

			$scope.$on(exploreEvents.trailheadSelected, function(event, trailheadId) {
				//console.log('explore-controller.$on(' + exploreEvents.trailheadSelected + ')', trailheadId);
				exploreLocationService.goToTrailhead(trailheadId);
			});

			$scope.$on(exploreEvents.routeSelected, function(event, routeId) {
				console.log('explore-controller.$on(' + exploreEvents.routeSelected + ')', routeId);
				exploreLocationService.goToRoute(routeId);
			});

			$scope.$on(exploreEvents.routeFilterUpdated, function() {
				//console.log('explore-controller.$on(' + exploreEvents.routeFilterUpdated + ')');
				$scope.service.clearAll();
				update();
			});

			$scope.$on(userEvents.unitOfMeasurePreferenceChanged, function() {
				// console.log(userEvents.unitOfMeasurePreferenceChanged);
				if(restoringView) {
					return;
				}
				$scope.service.clearAll();
				update();
			});

			function update() {
				$scope.cardTray.clear();
				if ($scope.explore.map.service.hasBounds()) {
					//We need the card tray to show so that our bounds will reflect the map with the card tray.
					//This prevents an infinit loop where the card tray is minimized and maximized when the only
					//trailheads are under where the card tray goes.
					updateTrailheads();
					updateRoutes();
				}
			}

			function restoreView() {
				restoringView = true;
				var thWebApiService = $scope.service.trailheads.trailheadsWebApiService;
				if (thWebApiService) {
					$timeout(function() {
						$log.debug('restoring view');
						$scope.explore.initialize($scope.service.map.center, $scope.service.map.zoom);
						$scope.explore.map.service.onInitialized().then(function() {
							displayTrailheads(thWebApiService.entities);
							displayRoutes();
							restoringView = false;
						});
					}, 50);
				} else {
					//Looks like this is the first time someone navigated here.
					// If ClientLocation was filled in by the loader, use that info instead
					// if (google.loader.ClientLocation) {
					// 	var zoom = 13;
					// 	var center = Location.createFromLatLon(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
					// 	$scope.explore.initialize(center, zoom);
					// } else {
						$scope.explore.initialize();
					// }
					$scope.explore.map.service.onInitialized().then(function() {
						update();
					});
				}
			}

			/**
			 * Determines if the trailheads should be updated. If they
			 * should be updated, retreives and displays them.
			 *
			 * @return {[type]} [description]
			 */
			function updateTrailheads() {
				var deferred = $q.defer();
				var bounds = $scope.explore.map.service.bounds();
				var grownBounds = $scope.service.trailheads.bounds;
				var crossGrownBoundsBoundary;
				if (grownBounds) {
					crossGrownBoundsBoundary = !bounds.containsBounds(grownBounds);
				}

				if (_.isUndefined(grownBounds) || (!_.isUndefined(bounds) && (crossGrownBoundsBoundary))) {
					console.log('retrieving more trailheads trailheads');

					//Map has changed enough, get more trailheads.
					grownBounds = bounds.grow(0.5);
					getTrailheads(grownBounds).then(function(trailheads) {
						$scope.service.trailheads.trailheadsWebApiService = trailheadsWebApiService;
						$scope.service.trailheads.bounds = grownBounds;
						var displayedTrailheads = displayTrailheads(trailheads);
						updateCardTray();
						deferred.resolve(displayedTrailheads);
					}, function(error) {
						console.log('Error retreiving trailheads', error);
						deferred.reject(error);
					});
				} else {
					var displayedTrailheads = displayTrailheads($scope.service.trailheads.trailheadsWebApiService.entities);
					deferred.resolve(displayedTrailheads);
					updateCardTray();
				}
				return deferred.promise;
			}

			/**
			 * Retreives trailheads from the server
			 *
			 * @param {tn.measure.BoundingBox} bounds the bounds to get trailheads in.
			 * @return {promise} [description]
			 */
			function getTrailheads(bounds) {
				var deferred = $q.defer();

				if (trailheadsWebApiService) {
					trailheadsWebApiService.cancel();
				}
				trailheadsWebApiService = new TrailheadsWebApiService()
					.setLimit(MAX_TRAILHEADS)
					.filteredByRouteFilter(routeFilterService)
					.limitByBounds(bounds);

				if ($scope.tn.allRoutes) {
					trailheadsWebApiService.allRoutes();
				}

				usSpinnerService.spin(routeCardsSpinnerKey);
				trailheadsWebApiService.nextPage().then(function(ths) {
					deferred.resolve(ths);
					//Let getRoutes do this. TODO: make it possible for getRoutes and getTrailheads to communicate
					//so we can stop the spinner correctly.
					usSpinnerService.stop(routeCardsSpinnerKey);
				}, function(error) {
					deferred.reject(error);
					usSpinnerService.stop(routeCardsSpinnerKey);
				});
				return deferred.promise;
			}

			/**
			 * display the trailheads on the map.
			 *
			 * @param {trailheadResponse object} [description]
			 * @return {Array[tn.trailhead.Trailhead]} the trailheads displayed.
			 */
			function displayTrailheads(trailheads) {
				$scope.clearPrevalentTrailheads();
				if (trailheads && trailheads.length > 0) {
					var trailheadsWithinBounds = trailheadUtil.filterToBounds(trailheads, $scope.explore.map.service.bounds());
					trailheadsWithinBounds = _.sortBy(trailheadsWithinBounds, 'score').reverse();
					trailheadsWithinBounds = _.first(trailheadsWithinBounds, MAX_TRAILHEADS_IN_VIEW);
					trailheadsWithinBounds = _.reject(trailheadsWithinBounds, function(th) {
						return th.score <= 0;
					});
					$scope.addPrevalentTrailheads(trailheadsWithinBounds);
				}
				return $scope.getPrevalentTrailheads();
			}

			function trailheadsInView() {
				return trailheadUtil.filterToBounds($scope.getPrevalentTrailheads(), $scope.explore.map.service.bounds());
			}

			/**
			 * Determines if the routes should be updated. If they
			 * should be updated, retreives and displays them.
			 *
			 * @return {[type]} [description]
			 */
			function updateRoutes() {
				var previousTrailheadsInView = $scope.service.trailheads.trailheadsInBounds;
				var currentTrailheadsInView = trailheadsInView();
				var previousThIds = _.pluck(previousTrailheadsInView, 'id');
				var currentThIds = _.pluck(currentTrailheadsInView, 'id');
				var different = !commonUtil.arraysEqual(previousThIds, currentThIds);

				console.log('zoom level', currentZoomLevel());
				// if ($scope.prominentRoute.zoomLevelToDisplay > currentZoomLevel()) {
				// 	console.log('zoom level too low, not showing routes');
				// 	$scope.clearRoutes();
				// 	$scope.clearProminentTrailheads();
				// 	if (currentTrailheadsInView.isEmpty()) {
				// 		$scope.service.trailheads.trailheadsInBounds.clear();
				// 	} else {
				// 		$scope.service.trailheads.trailheadsInBounds.clear();
				// 	}
				// } else {
				if (previousTrailheadsInView.isEmpty() || different) {
					console.log('Trailheads in view changed, retreiving routes');
					getRoutes();

					$scope.service.trailheads.trailheadsInBounds = currentTrailheadsInView;
				} else {
					console.log('Trailheads in view did not change, not retreiving routes');
				}
				//}
				updateCardTray();
			}

			/**
			 * Find routes out of the current view (or a larger virtual view) and remove them,
			 * clearing up any memory in the process.
			 *
			 * @return {[type]} [description]
			 */
			function clearOutOfBoundsRoutes() {
				var grownBounds = $scope.explore.map.service.bounds().grow(2);
				if (grownBounds) {
					var routesInBounds = routeUtil.filterIntersectingBounds($scope.service.routes.routes, grownBounds);
					$scope.service.routes.routes = routesInBounds;
				}
			}


			/**
			 * display the routes on the map and cardtray.
			 *
			 * @return {[type]} [description]
			 */
			function displayRoutes() {
				$log.info('Displaying Routes');
				//Checking to see if the state is active incase we transitioned to a new state. We don't want to add the routes in that case
				if (!$state.includes('explore.routes')) {
					return;
				}

				var routes = $scope.service.routes.routes;
				$scope.clearRoutes();
				$scope.clearProminentTrailheads();
				$scope.addRoutes(routes);

				_.each(routes, function(route) {
					$scope.addProminentTrailheads(route.startTrailhead);
				});

				return $scope.getRoutes();
			}

			function updateCardTray() {
				$scope.explore.cardTray.visible = true;
				$log.info('Updating Card Tray');
				var bounds = $scope.explore.map.service.bounds();
				var cardTrayRoutes = [];
				_.each($scope.getRoutes(), function(route) {
					if (trailheadUtil.isWithinBounds(route.startTrailhead, bounds)) {
						cardTrayRoutes.push(route);
					}
				});
				//cardTrayRoutes = _.sortBy(cardTrayRoutes, 'score').reverse();
				$scope.service.routes.cardTray.clear();
				$scope.service.routes.cardTray.add(cardTrayRoutes);

				$scope.cardTray.clear();

				if (cardTrayRoutes.length === 0 && (!$scope.routesWebApiService || ($scope.routesWebApiService && !$scope.routesWebApiService.busy))) {
					$scope.cardTray.displayNoRoutesMessage();
				}

				if (cardTrayRoutes.length === 0 && $scope.service.trailheads.trailheadsWebApiService && $scope.service.trailheads.trailheadsWebApiService.entities.length === 0) {
					$scope.cardTray.displayOutOfBoundsMessage();
				}
				// else if ($scope.prominentRoute.zoomLevelToDisplay > currentZoomLevel()) {
				// 	$scope.explore.cardTray.visible = false;
				// }
			}

			/**
				 *
				 Retreives routes from the server and updates the display.
				 *
				 *  @param {Array[tn.trailhead.Trailhead]} trailheads the trailheads to use as a basis for retreiving routes.
				 *  @return {promise for Array[tn.route.Routes]} [description]
				 */
			function getRoutes() {
				if ($scope.routesWebApiService) {
					$scope.routesWebApiService.cancel();
				}
				$scope.routesWebApiService = new RoutesWebApiService()
					.filteredByRouteFilter(routeFilterService)
					.limitByBounds($scope.explore.map.service.bounds())
					.setLimit(ROUTES_LIMIT);

				$scope.service.routes.routesWebApiService = $scope.routesWebApiService;

				if ($scope.tn.allRoutes) {
					$scope.routesWebApiService.allRoutes();
				}
				clearOutOfBoundsRoutes();
				$scope.nextPage();
			}

			/**
			 * request more routes and display them.
			 *
			 * @return {[type]} [description]
			 */
			$scope.nextPage = function() {
				usSpinnerService.spin(routeCardsSpinnerKey);

				if(_.isUndefined($scope.routesWebApiService) && !_.isUndefined($scope.service.routes.routesWebApiService)) {
					$scope.routesWebApiService = $scope.service.routes.routesWebApiService;
				} else if(_.isUndefined($scope.routesWebApiService)) {
					getRoutes();
				}

				$scope.routesWebApiService.nextPage().then(function(routes) {
					$scope.service.routes.routes.add(routes);
					displayRoutes();
					updateCardTray();
					usSpinnerService.stop(routeCardsSpinnerKey);
				}, function() {
					usSpinnerService.stop(routeCardsSpinnerKey);
					$scope.cardTray.displayErrorMessage();
				});
			};

			/**
			 * Look up the current zoom level
			 *
			 * @return {Object} Zoom level
			 */
			function currentZoomLevel() {
				return zoomLevelMap[$scope.explore.map.service.zoom()];
			}

			$scope.goToDefaultLocation = function() {
				$scope.explore.map.service.center(exploreLocationService.defaultCenter());
				$scope.explore.map.service.zoom(exploreLocationService.defaultZoom());
			};

			restoreView();
		});
});