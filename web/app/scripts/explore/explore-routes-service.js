define(['explore/explore-module', 'common/entities'], function(exploreModule, Entities) {
	'use strict';

	exploreModule.service('exploreRoutesService', function() {
		var service = {
			routes: {
				routes: undefined,
				filter: undefined,
				cardTray: undefined,
			},
			trailheads: {
				//The 'grown' bounds used to get all nearby trailheads.
				bounds: undefined,
				//trailheads currently in view (in current bounds)
				trailheadsInBounds: undefined,
				//the response from the trailheads xhr request.
				trailheadResponse: undefined,
			},
			map: {
				//current search parameters
				search: undefined,
				//The map zoom level (google)
				zoom: undefined,
				//The TN zoom level, which is persisted to determine if the zoom level has changed.
				zoomLevel: undefined,
				bounds: undefined,
			}
		};

		service.clearAll = function() {
			service.trailheads = {};
			service.routes = {};

			service.trailheads.trailheadsInBounds = new Entities();
			service.routes.routes = new Entities();
			service.routes.cardTray = new Entities();
			// service.trailheads.clear();
			// service.routes.clear();
		};

		// service.trailheads.clear = function() {
		// 	service.trailheads.bounds = undefined;
		// 	service.trailheads.trailheadsInBounds.clear();
		// 	service.trailheads.trailheadResponse = undefined;
		// };

		// service.routes.clear = function() {
		// 	service.routes.routes.clear();
		// 	service.routes.filter = undefined;
		// 	service.routes.cardTray.clear();
		// };

		service.clearAll();

		return service;
	});
});