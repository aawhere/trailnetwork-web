define(
	[
		'angular',
		'map/map-module',
		'map/map-directive',
		'trailhead/trailheads-directive',
		'route/route-module',
		'route/routes-directive',
		'route/route-filter-directive',
		'common/common-module',
		'user/signin-button-directive',
	], function(angular) {
		'use strict';
		var exploreModule;

		exploreModule = angular.module('tn.explore', ['tn.map', 'tn.trailhead', 'tn.route', 'tn.common']);

		exploreModule.value('routeCardsSpinnerKey', 'route-cards-spinner');

		return exploreModule;

	});