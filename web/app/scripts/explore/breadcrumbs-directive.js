define(['explore/explore-module',
	//
	'common/app-route-service'
], function(exploreModule) {
	'use strict';

	exploreModule.directive('tnBreadcrumbs', function(exploreRoutesService, appRouteService) {

		return {
			restrict: 'EA',
			transclude: false,
			templateUrl: '/views/explore/breadcrumbs-directive.html',
			replace: false,
			scope: {
				route: '=',
				trailhead: '=',
			},
			link: function(scope) {

				scope.crumbs = [];

				scope.$watch('route', function() {
					createCrumbs();
				});

				scope.$watch('trailhead', function() {
					createCrumbs();
				});

				function createCrumbs() {
					scope.crumbs = [];
					scope.crumbs.push({
						name: 'TrailNetwork',
						href: appRouteService.explore.routesUrl(exploreRoutesService.map.search),
					});
					if (scope.trailhead) {
						scope.crumbs.push({
							name: scope.trailhead.name.$,
							href: appRouteService.explore.trailheadUrl(scope.trailhead.id),
							icon: 'icon-trailhead-large small',
						});
					}
					if (scope.route) {
						scope.crumbs.push({
							name: scope.route.name.$,
							href: appRouteService.explore.routeUrl(scope.route.mainId.value),
							icon: 'icon-route orange small',
						});
					}
				}

			}
		};

	});
});