define(['explore/explore-module',
	'route/route-util',
	//
	'common/browser-service',
	'common/notification-service',
	'common/location-service',
	'explore/breadcrumbs-directive',
	'route/route-web-api-service',
], function(exploreModule) {
	'use strict';

	return exploreModule
		.controller('tnExploreRouteLeafletsController', function($rootScope,
			$scope,
			browserService,
			$translate,
			notificationService,
			locationService,
			$log,
			$location,
			exploreRouteService,
			matchmedia) {

			$log.info('loading tnExploreRouteLeafletsController');

			$scope.exploreRoute = {
				routeFilter: {
					expanded: false,
				},
				routeDetail: {},
			};

			$scope.showFilter = function() {
				$scope.filterVisible = true;
				$scope.hideDetails();
			};

			$scope.showDetails = function() {
				$scope.detailsVisible = true;
				$scope.hideFilter();
			};

			$scope.hideFilter = function() {
				$scope.filterVisible = false;
			};

			$scope.hideDetails = function() {
				$scope.detailsVisible = false;
			};

			$scope.hide = function() {
				$scope.hideFilter();
				$scope.hideDetails();
			};

			$scope.toggleFilter = function() {
				if ($scope.filterVisible) {
					$scope.hideFilter();
				} else {
					$scope.showFilter();
				}
			};

			$scope.back = function() {
				locationService.goExplore();
			};

			$scope.toggleDetails = function() {
				if ($scope.detailsVisible) {
					$scope.hideDetails();
				} else {
					$scope.showDetails();
				}
			};

			$scope.toggleMinimizeDetails = function() {
				$scope.exploreRoute.routeDetail.minimized = !$scope.exploreRoute.routeDetail.minimized;
			};

			/**
			 * Translate and format the route name. Typically this involves adding "Route" to the end of the route name.
			 *
			 * Returns a promise for the name.
			 */
			function routeTitle(route) {
				$translate('ROUTE_TITLE', {
					routeName: route.name.$
				}).then(function(name) {
					browserService.title(name);
				});
			}

			function updateRoute() {
				var route = $scope.exploreRoute.route;
				var mainRoute = $scope.exploreRoute.mainRoute;
				routeTitle(route);

				if (mainRoute.id !== route.id) {
					$scope.exploreRoute.breadcrumbRoute = mainRoute;
				}
			}

			function initialize(route, mainRoute) {
				$scope.exploreRoute.route = route;
				$scope.exploreRoute.mainRoute = mainRoute;
				$scope.exploreRoute.mainId = mainRoute.id;
				//for chart controller
				$scope.exploreRoute.routeId = route.id;
				updateRoute();
			}

			$scope.$watch(function() {
				return exploreRouteService.route;
			}, function(route) {
				if (route) {
					initialize(route, exploreRouteService.mainRoute);
				}
			});

			$scope.detailsVisible = !(matchmedia.isPhone());
		});
});