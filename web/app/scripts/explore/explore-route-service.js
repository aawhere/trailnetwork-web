define(['route/route-module',
	//
], function(routeModule) {
	'use strict';

	routeModule.service('exploreRouteService', function() {

		var ExploreRouteService = function() {

			this.reset = function() {
				this.route = undefined;
				this.mainRoute = undefined;
				this.alternateService = undefined;
			};
		};

		return new ExploreRouteService();
	});
});