define(function() {
	'use strict';

	/**
	 * Events for the explore page.
	 */
	return {
		routeFilterUpdated: 'route-filter-updated',
		// Sent with the routeId
		routeSelected: 'route-selected',
		// Sent with the routeId
		routeUnSelected: 'route-unselected',
		routeUnSelectAll: 'route-unselect-all',
		// Sent with the routeId
		routeHighlighted: 'route-highlighted',
		// Sent with the routeId
		routeUnHighlighted: 'route-unhighlighted',
		// Sent with the routeId
		markerHighlighted: 'marker-highlighted',
		// Sent with the routeId
		markerUnHighlighted: 'marker-unhighlighted',
		markerSelected: 'marker-selected',
		markerUnSelected: 'marker-unselected',
		// Sent with the trailheadId
		trailheadSelected: 'trailhead-selected',
		// Sent with the trailheadId
		trailheadUnSelected: 'trailhead-unselected',
		trailheadUnSelectAll: 'trailhead-unselect-all',
		// Sent with the trailheadId
		trailheadHighlighted: 'trailhead-highlighted',
		// Sent with the routeId
		trailheadUnHighlighted: 'trailhead-unhighlighted',
		chartHighlighted: 'chart-highlighted',
		chartUnHighlighted: 'chart-unhighlighted',
		chartInitialized: 'chart-initialized',
		chartStopAnimation: 'chart-stop-animation',
		chartSelect: 'chart-select',
		clearChart: 'chart-clear',
	};

});