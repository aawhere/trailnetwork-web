define(['explore/explore-module',
	'app-route-constants',
	'explore/explore-events',
	'map/map-util',
	//
	'common/browser-service',
	'common/notification-service',
	'common/app-route-service',
	'common/location-service',
	'explore/breadcrumbs-directive',
	'explore/explore-location-service',
	'map/geo-service',
	'route/route-filter-service',
	'trailhead/trailhead-detail-directive',
	'trailhead/trailheads-web-api-service',
], function(exploreModule, appRouteConstants, exploreEvents, mapUtil) {
	'use strict';

	return exploreModule
		.controller('tnExploreTrailheadLeafletsController',
			function($scope,
				$stateParams,
				exploreLocationService,
				browserService,
				$state,
				geoService,
				notificationService,
				$translate,
				appRouteService,
				routeFilterService,
				TrailheadsWebApiService,
				locationService,
				matchmedia) {

				$scope.trailheadId = $stateParams.trailheadId;
				$scope.routeFilter = {
					expanded: false,
				};

				$scope.showFilter = function() {
					$scope.filterVisible = true;
					$scope.hideDetails();
				};

				$scope.showDetails = function() {
					$scope.detailsVisible = true;
					$scope.hideFilter();
				};

				$scope.hideFilter = function() {
					$scope.filterVisible = false;
				};

				$scope.hideDetails = function() {
					$scope.detailsVisible = false;
				};

				$scope.hide = function() {
					$scope.hideFilter();
					$scope.hideDetails();
				};

				$scope.toggleFilter = function() {
					if ($scope.filterVisible) {
						$scope.hideFilter();
					} else {
						$scope.showFilter();
					}
				};

				$scope.back = function() {
					locationService.goExplore();
				};

				$scope.toggleDetails = function() {
					if ($scope.detailsVisible) {
						$scope.hideDetails();
					} else {
						$scope.showDetails();
					}
				};

				function updateTrailhead(th) {
					$scope.trailhead = th;
					browserService.title(th.name.$ + ' Trailhead');
				}

				$scope.$on(exploreEvents.routeFilterUpdated, function() {
					getTrailhead($scope.trailheadId);
				});

				/**
				 * get the trailhead and routes
				 *
				 * @param  {trailhead} th The trailhead of interest
				 */
				function getTrailhead(thId, noFilter) {
					var thService = new TrailheadsWebApiService()
						.withRouteCompletionStats()
						.withRelativeStarts()
						.withTrailheadId(thId);
					if (!noFilter) {
						thService.filteredByRouteFilter(routeFilterService);
					}
					thService.get().then(function(ths) {
						var th = ths[0];
						if (th) {
							updateTrailhead(th);
						} else {
							if (!noFilter) {
								//Don't use the filter this time.
								getTrailhead(thId, true);
							}
						}
					}, function(error) {
						console.log('ERROR: Trailhead ' + thId + ' was not found. ', error);
						handleMissingTrailhead(thId);
					});
				}

				function handleMissingTrailhead(thId) {
					//Try to get the bounds and display empty area. If no bounds, not found page.
					geoService.boundsForCell(thId).then(function(boundingBox) {
						console.log('displaying location where missing trailhead was', boundingBox);
						appRouteService.explore.goToRoutes(mapUtil.centerLocationFromBoundingBox(boundingBox), exploreLocationService.defaultZoom());
						$translate('TH_NOT_FOUND').then(function(msg) {
							notificationService.notification(msg);
						});
					}, function(error) {
						console.log('ERROR: Bounds for ' + thId + ' was not found. ', error);
						$state.go(appRouteConstants.states.exploreRoutes);
						$translate('TH_NOT_FOUND').then(function(msg) {
							notificationService.notification(msg);
						});
					});
				}

				getTrailhead($scope.trailheadId);
				$scope.detailsVisible = !(matchmedia.isPhone());
			}
		);
});