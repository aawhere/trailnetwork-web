define(['explore/explore-module',
	'explore/explore-events',
	'route/route-util',
	'common/common-util',
	'user/user-events',
	//
	'common/browser-service',
	'common/notification-service',
	'common/modal-directive',
	'explore/breadcrumbs-directive',
	'explore/explore-location-service',
	'route/route-web-api-service',
	'route/routes-web-api-service',
	'route/route-filter-service',
	'explore/explore-route-service',
	'common/app-route-service',
], function(exploreModule, exploreEvents, routeUtil, commonUtil, userEvents) {
	'use strict';

	return exploreModule
		.controller('tnExploreRouteCardsController', function($rootScope,
			$scope,
			$state,
			browserService,
			exploreLocationService,
			$translate,
			notificationService,
			RouteWebApiService,
			$log,
			RoutesWebApiService,
			routeFilterService,
			$timeout,
			exploreRouteService,
			$location,
			appRouteService,
			routeId) {

			$log.info('loading tnExploreRouteCardsController');

			$scope.exploreRoute = {
				initialized: false,
				cardTray: {
					banner: {
						onClose: function() {
							exploreLocationService.restoreExploreRoutes();
						},
						text: "",
					},
					showNoRoutesMessage: false,
				},
				routeId: routeId,
				service: exploreRouteService,
			};
			$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
				$log.info('$stateChangeStart');
				if (fromState.name === $state.current.name) {
					$rootScope.$broadcast(exploreEvents.routeUnSelectAll);
					$scope.clearTrailheads();
					$scope.clearRoutes();
					$scope.clearLocations();
					$scope.clearInvisibleLocations();
				}
			});

			$scope.$on('$locationChangeStart', function() {
				$log.info('$locationChangeStart');
				var id = routeIdFromUrl();
				if (id && id !== routeId) {
					routeId = routeIdFromUrl();
					$scope.exploreRoute.routeId = routeId;
					initialize();
				}
			});


			$scope.$on(exploreEvents.trailheadSelected, function(event, trailheadId) {
				exploreLocationService.goToTrailhead(trailheadId);
			});

			$scope.$on(exploreEvents.routeSelected, function(event, routeId) {
				if ($scope.exploreRoute.routeId !== routeId) {
					var routeUrl = appRouteService.explore.routeUrl(routeId);
					$location.url(routeUrl);
				}
			});

			$scope.$on(exploreEvents.routeHighlighted, function(event, routeId) {
				$log.debug(exploreEvents.routeHighlighted, routeId);
				if (routeId !== $scope.exploreRoute.routeId) {
					$rootScope.$broadcast(exploreEvents.routeUnSelected, $scope.exploreRoute.routeId);
					$rootScope.$broadcast(exploreEvents.chartStopAnimation);
				}
			});

			$scope.$on(exploreEvents.routeUnHighlighted, function(event, routeId) {
				$log.debug(exploreEvents.routeUnHighlighted, routeId);
				$rootScope.$broadcast(exploreEvents.routeSelected, $scope.exploreRoute.routeId);
			});

			$scope.$on(exploreEvents.routeFilterUpdated, function() {
				console.log('onRouteFitlerUpdated');
				exploreRouteService.reset();
				initialize();
			});

			$scope.$on(userEvents.unitOfMeasurePreferenceChanged, function() {
				exploreRouteService.reset();
				initialize();
			});

			//TODO: this should not be necessary, but since we don't want a state change,
			// which will flash the screen, we cannot simply change the url to trigger an
			// updated $stateParams.
			function routeIdFromUrl() {
				var url = $location.path();
				var routeIdString = /\/route\/([0-9]*)/.exec(url)[1];
				return routeUtil.routeIdAsNumber(routeIdString);
			}


			/**
			 * Translate and format the route name. Typically this involves adding "Route" to the end of the route name.
			 *
			 * Returns a promise for the name.
			 */
			function routeTitle(route) {
				$translate('ROUTE_TITLE', {
					routeName: route.name.$
				}).then(function(name) {
					browserService.title(name);
				});
			}

			/**
			 * Translate and format the Variations name. Typically this involves adding "Variations of" to the beginning
			 * of the main name and adding "Route" to the end.
			 *
			 * Returns a promise for the name.
			 */
			function variationsTitle(route) {
				$translate('ROUTE_VARIATIONS_MAIN_TITLE', {
					mainName: route.name.$
				}).then(function(name) {
					$scope.exploreRoute.cardTray.banner.text = name;
				});
			}

			function updateRoute(route) {
				exploreRouteService.route = route;
				routeTitle(route);
				addRoutes(route);
				$rootScope.$broadcast(exploreEvents.routeSelected, route.id);
			}

			function updateMainRoute(route) {
				exploreRouteService.mainRoute = route;
				variationsTitle(route);
			}

			function findMainRoute(routes) {
				return routes[0];
			}

			/**
			 * Intitializes the routes. This is called for the
			 * first page of results only.
			 *
			 * @param  {[type]} routes [description]
			 * @return {[type]}        [description]
			 */
			function showRoutes() {
				var routes = exploreRouteService.alternateService.entities;
				$scope.clearTrailheads();
				$scope.clearRoutes();
				$scope.exploreRoute.cardTray.showNoRoutesMessage = false;

				var firstPage = exploreRouteService.alternateService.isFirstPage();
				if (firstPage && !hasAlternates(routes)) {
					$log.info('no alternatives found');

					$scope.exploreRoute.cardTray.visible = false;
				}
				if (routes.isEmpty()) {
					//No routes means we didn't event receive the requested route, same as a 404 response.
					$scope.exploreRoute.cardTray.messageKey = "empty";
					retreiveRoute(routeId).then(function(route) {
						$scope.explore.initialize(null, null, route.boundingBox);
						$scope.explore.map.service.onInitialized().then(function() {
							updateRoutes(route, route);
						});
					});
				} else {
					$scope.exploreRoute.cardTray.messageKey = undefined;
					var bounds = exploreRouteService.alternateService && exploreRouteService.alternateService.bounds;
					if (bounds) {
						$scope.explore.initialize(null, null, bounds);

						$scope.explore.map.service.onInitialized().then(function() {
							addRoutes(routes);

							var route = routes.find(routeId);
							if (!route) {
								//perhaps a filter excluded it. Obtain route directly.
								retreiveRoute(routeId).then(function(r) {
									updateRoutes(r, r);
								});
							} else {
								$timeout(function() {
									var route = routes.find(routeId);
									var mainRoute = findMainRoute(routes);
									updateRoutes(route, mainRoute);
								});
							}
						});
					}
				}
			}

			function retreiveRoute(routeId) {
				return new RouteWebApiService(routeId).get();
			}

			function updateRoutes(route, main) {
				updateMainRoute(main);
				updateRoute(route);

				$scope.exploreRoute.initialized = true;
			}

			function addRoutes(routes) {
				routes = commonUtil.toArray(routes);
				$scope.addRoutes(routes);

				_.each(routes, function(route) {
					$scope.addProminentTrailheads(route.startTrailhead);
				});

				//We are guaranteed one route, the requested route. If we don't have alternates > 1 routes, then don't show card tray.
				if ($scope.getRoutes().length < 2) {
					//$scope.exploreRoute.cardTray.minimized = true;
				} else {
					$scope.exploreRoute.cardTray.minimized = false;
				}
			}

			function hasAlternates(routes) {
				var result = false;
				if (routes.length > 1) {
					result = true;
				}
				_.each(routes, function(route) {
					if (route.id !== $scope.exploreRoute.routeId) {
						result = true;
					}
				});
				return result;
			}

			function handleError() {
				//Route was not found, redirect to home
				exploreLocationService.restoreExploreRoutes();
				notificationService.networkingError();
			}

			$scope.nextPage = function() {
				return exploreRouteService.alternateService.nextPage().then(function() {
					showRoutes();
				}, handleError);
			};

			function initialize() {
				$scope.route.highlight = true;
				exploreRouteService.route = undefined;
				exploreRouteService.mainRoute = undefined;

				if (!exploreRouteService.alternateService || !exploreRouteService.alternateService.busy) {
					if (exploreRouteService.alternateService && exploreRouteService.alternateService.entities.has(routeId)) {
						showRoutes();
					} else {
						exploreRouteService.alternateService = new RoutesWebApiService()
							.filteredByRouteFilter(routeFilterService)
							.limitToAlternatesOfMain(routeId);
						if (routeFilterService.hasConditions()) {
							exploreRouteService.alternateService.withRelativeCompletionStats();
						}
						$scope.nextPage();
					}
				}
			}

			initialize();
		});
});