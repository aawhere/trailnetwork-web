define([], function() {
	'use strict';
	var MapOverlayBaseDirectiveLink;

	/**
	 * Base overlay directive that provides common methods and data structures for overlays.
	 * This just provides the link function.
	 *
	 * @param {angular.$timout} $timeout
	 *
	 * @param {tn.map.overlayUtil} overlayUtil
	 *
	 * @param {object}
	 *            scope
	 *
	 * @param {Object} mapCtrl the controler of the angualr-google-maps map directive.
	 *
	 */
	MapOverlayBaseDirectiveLink = function($timeout, overlayUtil, scope, mapCtrl) {

		this.$timeout = $timeout;
		this.overlayUtil = overlayUtil;
		this.scope = scope;
		this.mapCtrl = mapCtrl;
		this.scope.markers = {};
		this.$timeout = $timeout;

		this.$timeout(function() {
			scope.map = mapCtrl.getMap();
			this.setupWatches();
		});
	};

	MapOverlayBaseDirectiveLink.prototype = {

		setupWatches: function() {
			this.scope.$watch('models', function() {
				this.clearOverlays();
				_.each(this.scope.models, function(model) {
					this.createOverlayFromModel(model);
				});
			}, true);
		},

		/**
		 * Intended to be overridden. Creates a new overlay from the model
		 * and adds it to the scope.models array.
		 *
		 * @param  {Object} model
		 * @return {google.maps overlay} the newly created overlay
		 */
		createOverlayFromModel: function(model) {
			throw "Not Implemented Yet!" + model;
		},

		/**
		 *	Removes an overlay from the map.
		 *
		 * @param  {modelId} modelId the id of the model to remove
		 */
		removeOverlayFromMap: function(modelId) {
			if (this.scope.overlays && this.scope.overlays.hasOwnProperty(modelId)) {
				var overlay = this.scope.overlays[modelId];
				this.overlayUtil.destroyOverlay(overlay);
				delete this.scope.overlays[modelId];
			}
		},

		/**
		 * Remove all overlays from the map.
		 *
		 */
		clearTrailheads: function() {
			_.each(_.keys(this.scope.overlays), function(modelId) {
				this.removeOverlaysFromMap(modelId);
			});
		}
	};

	return MapOverlayBaseDirectiveLink;
});