/*global google:false */
define([], function() {
	'use strict';

	var util;
	var MAP_TYPE_IDS = ['hybrid', 'roadmap', 'satellite', 'terrain'];

	util = {};

	/**
	 * Triggers a resize event on the map. Helpful if the size of the containing
	 * box changes.
	 *
	 * @param {google.maps.Map}
	 *            map
	 */
	util.refresh = function(map) {
		google.maps.event.trigger(map, 'resize');
	};

	util.isBounds = function(boundingBox) {
		return (!_.isUndefined(boundingBox) &&
			!_.isUndefined(boundingBox.south) &&
			!_.isUndefined(boundingBox.west) &&
			!_.isUndefined(boundingBox.north) &&
			!_.isUndefined(boundingBox.east));
	};

	util.isLocation = function(location) {
		return (!_.isUndefined(location) &&
			!_.isUndefined(location.latitude) &&
			!_.isUndefined(location.latitude.value) &&
			!_.isUndefined(location.longitude) &&
			!_.isUndefined(location.longitude.value));
	};

	/**
	 * Test if the input is a valid map type id.
	 *
	 * @param  {String}  mapTypeId [description]
	 * @return {Boolean}           [description]
	 */
	util.isValidMapTypeId = function(mapTypeId) {
		return _.contains(MAP_TYPE_IDS, mapTypeId);
	};

	return util;
});