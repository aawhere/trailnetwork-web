define(['angular',
	'map/map-module',
	'map/map-events',
	'map/bounding-box',
	//
	'map/map-service',
	'common/on-resize-directive'
], function(angular,
	mapModule,
	mapEvents,
	BoundingBox) {
	'use strict';

	/**!
	 * The MIT License
	 *
	 * Copyright (c) 2010-2013 Google, Inc. http://angularjs.org
	 *
	 * Permission is hereby granted, free of charge, to any person obtaining a copy
	 * of this software and associated documentation files (the "Software"), to deal
	 * in the Software without restriction, including without limitation the rights
	 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	 * copies of the Software, and to permit persons to whom the Software is
	 * furnished to do so, subject to the following conditions:
	 *
	 * The above copyright notice and this permission notice shall be included in
	 * all copies or substantial portions of the Software.
	 *
	 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	 * THE SOFTWARE.
	 *
	 * angular-google-maps
	 * https://github.com/nlaplante/angular-google-maps
	 *
	 * @authors
	 *  Nicolas Laplante - https://plus.google.com/108189012221374960701
	 *  Nicholas McCready - https://twitter.com/nmccready
	 *  Brian Chapman - trailnetwork.com
	 */

	mapModule.directive('tnMap', function($timeout, $log, $rootScope) {

		return {
			restrict: 'ECMA',
			transclude: true,
			replace: false,
			//priority: 100,
			template: '<div class="tn" tn-on-resize="onResize"><div class="map"></div><div ng-transclude style="display: none"></div></div>',
			scope: {
				map: '=',
			},
			controller: ['$scope',
				function($scope) {
					/**
					 * @return the map instance
					 */
					this.getGoogleMap = function() {
						return $scope.googleMap;
					};

					this.bounds = function() {
						return $scope.bounds;
					};

					$scope.onResize = function() {
						if ($scope.map) {
							// $log.info('resizing map');
							google.maps.event.trigger($scope.googleMap, "resize");
						}
					};
				}
			],
			link: function(scope, element) {
				var _m;
				var zoom;
				var el = angular.element(element);
				element.addClass("tn map");

				scope.$watch('map', function() {
					if (scope.map) {
						createMap();
					}
				});

				// Create the map
				function createMap() {
					google.maps.visualRefresh = true;
					_m = new google.maps.Map(el.find('div')[1], scope.map.options);
					google.maps.event.addListenerOnce(_m, 'idle', function() {
						scope.map.setGoogleMap(_m);
						google.maps.event.trigger(_m, "resize");
					});

					google.maps.event.addListener(_m, 'zoom_changed', function() {
						if (zoom !== _m.zoom) {

							$timeout(function() {
								scope.$apply(function() {
									zoom = _m.getZoom();
									$rootScope.$broadcast(mapEvents.mapZoomChanged, zoom);
								});
							});
						}
					});

					google.maps.event.addListener(_m, 'maptypeid_changed', function() {
						$timeout(function() {
							$rootScope.$broadcast(mapEvents.mapTypeChanged, _m.getMapTypeId());
						});
					});

					google.maps.event.addListener(_m, 'idle', function() {
						$timeout(function() {
							if (_m.getBounds()) {
								scope.$apply(function() {
									var boundingBox = BoundingBox.createFromGoogleLatLngBounds(_m.getBounds());
									$rootScope.$broadcast(mapEvents.mapBoundsChanged, boundingBox);
								});
							}
						});
					});

					google.maps.event.addListener(_m, 'click', function(event) {
						$timeout(function() {
							$rootScope.$broadcast(mapEvents.mapClick, event);
						}, 0);
					});

					// Put the map into the scope
					scope.googleMap = _m;

					$timeout(function() {
						scope.map.reset();
					}, 200);
				}
			}
		};
	});


});