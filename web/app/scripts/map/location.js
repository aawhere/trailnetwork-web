/*global google:false */
define([], function() {
	'use strict';

	var Location;
	var LOCATION_STRING_DELIMINATOR = ',';
	var LOCATION_STRING_LAT_INDEX = 0;
	var LOCATION_STRING_LON_INDEX = 1;
	var LAT_DECIMALS = 7;
	var LON_DECIMALS = LAT_DECIMALS;

	var createLocation = function(lat, lon) {
		return {
			latitude: {
				value: lat
			},
			longitude: {
				value: lon
			}
		};
	};

	var mergeLocation = function(location, context) {
		context.latitude = location.latitude;
		context.longitude = location.longitude;
		return context;
	};

	/**
	 * Create a tn.measure.GeoCorrdinate from the string representation of a corrdinate
	 * @param  {Stirng} string [description]
	 * @return {tn.measure.GeoCorrdinate} the location
	 */
	var locationFromString = function(string) {
		var parts = string.split(LOCATION_STRING_DELIMINATOR);
		return createLocation(parts[LOCATION_STRING_LAT_INDEX], parts[LOCATION_STRING_LON_INDEX]);
	};

	/**
	 * Create a location from latitude, longitude values, an tn.measure.geocoordinate,
	 * or a google LatLng.
	 *
	 * @param {Object} options format:
	 * {
	 *   location: tn.measure.geocoordinate (optional),
	 *   lat: Number (optional)
	 *   lng: Number (optional)
	 *   googleLatLng: google.maps.LatLng (optional),
	 *   string: String (optional)
	 * }
	 */
	Location = function(options) {
		if (options.location) {
			mergeLocation(options.location, this);
		} else if (options.lat && options.lon) {
			mergeLocation(createLocation(options.lat, options.lon), this);
		} else if (options.googleLatLng) {
			mergeLocation(createLocation(options.googleLatLng.lat(), options.googleLatLng.lng()), this);
		} else if (options.string) {
			mergeLocation(locationFromString(options.string), this);
		}

	};

	Location.createFromLocation = function(location) {
		return new Location({
			location: location
		});
	};

	Location.createFromGoogleLatLng = function(latLng) {
		return new Location({
			googleLatLng: latLng,
		});
	};

	Location.createFromString = function(string) {
		return new Location({
			string: string,
		});
	};

	Location.createFromLatLon = function(lat, lng) {
		return new Location({
			lat: lat,
			lon: lng,
		});
	};

	/**
	 * Create a google.maps.LatLng.
	 *
	 * @param {number}
	 *            lat
	 * @param {number}
	 *            lng
	 * @returns {google.maps.LatLng}
	 */
	Location.prototype.googleLatLng = function() {
		return new google.maps.LatLng(this.latitude.value, this.longitude.value);
	};

	/**
	 * Compares two Location
	 * @param  {Location} location2 [description]
	 * @return {Boolean}           [description]
	 */
	Location.prototype.equalsLocation = function(loc) {
		var loc1String = this.toString();
		var loc2String = loc.toString();
		return loc1String === loc2String;
	};

	Location.prototype.toString = function() {
		var lat = this.latitude.value.toFixed(LAT_DECIMALS);
		var lon = this.longitude.value.toFixed(LON_DECIMALS);
		return lat + LOCATION_STRING_DELIMINATOR + lon;
	};

	Location.prototype.deepCopy = function() {
		return new Location({
			string: this.toString(),
		});
	};

	Location.prototype.distanceFrom = function(loc) {
		var deltaLat = this.latitude.value - loc.latitude.value;
		var deltaLon = this.longitude.value - loc.longitude.value;
		return Math.sqrt(Math.pow(deltaLat, 2) + Math.pow(deltaLon, 2));
	};

	/**
	 * Convert from a GeoCoordinate to a location understood by
	 * angular-google-maps plugin
	 *
	 * @param  {tn.measure.GeoCoordinate} tnLocation
	 * @return {angular-google-maps Location}
	 */
	Location.prototype.angularMapsLocation = function() {
		return {
			latitude: this.latitude.value,
			longitude: this.longitude.value
		};
	};

	return Location;
});