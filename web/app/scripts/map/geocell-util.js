define(['geomodel', 'map/bounding-box'], function(geoModel, BoundingBox) {
	'use strict';

	var util;

	util = {};

	//TODO: these are copied from Geomodel since they are not public. Consider
	//working to get these exposed.
	util.GEOCELL_GRID_SIZE = 4;
	util.GEOCELL_ALPHABET = "0123456789abcdef";

	util.NUM_OF_CHILD_CELLS_IN_CELL = Math.pow(util.GEOCELL_GRID_SIZE, 2);

	/**
	 *	Given a BoundingBox this will convert it to a format
	 *	reconized by GeoModel.geoTypes.Box
	 *
	 * @param  {tn.measure.BoundingBox} boundingBox [description]
	 * @return {GeoModel.geoTypes.Box}   [description]
	 */
	util.geoModelBoundsFromBoundingBox = function(boundingBox) {
		var geomodel;
		geomodel = geoModel.createGeomodel();
		/*jshint -W106 */
		return geomodel.create_bounding_box(boundingBox.north,
			boundingBox.east,
			boundingBox.south,
			boundingBox.west);
	};

	/**
	 *	Given a GeoModel.geoTypes.Box this will convert it to a format
	 *	reconized by tn
	 *
	 * @param  {GeoModel.geoTypes.Box } boundingBox [description]
	 * @return {tn.measure.BoundingBox}   [description]
	 */
	util.boundingBoxFromGeoModelBounds = function(geomodelBox) {
		return BoundingBox.createFromCorrds(geomodelBox.getNorth(), geomodelBox.getEast(),
			geomodelBox.getSouth(), geomodelBox.getWest());
	};

	/**
	 * Determine the minimum set of geocells that contains the bounds
	 * Optionally limit the resolution to one resolution.
	 *
	 * @param  {tn.measure.BoundingBox} bounds
	 * @param  {String} limitToResolution resolution to limit to.
	 * @return {Array[String]} array of GeoCells
	 */
	util.geoCellsFromBounds = function(bounds, maxCells) {
		var geomodel, costFunction, geoModelBounds;
		/*jshint -W106 */
		geomodel = geoModel.createGeomodel();
		/*jshint +W106 */
		geoModelBounds = util.geoModelBoundsFromBoundingBox(bounds);

		//Limit to resolution.
		costFunction = function(numCells /*, resolution*/ ) {
			return numCells > maxCells ? Math.exp(10000) : 0;
		};

		/*jshint -W106 */
		return geomodel.best_bbox_search_cells(geoModelBounds, costFunction);
	};

	util.boundsFromCell = function(cell) {
		/*jshint -W106 */
		var geomodel = geoModel.createGeomodel();
		var geomodelBox = geomodel.compute_box(cell);
		/*jshint +W106 */
		return util.boundingBoxFromGeoModelBounds(geomodelBox);
	};

	/**
	 * Limits the cells to one resolution by adding cells for lower resolution
	 * cells and truncating cells for higher resolution cells.
	 *
	 * @param  {Array[String]} cells      Array of geocells
	 * @param  {Integer} resolution [description]
	 * @return {Array[String]}           Array of geocells
	 */
	util.toSingleResolution = function(cells, resolution) {
		var result = [];
		_.each(cells, function(cell) {
			if (cell.length === resolution) {
				result.push(cell);
			} else if (cell.length < resolution) {
				var exploded;
				exploded = util.explode(cell);
				if (exploded[0].length < resolution) {
					exploded = util.toSingleResolution(exploded, resolution);
				}
				result = _.union(result, exploded);
			} else if (cell.length > resolution) {
				result.push(cell.substring(0, resolution));
			}

		});
		result = _.uniq(result);
		return result;
	};

	util.explode = function(cell) {
		var result = [];
		_.each(util.GEOCELL_ALPHABET.split(''), function(c) {
			result.push(cell + c);
		});
		return result;
	};

	return util;
});