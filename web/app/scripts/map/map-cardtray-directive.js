define(['trailhead/trailhead-module', 'jquery'], function(thModule, $) {
	'use strict';

	/**
	 * Provides a banner to display across the top of the map.
	 *
	 * @param {String}
	 *            text: The text to display in the banner
	 *
	 */
	thModule.directive('tnMapCardTray', function() {
			return {
				restrict: 'EA',
				transclude: true,
				templateUrl: '/views/map/map-cardtray-directive.html',
				replace: false,
				scope: {
					visible: '=',
					minimized: '='
				},
				link: function(scope, el) {
					var resize;

					scope.visible = scope.visible || true;
					scope.minimized = scope.minimized || false;

					resize = function() {
						if (scope.minimized) {
							el.addClass('minimized');
						} else {
							el.removeClass('minimized');
						}
					};

					$(window).resize(function() {
						//resize();
					});

					scope.$watch('minimized', function() {
						resize();
					});
				}
			};
		}
	);
});