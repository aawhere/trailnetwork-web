define(['angular',
		'map/map-module',
		'restangular'
	],
	function(angular, mapModule) {
		'use strict';

		mapModule.factory('geoService', function(Restangular) {
				var service = {};
				var geoPath = 'geo';
				var boundsPath = 'bounds';

				/**
				 * Get the bounds for the provided geocell.
				 *
				 * @param  {String} geocell [description]
				 * @return {tn.measure.BoundingBox}  the bounds for the geocell.
				 */
				service.boundsForCell = function(geocell) {
					return Restangular.one(geoPath).one(boundsPath, geocell).get();
				};

				return service;
			}
		);
	});