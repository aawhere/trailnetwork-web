define(['map/map-module',
	'underscore',
	'map/map-util',
	'map/location',
	'map/bounding-box'
], function(mapModule,
	_,
	mapUtil,
	Location,
	BoundingBox) {
	'use strict';

	var DEFAULTS = {
		center: undefined,
		zoom: 0,
		mapTypeId: google.maps.MapTypeId.TERRAIN,
		panControl: false,
		zoomControlOptions: {
			position: google.maps.ControlPosition.RIGHT_TOP,
			style: google.maps.ZoomControlStyle.DEFAULT
		},
		streetViewControlOptions: {
			position: google.maps.ControlPosition.RIGHT_TOP
		}
	};

	mapModule.factory('Map', function($q) {

		/**
		 * Controls for a Google map instance. Map must first be initialized by passing a valid Google Maps Options to the
		 * new constructor or by calling initailize(). initialize() can only be called once.
		 *
		 * @param {[type]} options [description]
		 */
		var Map = function Map(optionsOrMap) {
			var initialization = $q.defer();
			this.initialized = initialization.promise;

			this.setGoogleMap = function(googleMap) {
				this._googleMap = googleMap;
				initialization.resolve();
			};

			if (optionsOrMap && optionsOrMap.hasOwnProperty('mapTypes')) {
				this.setGoogleMap(optionsOrMap);
			} else {
				if (!this.options) {
					var mergedOptions = _.extend({}, DEFAULTS, optionsOrMap);
					this.options = mergedOptions;
				} else {
					throw ("Map Service is already initialized. Cannot re-initialize");
				}
				if (_.isUndefined(this.options.center)) {
					throw ("Map Service cannot be initialized, set center first");
				}
			}

		};

		Map.prototype.map = function() {
			if (this._googleMap) {
				return this._googleMap;
			}
		};

		Map.prototype.onInitialized = function() {
			return this.initialized;
		};

		/**
		 * Calls the google maps resize trigger to force Google Maps to redraw itself in the current container.
		 *
		 */
		Map.prototype.reset = function() {
			var m = this.map();
			if (m) {
				google.maps.event.trigger(m, "resize");
			}
		};

		/**
		 * Sets the mapType to the provided mapTypeId if provided.
		 * Otherwise returns the current map type.
		 *
		 * @param  {String} mapTypeId https://developers.google.com/maps/documentation/javascript/reference#MapTypeId
		 * @return {String}           [description]
		 */
		Map.prototype.mapTypeId = function(mapTypeId) {
			if (mapTypeId) {
				if (mapUtil.isValidMapTypeId(mapTypeId) && this.mapTypeId() !== mapTypeId) {
					this._googleMap.setMapTypeId(mapTypeId);
				}
			} else {
				return this._googleMap.getMapTypeId();
			}
		};

		Map.prototype.maxZoomForMapType = function(mapTypeId) {
			var mapTypes = this._googleMap.mapTypes;
			var mapType = mapTypes[mapTypeId];
			if (mapType) {
				return mapType.maxZoom;
			} else {
				return undefined;
			}
		};

		/**
		 * A setter/getter function that sets the map bounds to the supplied
		 * bounding box or returns the current bounding box if no arguments are supplied.
		 *
		 * @param  {tn.measure.BoundingBox} boundingBox [description]
		 * @return {tn.measure.BoundingBox}             [description]
		 */
		Map.prototype.bounds = function(boundingBox) {
			if (this._googleMap) {
				if (boundingBox) {
					var bounds = BoundingBox.createFromBoundingBox(boundingBox).googleLatLngBounds();
					//console.log('setting map bounds', bounds);
					this._googleMap.fitBounds(bounds);

				}
				if (!_.isEmpty(this._googleMap.getBounds())) {
					return BoundingBox.createFromGoogleLatLngBounds(this._googleMap.getBounds());
				} else {
					//Map doesn't have a bounds?
					return undefined;
				}
			}
		};

		Map.prototype.hasBounds = function() {
			return mapUtil.isBounds(this.bounds());
		};

		/**
		 * A setter/getter function that pans to the supplied bounding box or returns the
		 * current bounding box if no arguments are supplied.
		 *
		 * @param  {tn.measure.BoundingBox} boundingBox [description]
		 * @return {tn.measure.BoundingBox}             [description]
		 */
		Map.prototype.panTo = function(boundingBox) {
			if (this._googleMap) {
				if (boundingBox) {
					var bounds = BoundingBox.createFromBoundingBox(boundingBox).googleLatLngBounds();
					this._googleMap.panToBounds(bounds);
				}
				return BoundingBox.createFromGoogleLatLngBounds(this._googleMap.getBounds());
			}
		};

		/**
		 * A setter/getter function that sets the map center to the supplied
		 * location or returns the current location if no arguments are supplied.
		 *
		 * @param  {tn.measure.GeoCoordinate} location
		 * @return {tn.measure.GeoCoordinate}
		 */
		Map.prototype.center = function(location) {
			if (this._googleMap) {
				if (location) {
					var latLng = Location.createFromLocation(location).googleLatLng();
					//console.log('setting map center', location, latLng);
					this._googleMap.setCenter(latLng);
				}
				return Location.createFromGoogleLatLng(this._googleMap.getCenter());
			}
		};

		/**
		 * A setter/getter function that sets the map zoom to the supplied
		 * zoom or returns the current zoom if no arguments are supplied.
		 *
		 * @param  {Number} zoom
		 * @return {Number}
		 */
		Map.prototype.zoom = function(zoom) {
			if (this._googleMap) {
				if (zoom) {
					console.log('setting map zoom', zoom);
					this._googleMap.setZoom(zoom);
				}
				return this._googleMap.getZoom();
			}
		};

		return Map;
	});

});