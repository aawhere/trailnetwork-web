define(['map/map-module',
		'underscore',
		//
	],
	function(mapModule, _) {
		'use strict';

		/**
		 * Provides the markers directive, which displays a list of locations on a map.
		 *
		 * @param {object}
		 *            locations: Array[tn.measure.GeoCorrdinate].
		 *
		 */
		mapModule.directive('tnMapControl', function() {
			return {
				restrict: 'ECMA',
				require: '^tnMap',
				transclude: true,
				template: '<div ng-transclude class="map-control"></div>',
				replace: true,
				scope: {
					// Array[tn.measure.GeoLocation]
					location: '@',
					index: '&',
				},
				controller: ['$scope',
					function($scope) {
						this.getGoogleMap = function() {
							return $scope.map;
						};

						this.mapTypeId = function() {
							if ($scope.map) {
								return $scope.map.getMapTypeId();
							}
						};
					}
				],
				link: function(scope, el, attr, mapCtrl) {
					scope.$watch(function() {
						return (!_.isUndefined(mapCtrl.getGoogleMap()) && !_.isUndefined(scope.location));
					}, function(ready) {
						var map = mapCtrl.getGoogleMap();
						scope.map = map;
						if (ready) {
							var control = el[0];
							var index = scope.index();
							control.index = index;
							if (_.isNumber(index)) {
								map.controls[google.maps.ControlPosition[scope.location]].insertAt(index, control);
							}
							map.controls[google.maps.ControlPosition[scope.location]].push(control);
							console.log(map.controls[google.maps.ControlPosition[scope.location]]);
						}
					});
				}
			};
		});
	});