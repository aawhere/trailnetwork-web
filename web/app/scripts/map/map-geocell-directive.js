define(['route/route-module',
		'underscore',
		'map/geocell-util',
		//
		'map/map-overlay'
	],
	function(routeModule, _, geoCellUtil) {
		'use strict';

		/**
		 * Provides the route directive, which efficiently displays a list of routes on a map.
		 *
		 * @param {object}
		 *            routes: array of routes.
		 *
		 * @param {object}
		 *            selected: [SelectedRoutes]
		 *
		 * {
		 *     route //The tn.route.Route
		 *     options //The google.maps.PolylineOptions
		 *     category //optional category which is used herein (not on the route directive);
		 * }
		 *
		 */
		routeModule.directive('tnGeoCells', function($timeout, mapOverlay) {
			return {
				restrict: 'ECMA',
				require: '^tnMap',
				transclude: false,
				template: '',
				replace: false,
				scope: {},
				link: function(scope, element, attr, mapCtrl) {

					var rectangles = {};

					$timeout(function() {
						scope.map = mapCtrl.getMap();
						initialize();
					});

					function updateGeoCells(bounds) {
						var cells = geoCellUtil.geoCellsFromBounds(bounds);
						clearRectangles();
						_.each(cells, function(cell) {
							var rect = mapOverlay.createRectangle(scope.map, geoCellUtil.boundsFromCell(cell));
							rectangles[cell] = rect;
						});
					}

					function clearRectangles() {
						_.each(_.keys(rectangles), function(cell) {
							clearRectangle(cell);
						});
					}

					function clearRectangle(cell) {
						var rect = rectangles[cell];
						mapOverlay.destroyOverlay(rect);
						delete rectangles[cell];
					}

					function initialize() {
						scope.$watch(function() {
							return mapCtrl.bounds();
						}, function(newVal) {
							if (newVal && newVal.northEast) {
								updateGeoCells(newVal);
							}
						}, true);
					}
				}
			};
		});
	});