/*global google:false */
define(['map/location'], function(Location) {
	'use strict';

	var BoundingBox;

	var mergeBoundingBox = function(boundingBox, context) {
		context.north = boundingBox.north;
		context.east = boundingBox.east;
		context.south = boundingBox.south;
		context.west = boundingBox.west;
	};

	var createBoundingBox = function(north, east, south, west) {
		return {
			north: north,
			east: east,
			south: south,
			west: west,
		};
	};

	/**
	 * Converts a google.maps.LatLngBounds to a tn.measure.BoundingBox.
	 *
	 * @param {google.maps.LatLngBounds}
	 *            boundingBox
	 * @returns {tn.measure.BoundingBox}
	 */
	var boundingBoxFromLatLngBounds = function(latLngBounds) {
		return createBoundingBox(latLngBounds.getNorthEast().lat(),
			latLngBounds.getNorthEast().lng(),
			latLngBounds.getSouthWest().lat(),
			latLngBounds.getSouthWest().lng());
	};

	/**
	 * Convert a string representation of a bounding box into a
	 * BoundingBox.
	 *
	 * @param {string}
	 *            string
	 * @returns {tn.measure.BoundingBox}
	 */
	var stringToBoundingBox = function(string) {
		var split1, splitNe, splitSw;
		if (string === '') {
			throw 'Malformed BoundingBox String';
		}
		split1 = string.split('|');
		splitSw = split1[0].split(',');
		splitNe = split1[1].split(',');
		return createBoundingBox(splitNe[0], splitNe[1], splitSw[0], splitSw[1]);
	};


	BoundingBox = function(options) {
		if (options.boundingBox) {
			mergeBoundingBox(options.boundingBox, this);
		} else if (options.googleLatLngBounds) {
			mergeBoundingBox(boundingBoxFromLatLngBounds(options.googleLatLngBounds), this);
		} else if (options.north && options.east && options.south && options.west) {
			mergeBoundingBox(createBoundingBox(options.north, options.east, options.south, options.west), this);
		} else if (options.string) {
			mergeBoundingBox(stringToBoundingBox(options.string), this);
		}
	};

	BoundingBox.createFromBoundingBox = function(boundingBox) {
		if(_.isUndefined(boundingBox)) {
			return undefined;
		} else {
			return new BoundingBox({
				boundingBox: boundingBox,
			});
		}
	};

	BoundingBox.createFromCorrds = function(n,e,s,w){
		return new BoundingBox({
			north: n,
			east: e,
			south: s,
			west: w,
		});
	};

	BoundingBox.createFromGoogleLatLngBounds = function(latLngBounds) {
		return new BoundingBox({
			googleLatLngBounds: latLngBounds,
		});
	};

	BoundingBox.maxBoundingBoxString = '-90,-180|90,180';

	/**
	 * [center description]
	 * @return {[type]} [description]
	 */
	BoundingBox.prototype.center = function() {
		var centerLat, centerLon;
		var n, e, s, w;

		n = this.north;
		s = this.south;
		e = this.east;
		w = this.west;

		centerLat = s + (n - s) / 2;
		centerLon = w + (e - w) / 2;

		return new Location({
			lat: centerLat,
			lon: centerLon
		});
	};

	/**
	 * Converts a tn.measure.BoundingBox into a
	 * google.maps.LatLngBounds.
	 *
	 * @param {tn.measure.BoundingBox}
	 *            boundingBox
	 * @returns {google.maps.LatLngBounds}
	 */
	BoundingBox.prototype.googleLatLngBounds = function() {
		var ne, sw;
		ne =
			new Location({
				lat: this.north,
				lon: this.east,
			});
		sw = new Location({
			lat: this.south,
			lon: this.west
		});
		return new google.maps.LatLngBounds(sw.googleLatLng(), ne.googleLatLng());
	};

	/**
	 * Increase the size of the BoundingBox by the provided percentage
	 *
	 * @param  {Float} percent [description]
	 * @return {BoundingBox}         [description]
	 */
	BoundingBox.prototype.grow = function(percent) {
		var latDiff = this.north - this.south;
		var lonDiff = this.east - this.west;

		var latIncrease = latDiff * percent;
		var lonIncrease = lonDiff * percent;

		var north = this.north + (latIncrease / 2);
		var east = this.east + (lonIncrease / 2);
		var south = this.south - (latIncrease / 2);
		var west = this.west - (lonIncrease / 2);

		return new BoundingBox({
			north: north,
			east: east,
			south: south,
			west: west
		});

	};

	BoundingBox.prototype.union = function(other) {
		var n,e,s,w;

		n = this.north > other.north ? this.north : other.north;
		e = this.east > other.east ? this.east : other.east;
		s = this.south < other.south ? this.south : other.south;
		w = this.west < other.west ? this.west : other.west;

		return new BoundingBox({
			north: n,
			east: e,
			south: s,
			west: w,
		});
	};

	/**
	 * Convert a BoundingBox into a string representation as
	 * follows:
	 *
	 * south,west|north,east
	 *
	 * e.g. -123,34|-122,35
	 *
	 * @param {tn.measure.BoundingBox}
	 *            bounds
	 * @returns {string} String Representation
	 *
	 */
	BoundingBox.prototype.toString = function() {
		var string;

		string =
			this.south + ',' + this.west + '|' +
			this.north + ',' + this.east;
		return string;
	};


	BoundingBox.prototype.equalsBoundingBox = function(secondBox) {
		return (this.toString() === secondBox.toString());
	};

	/**
	 * Does this contain the point?
	 *
	 * @param  {tn.measure.GeoCoordinate} point
	 * @return {boolean}
	 */
	BoundingBox.prototype.containsLocation = function(location) {
		return (location.latitude.value <= this.north &&
			location.latitude.value >= this.south &&
			location.longitude.value <= this.east &&
			location.longitude.value >= this.west);
	};

	/**
	 * Is this bounds fully contained within the container?
	 * If the edges of both boxes lie on the same line, they are considered to
	 * contain.
	 *
	 * @param  {tn.measure.BoundingBox} container the container to contain the bounds
	 * @return {Boolean}
	 */
	BoundingBox.prototype.containsBounds = function(container) {
		return (this.north <= container.north &&
			this.south >= container.south &&
			this.east <= container.east &&
			this.west >= container.west);
	};

	/**
	 * Does the provided bounds intersect the container?
	 * If the edges of both boxes lie on the same line, they are considered to
	 * intersect.
	 *
	 * @param  {tn.measure.BoundingBox} bounds    what should be inbtersected
	 * @param  {tn.measure.BoundingBox} container the container to intersect the bounds
	 * @return {Boolean}
	 */
	BoundingBox.prototype.intersectsBounds = function(container) {
		var boundsNorth = this.north;
		var boundsEast = this.east;
		var boundsSouth = this.south;
		var boundsWest = this.west;

		var containerNorth = container.north;
		var containerEast = container.east;
		var containerSouth = container.south;
		var containerWest = container.west;

		return ((boundsEast >= containerWest && boundsWest <= containerEast) &&
			(boundsNorth >= containerSouth && boundsSouth <= containerNorth));
	};

	return BoundingBox;
});