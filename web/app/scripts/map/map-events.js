define(function() {
	'use strict';

	/**
	 * Events for the map page.
	 */
	return {
		mapSearchFocus: 'map-search-focus',
		mapSearchBlur: 'map-search-blur',
		mapSearchMouseOver: 'map-search-mouseover',
		mapSearchMouseLeave: 'map-search-mouseleave',
		mapSearchBefore: 'map-search-before-search',
		mapSearch: 'map-search',
		mapClick: 'map-click',
		mapZoomChanged: 'map-zoom-changed',
		mapBoundsChanged: 'map-bounds-changed',
		mapTypeChanged: 'map-type-changed',
	};
});