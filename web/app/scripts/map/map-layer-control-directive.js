define(['map/map-module',
		'underscore',
		//
		'map/map-service',
	],
	function(mapModule,
	         _) {
		'use strict';

		/**
		 * Provides the markers directive, which displays a list of locations on a map.
		 *
		 * @param {object}
		 *            locations: Array[tn.measure.GeoCorrdinate].
		 *
		 */
		mapModule.directive('tnMapLayerControl', function(Map) {
			return {
				restrict: 'ECMA',
				require: '^tnMapControl',
				transclude: false,
				templateUrl: '/views/map/map-layer-control-directive.html',
				replace: false,
				scope: {},
				link: function(scope, el, attr, tnMapControl) {

					scope.mapCtrl = tnMapControl;

					scope.changeMapLayer = function(layer) {
						if (scope.mapService) {
							scope.mapService.mapTypeId(layer);
							scope.layer = layer;
						}
					};

					scope.$watch(function() {
						return !_.isUndefined(tnMapControl.getGoogleMap());
					}, function(ready) {
						if (ready) {
							var map = tnMapControl.getGoogleMap();
							scope.mapService = new Map(map);
							scope.changeMapLayer(scope.mapService.mapTypeId());
						}
					});
				}
			};
		});
	});