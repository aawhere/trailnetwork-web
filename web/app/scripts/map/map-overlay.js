/*global google:false */

define(['underscore',
	'map/location',
	'map/bounding-box'
], function(_,
	Location,
	BoundingBox) {
	'use strict';
	var overlay;

	overlay = {};

	/**
	 * Create a google.maps.Marker s
	 *
	 * @param {google.maps.Map}
	 *            map
	 * @param {tn.geo.Location}
	 *            location
	 * @param {google.maps.MarkerOptions}
	 *            options (Optional)
	 *
	 * @returns {google.maps.Marker}
	 */
	overlay.createMarker = function(map, location, options) {
		options = options || {};
		options = _.extend({
			map: map,
			position: Location.createFromLocation(location).googleLatLng(),
		}, options);
		return new google.maps.Marker(options);
	};

	/**
	 * Creates a google.maps.Polyline from an array of google.maps.LatLng.
	 *
	 * @param {google.maps.Map}
	 *            map
	 * @param {google.maps.LatLng[]}
	 *            latLngArray
	 * @param {google.maps.PolylineOptions}
	 *            options (Optional)
	 *
	 * @returns {google.maps.Polyline}
	 */
	overlay.createPolyline = function(map, latLngArray, options) {
		options = options || {};
		options = _.extend({
			map: map,
			path: latLngArray
		}, options);
		return new google.maps.Polyline(options);
	};

	/**
	 * Update an overlay's options
	 *
	 * @param  {google.maps.Overlay} overlay the overlay to update (marker, polyline)
	 * @param  {google.maps.*Options} options
	 * @return {[type]}         [description]
	 */
	overlay.updateOverlay = function(overlay, options) {
		overlay.setOptions(options);
	};

	/**
	 * Convenience method for creating a google.maps.Polyline from an
	 * encodedPath.
	 *
	 * @param {google.maps.Map}
	 *            map
	 * @param {string}
	 *            encodedPath
	 * @param {google.maps.PolylineOptions}
	 *            options (Optional)
	 * @returns {google.maps.Polyline}
	 */
	overlay.createPolylineFromEncoded = function(map, encodedPath, options) {
		return overlay.createPolyline(map, overlay.decode(encodedPath), options);
	};

	overlay.createRectangle = function(map, bounds) {
		var rect = new google.maps.Rectangle({
			bounds: BoundingBox.createFromBoundingBox(bounds).googleLatLngBounds(),
			map: map,
			fillOpacity: 0,
			strokeColor: '#0099cb',
			strokeWeight: 1,
			clickable: false,
		});
		return rect;
	};

	/**
	 * Removes overlays from map and sets their value to null to allow garbage
	 * collection to take them.
	 *
	 * @param {object}
	 *            overlays
	 */
	overlay.destroyOverlays = function(overlays) {
		var i;
		for (i = 0; i < overlays.length; i++) {
			overlay.destroyOverlay(overlays[i]);
		}
	};

	/**
	 * Removes overlay from map and sets its value to null to allow garbage
	 * collection to take them.
	 *
	 * @param {object}
	 *            overlay
	 */
	overlay.destroyOverlay = function(overlay) {
		google.maps.event.clearInstanceListeners(overlay);
		overlay.setMap(null);
		overlay = null;
	};

	/**
	 * Decode an encoded path.
	 *
	 * @param {string}
	 *            encodedPath
	 * @returns {google.maps.MVCArray<LatLng>}
	 */
	overlay.decode = function(encodedPath) {
		return google.maps.geometry.encoding.decodePath(encodedPath);
	};

	/**
	 * Wraps the google maps event addListener
	 *
	 * @param {object}
	 *            overlay
	 * @param {string}
	 *            event
	 * @param {function}
	 *            callback accepting one parameter, the event
	 */
	overlay.addListener = function(overlay, event, callback) {
		google.maps.event.addListener(overlay, event, callback);
	};

	return overlay;
});