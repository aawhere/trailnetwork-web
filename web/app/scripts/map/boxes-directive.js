define(['map/map-module',
		'underscore',
		'map/map-events',
		'map/geocell-util',
		'marker-with-label',
		'map/location',
		'map/bounding-box',
		//
		'map/map-overlay',
	],
	function(mapModule, _, mapEvents, geocellUtil, MarkerWithLabel, Location, BoundingBox) {
		'use strict';

		/**
		 * Displays geocell boxes on the screen
		 *
		 */
		mapModule.directive('tnBoxes', function($timeout, mapOverlay) {
				return {
					restrict: 'ECMA',
					require: '^tnMap',
					template: '',
					replace: false,
					scope: {},
					link: function(scope, element, attr, mapCtrl) {

						var MAX_CELLS = 24;
						var boxes = [];
						var textMarkers = [];

						$timeout(function() {
							scope.map = mapCtrl.getGoogleMap();
						});

						scope.$on(mapEvents.mapBoundsChanged, function() {
							clearBoxes();
							clearTextMarkers();
							var bounds = BoundingBox.createFromGoogleLatLngBounds(scope.map.getBounds());
							var geocells = geocellUtil.geoCellsFromBounds(bounds, MAX_CELLS);
							_.each(geocells, function(cell) {
								var cellBounds = geocellUtil.boundsFromCell(cell);
								var box = mapOverlay.createRectangle(scope.map, cellBounds);
								boxes.push(box);
								createTextbox(cell, cellBounds);

							});
						});

						function createTextbox(cell, cellBounds) {
							var center = BoundingBox.createFromBoundingBox(cellBounds).center();
							//console.log('geocell', cell, bounds, center);
							var text = new MarkerWithLabel({
								position: Location.createFromLocation(center).googleLatLng(),
								map: scope.map,
								icon: '/images/blank.gif',
								labelAnchor: new google.maps.Point(22, 0),
								labelContent: cell,
								labelStyle: {
									backgroundColor: '#fff',
									opacity: 0.75
								}
							});
							textMarkers.push(text);
						}

						function clearBoxes() {
							_.each(boxes, function(box) {
								box.setMap(null);
							});
						}

						function clearTextMarkers() {
							_.each(textMarkers, function(text) {
								text.setMap(null);
							});
						}

					}
				};
			}
		);
	});