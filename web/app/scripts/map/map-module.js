define(
	['angular', 'map/map-util', 'map/map-overlay'],
	function(angular, mapUtil, mapOverlay) {
		'use strict';
		var tnMapModule;

		tnMapModule = angular.module('tn.map', [/*'mgcrea.ngStrap.typeahead'*/]);

		/**
		 * Angularjs dependency for tn.map.util
		 */
		tnMapModule.factory('mapUtil', function() {
			return mapUtil;
		});

		/**
		 * Angularjs dependency for mapOverlay
		 */
		tnMapModule.factory('mapOverlay', function() {
			return mapOverlay;
		});

		return tnMapModule;
	});