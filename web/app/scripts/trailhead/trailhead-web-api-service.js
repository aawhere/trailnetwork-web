/**
 * 	Responsible for retreiving a trailhead. May implement caching transparently to the consumer of the trailhead.
 *
 */
define(['trailhead/trailhead-module',
		'common/common-tn-api-constants',
		'trailhead/trailhead-tn-api-constants',
		'common/tn-api-constants',
		//
		'restangular',
		'common/notification-service',
		'common/entity-web-api-service',
		'trailhead/trailhead-repository',
	],
	function(routeModule, commonTnApiConstants, trailheadTnApiConstants, tnApiConstants) {
		'use strict';

		/**
		 * Responsible for retreiving a route.
		 *
		 */
		routeModule.factory('TrailheadWebApiService', function(Restangular, notificationService, $translate, trailheadRepository, EntityWebApiService) {

				var Trailhead = function(trailheadId) {
					EntityWebApiService.call(this, {
						entityName: 'trailhead',
						id: trailheadId,
						repo: trailheadRepository
					});
				};

				Trailhead.inheritsFrom(EntityWebApiService);

				Trailhead.prototype.withRelativeStarts = function() {
					this.expandFields(tnApiConstants.fields.trailheadRelativeStarts);
					return this;
				};

				Trailhead.prototype.withRouteCompletionStats = function() {
					this.expandFields(tnApiConstants.fields.trailheadRouteCompletionStats);
					return this;
				};

				/**
				 * Get the route from the server if not already retreived.
				 *
				 * @return {[Promise]} A promise for an tn.route.Route.
				 */
				Trailhead.prototype.getFromWebApi = function() {
					var p = Restangular.one(trailheadTnApiConstants.paths.trailheads, this.id).get(this.requestParameters);
					return p;
				};

				return Trailhead;
			}

		);
	});