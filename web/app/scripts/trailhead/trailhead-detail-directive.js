define(['trailhead/trailhead-module',
	'app-route-constants',
	'jquery',
	//
	'common/debug-service',
	'common/location-service',
	'route/activity-types-directive',
], function(trailheadModule, appRouteConstants) {
	'use strict';

	/**
	 * Provides a list of cards to be used in a map-cardtray.
	 *
	 * @param {Array(Routes)} The routes to display
	 *
	 */
	trailheadModule.directive('tnTrailheadDetail', function(debugService, $rootScope, locationService, $state) {
			return {
				restrict: 'EA',
				transclude: false,
				templateUrl: '/views/trailhead/trailhead-detail-directive.html',
				replace: false,
				scope: {
					//tn.trailhead.Trailhead
					trailhead: '=',
				},
				link: function(scope) {

					scope.debug = debugService.enabled;
					scope.dictionary = {};

					scope.back = function() {
						$state.go(appRouteConstants.states.exploreRoutes);
					};
				}
			};
		}
	);
});