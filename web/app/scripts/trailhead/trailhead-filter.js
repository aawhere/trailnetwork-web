define(['underscore',
	'map/geocell-util',
	'trailhead/trailhead-util',
	'map/bounding-box'
], function(_,
	geocellUtil,
	thUtil,
	BoundingBox) {
	'use strict';
	var filter, MAX_RESOLUTION = 6;

	filter = function() {
		this.conditions = {};

		this.maxResolution = MAX_RESOLUTION;

		/**
		 * An object describing how many trailheads to display at each
		 * zoomLevel.
		 * @type {Object}
		 */
		this.conditions.zoomLevels = {
			20: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			19: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			18: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			17: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			16: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			15: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			14: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			13: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			12: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			11: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			10: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			9: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			8: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			7: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			6: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			5: {
				geoCellResolution: 5,
				prominence: {
					prominent: 1000,
					prevalent: 1000
				}
			},
			4: {
				geoCellResolution: 5,
				prominence: {
					prominent: 30,
					prevalent: 30
				}
			},
			3: {
				geoCellResolution: 4,
				prominence: {
					prominent: 15,
					prevalent: 15
				}
			},
			2: {
				geoCellResolution: 3,
				prominence: {
					prominent: 7,
					prevalent: 7
				}
			},
			1: {
				geoCellResolution: 2,
				prominence: {
					prominent: 15,
					prevalent: 15
				}
			}
		};

	};

	/**
	 * Filter the trailheads using the prevalent prominence and zoomLevel conditions. Results are sorted by score.
	 *
	 * @see filterProminence
	 */
	filter.prototype.filterPrevalent = function(trailheads, bounds, zoomLevel) {
		return this.filterProminence(trailheads, bounds, zoomLevel, 'prevalent');
	};

	/**
	 * Filter the trailheads using the prominence and zoomLevel conditions. Results are sorted by score.
	 *
	 * @param  {Array[tn.trailhead.Trailhead]} trailheads the trailheads to filter.
	 * @param  {tn.measure.BoundingBox} bounds the bounds of the provided trailheads (typically comes from server).
	 * @param  {int} zoomLevel the zoomLevel to filter to (see zoom-levels.js)
	 * @param  {String} prominence the prominence (i.e. prevalent, prominent, obscure)
	 * @return {Array[tn.trailhead.Trailhead]}  the filtered response.
	 */
	filter.prototype.filterProminence = function(trailheads, bounds, zoomLevel, prominence) {
		var cells, level, result = [],
			context = this;

		level = this.conditions.zoomLevels[zoomLevel];
		cells = geocellUtil.geoCellsFromBounds(bounds, level.geoCellResolution);
		cells = geocellUtil.toSingleResolution(cells, level.geoCellResolution);

		_.each(cells, function(cell) {
			var ths;
			ths = context.filterToCell(trailheads, cell);
			ths = context.filterOutTrailheadsWithoutMains(trailheads);
			ths = thUtil.sortByScoreDescending(ths);
			result = _.union(result, _.first(ths, level.prominence[prominence]));
		});
		result = thUtil.sortByScoreDescending(result);
		return result;
	};

	/**
	 * Filter the provided trailheads to those contained in the geocell.
	 *
	 * @param  {Array[tn.trailhead.Trailhead]} trailheads
	 * @param  {String} GeoCell
	 * @return {Array[tn.trailhead.Trailhead]} filtered trailheads
	 */
	filter.prototype.filterToCell = function(trailheads, cell) {
		var result = [];
		var bounds = geocellUtil.boundsFromCell(cell);
		_.each(trailheads, function(trailhead) {
			if (BoundingBox.createFromBoundingBox(bounds).containsLocation(trailhead.location)){
				result.push(trailhead);
			}
		});
		return result;
	};

	filter.prototype.filterOutTrailheadsWithoutMains = function(trailheads) {
		return _.filter(trailheads, function(trailhead) {
			return (trailhead.numberOfMains > 0);
		});
	};

	/**
	 * Provided the current bounds this will compute the trailheads
	 * to display on the map. The provided object will have trailheads organized by prominence.
	 *
	 * Prominence tells us how popular or well known a trailhead is. The more prominent a trailhead
	 * is, the more emphasis we can put on it when displaying on a map.
	 *
	 * Prominences:
	 *		prominent
	 *		prevalent
	 *		obscure
	 *
	 * @param  {tn.measure.BoundingBox} bounds BoundingBox representing the current view.
	 * @param  {Integer} zoom level
	 * @return {Object}   the computed trailheads.
	 */
	/*filter.prototype.getWithProminence = function(bounds, zoomLevel) {
		var cells, level, result = {}, prominent = [],
			prevalent = [],
			obscure = [];
		level = zoomLevels[zoomLevel];
		cells = geocellUtil.geoCellsFromBounds(bounds, level.geoCellResolution);
		cells = geocellUtil.toSingleResolution(cells, level.geoCellResolution);

		_.each(cells, function(cell) {
			var ths;
			ths = _.values(context.get(cell));
			ths = thUtil.sortByScoreDescending(ths);
			prominent = _.union(prominent, _.first(ths, level.prominence.prominent));
			prevalent = _.union(prevalent, _.first(_.rest(ths, level.prominence.prominent), level.prominence.prevalent));
			obscure = _.union(obscure, _.first(ths, level.prominence.promenent + level.prominence.prevalent));
		});
		result.prominent = thUtil.sortByScoreDescending(prominent);
		result.prevalent = thUtil.sortByScoreDescending(prevalent);
		result.obscure = thUtil.sortByScoreDescending(obscure);
		return result;
	};
*/


	return filter;

});