define(['map/map-module'], function(mapModule) {
	'use strict';

	/**
	 * Provides a banner to display across the top of the map.
	 *
	 * @param {String}
	 *            text: The text to display in the banner
	 *
	 */
	mapModule.directive('tnTrailheadCardtrayBanner', function() {
			return {
				restrict: 'EA',
				transclude: false,
				templateUrl: '/views/trailhead/trailhead-cardtray-banner.html',
				replace: false,
				scope: {
					text: '=',
					close: '&onClose',
				},
				link: function(/*scope, element, attr, mapCtrl*/) {

				}
			};
		}
	);
});