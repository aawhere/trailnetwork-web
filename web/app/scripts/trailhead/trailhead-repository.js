define(['common/entities', 'underscore'], function(Entities, _) {
	'use strict';

	var service = {};

	service = function() {
		this.thRepo = new Entities();
		this.MAX_RESOLUTION = 11;
	};

	service.prototype.add = function(trailheads) {
		this.thRepo.add(trailheads);
	};

	service.prototype.get = function() {
		return this.thRepo;
	};

	service.prototype.find = function(id) {
		return this.thRepo.find(id);
	};

	service.prototype.getByGeoCell = function(geoCell) {
		var ths = new Entities();

		if(geoCell.length > this.MAX_RESOLUTION) {
			throw new Error('Max resolution of repository exceeded: ' + geoCell);
		}

		_.each(this.thRepo, function(th) {
			var thCell = th.id.substring(0,geoCell.length);
			if(thCell === geoCell) {
				ths.add(th);
			}
		});
		return ths;
	};

	return service;
});