define(['angular',
		'trailhead/trailhead-repository'
	],
	function(angular, TrailheadRepository) {
		'use strict';

		var trailheadModule;

		trailheadModule = angular.module('tn.trailhead', ['restangular']);

		trailheadModule.service('trailheadRepository', function() {
			return new TrailheadRepository();
		});

		return trailheadModule;
	});