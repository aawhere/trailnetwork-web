define(['trailhead/trailhead-module',
		'common/tn-api-constants',
		'map/bounding-box',
		'common/filter-condition-operators',
		//
		'restangular',
		'common/entities-web-api-service',
	],
	function(trailheadModule, apiConstants, BoundingBox, operators) {
		'use strict';

		/**
		 * Responsible for retreiving trailheads.
		 *
		 */
		trailheadModule.factory('TrailheadsWebApiService', function(Restangular, $q, EntitiesWebApiService) {

				var Trailheads = function() {
					EntitiesWebApiService.call(this, {
						entitiesPropertyName: 'trailhead',
					});
				};

				Trailheads.inheritsFrom(EntitiesWebApiService);

				/**
				 *	Adds a bounds filter to the request.
				 *
				 * @param  {tn.measure.BoundingBox} location [description]
				 * @return {this}        [description]
				 */
				Trailheads.prototype.limitByBounds = function(location) {
					if (location) {
						var boundsAsString = BoundingBox.createFromBoundingBox(location).toString();
						this.requestParameters[apiConstants.params.bounds] = boundsAsString;
					}
					return this;
				};

				/**
				 * Limit results to the provided filter.
				 *
				 * @param  {RouteFitler} filter
				 * @return {this}
				 */
				Trailheads.prototype.filteredByRouteFilter = function(routeFitler) {
					this.routeFilter = routeFitler;
					return this;
				};

				Trailheads.prototype.allRoutes = function() {
					this.orderBy(apiConstants.orderBy.none);
				};

				Trailheads.prototype.withRelativeStarts = function() {
					this.expandFields(apiConstants.fields.trailheadRelativeStarts);
					return this;
				};

				Trailheads.prototype.withRouteCompletionStats = function() {
					this.expandFields(apiConstants.fields.trailheadRouteCompletionStats);
					return this;
				};


				Trailheads.prototype.withTrailheadId = function(id) {
					this.addFilterCondition(apiConstants.fields.trailheadId, operators.equals, id);
					return this;
				};

				Trailheads.prototype.withTrailheadIds = function(ids) {
					this.addFilterCondition(apiConstants.fields.trailheadId, operators.in, ids);
					return this;
				};

				/**
				 * Get the next page of Trailheads. If this is the first request, retreives the first page of results.
				 *
				 * @return {[Promise]} A promise for an array of tn.event.Event
				 */
				Trailheads.prototype.makeRequest = function() {
					var deferred = $q.defer();
					var restBuilder = getRestBuilder(this);

					restBuilder.get(requestParameters(this)).then(function(events) {
						deferred.resolve(events);
					}, function(err) {
						deferred.reject(err);
					});
					return deferred.promise;
				};

				function getRestBuilder() {
					var builder = Restangular;
					builder = builder.one(apiConstants.paths.trailheads);
					return builder;
				}

				function requestParameters(context) {
					if (context.routeFilter) {
						conditionsFromRouteFilter(context.routeFilter, context);
					}

					return context.requestParameters;
				}

				/**
				 * Produce a set of conditions for use with the api from the routeFilter
				 *
				 * @param  {RouteFilter} routeFilter [description]
				 * @param  {EntitiesWebApiService}
				 */
				function conditionsFromRouteFilter(routeFilter, context) {
					var distanceCategory = routeFilter.distance();
					if (distanceCategory) {
						context.addFilterCondition(apiConstants.fields.trailheadDistanceCategory, operators.equals, distanceCategory);
					}

					var activityType = routeFilter.activityType();
					if (activityType && !distanceCategory) {
						context.addFilterCondition(apiConstants.fields.trailheadRouteCompletionStatsActivityType, operators.equals, activityType);
					}

					var persons = routeFilter.persons();
					var personIds = _.pluck(persons, 'id');
					if(persons) {
						context.addFilterCondition(apiConstants.fields.trailheadRelativeStartsPersonId, operators.equals, personIds);
					}
				}


				return Trailheads;
			}

		);
	});