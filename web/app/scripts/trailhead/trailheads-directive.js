define(['trailhead/trailhead-module',
	'explore/explore-events',
	//
	'map/map-overlay'
], function(thModule, exploreEvents) {
	'use strict';

	/**
	 * Provides the trailhead directive, which efficiently displays a list of trailheads on a map.
	 *
	 * @param {object}
	 *            trailheads: array of trailheads.
	 *
	 * @param {object}
	 *            routes: attribute specifying the name of a variable containing an
	 *            array of routes.
	 *
	 * @param {object}
	 *            trailheads: attribute specifying the name of a variable containing
	 *            an array of trailheads.
	 *
	 */
	thModule.directive('tnTrailheads', function($timeout, mapOverlay, $rootScope) {
		return {
			restrict: 'EA',
			require: '^tnMap',
			transclude: true,
			template: '',
			replace: false,
			scope: {
				trailheads: '=trailheads',
				options: '=',
				selectedOptions: '=',
				selectable: '=',
				onClick: '&'
			},
			link: function(scope, element, attr, mapCtrl) {
				scope.markers = {};

				scope.defaultOptions = {};
				scope.defaultOptions.icon = '/images/thMarker-16-green.png';
				scope.defaultSelectedOptions = {};
				scope.defaultSelectedOptions.zIndex = 1000;
				scope.defaultSelectedOptions.icon = '/images/thMarker-16-red.png';

				scope.unselectedOptions = _.extend(scope.defaultOptions, scope.options);
				scope.selectedOptions = _.extend(scope.defaultSelectedOptions, scope.selectedOptions);

				scope.$watch(function() {
					return mapCtrl.getGoogleMap();
				}, function(map) {
					if (map) {
						scope.map = map;
						setupWatches();
					}
				});

				function setupWatches() {
					scope.$watch('trailheads', function() {
						var marker;
						clearMarkers();
						_.each(scope.trailheads, function(th) {

							scope.unselectedOptions.clickable = scope.selectable;
							marker = mapOverlay.createMarker(scope.map, th.location,
								scope.unselectedOptions);
							var thId = th.id;

							mapOverlay.addListener(marker, 'click', function() {
								scope.$apply(function() {
									console.log('onclick', scope.selected, thId);
									if (scope.selected === thId) {
										$rootScope.$broadcast(exploreEvents.trailheadUnSelected, thId);
									} else {
										$rootScope.$broadcast(exploreEvents.trailheadSelected, thId);
									}
								});
							});

							mapOverlay.addListener(marker, 'mouseover', function() {

								scope.$apply(function() {
									$rootScope.$broadcast(exploreEvents.trailheadHighlighted, th.id);
								});
							});

							mapOverlay.addListener(marker, 'mouseout', function() {
								scope.$apply(function() {
									$rootScope.$broadcast(exploreEvents.trailheadUnHighlighted, th.id);
								});
							});

							scope.markers[th.id] = marker;
						});
					}, true);

					scope.$on(exploreEvents.trailheadHighlighted, function(event, trailheadId) {
						var marker = findMarker(trailheadId);
						if (marker) {
							mapOverlay.updateOverlay(marker,
								scope.selectedOptions);
							scope.highlighted = trailheadId;
						}
					});

					scope.$on(exploreEvents.trailheadUnHighlighted, function(event, trailheadId) {
						if (scope.selected !== trailheadId) {
							var marker = findMarker(trailheadId);
							if (marker) {
								mapOverlay.updateOverlay(marker,
									scope.unselectedOptions);
							}
						}
						scope.highlighted = undefined;
					});

					scope.$on(exploreEvents.trailheadSelected, function(event, trailheadId) {
						console.log('onTrailheadSelected', trailheadId);
						if (scope.selected) {
							unselectTrailhead(scope.selected);
						}
						var marker = findMarker(trailheadId);
						if (marker) {
							mapOverlay.updateOverlay(marker,
								scope.selectedOptions);
						}
						scope.selected = trailheadId;
					});

					scope.$on(exploreEvents.trailheadUnSelected, function(event, trailheadId) {
						unselectTrailhead(trailheadId);
					});
				}

				function unselectTrailhead(trailheadId) {
					console.log('onTrailheadUnSelected', trailheadId);
					var marker = findMarker(trailheadId);
					if (marker) {
						mapOverlay.updateOverlay(marker,
							scope.unselectedOptions);
					}
					scope.selected = undefined;
				}

				function clearMarkers() {
					_.each(scope.markers, function(th, thId) {
						clearMarker(thId);
					});
					scope.markers = {};
				}

				function clearMarker(thId) {
					var marker = findMarker(thId);
					mapOverlay.destroyOverlay(marker);
					delete scope.markers[thId];
				}

				function findMarker(thId) {
					return scope.markers[thId];
				}
			}
		};
	});
});