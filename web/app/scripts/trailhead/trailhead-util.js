define(['underscore',
	'map/bounding-box',
	'common/entities'
], function(_,
	BoundingBox,
	Entities) {
	'use strict';

	var util;

	util = {};

	util.sortByScoreDescending = function(ths) {
		return _.sortBy(ths, function(th) {
			return -th.score;
		});
	};

	/**
	 * Find the trailhead with provided id from the provided array of trailheads.
	 *
	 * @param  {Array[tn.trailhead.Trailhead]} trailheads
	 * @param  {String} id  Trailhead id
	 * @return {tn.trailhead.Trailhead}  The found trailhead or null.
	 */
	util.findTrailheadWithId = function(trailheads, id) {
		return _.filter(trailheads, function(th) {
			return th.id === id;
		})[0];
	};

	util.toArrayOfIds = function(trailheads) {
		return _.pluck(trailheads, 'id');
	};

	/**
	 * Fitler the trailheads to those that are within the provided bounds.
	 *
	 * @param  {Entities[tn.trailhead.Trailhead]} trailheads
	 * @param  {tn.measure.BoundingBox} bounds
	 * @return {Entities[tn.trailhead.Trailhead]} copy with filtered results.
	 */
	util.filterToBounds = function(trailheads, bounds) {
		var result = new Entities();
		var filteredThs = _.filter(trailheads, function(th) {
			return util.isWithinBounds(th, bounds);
		});
		result.add(filteredThs);
		return result;
	};

	util.isWithinBounds = function(th, bounds) {
		return BoundingBox.createFromBoundingBox(bounds).containsLocation(th.location);
	};

	/**
	 * find the minimum BoundingBox that contains all trailheads.
	 * Does not consider the 180 meridian.
	 *
	 * @param  {Array[tn.trailhead.Trailhead]} trailheads [description]
	 * @return {tn.measure.BoundingBox} the bounds of the supplied trailheads.
	 */
	util.bounds = function(trailheads) {
		var north, east, south, west;

		_.each(trailheads, function(th) {
			var lat = th.location.latitude.value;
			var lon = th.location.longitude.value;
			if (!north || north < lat) {
				north = lat;
			}
			if (!east || east < lon) {
				east = lon;
			}
			if (!south || south > lat) {
				south = lat;
			}
			if (!west || west > lon) {
				west = lon;
			}
		});
		return BoundingBox.createFromCorrds(north, east, south, west);
	};

	return util;

});