define(['person/person-module', 'underscore', 'account/account-util'], function(personModule, _, accountUtil) {
	'use strict';

	/**
	 * Provides a formatted link to a provider
	 *
	 * @param {String} The url of the provider
	 *
	 */
	personModule.directive('tnPersonAccountLink', function() {
			return {
				restrict: 'EA',
				template: '<a href="{{url}}" class="account-link" target="_blank" ng-click="$event.stopPropagation()"><img src="{{iconLink}}"></img></a>',
				replace: true,
				scope: {
					account: '=',
					compact: '@'
				},
				link: function(scope) {
					scope.$watch('account', function() {
						if (scope.account && scope.account.applicationReference) {
							scope.url = scope.account.applicationReference.url;
						}

						if (scope.account) {
							var applicationKey = accountUtil.applicationKey(scope.account);

							scope.iconLink = '/images/provider/' + applicationKey;
							if (scope.compact) {
								scope.iconLink = scope.iconLink + '/logo.png';
							} else {
								scope.iconLink = scope.iconLink + '/logo-full.png';
							}
						}
					});
				}
			};
		}
	);
});