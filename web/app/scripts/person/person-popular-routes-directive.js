define(['person/person-module',
		//
		'route/completions-web-api-service',
		'common/app-route-service',
		'route/route-completions-directive'
	],
	function(personModule) {
		'use strict';

		/**
		 * A widget that displays the most common routes for a person.
		 *
		 */
		personModule.directive('tnPersonPopularRoutes', function(CompletionsWebApiService,
				appRouteService) {
				return {
					restrict: 'EA',
					template: '<tn-route-completions completions-web-api-service="completions.service" template-url="{{template.url}}" context="person" compact="{{compact}}"></tn-route-completions>',
					replace: false,
					scope: {
						personId: '=',
						completionsWebApiService: '=',
						//template url
						templateUrl: '@',
						limit: '=',
						compact: '@',
					},
					link: {
						pre: function($scope, el, attr) {
							$scope.template = {};
							if (attr.templateUrl) {
								$scope.template.url = attr.templateUrl;
							} else {
								$scope.template.url = '/views/person/person-popular-routes-card.html';
							}
						},
						post: function($scope) {
							$scope.appRouteService = appRouteService;

							$scope.completions = {};

							$scope.configureCompletions = function() {
								var service = new CompletionsWebApiService()
									.orderByStartTimeDesc()
									.withRoute()
									.limitByPersonId($scope.personId)
									.orderByPersonalTotalDesc();
								if ($scope.limit) {
									service.setLimit($scope.limit);
								}
								$scope.completionsWebApiService = service;
								$scope.completions.service = service;
								return service;
							};
							$scope.configureCompletions().nextPage();

						}
					}
				};
			}
		);
	});