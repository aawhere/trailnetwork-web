define(['angular',
		'person/person-module',
		'common/common-util',
		'common/tn-api-constants',
		//
		'restangular',
		'common/notification-service',
		'common/entity-web-api-service',
	],
	function(angular,
	         personModule,
	         commonUtil,
	         apiConstants) {
		'use strict';

		/**
		 * Responsible for retreiving route completions.
		 *
		 */
		personModule.factory('PersonWebApiService', function(Restangular, $q, EntityWebApiService) {

				var Person = function(personId) {
					var parsedPersonId = commonUtil.idAsNumber(personId);
					EntityWebApiService.call(this, {
						entityName: 'person',
						id: parsedPersonId,
						//The person repository cannot know if a person was retreived with or without
						//an account, which means that we should not be caching the result.
						//
						//TODO: make the cache understand the differences between added request options.
						//
						//repo: personRepository
					});
					this.requestExpand = [];
				};

				Person.inheritsFrom(EntityWebApiService);

				Person.prototype.withAccount = function() {
					this.requestExpand.push(apiConstants.fields.personAccountId);
					return this;
				};

				Person.prototype.forAlias = function(alias) {
					this.alias = alias;
					return this;
				};

				/**
				 * Get the person fronm the server if on already retreived.
				 *
				 * @return {[Promise]} A promise for an array of tn.route.RouteCompletion.
				 */
				Person.prototype.getFromWebApi = function() {
					var deferred = $q.defer();

					var restBuilder;
					if(this.alias) {
						restBuilder = Restangular.one(apiConstants.paths.accounts, this.alias).one(apiConstants.paths.persons);
					} else {
						restBuilder = Restangular.one(apiConstants.paths.persons, this.id);
					}
					restBuilder.get(requestParameters(this)).then(function(person) {
						deferred.resolve(person);
					}, function(err) {
						deferred.reject(err);
					});
					return deferred.promise;
				};

				function requestParameters(context) {
					var params = [];
					params[apiConstants.params.expand] = requestExpand(context);
					return params;
				}

				function requestExpand(context) {
					return context.requestExpand;
				}

				return Person;
			}

		);
	});