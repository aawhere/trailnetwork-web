define(['person/person-module', ], function(personModule) {
	'use strict';

	return personModule.controller('tnPersonContentController', function($scope) {
			$scope.content = {};
		}
	);
});