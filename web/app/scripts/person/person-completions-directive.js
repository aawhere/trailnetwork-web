define(['person/person-module',
		//
		'route/completions-web-api-service',
		'common/app-route-service',
		'route/route-completions-directive',
		'activity/activities-directive',
	],
	function(personModule) {
		'use strict';

		/**
		 * A widget that displays the most common routes for a person.
		 *
		 */
		personModule.directive('tnPersonCompletions', function(CompletionsWebApiService,
				appRouteService) {
				return {
					restrict: 'EA',
					template: '<tn-route-completions completions-web-api-service="completions.service" template-url="{{template.url}}" context="person"></tn-route-completions>',
					replace: false,
					scope: {
						personId: '=',
						completionsWebApiService: '=',
						//template url
						templateUrl: '@',
						limit: '=',
						person: '=',
					},
					link: {
						pre: function($scope, el, attr) {
							$scope.template = {};
							if (attr.templateUrl) {
								$scope.template.url = attr.templateUrl;
							} else {
								$scope.template.url = '/views/person/person-completions-card.html';
							}
						},
						post: function($scope) {
							var DEFAULT_LIMIT = 20;

							$scope.appRouteService = appRouteService;

							$scope.completions = {};
							$scope.completions.limit = $scope.limit ? $scope.limit : DEFAULT_LIMIT;

							$scope.filter = {};
							$scope.filter.showAll = false;

							$scope.$watch('filter.showAll', function() {
								$scope.configureCompletions().nextPage();
							});

							$scope.configureCompletions = function() {
								var service = new CompletionsWebApiService()
									.limitByPersonId($scope.personId)
									.withRoute();
								if($scope.limit) {
									service.setLimit($scope.limit);
								}
								if (!$scope.filter.showAll) {
									service.orderByStartTimeDesc();
								}

								$scope.completionsWebApiService = service;
								$scope.completions.service = service;
								return service;
							};

							$scope.configureCompletions().nextPage();

						}
					}
				};
			}
		);
	});