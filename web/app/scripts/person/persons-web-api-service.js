/**
 * 	Responsible for retreiving routes. May add to the route repository as required.
 *
 */
define(['person/person-module',
		'app-route-constants',
		'underscore',
		'common/tn-api-constants',
		'common/filter-condition-operators',
		//
		'restangular',
		'common/entities-web-api-service',
	],
	function(personModule, appRouteConstants, _, apiConstants, operators) {
		'use strict';

		/**
		 * Responsible for retreiving a route.
		 *
		 */
		personModule.factory('PersonsWebApiService', function(Restangular, $q, EntitiesWebApiService) {

				var People = function() {
					EntitiesWebApiService.call(this, {
						entitiesPropertyName: 'person',
					});
					this.accountIds = [];
				};

				People.inheritsFrom(EntitiesWebApiService);

				/**
				 * Limits the results to routes starting from the provided trailheads.
				 *
				 * @param  {Array[tn.trailhead.Trailhead]} trailheads
				 * @return {this}
				 */
				People.prototype.limitToAccounts = function(accountIds) {
					if (!_.isArray(accountIds)) {
						accountIds = [accountIds];
					}
					if (accountIds && accountIds.length > 0) {
						this.addFilterCondition(apiConstants.fields.personAccountId, operators.equals, _.reduce(accountIds, function(memo, acct) {
							return memo + ' ' + acct;
						}, ''));
					}
					return this;
				};

				/**
				 * Get the route from the server.
				 *
				 * @return {[Promise]} A promise for an tn.route.Route.
				 */
				People.prototype.makeRequest = function() {
					var deferred = $q.defer();
					var restBuilder;
					restBuilder = getRequestBuilder();
					restBuilder
						.get(this.requestParameters).then(function(personsRequest) {
							deferred.resolve(personsRequest);
						}, function(err) {
							deferred.reject(err);
						});
					return deferred.promise;
				};

				function getRequestBuilder() {
					return Restangular.one(appRouteConstants.paths.persons);
				}

				return People;
			}

		);
	});