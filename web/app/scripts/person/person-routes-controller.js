define(['person/person-module',
	//
	'common/header-directive'
], function(personModule) {
	'use strict';

	return personModule.controller('tnPersonRoutesController', function() {
		}
	);
});