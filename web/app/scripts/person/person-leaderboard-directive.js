define(['person/person-module',
		//
		'route/completions-web-api-service',
		'common/app-route-service',
		'route/route-completions-directive'
	],
	function(personModule) {
		'use strict';

		/**
		 * A widget that displays the most common routes for a person.
		 *
		 */
		personModule.directive('tnPersonLeaderboard', function(CompletionsWebApiService,
				appRouteService) {
				return {
					restrict: 'EA',
					template: '<tn-route-completions completions-web-api-service="completions.service" template-url="{{template.url}}" context="person" compact="{{compact}}"></tn-route-completions>',
					replace: false,
					scope: {
						personId: '=',
						completionsWebApiService: '=',
						//template url
						templateUrl: '@',
						limit: '@',
						raceRankLimit: '@',
						person: '=',
						compact: '@',
					},
					link: {
						pre: function($scope, el, attr) {
							$scope.template = {};
							if (attr.templateUrl) {
								$scope.template.url = attr.templateUrl;
							} else {
								$scope.template.url = '/views/person/person-leaderboard-card.html';
							}
						},
						post: function($scope) {
							var DEFAULT_LIMIT = 20;
							var configureCompletions;

							$scope.appRouteService = appRouteService;

							$scope.completions = {};
							$scope.completions.limit = $scope.limit ? $scope.limit : DEFAULT_LIMIT;

							configureCompletions = function() {
								var service = new CompletionsWebApiService()
									.withRoute()
									.limitByPersonId($scope.personId)
									.setLimit($scope.completions.limit);
								if ($scope.raceRankLimit) {
									service.limitBySocialRaceRank($scope.raceRankLimit)
										.orderBySocialRaceRank();
								} else {
									service.tempPersonRaceRank();
								}

								$scope.completionsWebApiService = service;
								$scope.completions.service = service;
								return service;
							};

							configureCompletions().nextPage();

						}
					}
				};
			}
		);
	});