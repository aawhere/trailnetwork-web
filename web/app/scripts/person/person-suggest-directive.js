define(['person/person-module',
		'underscore',
		//
		'search/search-web-api-service'
	],
	function(personModule, _) {
		'use strict';

		/**
		 * Provides a ui for filtering routes
		 *
		 */
		personModule.directive('tnPersonSuggest', function($log, $rootScope, $timeout, $q, SearchWebApiService) {
			return {
				restrict: 'EA',
				transclude: true,
				templateUrl: '/views/person/person-suggest-directive.html',
				replace: false,
				scope: {
					selection: '=',
					routeId: '=',
					activityType: '=',
					onAdded: '&',
					onRemoved: '&',
				},
				link: function($scope) {
					var buildQuery;

					$scope.selection = $scope.selection || [];

					buildQuery = function(query) {
						var fullQuery = query;
						if ($scope.routeId && $scope.activityType) {
							fullQuery = fullQuery + ' routeCompletion_routeIdActivityType: ' + $scope.routeId + '-' + $scope.activityType;
						} else
						if ($scope.routeId) {
							fullQuery = fullQuery + ' routeCompletion_routeId: ' + $scope.routeId;
						}
						return fullQuery;
					};

					$scope.onType = function(query) {
						var deferred = $q.defer();
						if (!query || query.length === 0) {
							$scope.clear();
						} else {
							var fullQuery = buildQuery(query);
							new SearchWebApiService(fullQuery).setLimit(5).withPersons().nextPage().then(function(results) {
								var suggestions = [];
								_.each(results, function(result) {
									var nameWithAliasesField = _.find(result.fields, function(field) {
										return field.key === 'person-nameWithAliases';
									});
									if (!_.isUndefined(nameWithAliasesField)) {
										suggestions.push({
											name: nameWithAliasesField.value,
											nameWithAliases: nameWithAliasesField.value,
											id: result.id
										});
									}
								});
								if (suggestions.length === 0) {
									suggestions.push({
										name: "No Results",
									});
								}
								deferred.resolve(suggestions);
							});
						}
						return deferred.promise;
					};

					$scope.clear = function() {
						$scope.selection = [];
					};

					$scope.onSelect = function(selection) {
						if (_.isUndefined(selection.id)) {
							var index = $scope.selection.indexOf(selection);
							if (index > -1) {
								$scope.selection.splice(index, 1);
							}
						} else {
							$scope.onAdded({
								person: selection
							});
						}
					};

					$scope.onRemove = function(selection) {
						$scope.onRemoved({
							person: selection
						});
					};

				}
			};
		});
	});