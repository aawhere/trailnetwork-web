define(['person/person-module',
	'account/account-util',
	//
	'common/header-directive',
	'person/person-web-api-service',
	'common/browser-service',
	'person/person-activities-directive',
	'person/person-popular-routes-directive',
	'person/person-leaderboard-directive',
	'person/person-completions-directive',
	'account/account-web-api-service'
], function(personModule, accountUtil) {
	'use strict';

	return personModule.controller('tnPersonController', function($scope,
		$stateParams,
		PersonWebApiService,
		$translate,
		browserService,
		$log,
		AccountWebApiService) {

		$scope.person = {};
		$scope.person.personId = $stateParams.personId;
		$scope.person.service = new PersonWebApiService($scope.person.personId);
		$scope.person.service.get().then(function(person) {
			$scope.person.person = person;
			$scope.personTitle();
			person.accounts = {};
			_.each(person.accountId, function(accountId) {
				new AccountWebApiService(accountId.value).get().then(function(account) {
					person.accounts[accountId.value] = account;
				});
			});
		}, function(err) {
			$scope.tn.displayErrorPage(err);
		});
		$scope.navigation = {};

		$scope.applicationKey = function(account) {
			return accountUtil.applicationKey(account);
		};

		$scope.aliases = function(account) {
			if(account && account.aliases) {
				return account.aliases.join('/');
			}
		};

		$scope.personTitle = function() {
			$translate('PERSON_TITLE', {
				personName: $scope.person.person.name
			}).then(function(name) {
				browserService.title(name);
				$scope.person.title = name;
			});
		};

		$log.debug('loaded tnPersonController');
	});
});