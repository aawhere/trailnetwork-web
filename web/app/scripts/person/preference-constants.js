define([], function() {
	'use strict';

	var preferences = {};

	preferences.unitsOfMeasure = {
		us: {
			translationKey: 'PREF_UOM_US',
			key: 'US_CUSTOMARY',
		},
		metric: {
			translationKey: 'PREF_UOM_METRIC',
			key: 'METRIC',
		}
	};

	return preferences;
});