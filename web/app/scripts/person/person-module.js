define(['angular',
       'person/person-repository'], function(angular, PersonRepository) {
	'use strict';

	var personModule;

	personModule = angular.module('tn.person', []);

	personModule.service('tnPersonRepository', function() {
		return new PersonRepository();
	});

	return personModule;
});