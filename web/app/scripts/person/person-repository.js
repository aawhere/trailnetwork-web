define(['common/entities'], function(Entities) {
	'use strict';

	var service = {};

	service = function() {
		this.repository = new Entities();
	};

	service.prototype.add = function(people) {
		this.repository.add(people);
	};

	service.prototype.get = function() {
		return this.repository;
	};

	service.prototype.find = function(id) {
		return this.repository.find(id);
	};

	return service;
});