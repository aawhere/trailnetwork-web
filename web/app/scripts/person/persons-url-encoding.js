define(['app-route-constants',
		'common/url-encoding-util',
		'underscore'
	],
	function(appRouteConstants, encodingUtil, _) {
		'use strict';

		/**
		 * Responsible for encoding and decoding url query parameters for persons.
		 *
		 * !n,123;brian
		 *
		 * @param {[type]} conditions [description]
		 */
		var util = {};

		util.constants = {
			PERSON_CODE: 'n',
			PERSON_NAME_DELIMINATOR: '~',
		};

		/**
		 * Encode the query into URL parameters.
		 *
		 * @param  {[type]} query [description]
		 * @return {[type]}       [description]
		 */
		util.encodeQueryParameter = function(persons) {
			var value = '';
			var n = '';

			if (_.isArray(persons) && !_.isEmpty(persons)) {
				if (persons && _.isArray(persons) && persons.length > 0) {
					_.each(persons, function(item) {
						if (n) {
							n += encodingUtil.constants.FILTER_CODE_SEPERATOR;
						}
						n += item.id + util.constants.PERSON_NAME_DELIMINATOR + item.name;
					});
				}
			}
			if (n && n.length > 0) {
				value = encodingUtil.encodeParams(util.constants.PERSON_CODE, n, value);
			}
			return value;
		};

		/**
		 * Decode the query parameter, if present.
		 *
		 * @param  {[type]} queryString the encoded query parameter value
		 * @return {[type]}             array of code => value pairs
		 */
		util.decodeQueryParameter = function(queryString) {
			var people = [];
			var codeToValueMap = encodingUtil.decodeParams(queryString);
			var personValue = codeToValueMap[util.constants.PERSON_CODE];
			if (personValue) {
				var values = personValue.split(encodingUtil.constants.FILTER_CODE_SEPERATOR);
				_.each(values, function(value) {
					if (value) {
						var components = value.split(util.constants.PERSON_NAME_DELIMINATOR);
						people.push({
							name: components[1],
							id: components[0]
						});
					}
				});
			}
			return people;
		};

		return util;

	});