define(['person/person-module',
	//
	'activity/activities-directive',
	'activity/activities-web-api-service'
], function(personModule) {
	'use strict';

	return personModule.directive('tnPersonActivities', function(ActivitiesWebApiService,
			appRouteService
		) {
			return {
				restrict: 'EA',
				template: '<tn-activities activities-web-api-service="activities.service" template-url="{{template.url}}" context="this"></tn-activities>',
				replace: false,
				scope: {
					personId: '=',
					activitiesWebApiService: '=',
					//template url
					templateUrl: '@',
					limit: '=',
				},
				link: {
					pre: function($scope, el, attr) {
						$scope.template = {};
						if (attr.templateUrl) {
							$scope.template.url = attr.templateUrl;
						} else {
							$scope.template.url = '/views/person/person-activities-card.html';
						}
					},
					post: function($scope) {
						$scope.appRouteService = appRouteService;

						$scope.activities = {};

						$scope.configureActivities = function() {
							var service = new ActivitiesWebApiService()
								.limitByPersonId($scope.personId)
								.orderByStartTimeDesc()
								.withAccount();
							if ($scope.limit) {
								service.setLimit($scope.limit);
							}
							$scope.activities.service = service;
							$scope.activitiesWebApiService = service;
							return service;
						};

						$scope.configureActivities().nextPage();
					}
				}
			};
		}
	);
});