define(['activity/activity-module', 'underscore', 'account/account-util'], function(activityModule, _, accountUtil) {
	'use strict';

	/**
	 * Provides a formatted link to a provider
	 *
	 * @param {String} The url of the provider
	 *
	 */
	activityModule.directive('tnProviderAccountLink', function() {
			return {
				restrict: 'EA',
				template: '<a href="{{url}}" target="_blank" ng-click="$event.stopPropagation()">{{userName}} </a>',
				replace: true,
				scope: {
					account: '=',

				},
				link: function(scope) {
					scope.$watch('account', function() {
						if (scope.account) {
							switch (accountUtil.applicationKey(scope.account)) {
								case 'gc':
									scope.userName = accountUtil.userName(scope.account);
									scope.url = 'http://connect.garmin.com/profile/' + accountUtil.userName(scope.account);
									break;
								case 'mm':
									scope.userName = accountUtil.userName(scope.account);
									scope.url = 'http://www.mapmyfitness.com/profile/' + accountUtil.remoteId(scope.account);
									break;
								case 'et':
									scope.userName = accountUtil.userName(scope.account);
									scope.url = 'http://www.everytrail.com/profile.php?user_id=' + accountUtil.remoteId(scope.account);
									break;
								default:
									scope.userName = scope.account.id;
									scope.url = '/account/' + scope.account.id;
							}
						}
					});
				}
			};
		}
	);
});