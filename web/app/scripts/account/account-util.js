define([], function() {
	'use strict';

	var accountUtil = {}, ID_DELIMINATOR = '-';

	accountUtil.applicationKey = function(account) {
		return account.id.split(ID_DELIMINATOR)[0];
	};

	accountUtil.remoteId = function(account) {
		return account.id.split(ID_DELIMINATOR)[1];
	};

	/**Provides the first username from the aliases or the id if no aliases are provided.*/
	accountUtil.userName = function(account) {
		if (account.aliases) {
			return account.aliases[0];
		} else {
			return account.id;
		}
	};

	return accountUtil;
});