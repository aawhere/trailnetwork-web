define([
       'angular'
], function(angular) {
	'use strict';

	var accountModule = angular.module('tn.account', []);

	return accountModule;
});