define(['person/person-module',
	'app-route-constants',
	//
	'person/person-web-api-service',
], function(personModule, rc) {
	'use strict';

	return personModule.controller('tnAccountAliasController', function($scope,
		$stateParams,
		PersonWebApiService,
		$state) {
		new PersonWebApiService().forAlias($stateParams.accountId).get().then(function(person) {
			$state.go(rc.states.personAll, {
				personId: person.id
			});
		}, function(err) {
			$scope.tn.displayErrorPage(err);
		});
	});
});