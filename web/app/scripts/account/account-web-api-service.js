define(['angular',
		'account/account-module',
		'common/tn-api-constants',
		//
		'restangular',
		'common/entity-web-api-service',
	],
	function(angular,
		personModule,
		apiConstants) {
		'use strict';

		/**
		 * Responsible for retreiving route completions.
		 *
		 */
		personModule.factory('AccountWebApiService', function(Restangular, $q, EntityWebApiService) {

				var Account = function(accountId) {
					EntityWebApiService.call(this, {
						entityName: 'account',
						id: accountId,
					});
				};

				Account.inheritsFrom(EntityWebApiService);

				/**
				 * Get the person fronm the server if on already retreived.
				 *
				 * @return {[Promise]} A promise for an array of tn.route.RouteCompletion.
				 */
				Account.prototype.getFromWebApi = function() {
					var deferred = $q.defer();

					Restangular.one(apiConstants.paths.accounts, this.id)
						.get(requestParameters(this)).then(function(account) {
							deferred.resolve(account);
						}, function(err) {
							deferred.reject(err);
						});
					return deferred.promise;
				};

				function requestParameters(context) {
					var params = [];
					params[apiConstants.params.expand] = requestExpand(context);
					return params;
				}

				function requestExpand(context) {
					return context.requestExpand;
				}

				return Account;
			}

		);
	});