define(['angular',
		'route/route-module',
		'map/map-util',
		'route/route-repository',
		'common/common-tn-api-constants',
		'route/route-tn-api-constants',
		//
		'restangular',
	],
	function(angular, routeModule, mapUtil, RouteRepository, tnApiConstants, tnApiRouteConstants) {
		'use strict';

		routeModule.factory('routeService', function(Restangular) {
				var service = {};

				var cachedRest = Restangular.withConfig(function(config) {
					config.setDefaultHttpFields({
						cache: true
					});
				});

				service.queryParams = {
					options: tnApiConstants.OPTIONS,
					routeCourse: 'route-course',
					routeCourses: 'route-courses',
					routeBounds: 'route-bounds',
					gpolyline: 'track-gpolyline',
					routeCourseInline: 'route-course-inline',
					routeStartInline: 'route-start-inline',
					routeFinishInline: 'route-finish-inline',
					applicationReference: 'activity-applicationReference',
					routeTrackGPolyline: 'route-track-gpolyline',
				};

				service.getRouteCourse = function(routeId, application) {
					var queryOptions = {};
					queryOptions.options = service.queryParams.applicationReference;
					if (application) {
						queryOptions.applicationReference = service.queryParams.applicationReference;
						queryOptions.applicationKey = application;
					}
					return Restangular.one(tnApiRouteConstants.paths.routes, routeId)
						.one(tnApiRouteConstants.paths.courses)
						.get(queryOptions);
				};

				service.routeDictionary = function() {
					return cachedRest.one(tnApiRouteConstants.paths.routes).one('dictionary').get();
				};

				return service;
			}
		);
	});