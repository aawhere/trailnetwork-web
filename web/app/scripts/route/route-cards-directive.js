define(['route/route-module',
		'trailhead/trailhead-util',
		'explore/explore-events',
		'common/dictionary-utils',
		'route/route-util',
		'jquery',
		//
		'jqueryMousewheel',
		'common/debug-service',
		'common/app-route-service',
		'explore/explore-location-service',
		'route/route-service',
		'route/activity-types-directive',
		'trailhead/trailhead-web-api-service',
		'spinner'
	],
	function(routeModule,
		thUtil,
		exploreEvents,
		dictionaryUtils,
		routeUtil
	) {
		'use strict';

		/**
		 * Provides a list of cards to be used in a map-cardtray.
		 *
		 * @param {Array(Routes)} The routes to display
		 *
		 */
		routeModule.directive('tnRouteCards', function($rootScope,
			debugService,
			exploreLocationService,
			routeService,
			TrailheadWebApiService,
			usSpinnerService,
			routeCardsSpinnerKey,
			appRouteService,
			$timeout,
			$q) {
			return {
				restrict: 'EA',
				transclude: true,
				templateUrl: '/views/route/route-cards-directive.html',
				replace: false,
				scope: {
					// 'route/routes.js'
					routes: '=',
					//true if more routes are available.
					hasNextPage: '=',
					//callback to get more routes.
					nextPage: '&',
					//show the legend
					showLegend: '@',
					busy: '=',
					messageKey: '=',
				},
				link: function(scope, element, attrs) {
					var showLegendParsed = false;

					scope.debug = debugService.enabled;
					showLegendParsed = scope.$eval(attrs.showLegend);
					scope.dictionary = {};

					scope.appRouteService = appRouteService;

					routeService.routeDictionary().then(function(dictionary) {
						scope.dictionary.elevationGain = dictionaryUtils.findByKey(dictionary, 'route-course-track-elevationGain');
					});

					scope.mouseoverTh = function(id) {
						$rootScope.$broadcast(exploreEvents.trailheadHighlighted, id);
					};

					scope.mouseleaveTh = function(id) {
						$rootScope.$broadcast(exploreEvents.trailheadUnHighlighted, id);
					};

					scope.mouseoverRoute = function(id) {
						$rootScope.$broadcast(exploreEvents.routeHighlighted, id);
					};

					scope.mouseleaveRoute = function(id) {
						$rootScope.$broadcast(exploreEvents.routeUnHighlighted, id);
					};

					scope.focusTrailhead = function(id) {
						if (scope.selectedTh && scope.selectedTh.id === id) {
							$rootScope.$broadcast(exploreEvents.trailheadUnSelected, id);
						} else {
							$rootScope.$broadcast(exploreEvents.trailheadSelected, id);
						}
					};

					scope.$on(exploreEvents.trailheadHighlighted, function(event, id) {
						findTrailhead(id).then(function(th) {
							if (th) {
								scope.highlightedTh = th;
							}
						});
					});

					scope.$on(exploreEvents.trailheadUnHighlighted, function() {
						scope.highlightedTh = undefined;
					});

					scope.$on(exploreEvents.trailheadSelected, function(event, id) {
						findTrailhead(id).then(function(th) {
							if (th) {
								scope.selectedTh = th;
							}
						});
					});

					scope.$on(exploreEvents.trailheadUnSelected, function(event, id) {
						if (scope.selectedTh && (scope.selectedTh.id === id)) {
							scope.selectedTh = undefined;
						}
					});

					scope.$on(exploreEvents.routeHighlighted, function(event, id) {
						var route = scope.routes.find(id);
						if (route) {
							scope.highlighted = route;
							scope.mouseoverTh(route.startTrailheadId.value);
						}
					});

					scope.$on(exploreEvents.routeUnHighlighted, function(event, id) {
						var route = scope.routes.find(id);
						if (route) {
							scope.highlighted = undefined;
							scope.mouseleaveTh(route.startTrailheadId.value);
						}
					});

					scope.$on(exploreEvents.routeSelected, function(event, id) {
						var route = scope.routes.find(id);
						if (route) {
							scope.selected = route;
						}
					});

					scope.$on(exploreEvents.routeUnSelected, function(event, id) {
						var route = scope.routes.find(id);
						if (route) {
							scope.selected = undefined;
						}
					});

					scope.$watch('busy', function() {
						if (scope.busy === true) {
							usSpinnerService.spin(routeCardsSpinnerKey);
						} else {
							$timeout(function() {
								usSpinnerService.stop(routeCardsSpinnerKey);
							}, 1);
						}
				});

					element.find('#route-cards-scroll-containter').mousewheel(function(e) {

						if (!Math.abs(e.deltaX)) {
							// most likely not trackpad
							this.scrollLeft -= (e.deltaY * 40);
							e.preventDefault();
						}

					});

					scope.displayLegend = function() {
						if (showLegendParsed && (scope.routes && !scope.routes.isEmpty())) {
							return true;
						} else {
							return false;
						}
					};

					function findTrailhead(id) {
						var routesWithTrailhead = routeUtil.findRoutesWithTrailhead(scope.routes, id);
						var th;
						var deferred = $q.defer();
						if (routesWithTrailhead.length > 0 && routesWithTrailhead[0].startTrailhead) {
							th = routesWithTrailhead[0].startTrailhead;
							deferred.resolve(th);
						} else {
							new TrailheadWebApiService(id).get().then(function(th) {
								deferred.resolve(th);
							});
						}
						return deferred.promise;
					}
				}
			};
		});
	});