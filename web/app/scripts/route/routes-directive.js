define(['route/route-module',
		'underscore',
		'angular',
		'explore/explore-events',
		'common/common-util',
		'map/location',
		//
		'map/map-overlay'
	],
	function(routeModule,
	         _,
	         angular,
	         exploreEvents,
	         commonUtils,
	         Location) {
		'use strict';

		/**
		 * Provides the route directive, which efficiently displays a list of routes on a map.
		 *
		 * @param {object}
		 *            routes: array of routes.
		 *
		 * @param {object}
		 *            selected: [SelectedRoutes]
		 *
		 * {
		 *     route //The tn.route.Route
		 *     options //The google.maps.PolylineOptions
		 *     category //optional category which is used herein (not on the route directive);
		 * }
		 *
		 */
		routeModule.directive('tnRoutes', function($timeout, mapOverlay, $rootScope, $log) {
			return {
				restrict: 'ECMA',
				require: '^tnMap',
				transclude: false,
				template: '',
				replace: false,
				scope: {
					routes: '=',
					//can one click on the route and have it change color?
					selectable: '=',
					selectedOptions: '=',
					//should we highlight the route?
					highlight: '=',
					highlightOptions: '=',
					options: '=',
				},
				link: function(scope, element, attr, mapCtrl) {
					var defaultSelectedOptions = {
						strokeColor: "#ff4343",
						strokeOpacity: 1,
						strokeWeight: 5,
						zIndex: 1,
					};

					scope.polylines = {};
					scope.defaultOptions = {
						strokeColor: "#000000",
						strokeOpacity: 0.6,
						strokeWeight: 3,
						zIndex: 0
					};
					var unselectedOptions = _.extend(scope.defaultOptions, scope.options);
					var selectedOptions = _.extend(defaultSelectedOptions, scope.selectedOptions);
					var highlightOptions = _.extend(defaultSelectedOptions, scope.highlightOptions);

					scope.$watch(function() {
						return mapCtrl.getGoogleMap();
					}, function(map) {
						if (map) {
							scope.map = map;
							setupWatches();
						}
					});

					function highlight(routeId) {
						var polyline = findPolyline(routeId);
						if (scope.highlight && polyline) {
							mapOverlay.updateOverlay(polyline, highlightOptions);
						}
					}

					function unhighlight(routeId) {
						var polyline = findPolyline(routeId);
						if (scope.highlight && (scope.selected !== routeId) && polyline) {
							mapOverlay.updateOverlay(polyline, unselectedOptions);
						}
					}

					function select(routeId) {
						//console.log('selecting', routeId);
						var polyline = findPolyline(routeId);
						if (polyline) {
							mapOverlay.updateOverlay(polyline, selectedOptions);
						}
					}

					function unselect(routeId) {
						var polyline = findPolyline(routeId);
						//console.log('unselect', routeId, scope.selectable, polyline);
						if (polyline) {
							mapOverlay.updateOverlay(polyline, unselectedOptions);
						}
					}

					/**
					 * Somtimes things get dirty, like when scope.selectable is changed and an
					 * event comes through before the digest has a chance to update this directive.
					 *
					 * @return {undefined}
					 */
					function updateSelection() {
						_.each(scope.polylines, function(polyline, routeId) {
							if (routeId === scope.selected) {
								select(routeId);
							} else {
								unselect(routeId);
							}
						});
					}

					function setupWatches() {
						//Watch for route changes and update smartly so that
						//existing polylines are not regdrawn (which makes the screen flicker)
						scope.$watch('routes.version', function() {
							if (scope.routes) {
								var toDelete, toCreate, polylineKeys, routeIds;
								polylineKeys = commonUtils.arrayOfStringToArrayOfNumberOrString(_.keys(scope.polylines));
								routeIds = idsFromRoutes(scope.routes);
								toDelete = _.difference(polylineKeys, routeIds);
								toCreate = _.difference(routeIds, polylineKeys);
								clearPolylines(toDelete);
								createPolylines(toCreate);
							}
						});

						scope.$watch('selectable', function(n) {
							//console.log('watch selectable', n, p);
							if (n === true) {
								updateSelection();
							}
						});

						scope.$on(exploreEvents.routeHighlighted, function(event, routeId) {
							if (scope.highlight) {
								highlight(routeId);
								scope.highlighted = routeId;
							}
						});

						scope.$on(exploreEvents.routeUnHighlighted, function(event, routeId) {
							if (scope.highlight) {
								unhighlight(routeId);
								scope.highlighted = undefined;
							}
						});

						scope.$on(exploreEvents.routeSelected, function(event, routeId) {
							$log.debug('route selected', routeId);
							if (_.isUndefined(scope.selected)) {
								select(routeId);
							} else {
								unselect(scope.selected);
								select(routeId);
							}
							scope.selected = routeId;
						});

						scope.$on(exploreEvents.routeUnSelected, function(event, routeId) {
							unselect(routeId);
						});

						scope.$on(exploreEvents.routeUnSelectAll, function() {
							//console.log('onUnSelectAll', scope);
							scope.selected = undefined;
							_.each(_.keys(scope.polylines), function(routeId) {
								unselect(routeId);
							});
						});
					}

					function clearPolylines(routeIds) {
						//console.log('clearing polylines for routes', routeIds);
						_.each(routeIds, function(routeId) {
							clearPolyline(routeId);
						});
					}

					function clearPolyline(routeId) {
						var polyline = findPolyline(routeId);
						mapOverlay.destroyOverlay(polyline);
						delete scope.polylines[routeId];
					}

					function findPolyline(routeId) {
						return scope.polylines[routeId];
					}

					function findRoute(routeId) {
						return scope.routes.find(routeId);
					}

					function createPolylines(routeIds) {
						_.each(routeIds, function(routeId) {
							createPolyline(routeId);
						});
					}


					function createPolyline(routeId) {
						var route, polyline;
						route = findRoute(routeId);
						if (route) {
							if (angular.isDefined(scope.polylines[routeId])) {
								clearPolyline(routeId);
							}
							//If the request to select the route is before the route actually is availble, select it.
							var polylineOptions = unselectedOptions;
							if (scope.selected && scope.selected === routeId) {
								polylineOptions = selectedOptions;
							}
							polylineOptions.clickable = scope.selectable;
							polyline = mapOverlay.createPolyline(scope.map,
								mapOverlay.decode(route.points), polylineOptions);

							mapOverlay.addListener(polyline, 'click', function() {
								scope.$apply(function() {
									//console.log('onclick', scope.selected, routeId);
									$rootScope.$broadcast(exploreEvents.routeSelected, routeId);
								});
							});

							mapOverlay.addListener(polyline, 'mouseover', function(event) {
								scope.$apply(function() {
									//$log.debug('mouseover polyline', routeId);
									$rootScope.$broadcast(exploreEvents.routeHighlighted, routeId, Location.createFromGoogleLatLng(event.latLng));
								});
							});

							mapOverlay.addListener(polyline, 'mouseout', function() {
								scope.$apply(function() {
									$rootScope.$broadcast(exploreEvents.routeUnHighlighted, routeId);
								});
							});

							scope.polylines[routeId] = polyline;
						}
					}

					function idsFromRoutes(routes) {
						var ids = [];
						_.each(routes, function(route) {
							ids.push(route.id);
						});
						return commonUtils.arrayOfStringToArrayOfNumberOrString(ids);
					}

				}
			};
		});
	});