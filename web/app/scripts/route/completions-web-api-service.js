define(['angular',
		'route/route-module',
		'route/route-filter-util',
		'route/route-util',
		'common/common-util',
		'common/tn-api-constants',
		'common/filter-condition-operators',
		//
		'restangular',
		'common/entities-web-api-service',
	],
	function(angular,
		routeModule,
		routeFilterUtil,
		routeUtil,
		commonUtil,
		apiConstants,
		operators) {
		'use strict';

		/**
		 * Responsible for retreiving route completions.
		 *
		 */
		routeModule.factory('CompletionsWebApiService', function(Restangular, $q, EntitiesWebApiService, $log) {

				var Completions = function() {
					EntitiesWebApiService.call(this, {
						entitiesPropertyName: 'routeCompletion',
					});
					this.coursesOnly = false;
					//make the limit smaller for completions so that their load time is more responsive.
				};

				Completions.inheritsFrom(EntitiesWebApiService);

				/**
				 * Limit conditions to RouteId
				 *
				 * @param  {Number} routeId [description]
				 * @return {this}         [description]
				 */
				Completions.prototype.limitByRouteId = function(routeId) {
					$log.debug('CompletionsWebApiService with routeId', routeId);
					if (routeId) {
						this.routeId = routeUtil.routeIdAsNumber(routeId);
					}
					return this;
				};

				/**
				 * Limit conditions to EventId
				 *
				 * @param  {Number} routeId [description]
				 * @return {this}         [description]
				 */
				Completions.prototype.limitByEventId = function(eventId) {
					$log.debug('CompletionsWebApiService with eventId', eventId);
					if (eventId) {
						this.addFilterCondition(apiConstants.fields.completionRouteEventId, operators.in, eventId);
					}
					return this;
				};

				/**
				 * Limit results to the provided filter.
				 *
				 * @param  {RouteFitler} filter
				 * @return {this}
				 */
				Completions.prototype.filteredByRouteFilter = function(routeFitler) {
					$log.debug('CompletionsWebApiService with routeFilter');
					this.routeFilter = routeFitler;
					return this;
				};

				/**
				 * Limit results to the provided people.
				 *
				 * @param  {String or Array[String]} people
				 * @return {this}
				 */
				Completions.prototype.limitByPersonId = function(personIds) {
					if (personIds && !_.isArray(personIds)) {
						personIds = [personIds];
					}
					if (personIds) {
						personIds = commonUtil.idsAsNumbers(personIds);
					}
					if (personIds && personIds.length > 0) {
						this.addFilterCondition(apiConstants.fields.completionPersonId, operators.in, _.reduce(personIds, function(memo, personId) {
							return memo + ' ' + personId;
						}, ''));
					}
					$log.debug('CompletionsWebApiService with people', personIds);
					return this;
				};

				Completions.prototype.limitByActivityId = function(activityId) {
					if (activityId) {
						this.addFilterCondition(apiConstants.fields.completionActivityId, operators.in, activityId);
					}
					return this;
				};

				Completions.prototype.limitToCourses = function() {
					$log.debug('CompletionsWebApiService courses only. All other filtes will be ignored.');
					this.coursesOnly = true;
				};

				Completions.prototype.limitBySocialRaceRank = function(socialRaceRank) {
					if (socialRaceRank && !_.isArray(socialRaceRank)) {
						socialRaceRank = [socialRaceRank];
					}
					if (socialRaceRank) {
						this.addFilterCondition(apiConstants.fields.completionSocialRaceRank, operators.in, socialRaceRank);
					}
					return this;
				};

				/**
				 * Include the route with the response.
				 * @return {[type]} [description]
				 */
				Completions.prototype.withRoute = function() {
					this.expandFields(apiConstants.fields.completionRouteId);
					return this;
				};

				/**
				 * Include the person with the response.
				 * @return {[type]} [description]
				 */
				Completions.prototype.withPerson = function() {
					this.expandFields(apiConstants.fields.completionPersonId);
					return this;
				};

				/**
				 * Include the route event with the response.
				 * @return {[type]} [description]
				 */
				Completions.prototype.withEvent = function() {
					this.expandFields(apiConstants.fields.completionRouteEventId);
					return this;
				};

				/**
				 * Include the activity with the response.
				 * @return {[type]} [description]
				 */
				Completions.prototype.withActivity = function() {
					this.expandFields(apiConstants.fields.completionActivityId);
					return this;
				};

				Completions.prototype.orderBySocialOrder = function() {
					this.orderBy(apiConstants.fields.completionSocialOrder, apiConstants.orderBy.descending);
					return this;
				};

				Completions.prototype.orderBySocialCountRank = function() {
					this.orderBy(apiConstants.fields.completionSocialCountRank, apiConstants.orderBy.ascending);
					return this;
				};

				Completions.prototype.orderBySocialRaceRank = function() {
					this.orderBy(apiConstants.fields.completionSocialRaceRank, apiConstants.orderBy.ascending);
					return this;
				};

				Completions.prototype.orderByStartTimeDesc = function() {
					this.orderBy(apiConstants.fields.completionStartTime, apiConstants.orderBy.descending);
					return this;
				};

				Completions.prototype.orderByPersonalOrder = function() {
					this.orderBy(apiConstants.fields.completionPersonalOrder, apiConstants.orderBy.descending);
					return this;
				};

				Completions.prototype.orderByPersonalTotalDesc = function() {
					this.orderBy(apiConstants.fields.completionPersonalTotal, apiConstants.orderBy.descending);
					return this;
				};

				Completions.prototype.tempPersonRaceRank = function() {
					this.tempPersonRaceRankFlag = true;
					return this;
				};

				/**
				 * Get the next page of completions. If this is the first request, retreives the first page of results.
				 *
				 * @return {[Promise]} A promise for an array of tn.route.RouteCompletion.
				 */
				Completions.prototype.makeRequest = function() {
					var deferred = $q.defer();
					var restBuilder = getRestBuilder(this);

					restBuilder.get(requestParameters(this)).then(function(completions) {
						if (completions.routeCompletion) {
							addActivityIds(completions.routeCompletion);
						}
						deferred.resolve(completions);
					}, function(err) {
						deferred.reject(err);
					});
					return deferred.promise;
				};

				function getRestBuilder(context) {
					var builder = Restangular;
					if (context.routeId) {
						builder = builder.one(apiConstants.paths.routes, context.routeId);
					} else {
						builder = builder.one(apiConstants.paths.routes);
					}
					return builder.one(apiConstants.paths.completions);
				}

				function requestParameters(context) {
					var params = {};
					if (context.routeFilter) {
						conditionsFromRouteFilter(context.routeFilter, context);
					}

					if (context.tempPersonRaceRankFlag) {
						params.orderByInMemory = 'routeCompletion-socialRaceRank';
						params.predicateConditions = ['routeCompletion-routeScore > 0',
							'routeCompletion-personalRaceRank = 1'
						];
						params.limit = 1000;
					}

					//This ignores all other conditions. Should be last. It is a debug feature, which we know ignores all other conditions.
					if (context.coursesOnly) {
						context.clearOrderBy();
						context.clearRequestConditions();
						context.addFilterCondition(apiConstants.fields.completionIsCourse, operators.equals, 'true');
					}

					return _.extend(context.requestParameters, params);
				}

				/**
				 * Produce a set of conditions for use with the api from the routeFilter
				 *
				 * @param  {RouteFilter} routeFilter [description]
				 * @param  {EntitiesWebApiService}
				 */
				function conditionsFromRouteFilter(routeFilter, context) {
					routeFilterUtil.condition.activityType(routeFilter, apiConstants.fields.completionActivityType, context);
					routeFilterUtil.condition.application(routeFilter, context);
				}

				function addActivityIds(routeCompletions) {
					_.each(routeCompletions, function(routeCompletion) {
						routeCompletion.activityIds = [];
						routeCompletion.activityIds.push(routeCompletion.activityId.value);
						if (routeCompletion.duplicateActivityIds) {
							routeCompletion.activityIds = _.union(routeCompletion.activityIds, routeCompletion.duplicateActivityIds);
						}
					});
				}

				return Completions;
			}

		);
	});