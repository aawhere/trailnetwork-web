define(['underscore', 'route/route-util'], function(_, routeUtil) {
	'use strict';
	var routeFunction, defaultCategoryIfUndefined, DEFAULT_CATEGORY;

	DEFAULT_CATEGORY = 'default';

	defaultCategoryIfUndefined = function(category) {
		return (category) ? category : DEFAULT_CATEGORY;
	};

	routeFunction = function(filter) {
		this.routes = {};

		/**
		 * The number of routes to display per trailhead. Trailheads displayed
		 * are the highest scored routes in decending order by score.
		 *
		 * Set to undefined or 0 to allow any number of trailheads per route.
		 *
		 * @type {Integer}
		 */
		this.DEFAULT_NUM_OF_ROUTES_PER_TRAILHEAD = 2;
		this.numberOfRoutesPerTrailhead = this.DEFAULT_NUM_OF_ROUTES_PER_TRAILHEAD;

		if (_.isUndefined(filter)) {
			throw new Error('filter is required!');
		}
		this.routeFilter = filter;
	};



	/**
	 * Adds the route to the collection with an optional category.
	 *
	 * @param {tn.route.Route} route      [description]
	 * @param {String} category the category to assign it to
	 */
	routeFunction.prototype.add = function(route, category) {
		var thRoutes, context;

		context = this;
		category = defaultCategoryIfUndefined(category);
		route = (_.isArray(route)) ? route : [route];

		_.each(route, function(r) {

			if (r && r.startTrailheadId) {
				if (!context.routes[category]) {
					context.routes[category] = {};
				}

				if (!context.routes[category][r.startTrailheadId]) {
					thRoutes = [];
				} else {
					thRoutes = context.routes[category][r.startTrailheadId];
				}

				thRoutes.push(r);

				thRoutes = _.uniq(thRoutes, function(r) {
					return r.id;
				});
				thRoutes = routeUtil.sortByScoreDesc(thRoutes);
				context.routes[category][r.startTrailheadId] = thRoutes;
			}
		});
	};

	routeFunction.prototype.get = function(category) {
		var context = this;
		return _.flatten(_.values(context.getByTrailhead(category)));
	};

	/**
	 * Returns an object where the keys are trailheads and the value
	 * is an Array of routes. The Array of routes are sorted by score (desc).
	 *
	 * The numberOfRoutesPerTrailhead limit is not applied (@see filter)
	 *
	 * @param  {[type]} category [description]
	 * @return {[type]}          [description]
	 */
	routeFunction.prototype.getByTrailhead = function(category) {
		category = defaultCategoryIfUndefined(category);
		if (!this.routes[category]) {
			return [];
		}
		return this.routes[category];
	};

	routeFunction.prototype.filter = function(category) {
		var all, result, context = this;
		result = [];
		all = this.getByTrailhead(category);

		_.each(all, function(routes) {
			var filtered = context.routeFilter.filterAll(routes);
			if (context.numberOfRoutesPerTrailhead >= 0) {
				result = _.union(result, filtered.slice(0, context.numberOfRoutesPerTrailhead));
			} else {
				result = _.union(result, filtered);
			}
		});

		return routeUtil.sortByScoreDesc(result);
	};

	routeFunction.prototype.clearAll = function() {
		this.routes = {};
	};

	routeFunction.prototype.find = function(routeId, category) {
		return _.find(this.get(category), function(r) {
			//Watch the types here. == does type conversion, === does not.
			return r.id == routeId; // jshint ignore:line
		});
	};

	return routeFunction;
});