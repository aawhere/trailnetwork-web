define(['route/route-module', 'explore/explore-events', 'underscore'],
	function(routeModule, exploreEvents, _) {
		'use strict';

		routeModule.service('routeFilterService', function($rootScope, $log) {

			var broadcastRouteFilterChanged = function() {
				$log.info('exploreEvents.routeFilterUpdated');
				$rootScope.$broadcast(exploreEvents.routeFilterUpdated);
			};


			var Service = function() {

				this.encodeUrlParams = true;

				this.conditions = {};

				this.initalizeConditions = function() {
					this.conditions.activityType = undefined;
					this.conditions.distance = undefined;
					this.conditions.application = undefined;
					this.conditions.persons = undefined;
				};

				this.initalizeConditions();
			};

			/**
			 * Clear activity type filter.
			 *
			 * @return {[type]} [description]
			 */
			Service.prototype.clearActivityType = function() {
				if (!_.isUndefined(this.conditions.activityType)) {
					this.conditions.activityType = undefined;
					this.conditions.distance = undefined;
					broadcastRouteFilterChanged();
				}
			};

			/**
			 * True if it has activity type set, false otherwise.
			 *
			 * @return {Boolean} [description]
			 */
			Service.prototype.hasActivityType = function() {
				return this.conditions.activityType ? true : false;
			};

			/**
			 * Getter/Setter for activity type filter.
			 *
			 * @param  {String} activityTypeKey The key of the activity type.
			 * @return {activityTypeKey}              [description]
			 */
			Service.prototype.activityType = function(activityTypeKey) {
				if (activityTypeKey && activityTypeKey !== this.conditions.activityType) {
					this.conditions.activityType = activityTypeKey;
					broadcastRouteFilterChanged();
				} else {
					return this.conditions.activityType;
				}
			};

			/**
			 * Clear distance filter.
			 *
			 * @return {[type]} [description]
			 */
			Service.prototype.clearDistance = function() {
				if (!_.isUndefined(this.conditions.distance)) {
					this.conditions.distance = undefined;
					broadcastRouteFilterChanged();
				}
			};

			/**
			 * True if it has distance set, false otherwise.
			 *
			 * @return {Boolean} [description]
			 */
			Service.prototype.hasDistance = function() {
				return this.conditions.distance ? true : false;
			};

			/**
			 * Getter/Setter for distance filter.
			 *
			 * @param  {String} distance The key of the distance.
			 * @return {null}              [description]
			 */
			Service.prototype.distance = function(distance) {
				if (distance) {
					this.conditions.distance = distance;
					broadcastRouteFilterChanged();
				} else {
					return this.conditions.distance;
				}
			};

			/**
			 * Clear application filter.
			 *
			 * @return {[type]} [description]
			 */
			Service.prototype.clearApplication = function() {
				if (!_.isUndefined(this.conditions.application)) {
					this.conditions.application = undefined;
					broadcastRouteFilterChanged();
				}
			};

			/**
			 * True if it has application set, false otherwise.
			 *
			 * @return {Boolean} [description]
			 */
			Service.prototype.hasApplication = function() {
				return this.conditions.application ? true : false;
			};

			/**
			 * Getter/Setter for application filter.
			 *
			 * @param  {String} application The key of the application.
			 * @return {null}              [description]
			 */
			Service.prototype.application = function(application) {
				if (application) {
					this.conditions.application = application;
					broadcastRouteFilterChanged();
				} else {
					return this.conditions.application;
				}
			};

			/**
			 * Clear people filter.
			 *
			 * @return {[type]} [description]
			 */
			Service.prototype.clearPersons = function() {
				if (!_.isUndefined(this.conditions.persons)) {
					this.conditions.persons = undefined;
					broadcastRouteFilterChanged();
				}
			};

			/**
			 * True if it has persons set, false otherwise.
			 *
			 * @return {Boolean} [description]
			 */
			Service.prototype.hasPersons = function() {
				var result;
				if (this.conditions.persons && !_.isEmpty(this.conditions.persons)) {
					result = true;
				} else {
					result = false;
				}
				return result;
			};

			/**
			 * Getter/Setter for persons filter.
			 *
			 * @param  {String} persons The key of the persons.
			 * @return {null}              [description]
			 */
			Service.prototype.persons = function(persons) {
				if (persons) {
					// if(_.isArray(persons) && _.isEmpty(persons)) {
					// 	persons = undefined;
					// }
					this.conditions.persons = persons;
					broadcastRouteFilterChanged();
					console.log('persons filter changed', this.conditions.persons);
				} else {
					return this.conditions.persons;
				}
			};

			Service.prototype.hasConditions = function() {
				return this.hasActivityType() ||
					this.hasDistance() ||
					this.hasPersons() ||
					this.hasApplication();
			};

			Service.prototype.clearAll = function() {
				var hadConditions = this.hasConditions();
				this.initalizeConditions();
				if (hadConditions) {
					broadcastRouteFilterChanged();
				}
			};

			return new Service();
		});
	});