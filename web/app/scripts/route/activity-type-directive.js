define(['route/route-module',
	'route/activityTypes',
		'route/activity-type-aliases',
], function(routeModule, activityTypes, activityTypeAliases) {
	'use strict';

	/**
	 * Provides a list of cards to be used in a map-cardtray.
	 *
	 * @param {tn.ActivityTypeStats} The stats to display
	 *
	 */
	routeModule.directive('tnActivityType', function() {
		return {
			restrict: 'EA',
			transclude: false,
			templateUrl: '/views/route/activity-type-directive.html',
			replace: false,
			scope: {
				activityType: '=',
				//css styles for icon, space delimited.
				iconStyles: '=',
			},
			link: function(scope) {

				var processActivityType = function(activityType) {
					var atKey = activityType.key;
					var mappingKey = activityTypeAliases[atKey];
					activityType.icon = _.find(activityTypes, function(at) {
						return at.key === mappingKey;
					}).icon;
					return activityType;
				};

				scope.$watch('activityType', function() {
					if (scope.activityType) {
						scope.activityType = processActivityType(scope.activityType);
					}
				});
			}
		};
	});
});