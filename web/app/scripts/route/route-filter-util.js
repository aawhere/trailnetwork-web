define(['route/route-tn-api-constants',
	'common/tn-api-constants',
	'underscore',
	'common/common-util',
	'common/filter-condition-operators',
], function(routeTnApiConstants,
	tnApiConstants,
	_,
	commonUtil,
	operators) {
	'use strict';

	var util = {};

	util.personsAsIdList = function(persons) {
		return commonUtil.arrayToApiString(_.pluck(persons, 'id'));
	};

	util.condition = {
		activityType: function(routeFilter, domainKey, entitiesWebApiService) {
			var key = domainKey;
			if (!key) {
				key = tnApiConstants.fields.routeActivityType;
			}
			var activityType = routeFilter.activityType();
			if (activityType) {
				entitiesWebApiService.addFilterCondition(key, operators.equals, activityType);
				return true;
			} else {
				return false;
			}
		},
		application: function(routeFilter, entitiesWebApiService) {
			var application = routeFilter.application();
			if (application) {
				entitiesWebApiService.addFilterCondition(routeTnApiConstants.conditions.routeCompletionApplicationStatsApplication, operators.equals, application);
				return true;
			} else {
				return false;
			}
		},
		distance: function(routeFilter, entitiesWebApiService) {
			var distance = routeFilter.distance();
			if (distance) {
				entitiesWebApiService.addFilterCondition(tnApiConstants.fields.routeDistanceCategory, operators.equals, distance);
				return true;
			} else {
				return false;
			}
		},
		persons: function(routeFilter, entitiesWebApiService) {
			var persons = routeFilter.persons();
			if (_.isArray(persons) && !_.isEmpty(persons)) {
				entitiesWebApiService.addFilterCondition(tnApiConstants.fields.routeCompletionPersonId, operators.in, util.personsAsIdList(persons));
				return true;
			} else {
				return false;
			}
		}
	};

	return util;
});