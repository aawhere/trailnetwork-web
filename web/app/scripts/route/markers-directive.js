define(['route/route-module',
		'underscore',
		'angular',
		'explore/explore-events',
		'map/location',
		//
		'map/map-overlay'
	],
	function(routeModule, _, angular, exploreEvents, Location) {
		'use strict';

		/**
		 * Provides the markers directive, which displays a list of locations on a map.
		 *
		 * @param {object}
		 *            locations: Array[tn.measure.GeoCorrdinate].
		 *
		 */
		routeModule.directive('tnMarkers', function($timeout, mapOverlay, $rootScope) {
			return {
				restrict: 'ECMA',
				require: '^tnMap',
				transclude: false,
				template: '',
				replace: false,
				scope: {
					// Array[tn.measure.GeoLocation]
					locations: '=',
					defaultOptions: '=',
				},
				link: function(scope, element, attr, mapCtrl) {

					scope.markers = [];

					var defaultOptions = angular.extend({
						icon: {
							path: google.maps.SymbolPath.CIRCLE,
							fillColor: '#0099cb',
							fillOpacity: 0.8,
							strokeColor: '#000000',
							strokeWeight: 2,
							strokeOpacity: 0.4,
							scale: 7
						}
					}, scope.defaultOptions);

					scope.$watch(function() {
						return mapCtrl.getGoogleMap();
					}, function(map) {
						if (map) {
							scope.map = map;
							setupWatches();
						}
					});

					function setupWatches() {
						//Watch for location changes
						scope.$watchCollection('locations', function() {
							//console.log('locations changed');
							if (scope.locations) {
								createMarkers();
							}
						});
					}

					// function clearMarkers() {
					// 	//console.log('clearing markers');
					// 	_.each(scope.markers, function(marker) {
					// 		clearMarker(marker);
					// 	});
					// }

					function clearMarker(marker) {
						mapOverlay.destroyOverlay(marker);
						scope.markers = _.without(scope.markers, marker);
					}

					/**
					 * Updates the markers based on the scope.locations.
					 * Attempts to reuse markers as much as possible while keeping
					 * the marker's index and scope.location index synced so that
					 * dot races maintain the current color/shape.
					 *
					 * @return {[type]} [description]
					 */
					function createMarkers() {
						var i = 0;
						_.each(scope.locations, function(location) {
							var marker = scope.markers[i];
							if (marker) {
								marker.setPosition(Location.createFromLocation(location).googleLatLng());
							} else {
								createMarker(location);
							}
							i++;
						});
						while (scope.markers[i]) {
							clearMarker(scope.markers[i]);
						}
					}

					function createMarker(location) {
						//console.log('creating marker for location', location);
						var marker;

						if (location && location.latitude && location.latitude.value) {
							var options = angular.extend({}, defaultOptions, location.options);
							//console.log('creating marker for location ', location);
							marker = mapOverlay.createMarker(scope.map,
								location, options);

							mapOverlay.addListener(marker, 'click', function() {
								scope.$apply(function() {
									$rootScope.$broadcast(exploreEvents.markerSelected, Location.createFromGoogleLatLng(marker.getPosition()));
								});
							});


							mapOverlay.addListener(marker, 'mouseover', function() {
								scope.$apply(function() {
									$rootScope.$broadcast(exploreEvents.markerHighlighted, Location.createFromGoogleLatLng(marker.getPosition()));
								});
							});

							mapOverlay.addListener(marker, 'mouseout', function() {
								scope.$apply(function() {
									$rootScope.$broadcast(exploreEvents.markerUnHighlighted, Location.createFromGoogleLatLng(marker.getPosition()));
								});
							});

							scope.markers.push(marker);
						}
					}
				}
			};
		});
	});