define(['explore/explore-events',
	'route/completions-url-encoding',
	'app-route-constants',
	'route/completions-util',
	'common/common-util'
], function(exploreEvents, completionsUrlEncoder, appRouteConstants, completionsUtil, commonUtil) {
	'use strict';

	var BaseCompletionsController = function($scope, $log, $location, routeFilterService, locationService, CompletionsWebApiService) {
		$scope.completions = {};
		$scope.completions.courses = false;
		$scope.completions.people = [];
		$scope.routeFilterService = routeFilterService;
		$scope.completions.activityTypeKeys = [];
		$scope.completions.activityType = null;
		$scope.events = {};

		$scope.$watch('route.route', function() {
			if ($scope.route.route) {
				$scope.completions.activityTypeKeys = completionsUtil.activityTypeKeys($scope.route.route);
			}
		});

		$scope.$on(exploreEvents.routeFilterUpdated, function() {
			$log.debug('routeFilterUpdated', $scope.completions.courses);
			$scope.configureCompletions().nextPage();
			$scope.encodeCompletionsSearchParams();
		});

		$scope.$watch('completions.people', function(people) {
			if (!_.isUndefined(people)) {
				$log.debug('completions.people changed', $scope.completions.people);
				$scope.completions.peopleIds = _.pluck($scope.completions.people, 'id');
				$scope.configureCompletions().nextPage();
				$scope.encodeCompletionsSearchParams();
			}
		}, true);

		$scope.$watch('events.eventId', function(eventId, previous) {
			if (eventId !== previous) {
				$log.debug('events.eventId changed', $scope.events.eventId);
				$scope.configureCompletions().nextPage();
				$scope.encodeCompletionsSearchParams();
			}
		});

		$scope.$watch('completions.courses', function(courses, previous) {
			if (courses !== previous) {
				$log.debug('completions.courses changed', $scope.completions.courses);
				$scope.configureCompletions().nextPage();
				$scope.encodeCompletionsSearchParams();
			}
		});

		$scope.addFocusOnPerson = function(person) {
			if (!$scope.isFocusedOn(person)) {
				$scope.completions.people.push({
					id: person.id,
					text: person.name,
				});
			}
		};

		$scope.isFocusedOn = function(person) {
			if (_.find($scope.completions.people, function(p) {
				return person.id === commonUtil.idAsNumber(p.id);
			})) {
				return true;
			} else {
				return false;
			}
		};

		$scope.encodeCompletionsSearchParams = function() {
			$log.debug('encodeCompletionsSearchParams');
			var query = {
				people: $scope.completions.people,
				routeEvent: $scope.events.eventId,
			};
			var params = completionsUrlEncoder.encode(query);
			if (params[appRouteConstants.routeCompletionQueryParameterName]) {
				locationService.extendLocationSearch(params);
			} else {
				locationService.removeLocationSearchParameter(appRouteConstants.routeCompletionQueryParameterName);
			}
		};

		$scope.decodeCompletionsSearchParams = function() {
			var search = $location.search();
			$log.debug('decodeCompletionsSearchParams', search);

			try {
				var query = completionsUrlEncoder.decode(search[appRouteConstants.routeCompletionQueryParameterName]);
				$scope.completions.people = query.people;
				$scope.events.eventId = query.routeEvent;
			} catch (err) {
				//Invalid input.
				$log.error('error parsing completions parameters: ' + err);
			}
		};

		$scope.configureCompletionsBuilder = function() {
			var builder = new CompletionsWebApiService()
				.limitByRouteId($scope.route.routeId)
				.limitByPersonId($scope.completions.peopleIds)
				.filteredByRouteFilter($scope.routeFilterService)
				.withPerson();
			if ($scope.completions.courses === true) {
				builder.limitToCourses();
			}
			return builder;
		};

		$scope.configureCompletions = function() {
			throw "override me";
		};

		$scope.decodeCompletionsSearchParams();
	};

	return BaseCompletionsController;

});