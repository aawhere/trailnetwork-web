define(['app-route-constants',
	'route/route-module',
	'route/completions-url-encoding',
	'explore/explore-events',
	'route/route-completions-base-controller',
	//
	'common/browser-service',
	'common/debug-service',
	'common/header-directive',
	'common/location-service',
	'route/route-filter-service',
	'route/route-completions-directive',
	'route/completions-web-api-service',
	'route/route-completions-filter-directive'
], function(appRouteConstants, routeModule, completionsUrlEncoder, exploreEvents, BaseCompletionsController) {
	'use strict';

	return routeModule.controller('tnRouteEventsController', function($scope,
			browserService,
			CompletionsWebApiService,
			debugService,
			locationService,
			routeFilterService,
			$log,
			$location,
			$translate) {

			BaseCompletionsController.call(this,
				$scope,
				$log,
				$location,
				routeFilterService,
				locationService,
				CompletionsWebApiService);

			$scope.debug = debugService.enabled;

			$scope.$watch('route.route', function() {
				if ($scope.route.route) {
					$translate('ROUTE_COMPLETIONS_TITLE', {
						routeTitle: $scope.route.route.name.$
					}).then(function(msg) {
						browserService.title(msg);
						$scope.completions.title = msg;
					});
				}
			});

			this.configureCompletions = function() {
				var service = this.configureCompletionsBuilder();
				service.orderBySocialOrder()
					.withEvent();
				if ($scope.completions.courses === true) {
					service.limitToCourses();
				}
				$scope.completions.service = service;
				return service;
			};
		}
	);
});