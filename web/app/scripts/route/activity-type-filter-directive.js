define(['route/route-module',
	'route/activityTypes'
], function(routeModule, activityTypes) {
	'use strict';

	routeModule.directive('tnActivityTypeFilter', function() {
		return {
			restrict: 'EA',
			transclude: false,
			templateUrl: '/views/route/activity-type-filter.html',
			replace: false,
			scope: {
				filter: '=',
				activityTypeKeys: '=', //A list of activity type keys to include.
			},
			link: function(scope, el, attr) {

				if (_.isUndefined(attr.toggleMode)) {
					scope.toggleMode = false; //the default
				} else {
					scope.toggleMode = scope.$eval(attr.toggleMode);
				}

				scope.activityType = {};

				scope.isActivityTypeActive = function(key) {
					return isActive(key, scope.filter.activityType());
				};

				function isActive(key, condition) {
					return key === condition;
				}

				scope.setActivityTypeActive = function(at) {
					scope.filter.clearDistance();
					if (scope.isActivityTypeActive(at.key) && !scope.toggleMode) {
						scope.filter.clearActivityType();
					} else {
						scope.filter.activityType(at.key);
					}
				};


				function setActivityTypes(activityTypeKeys) {
					scope.activityType.current = _.filter(activityTypes, function(at) {
						if (_.contains(activityTypeKeys, at.key)) {
							return true;
						} else {
							return false;
						}
					});
				}

				/**
				 * All instances should receive the activityType icon class.
				 * If the activityType is active, it should receive the active class.
				 * If toggleMode is true, then do not apply the clickable class (showes pointer),
				 * otherwise show clickable class.
				 * @param  {[type]} at [description]
				 * @return {[type]}    [description]
				 */
				scope.activityTypeClass = function(at) {
					var css = at.icon;
					var isActive = scope.isActivityTypeActive(at.key);
					if (isActive) {
						css += ' active';
					}
					if (!scope.toggleMode || !isActive) {
						css += ' clickable';
					}
					return css;
				};

				scope.$watch('activityTypeKeys', function() {
					setActivityTypes(scope.activityTypeKeys);
				});
			}
		};
	});

});