define(['common/common-tn-api-constants'],
	function(commonTnApiConstants) {
		'use strict';

		var routeConstants = {

			conditions: {
				//TODO: move to activity-tn-api-constants
				applicationReference: 'activity-applicationReference',
				gpolyline: 'track-gpolyline',
				routeCompletionsSocialOrder: 'routeCompletion-socialOrder',
				routeCompletionsSocialRaceRank: 'routeCompletion-socialRaceRank',
				routeCompletionsStartTimeDate: 'routeCompletion-startTime-date',
				routeCompletionsPersonId: 'routeCompletion-personId',
				routeCompletionsDuration: 'routeCompletion-duration',
				routeCompletionActivityTypeStatsActivityType: 'route-completionStats-activityTypeStats-activityType',
				routeCompletionApplicationStatsApplication: 'route-completionStats-applicationStats-application',
				routeCompletionActivityType: 'routeCompletion-activityType',
				routeCompletionStartTime: 'routeCompletion-startTime',
				routeCompletionPersonalOrder: 'routeCompletion-personalOrder',
				routeCourse: 'route-course',
				routeCourses: 'route-courses',
				routeBounds: 'route-bounds',
				routeCourseInline: 'route-course-inline',
				routeDistanceCategory: 'route-distanceCategory',
				routeStartInline: 'route-start-inline',
				routeFinishInline: 'route-finish-inline',
				routeTrackGPolyline: 'route-track-gpolyline',
			},

			paths: {
				alternates: 'alternates',
				completions: 'completions',
				courses: 'courses',
				mains: 'mains',
				routes: 'routes',
				trailheads: 'trailheads',
			},

			options: {
				routeCompletionPerson: 'routeCompletion-person',
				routeCompletionRoute: 'routeCompletion-route',
				route: 'route',
				person: 'person',
				account: 'account',
				activity: 'activity',
				event: 'routeEvent'
			},

			headers: {
				gpolyline: 'application/vnd.google-maps.polyline',
			}
		};

		routeConstants.defaultRouteParameters = {};
		routeConstants.defaultRouteParameters[commonTnApiConstants.options] = [
			routeConstants.conditions.routeTrackGPolyline,
			routeConstants.conditions.routeStartInline,
		];

		return routeConstants;

	});