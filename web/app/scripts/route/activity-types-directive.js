define(['route/route-module',
		'route/activity-type-aliases',
		'common/debug-service',
		'route/activity-type-directive'
	],
	function(routeModule, activityTypeAliases) {
		'use strict';

		/**
		 * 
		 * Displays a list of activity types.
		 *
		 * @param {tn.ActivityTypeStats} The stats to display
		 *
		 */
		routeModule.directive('tnActivityTypes', function(debugService) {
				return {
					restrict: 'EA',
					transclude: false,
					templateUrl: '/views/route/activity-types-directive.html',
					replace: false,
					scope: {
						activityTypeStats: '=',
					},
					link: function(scope) {
						scope.debug = debugService.enabled;

						//limit to stats greater than 3%
						scope.percentCutOff = 0.00;

						var processActivityTypes = function(activityTypeStats) {
							var reduced = {};
							_.each(activityTypeStats, function(stat) {
								var statKey = stat.activityType.key;
								var mappingKey = activityTypeAliases[statKey];
								var statMapping = reduced[mappingKey];
								var combinedPercentage = stat.percent.value;
								if (statMapping) {
									combinedPercentage += statMapping.percent;
								}
								reduced[mappingKey] = {
									percent: combinedPercentage,
									activityType: stat.activityType,
									color: combinedPercentage > 0.33 ? 'orange' : 'grey2'
								};
							});
							return _.sortBy(reduced, 'percent').reverse();
						};

						scope.$watch('activityTypeStats', function() {
							if (scope.activityTypeStats) {
								scope.mapping = processActivityTypes(scope.activityTypeStats);
								scope.nothing = '';
							}
						});
					}
				};
			}
		);
	});