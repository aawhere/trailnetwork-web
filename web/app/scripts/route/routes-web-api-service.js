/**
 * 	Responsible for retreiving routes. May add to the route repository as required.
 *
 */
define(['angular',
		'route/route-module',
		'route/route-util',
		'route/route-filter-util',
		'underscore',
		'map/bounding-box',
		'common/tn-api-constants',
		'common/filter-condition-operators',
		//
		'restangular',
		'common/notification-service',
		'route/route-gpolyline-service',
		'common/entities-web-api-service',
	],
	function(angular,
		routeModule,
		routeUtil,
		routeFilterUtil,
		_,
		BoundingBox,
		apiConstants,
		operators) {
		'use strict';

		/**
		 * Responsible for retreiving a route.
		 *
		 */
		routeModule.factory('RoutesWebApiService', function(Restangular, $q, $log, EntitiesWebApiService) {

				var Routes = function() {
					EntitiesWebApiService.call(this, {
						entitiesPropertyName: 'route',
					});
					this.routeFilter = undefined;
					this.mains = false;
					this.alternates = false;
					this.mainId = undefined;

					this.addRequestOptions([
						apiConstants.options.routeGpolyline
					]);

					this.expandFields([
						apiConstants.fields.routeStartTrailheadId
					]);
					this.withCompletionStats();
					this.withSocialCompletionStats();
				};

				Routes.inheritsFrom(EntitiesWebApiService);

				Routes.prototype.withCompletionStats = function() {
					this.expandFields(apiConstants.fields.routeCompletionStats);
					return this;
				};

				Routes.prototype.withSocialCompletionStats = function() {
					this.expandFields(apiConstants.fields.routeSocialCompletionStats);
					return this;
				};

				Routes.prototype.withRelativeCompletionStats = function() {
					this.expandFields(apiConstants.fields.routeRelativeCompletionStats);
					return this;
				};

				Routes.prototype.autoLocate = function() {
					this.addRequestParam(apiConstants.params.autoLocate, 'true');
				};

				/**
				 * Limits the results to routes starting from the provided trailheads.
				 *
				 * @param  {Array[tn.trailhead.Trailhead]} trailheads
				 * @return {this}
				 */
				Routes.prototype.limitToTrailheads = function(trailheads) {
					if (!_.isArray(trailheads)) {
						trailheads = [trailheads];
					}
					this.limitToTrailheadIds(_.pluck(trailheads, 'id'));
					return this;
				};

				/**
				 * Limits the results to routes starting from the provided trailheadIds.
				 *
				 * @param  {Array[String]} trailheadIds
				 * @return {this}
				 */
				Routes.prototype.limitToTrailheadIds = function(trailheadIds) {
					if (!_.isArray(trailheadIds)) {
						trailheadIds = [trailheadIds];
					}
					this.addFilterCondition(apiConstants.fields.routeStartTrailheadId, operators.equals, _.reduce(trailheadIds, function(memo, thId) {
						if (_.isEmpty(memo)) {
							return memo + thId;
						} else {
							return memo + ' ' + thId;
						}
					}, ''));
					return this;
				};

				/**
				 *	Adds a bounds filter to the request.
				 *
				 * @param  {tn.measure.BoundingBox} location [description]
				 * @return {this}        [description]
				 */
				Routes.prototype.limitByBounds = function(location) {
					if (location) {
						var boundsAsString = BoundingBox.createFromBoundingBox(location).toString();
						this.requestParameters[apiConstants.params.bounds] = boundsAsString;
					}
					return this;
				};

				/**
				 * Limit results to the provided filter.
				 *
				 * @param  {RouteFitler} filter
				 * @return {this}
				 */
				Routes.prototype.filteredByRouteFilter = function(routeFitler) {
					this.routeFilter = routeFitler;
					return this;
				};

				Routes.prototype.limitToMainsOnly = function() {
					this.mains = true;
					this.alternates = false;
					return this;
				};

				Routes.prototype.limitToAlternatesOfMain = function(mainId) {
					this.mains = false;
					this.alternates = true;
					this.mainId = mainId;
					return this;
				};

				Routes.prototype.proximity = function() {
					this.requestParameters.proximity = "true";
					return this;
				};

				Routes.prototype.allRoutes = function() {
					this.orderBy(apiConstants.orderBy.none);
				};

				/**
				 * Get the route from the server.
				 *
				 * @return {[Promise]} A promise for an tn.route.Route.
				 */
				Routes.prototype.makeRequest = function() {
					var deferred = $q.defer();
					var context = this;
					var restBuilder;
					if (this.mains) {
						restBuilder = getMainsRequestBuilder();
					} else if (this.alternates) {
						restBuilder = getAlternatesBuilder(this.mainId);
					} else {
						restBuilder = getRoutesBuilder();
					}
					// $log.debug('RoutesWebApiService, requestedUrl', restBuilder.getRequestedUrl());
					// $log.debug('RoutesWebApiService, restangularUrl', restBuilder.getRestangularUrl());
					// $log.debug('RoutesWebApiService, requestParams', restBuilder.requestParams);
					// $log.debug('RoutesWebApiService, url', restBuilder.getRequestedUrl() + JSON.stringify(context.requestParameters));

					restBuilder
						.get(requestParameters(this)).then(function(routeRequest) {
							if (context.bounds) {
								context.bounds = context.bounds.union(routeRequest.boundingBox);
							} else {
								context.bounds = new BoundingBox({
									boundingBox: routeRequest.boundingBox
								});
							}
							if (routeRequest && routeRequest.route) {
								routeUtil.movePointsIntoRoutes(routeRequest);
							}
							deferred.resolve(routeRequest);
						}, function(err) {
							deferred.reject(err);
						});
					return deferred.promise;
				};

				function getMainsRequestBuilder() {
					return getRoutesBuilder()
						.one(apiConstants.paths.mains);
				}

				function getAlternatesBuilder(mainId) {
					return Restangular.one(apiConstants.paths.routes, mainId)
						.one(apiConstants.paths.alternates);
				}

				function getRoutesBuilder() {
					return Restangular.one(apiConstants.paths.routes);
				}

				function requestParameters(context) {
					if (context.routeFilter) {
						conditionsFromRouteFilter(context.routeFilter, context);
					}

					return context.requestParameters;
				}

				/**
				 * Produce a set of conditions for use with the api from the routeFilter
				 *
				 * @param  {RouteFilter} routeFilter [description]
				 * @param  {EntitiesWebApiService}
				 */
				function conditionsFromRouteFilter(routeFilter, context) {
					var distConditionExists = routeFilterUtil.condition.distance(routeFilter, context);
					if (!distConditionExists) {
						routeFilterUtil.condition.activityType(routeFilter, undefined, context);
					}
					routeFilterUtil.condition.persons(routeFilter, context);
					routeFilterUtil.condition.application(routeFilter, context);
				}

				return Routes;
			}

		);
	});