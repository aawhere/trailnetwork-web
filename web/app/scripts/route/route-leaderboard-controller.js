define(['app-route-constants',
	'route/route-module',
	'route/completions-url-encoding',
	'explore/explore-events',
	'route/route-completions-base-controller',
	'route/completions-util',
	//
	'common/browser-service',
	'common/debug-service',
	'common/header-directive',
	'common/location-service',
	'route/route-filter-service',
	'route/route-completions-directive',
	'route/completions-web-api-service',
	'route/route-completions-filter-directive',
], function(appRouteConstants,
	routeModule,
	completionsUrlEncoder,
	exploreEvents,
	BaseCompletionsController,
	completionsUtil) {
	'use strict';

	return routeModule.controller('tnRouteLeaderboardController', function($scope,
			browserService,
			CompletionsWebApiService,
			debugService,
			locationService,
			routeFilterService,
			$log,
			$location,
			$translate) {

			BaseCompletionsController.call(this,
				$scope,
				$log,
				$location,
				routeFilterService,
				locationService,
				CompletionsWebApiService);

			$scope.debug = debugService.enabled;
			//for html binding
			$scope.routeFilterService = routeFilterService;

			var context = this;
			//a flag to indicate to the event callback that we want to not update the url params.
			var initializingRouteFilter = false;
			var routeFilterManuallySet = false;

			$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
					$log.debug('$stateChangeStart', toState);
					if (fromState.name === appRouteConstants.states.routeLeaderboard && !routeFilterManuallySet) {
						//reset activity type.
						routeFilterService.clearActivityType();
					}
				});

			$scope.$watch('route.route', function() {
				if ($scope.route.route) {
					context.chooseDefaultActivityTypeFilter();
					// $scope.retrieveEvents();
				}
			});

			$scope.$watch('route.title', function(title) {
				if (title) {
					$translate('ROUTE_LEADERBOARD_TITLE', {
						routeTitle: title,
					}).then(function(msg) {
						browserService.title(msg);
						$scope.completions.title = msg;
					});
				}
			});

			$scope.$on(exploreEvents.routeFilterUpdated, function() {
				if (initializingRouteFilter) {
					routeFilterService.encodeUrlParams = false;
					initializingRouteFilter = false;
				} else {
					routeFilterService.encodeUrlParams = true;
					routeFilterManuallySet = true;
				}
			});

			this.chooseDefaultActivityTypeFilter = function() {
				if (_.isUndefined(routeFilterService.activityType())) {
					initializingRouteFilter = true;
					if ($scope.route.route) {
						completionsUtil.chooseDefaultActivityTypeFilter(routeFilterService, $scope.route.route);
					}
				} else {
					initializingRouteFilter = false;
					routeFilterManuallySet = true;
				}
			};

			$scope.configureCompletions = function() {
				var service = this.configureCompletionsBuilder();
				if ($scope.completions.courses === true) {
					service.limitToCourses();
				}
				if ($scope.events.eventId) {
					service.limitByEventId($scope.events.eventId);
					service.orderBySocialRaceRank();
				} else {
					//only order if not limiting to an event.
					service.orderBySocialRaceRank()
					.withPerson();
				}
				$scope.completions.service = service;
				return service;
			};

		}
	);
});