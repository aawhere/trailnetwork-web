define(['route/route-module',
		'underscore',
		'explore/explore-events',
		'map/map-events',
		'application/applications',
		'route/activityTypes',
		'route/activity-type-aliases',
		//
		'common/debug-service',
		'application/application-display-directive',
		'route/route-filter-service',
		'route/activity-type-filter-directive'
	],
	function(routeModule, _, exploreEvents, mapEvents, applications, activityTypes, activityTypeAliases) {
		'use strict';

		/**
		 * Provides a ui for filtering routes
		 *
		 */
		routeModule.directive('tnRouteFilter', function($log,
			$rootScope,
			$timeout,
			debugService,
			routeFilterService) {
			return {
				restrict: 'EA',
				transclude: true,
				templateUrl: '/views/route/route-filter.html',
				replace: false,
				scope: {
					expanded: '=?',
				},
				link: function(scope) {
					var OTHER = 'OTHER';
					var toggle, filterChanged,
					filterFocused = false,
					dontFocusFilter = false,
					debounceCloseFilter;

					scope.debug = debugService.enabled;
					scope.providers = applications;
					scope.filter = routeFilterService;
					scope.persons = {
						selection: [],
					};

					scope.distance = {};
					scope.distance.types = [{
						name: '5K',
						key: 'FIVE_K_RUN',
						activityTypeKey: 'RUN',
					}, {
						name: '10K',
						key: 'TEN_K_RUN',
						activityTypeKey: 'RUN',
					}, {
						name: 'Half',
						key: 'HALF_MARATHON',
						activityTypeKey: 'RUN',
					}, {
						name: 'Marathon',
						key: 'MARATHON',
						activityTypeKey: 'RUN',
					}, {
						name: 'Ultra',
						key: 'ULTRA_RUN',
						activityTypeKey: 'RUN',
					}, {
						name: 'Short',
						key: 'SHORT_BIKE',
						activityTypeKey: 'BIKE',
					}, {
						name: 'Medium',
						key: 'MEDIUM_BIKE',
						activityTypeKey: 'BIKE',
					}, {
						name: 'Long',
						key: 'LONG_BIKE',
						activityTypeKey: 'BIKE',
					}, {
						name: 'Metric Century',
						key: 'CENTURY',
						activityTypeKey: 'BIKE',
					}, {
						name: 'Short',
						key: 'SHORT_MTB',
						activityTypeKey: 'MTB',
					}, {
						name: 'Medium',
						key: 'MEDIUM_MTB',
						activityTypeKey: 'MTB',
					}, {
						name: 'Long',
						key: 'LONG_MTB',
						activityTypeKey: 'MTB',
					}, {
						name: 'Epic',
						key: 'EPIC',
						activityTypeKey: 'MTB',
					}, {
						name: 'Short',
						key: 'SHORT_WALK',
						activityTypeKey: 'WALK',
					}, {
						name: 'Medium',
						key: 'MEDIUM_WALK',
						activityTypeKey: 'WALK',
					}, {
						name: 'Long',
						key: 'LONG_WALK',
						activityTypeKey: 'WALK',
					}, ];

					scope.elevation = [{
						min: 0,
						max: 0.005,
						active: false,
						icon: 'icon-terain-1'
					}, {
						min: 0.005,
						max: 0.02,
						active: false,
						icon: 'icon-terain-2'
					}, {
						min: 0.02,
						max: 0.03,
						active: false,
						icon: 'icon-terain-3'
					}, {
						min: 0.03,
						max: 0.06,
						active: false,
						icon: 'icon-terain-4'
					}, {
						min: 0.060,
						max: 10000, //essentially infinity for our purposes
						active: false,
						icon: 'icon-terain-5'
					}];

					scope.activityType = {};
					scope.activityTypeKeys = _.without(_.unique(_.values(activityTypeAliases)), OTHER);
					scope.activityType.types = _.filter(activityTypes, function(at) {
						if (_.contains(scope.activityTypeKeys, at.key) && at.key !== OTHER) {
							return true;
						} else {
							return false;
						}
					});

					angular.extend(scope, {
						provider: {
							providers: [{
									name: 'Garmin Connect',
								}, {
									name: 'MapMyFitness',
								}, {
									name: 'EveryTrail'
								}

							]
						}
					});

					scope.$on(mapEvents.mapSearchFocus, function() {
						scope.focusFilter();
						scope.openFilter();
					});

					scope.$on(mapEvents.mapSearchBlur, function() {
						scope.closeFilter();
					});

					scope.$on(mapEvents.mapSearchMouseOver, function() {
						scope.focusFilter();
					});

					scope.$on(mapEvents.mapSearchMouseLeave, function() {
						scope.blurFilter();
					});

					scope.focusFilter = function() {
						//console.log('focusFilter');
						if (!dontFocusFilter) {
							filterFocused = true;
						}
					};

					scope.blurFilter = function() {
						//console.log('route filter blur');
						filterFocused = false;
						debounceCloseFilter();
					};

					scope.closeFilter = function() {
						scope.expanded = false;
					};

					scope.openFilter = function() {
						scope.expanded = true;
					};

					// scope.closeFilterBtn = function() {
					// 	scope.closeFilter();
					// 	dontFocusFilter = true;
					// 	$timeout(function() {
					// 		dontFocusFilter = false;
					// 	}, 1000);
					// };

					debounceCloseFilter = _.debounce(function() {
						if (!filterFocused) {
							scope.$apply(function() {
								scope.closeFilter();
							});
						}
					}, 2500);

					toggle = function(arg) {
						return arg ? false : true;
					};

					scope.hasFilter = function() {
						return scope.filter.hasConditions();
					};

					scope.activityTypeFilter = function() {
						var atKey = scope.filter.activityType();
						return _.find(activityTypes, function(at) {
							return (at.key === atKey);
						});
					};

					scope.setDistanceActive = function(distance) {
						if (scope.filter.distance() === distance.key) {
							scope.filter.clearDistance();
						} else {
							scope.filter.distance(distance.key);
						}
						filterChanged();
					};

					scope.isDistanceActive = function(key) {
						return isActive(key, scope.filter.distance());
					};

					scope.hasDistanceFilter = function() {
						return scope.filter.hasDistance();
					};

					scope.distanceFilter = function() {
						var distKey = scope.filter.distance();
						return _.find(scope.distance.types, function(dist) {
							return (dist.key === distKey);
						});
					};

					scope.currentDistanceTypes = function() {
						return _.where(scope.distance.types, {
							activityTypeKey: scope.filter.activityType()
						});
					};

					scope.setProviderActive = function(key) {
						if (scope.filter.application() === key) {
							scope.filter.clearApplication();
						} else {
							scope.filter.application(key);
						}
						filterChanged();
					};

					scope.isProviderActive = function(key) {
						return isActive(key, scope.filter.application());
					};

					function isActive(key, condition) {
						return key === condition;
					}

					scope.personList = function() {
						if (scope.hasPersonsFilter()) {
							return _.pluck(scope.persons.selection, 'name');
						}
					};

					scope.hasPersonsFilter = function() {
						return scope.filter.hasPersons();
					};

					scope.clearFilter = function() {
						scope.filter.clearAll();
						console.log(scope.filter.conditions);
					};


					filterChanged = function() {
						console.log('updateFilter');
						//Using a watch statement instead of relying on events, which are
						//easy to forget to broadcast.
						//$rootScope.$broadcast(exploreEvents.routeFilterUpdated);
					};

					// scope.$watch('persons.selection', function(persons, old) {
					// 	console.log('route-filter-directive', persons, old);
					// 	scope.filter.persons(persons);
					// }, true);

					scope.$watch(function() {
						return scope.filter.persons();
					}, function(persons) {
						scope.persons.selection = persons;
					}, true);


					scope.persons.personAdded = function(person) {
						console.log("person added", person);
						scope.filter.persons(scope.persons.selection);
					};

					scope.persons.personRemoved = function(person) {
						console.log("person removed", person);
						scope.filter.persons(scope.persons.selection);
					};

					// if (scope.openOnLoad) {
					// 	$timeout(function() {
					// 		scope.expanded = true;
					// 	}, 2000);
					// }
				}
			};
		});
	});