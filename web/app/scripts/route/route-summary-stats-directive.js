define(['route/route-module',
	//
	'common/app-route-service',
	'common/debug-service',
], function(routeModule) {
	'use strict';

	/**
	 * Provides a summary of a route.
	 *
	 * @param {Array(Routes)} The routes to display
	 *
	 */
	routeModule.directive('tnRouteSummaryStats', function(appRouteService, debugService) {
		return {
			restrict: 'EA',
			transclude: false,
			templateUrl: '/views/route/route-summary-stats-directive.html',
			replace: false,
			scope: {
				route: '=',
			},
			link: function($scope) {
				$scope.appRouteService = appRouteService;
				$scope.debug = debugService.enabled;
			}
		};
	});
});