define(['common/entities', 'underscore'], function(Entities, _) {
	'use strict';

	var service = {};

	service = function() {
		this.repository = new Entities();
		this.MAX_RESOLUTION = 8;
	};

	service.prototype.add = function(routes) {
		this.repository.add(routes);
	};

	service.prototype.get = function() {
		return this.repository;
	};

	service.prototype.find = function(id) {
		return this.repository.find(id);
	};

	service.prototype.getByGeoCell = function(geoCell) {
		var routes = new Entities();

		if(geoCell.length > this.MAX_RESOLUTION) {
			throw new Error('Max resolution of repository exceeded: ' + geoCell);
		}

		_.each(this.repository, function(route) {
			var thCell = route.startTrailheadId.substring(0,geoCell.length);
			if(thCell === geoCell) {
				routes.add(route);
			}
		});
		return routes;
	};

	return service;
});