define(['route/route-module',
	//
	'common/header-directive',
	'route/route-title-directive',
	'route/route-web-api-service',
	'route/route-map-controller',
	'route/route-summary-copy-directive',
	'route/route-summary-stats-directive',
	'common/browser-service',
], function(routeModule) {
	'use strict';

	return routeModule.controller('tnRouteController', function($scope,
		$stateParams,
		RouteWebApiService,
		$translate,
		browserService) {

		$scope.route = {};
		$scope.route.routeId = $stateParams.routeId;
		$scope.route.service = new RouteWebApiService($scope.route.routeId);
		$scope.navigation = {};

		$scope.route.service.get().then(function(route) {
			$scope.route.route = route;
			$scope.routeTitle();
		}, function(err) {
			$scope.tn.displayErrorPage(err);
		});

		$scope.routeTitle = function() {
			$translate('ROUTE_TITLE', {
				routeName: $scope.route.route.name.$
			}).then(function(name) {
				browserService.title(name);
				$scope.route.title = name;
			});
		};
	});
});