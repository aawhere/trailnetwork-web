define(['route/route-module',
	'explore/explore-events',
	'common/dictionary-utils',
	'app-route-constants',
	//
	'common/debug-service',
	'route/activity-types-directive',
	'route/route-title-directive',
	'route/route-service',
	'route/route-application-links-directive',
	'common/location-service',
	'trailhead/trailhead-web-api-service',
	'route/route-web-api-service',
], function(routeModule, exploreEvents, dictionaryUtils, appRouteConstants) {
	'use strict';

	/**
	 * Provides a list of cards to be used in a map-cardtray.
	 *
	 * @param {Array(Routes)} The routes to display
	 *
	 */
	routeModule.directive('tnRouteDetail', function(debugService, $rootScope, routeService, locationService, TrailheadWebApiService, RouteWebApiService) {
		return {
			restrict: 'EA',
			transclude: false,
			templateUrl: '/views/route/route-detail-directive.html',
			replace: false,
			scope: {
				route: '=',
				alternatesView: '=',
			},
			link: function(scope) {

				scope.appRouteConstants = appRouteConstants;
				scope.relatedRoutes = [];

				function getTrailhead(thId) {
					if (scope.route && !scope.route.startTrailhead) {
						new TrailheadWebApiService(thId).get().then(function(th) {
							scope.trailhead = th;
						}, function(err) {
							console.log('error retreiving traihlead', err);
						});
					} else {
						scope.trailhead = scope.route.startTrailhead;
					}
				}

				scope.debug = debugService.enabled;
				scope.dictionary = {};

				scope.$watch('route', function() {
					if (scope.route) {
						getTrailhead(scope.route.startTrailheadId.value);
						scope.getRelated(scope.route);
					}
				});

				scope.getRelated = function(route) {
					if (route && route.routeRelations) {
						_.each(route.routeRelations, function(relation) {
							scope.getRouteService(relation.relatedRouteId.value).then(function(route) {
								scope.relatedRoutes.push(route);
							});
						});
					}
				};

				scope.getRouteService = function(id) {
					var service = new RouteWebApiService(id);
					return service.get();
				};

				routeService.routeDictionary().then(function(dictionary) {
					scope.dictionary.elevationGain = dictionaryUtils.findByKey(dictionary, 'route-course-track-elevationGain');
				});
			}
		};
	});
});