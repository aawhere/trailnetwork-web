define(['route/route-module',
		'common/debug-service',
		'route/route-service',
		'application/application-link-directive'
	],
	function(routeModule) {
		'use strict';

		/**
		 * Provides a list of cards to be used in a map-cardtray.
		 *
		 * @param {Array(Routes)} The routes to display
		 *
		 */
		routeModule.directive('tnRouteApplicationLinks', function(debugService, routeService) {
				return {
					restrict: 'EA',
					transclude: false,
					templateUrl: '/views/route/route-application-links-directive.html',
					replace: false,
					scope: {
						route: '='
					},
					link: function(scope) {
						scope.debug = debugService.enabled;
						scope.applicationLinks = {};

						function getApplicationLinks() {
							_.each(scope.stats, function(stat) {
								var application = stat.application.applicationKey.value;
								routeService.getRouteCourse(scope.route.id, application).then(function(activity) {
									scope.applicationLinks[application] = [activity.applicationReference.url, stat];
								}, function() {
									//TODO: find some acceptable place to redirect folks to.
								});
							});
						}

						scope.$watch('route', function() {
							if (scope.route) {
								//Deal with bad data
								if (scope.route.courseCompletionStats) {
									scope.stats = scope.route.courseCompletionStats.applicationStats;
								} else {
									scope.stats = scope.route.completionStats.applicationStats;
								}
								getApplicationLinks();
							}

						});

					}
				};
			}
		);
	});