define(['app-route-constants',
		'common/url-encoding-util',
		'underscore',
		'person/persons-url-encoding'
	],
	function(appRouteConstants, encodingUtil, _, personsUrlEncoder) {
		'use strict';

		/**
		 * Responsible for encoding and decoding url query parameters.
		 *
		 * !n,123;brian!e,456
		 *
		 * @param {[type]} conditions [description]
		 */
		var util = {};

		util.constants = {
			EVENT_CODE: 'e',
		};

		/**
		 * Encode the query into URL parameters.
		 *
		 * @param  {[type]} query [description]
		 * @return {[type]}       [description]
		 */
		util.encode = function(query) {
			var value = '';
			var n = '';
			var e = '';

			if (query) {
				n = personsUrlEncoder.encodeQueryParameter(query.people);
				if (query.routeEvent) {
					e += query.routeEvent;
				}
			}

			var queryParams = {};
			if (n && n.length > 0) {
				value = n;
			}
			if (e && e.length > 0) {
				value = encodingUtil.encodeParams(util.constants.EVENT_CODE, e, value);
			}
			if (value) {
				queryParams[appRouteConstants.routeCompletionQueryParameterName] = value;
			}
			return queryParams;
		};

		/**
		 * Decode the query string, if present.
		 *
		 * @param  {[type]} queryString the encoded query parameter value
		 * @return {[type]}             array of code => value pairs
		 */
		util.decode = function(queryString) {
			var result = {
				people: personsUrlEncoder.decodeQueryParameter(queryString),
			};
			var codeToValueMap = encodingUtil.decodeParams(queryString);
			var eventValue = codeToValueMap[util.constants.EVENT_CODE];
			if(eventValue){
				result.routeEvent = eventValue;
			}
			return result;
		};

		return util;

	});