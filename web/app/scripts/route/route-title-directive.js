define(['route/route-module',
    //
    'route/activity-types-directive',
    'trailhead/trailhead-web-api-service'
], function(routeModule) {
    'use strict';

    /**
     * Provides a list of cards to be used in a map-cardtray.
     *
     * @param {Array(Routes)} The routes to display
     *
     */
    routeModule.directive('tnRouteTitle', function($rootScope,
            TrailheadWebApiService,
            $translate) {
            return {
                restrict: 'EA',
                transclude: false,
                templateUrl: '/views/route/route-title-directive.html',
                replace: false,
                scope: {
                    route: '=',
                    showTrailhead: '=',
                    showRouteLink: '=',
                    showActivityTypes: '=',
                },
                link: function(scope) {
                    if (_.isUndefined(scope.showTrailhead)) {
                        scope.showTrailhead = false;
                    }
                    if (_.isUndefined(scope.showActivityTypes)) {
                        scope.showActivityTypes = false;
                    }

                    function getTrailhead(thId) {
                        if (scope.route && !scope.route.startTrailhead) {
                            var service = new TrailheadWebApiService(thId);
                            service.get().then(function(th) {
                                scope.trailhead = th;
                            });
                        } else {
                            scope.trailhead = scope.route.startTrailhead;
                        }
                    }

                    scope.$watch('route', function() {
                        if (scope.route) {
                            getTrailhead(scope.route.startTrailheadId.value);
                            $translate('ROUTE_TITLE', {
                                routeName: scope.route.name.$
                            }).then(function(name) {
                                scope.routeTitle = name;
                            });
                        }
                    });
                }
            };
        }
    );
});