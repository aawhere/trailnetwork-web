define([], function() {
	'use strict';

	var util = {};

	util.chooseDefaultActivityTypeFilter = function(routeFilterService, route) {
		routeFilterService.activityType(route.completionStats.activityTypeStats[0].activityType.key);
	};

	util.activityTypeKeys = function(route) {
		return _.map(route.completionStats.activityTypeStats, function(stat) {
			return stat.activityType.key;
		});
	};

	return util;
});