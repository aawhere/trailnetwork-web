define(['angular',
       'route/route-repository',
       'activity/activity-module',
       'angular-bootstrap',
       // 'angular-strap-popover',
       // 'angular-strap-popover-tpl',
       // 'angular-strap-dropdown',
       // 'angular-strap-dropdown-tpl',
       ],
       function(angular, RouteRepository) {
	'use strict';

	var routeModule;

	routeModule = angular.module('tn.route', ['tn.activity', 'tn.common',
	                             'ui.bootstrap']);

	routeModule.service('tnRouteRepository', function() {
		return new RouteRepository();
	});

	return routeModule;
});