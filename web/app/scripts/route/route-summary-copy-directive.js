define(['route/route-module',
	'jquery',
	//
	'common/app-route-service',
	'common/debug-service',
], function(routeModule, jquery) {
	'use strict';

	/**
	 * Provides a summary of a route.
	 *
	 * @param {Array(Routes)} The routes to display
	 *
	 */
	routeModule.directive('tnRouteSummaryCopy', function(appRouteService,
			debugService) {
			return {
				restrict: 'EA',
				transclude: false,
				templateUrl: '/views/route/route-summary-copy-directive.html',
				replace: false,
				scope: {
					route: '=',
				},
				link: function(scope, el) {
					scope.appRouteService = appRouteService;
					scope.debug = debugService.enabled;

					scope.$watch('route', function(route) {
						if (route) {
							wrap(jquery(el).find('.completion-count'), appRouteService.route.completionsUrl(route.id));
							wrap(jquery(el).find('.person-count'), appRouteService.route.peopleUrl(route.id));
							wrap(jquery(el).find('.trailhead-name'), appRouteService.trailhead.trailheadUrl(route.startTrailheadId.value));
							wrap(jquery(el).find('.variation-count'), appRouteService.explore.routeUrl(route.mainId.value));
						}
					});

					function wrap(el, url) {
						el.wrap('<a href="'+url+'"></a>');
					}
				}
			};
		}
	);
});