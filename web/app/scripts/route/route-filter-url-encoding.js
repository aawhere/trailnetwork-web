define(['app-route-constants',
		'common/url-encoding-util',
		'person/persons-url-encoding',
		'underscore'
	],
	function(appRouteConstants, encodingUtil, personsUrlEncoder, _) {
		'use strict';

		var ACTIVITY_TYPE_CODE = 'at';
		var DISTANCE_CODE = 'dist';
		var APPLICATION_CODE = 'app';
		/**
		 * Responsible for encoding and decoding url query parameters.
		 *
		 * @param {[type]} conditions [description]
		 */
		var util = {};

		util.encodeActivityType = function(param, activityType) {
			return encodingUtil.encodeParams(ACTIVITY_TYPE_CODE, activityType, param);
		};

		util.asQueryParam = function(value) {
			var queryParams = {};
			if (value && value.length > 0) {
				queryParams[appRouteConstants.routeFilterQueryParameterName] = value;
			}
			return queryParams;
		};

		util.encode = function(filter) {
			var value = '';

			//activityTypes
			if (filter.hasActivityType()) {
				value = util.encodeActivityType(value, filter.activityType());
			}
			if (filter.hasDistance()) {
				value = encodingUtil.encodeParams(DISTANCE_CODE, filter.distance(), value);
			}
			if(filter.hasPersons()) {
				var n = personsUrlEncoder.encodeQueryParameter(filter.persons());
				if(n) {
					value = value + n;
				}
			}
			if (filter.hasApplication()) {
				value = encodingUtil.encodeParams(APPLICATION_CODE, filter.application(), value);
			}
			return util.asQueryParam(value);
		};

		util.decode = function(queryString, filter) {
			var currentEncoded = util.encode(filter);
			if ((currentEncoded[appRouteConstants.routeFilterQueryParameterName] !== queryString) && !_.isUndefined(queryString)) {
				var map = encodingUtil.decodeParams(queryString);

				if (map[ACTIVITY_TYPE_CODE]) {
					filter.activityType(map[ACTIVITY_TYPE_CODE]);
				} else {
					filter.clearActivityType();
				}
				if (map[DISTANCE_CODE]) {
					filter.distance(map[DISTANCE_CODE]);
				} else {
					filter.clearDistance();
				}
				var persons = personsUrlEncoder.decodeQueryParameter(queryString);
				if(_.isArray(persons) && !_.isEmpty(persons)) {
					filter.persons(persons);
				} else {
					filter.clearPersons();
				}
				if (map[APPLICATION_CODE]) {
					filter.application(map[APPLICATION_CODE]);
				} else {
					filter.clearApplication();
				}
			} else if (_.isUndefined(queryString)) {
				filter.clearAll();
			}
		};

		return util;

	});