define(['route/route-module',
	'common/tn-api-constants',
	//
	'application/application-link-directive',
	'common/app-route-service',
	'common/popover-html-directive',
	'route/completion-application-links-directive',
	'common/debug-service',
], function(routeModule, apiConstants) {
	'use strict';

	/**
	 * Provides a list of completions to be displayed in either a table or list.
	 *
	 * @param {Array(Routes)} The routes to <display></display>
	 *
	 */
	routeModule.directive('tnRouteCompletions', function(usSpinnerService,
		appRouteService,
		debugService) {
		return {
			restrict: 'EA',
			templateUrl: '/views/route/route-completions-directive.html',
			replace: false,
			scope: {
				//Array[String] or personIds
				completionsWebApiService: '=',
				//template url
				templateUrl: '@',
				//alternate context. Useful for providing custom vaiables for a template.
				context: '=',
				compact: '@',
			},
			link: {
				pre: function($scope, el, attr) {
					$scope.template = {};
					if (attr.templateUrl) {
						$scope.template.url = attr.templateUrl;
					} else {
						$scope.template.url = '/views/route/route-visits-table-directive.html';
					}
				},
				post: function($scope) {

					$scope.focusOnHtml = function(completion) {
						return 'Focus on <a ng-click=\"context.addFocusOnPerson(completion.person)\">' + completion.person.name + '</a>';
					};


					var spinnerKey = 'completionsSpinner';

					$scope.appRouteService = appRouteService;
					$scope.debug = debugService.enabled;

					$scope.apiConstants = apiConstants;

					$scope.$watch('completionsWebApiService.busy', function(isBusy) {
						if (isBusy) {
							if ($scope.completionsWebApiService.busy) {
								usSpinnerService.spin(spinnerKey);
							}
						} else {
							//usSpinnerService.stop(spinnerKey);
						}
					});
				}
			}
		};
	});
});