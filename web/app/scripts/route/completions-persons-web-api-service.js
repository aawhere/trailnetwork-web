define(['angular',
		'route/route-module',
		'route/route-tn-api-constants',
		'route/route-util',
		'person/person-tn-api-constants',
		//
		'restangular',
		'common/notification-service',
		'common/entities-web-api-service',
	],
	function(angular, routeModule, routeTnApiConstants, routeUtil,
		personTnApiConstants) {
		'use strict';

		/**
		 * Responsible for retreiving a list of people from completions.
		 *
		 */
		routeModule.factory('CompletionPeopleWebApiService', function(Restangular, $q, EntitiesWebApiService) {

				// var PEOPLE_DELIMINATOR = ',';

				var People = function(routeId, query) {
					this.routeId = routeUtil.routeIdAsNumber(routeId);
					this.query = query;
					EntitiesWebApiService.call(this, {
						entitiesPropertyName: 'person',
					});
				};

				People.inheritsFrom(EntitiesWebApiService);

				/**
				 * Get the next page of people. If this is the first request, retreives the first page of results.
				 *
				 * @return {[Promise]} A promise for an array of tn.person.Person.
				 */
				People.prototype.get = function() {
					var deferred = $q.defer();
					var restBuilder = getRestBuilder(this);

					restBuilder.get(buildRequestParameters(this, this.requestParameters)).then(function(people) {
						deferred.resolve(people);
					}, function(err) {
						deferred.reject(err);
					});
					return deferred.promise;
				};

				function getRestBuilder() {
					// return rest.one(routeTnApiConstants.paths.routes, context.routeId).one(routeTnApiConstants.paths.completions)
					// .one(personTnApiConstants.paths.persons);
					return Restangular.one(personTnApiConstants.paths.persons);
				}

				function buildRequestParameters(context, requestParameters) {
					var params = {};
					params.query = context.query;
					return _.extend(requestParameters, params);
				}

				return People;
			}

		);
	});