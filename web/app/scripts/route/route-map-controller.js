define(['underscore',
	'route/route-module',
	'common/entities',
	'map/map-events',
	'explore/explore-events',
	'map/bounding-box',
	//
	'map/map-service',
	'route/route-detail-directive',
], function(_, routeModule, Entities, mapEvents, exploreEvents, BoundingBox) {
	'use strict';

	return routeModule
		.controller('tnRouteMapController', function($scope,
			$rootScope,
			$timeout,
			Map) {

			$scope.map = {
				visible: true,
			};

			$scope.map.routes = {
				entities: new Entities(),
				selectable: false,
				highlight: false,
			};

			$scope.map.trailheads = {
				entities: new Entities(),
				options: {
					icon: '/images/thMarker-16-red.png'
				},
				selectedOptions: {
					icon: '/images/thMarker-16-red.png'
				},
			};

			$scope.$watch('route.route', function(route) {
				if (route) {
					$scope.mapService = new Map({
						center: BoundingBox.createFromBoundingBox(route.boundingBox).googleLatLngBounds().getCenter(),
						zoom: 14,
					});
					$scope.mapService.onInitialized().then(function() {
							$scope.mapService.bounds(route.boundingBox);
							$scope.map.routes.entities.add(route);
							$rootScope.$broadcast(exploreEvents.routeSelected, route.id);
							$scope.map.trailheads.entities.add(route.startTrailhead);
					});
				}
			});

			$scope.$on(mapEvents.mapBoundsChanged, function() {
				transitionMapType();
			});

			/**
			 * Convienence method for e2e testing.
			 *
			 * @param {[type]} mapTypeId [description]
			 */
			$scope.setMapTypeId = function(mapTypeId) {
				$scope.mapService.mapTypeId(mapTypeId);
			};

			function transitionMapType() {
				if ($scope.mapService.zoom() >= $scope.mapService.maxZoomForMapType($scope.mapService.mapTypeId())) {
					var currentMapType = $scope.mapService.mapTypeId();
					if (currentMapType === 'terrain') {
						$scope.mapService.mapTypeId('roadmap');
						$scope.map.transitionMapTypeId = currentMapType;
					}
				} else if ($scope.map.transitionMapTypeId && $scope.mapService.zoom() < $scope.mapService.maxZoomForMapType($scope.map.transitionMapTypeId)) {
					if ($scope.map.transitionMapTypeId === 'terrain' && $scope.mapService.mapTypeId() === 'roadmap') {
						$scope.mapService.mapTypeId($scope.map.transitionMapTypeId);
					}
					$scope.map.transitionMapTypeId = undefined;
				}
			}

		});
});