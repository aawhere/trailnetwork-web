define(function() {
	'use strict';

	/**
	 * Used to map activity types with the same icon to one type. Generally
	 * used to prevent displaying duplicate icons.
	 */
	return {
		RUN: 'RUN',
		BIKE: 'BIKE',
		ROAD: 'BIKE',
		MTB: 'MTB',
		HIKE: 'WALK',
		OTHER: 'OTHER',
		WALK: 'WALK',
		U: 'OTHER',
		SKI: 'OTHER',
		SKATE: 'OTHER',
		SWIM: 'OTHER'
	};

});