/**
 * 	Responsible for retreiving a route's gpolyline. May implement caching transparently to the consumer of the route.
 *
 */
define(['angular',
		'route/route-module',
		'common/common-tn-api-constants',
		'route/route-tn-api-constants',
		//
		'restangular',
		'common/notification-service',
	],
	function(angular, routeModule, commonTnApiConstants, routeTnApiConstants) {
		'use strict';

		/**
		 * Responsible for retreiving a route.
		 *
		 */
		routeModule.factory('tnRouteGpolylineService', function(Restangular, $q, notificationService, $translate) {

				var polylineRestangular = Restangular.withConfig(function(config) {
					config.setRequestSuffix('');
				});

				var GPolyline = function(routeId) {
					this.routeId = routeId;
					this.item = undefined;
					this.busy = false;
				};

				/**
				 * Get the route from the server if not already retreived.
				 *
				 * @return {[Promise]} A promise for an tn.route.Route.
				 */
				GPolyline.prototype.get = function() {
					var deferred = $q.defer();
					var context = this;

					if (!this.busy && !this.item) {

						this.busy = true;
						polylineRestangular.one(routeTnApiConstants.paths.routes, this.routeId).get({}, {
							'Accept': routeTnApiConstants.headers.gpolyline,
						}).then(function(points) {
							if (points) {
								context.item = points;
							} else {
								notFoundMessage(context.routeId);
							}
							deferred.resolve(points);
							context.busy = false;
						}, function(err) {
							console.log("Failed to retrieve Route " + context.RouteId + ".");
							notFoundMessage(context.routeId);
							deferred.reject(err);
							context.busy = false;
						});
					} else {
						deferred.resolve(context.item);
					}
					return deferred.promise;
				};

				GPolyline.prototype.reset = function() {
					this.item = undefined;
				};

				function notFoundMessage(routeId) {
					$translate('ROUTE_NOT_FOUND', {
						route: routeId
					}).then(function(msg) {
						notificationService.notify(msg);
					});
				}

				return GPolyline;
			}

		);
	});