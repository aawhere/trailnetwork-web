define(['route/route-module',
	'explore/explore-events',
	'app-route-constants',
	'route/route-filter-url-encoding',
	//
	'route/route-filter-service',
	'common/location-service'
], function(routeModule, exploreEvents, appRouteConstants, routeFilterUrlEncoder) {
	'use strict';

	routeModule.controller('tnRouteFilterController', function($scope, routeFilterService, locationService, $location, $log) {

		//ui router copies the previous state's parameters (the ones defined in the stateProvider) to the new state.
		//If we remove the filter, then it does not know we removed it and copies the old one over.
		//Here we just override them and supply the correct filter parameter.
		$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
			$log.info(event, toState, toParams, fromState, fromParams);
			var queryParams = routeFilterUrlEncoder.encode(routeFilterService);
			var filterQueryParamValue = queryParams[appRouteConstants.routeFilterQueryParameterName];
			toParams[appRouteConstants.routeFilterQueryParameterName] = filterQueryParamValue;
		});

		$scope.$on(exploreEvents.routeFilterUpdated, function() {
			if (routeFilterService.encodeUrlParams) {
				$scope.encodeFilterSearchParams();
			}
		});

		$scope.$watch(function() {
			return $location.url();
		}, function(url, last) {
			if (url && url !== last) {
				handleUrlChange();
			}
		});

		function handleUrlChange() {
			$scope.decodeFilterSearchParams();
		}

		$scope.encodeFilterSearchParams = function() {
			//console.log('encodeFilterSearchParams');
			var params = routeFilterUrlEncoder.encode(routeFilterService);
			if (params[appRouteConstants.routeFilterQueryParameterName]) {
				locationService.extendLocationSearch(params);
			} else {
				locationService.removeLocationSearchParameter(appRouteConstants.routeFilterQueryParameterName);
			}
		};

		$scope.decodeFilterSearchParams = function() {
			var search = $location.search();
			//console.log('decodeFilterSearchParams', search);

			try {
				routeFilterUrlEncoder.decode(search[appRouteConstants.routeFilterQueryParameterName], routeFilterService);
			} catch (err) {
				//Invalid input.
				$log.error('error parsing filter parameters: ' + err);
			}
		};

		$scope.decodeFilterSearchParams();
	});
});