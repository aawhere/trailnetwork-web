define(['underscore',
       'map/bounding-box',
       'common/entities',
       'common/common-util'], function(_, BoundingBox, Entities, commonUtil) {
	'use strict';

	var routeUtil = {};

	routeUtil.sortByScoreDesc = function(routes) {
		return _.sortBy(routes, function(r) {
			return r.score * -1;
		});
	};

	/**
	 * Returns the first route that has the provided id
	 * @param  {Array[tn.route.Route]} routes  [description]
	 * @param  {String} routeId [description]
	 * @return {tn.route.Route}         [description]
	 */
	routeUtil.findRoute = function(routes, routeId) {
		var self = this;
		return _.find(routes, function(route) {
			return (route.id === self.routeIdAsNumber(routeId));
		});
	};

	/**
	 * Returns an Array of tn.route.Route that have a startId equal to thId
	 * @param  {Entities[tn.route.Route]} routes [description]
	 * @param  {String or Number} thId   [description]
	 * @return {Array[tn.route.Route]}        [description]
	 */
	routeUtil.findRoutesWithTrailhead = function(routes, thId) {
		return _.filter(routes, function(route) {
			if (route.startTrailheadId && route.startTrailheadId.value === thId) {
				return true;
			} else {
				return false;
			}
		});
	};

	/**
	 * casts the routeId as a number if it is not a number (i.e. it is a string).
	 * @param  {[type]} routeId [description]
	 * @return {[type]}         [description]
	 */
	routeUtil.routeIdAsNumber = function(routeId) {
		return commonUtil.idAsNumber(routeId);
	};

	/**
	 * Fitler the routes to those that are within the provided bounds.
	 *
	 * @param  {Entities[tn.route.Route]} routes
	 * @param  {tn.measure.BoundingBox} bounds
	 * @return {Entities[tn.route.Route]} copy with filtered results.
	 */
	routeUtil.filterToBounds = function(routes, bounds) {
		var result = new Entities();
		var filtered = _.filter(routes, function(route) {
			return routeUtil.isWithinBounds(route, bounds);
		});
		result.add(filtered);
		return result;
	};

	routeUtil.isWithinBounds = function(route, bounds) {
		return BoundingBox.createFromBoundingBox(route.boundingBox).containsBounds(bounds);
	};

	/**
	 * Fitler the routes to those that intersect the provided bounds.
	 *
	 * @param  {Entities[tn.route.Route]} routes
	 * @param  {tn.measure.BoundingBox} bounds
	 * @return {Entities[tn.route.Route]} copy with filtered results.
	 */
	routeUtil.filterIntersectingBounds = function(routes, bounds) {
		var result = new Entities();
		var filtered = _.filter(routes, function(route) {
			return routeUtil.intersectsBounds(route, bounds);
		});
		result.add(filtered);
		return result;
	};

	routeUtil.intersectsBounds = function(route, bounds) {
		return BoundingBox.createFromBoundingBox(route.boundingBox).intersectsBounds(bounds);
	};

	routeUtil.movePointsIntoRoutes = function(routes) {
		_.each(routes.route, function(route) {
			if (routes.tracks) {
				var points = _.find(routes.tracks.routePoints, function(points) {
					if (points.routeId.value === route.id) {
						return true;
					}
					return false;
				});
				routeUtil.movePointsIntoRoute(route, points);
			}
		});
		return routes;
	};

	routeUtil.movePointsIntoRoute = function(route, points) {
		route.points = points.points;
		return route;
	};

	return routeUtil;
});