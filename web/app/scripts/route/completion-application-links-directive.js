define(['route/route-module',
       'application/application-util',
		'application/application-link-directive'
	],
	function(routeModule, appUtil) {
		'use strict';

		/**
		 * Provides a list of application links that link to the activity of the completion.
		 *
		 * @param {Array(Routes)} The routes to display
		 *
		 */
		routeModule.directive('tnCompletionApplicationLinks', function() {
				return {
					restrict: 'EA',
					transclude: false,
					template: '<span ng-if="completion.person.name" ng-repeat="(key, url) in uniqueApplicationList"><tn-application-link url="url" application-key="key" icon-only="true" class="medium"></tn-application-links></span>',
					replace: false,
					scope: {
						completion: '='
					},
					link: function(scope) {
						scope.applicationLinks = {};

						scope.uniqueApplicationList = [];

						scope.$watch('completion', function() {
							if (scope.completion) {
								scope.uniqueApplicationList = appUtil.uniqueApplicationsFromApplicationReferences(scope.completion.applicationReferences.applicationReference);
							}

						});

					}
				};
			}
		);
	});