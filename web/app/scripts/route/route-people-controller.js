define(['route/route-module',
	//
	'common/header-directive',
	'route/route-completions-directive',
	'route/completions-web-api-service',
], function(routeModule) {
	'use strict';

	return routeModule.controller('tnRoutePeopleController', function($scope,
			CompletionsWebApiService) {

			$scope.completions = {};

			$scope.configureCompletions = function() {
				var service = new CompletionsWebApiService()
					.limitByRouteId($scope.route.routeId)
					.orderBySocialCountRank()
					.withPerson()
					.withRoute();

				$scope.completions.service = service;
				return service;
			};

			$scope.configureCompletions();

		}
	);
});