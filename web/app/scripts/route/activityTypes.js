define([], function() {
	'use strict';

	return [{
			key: 'BIKE',
			name: 'Bike',
			icon: 'icon-bike',
			activityTitleSuffix: 'Bike Ride',
		}, {
			key: 'ROAD',
			name: 'Road Bike',
			icon: 'icon-bike',
			activityTitleSuffix: 'Bike Ride',
		}, {
			key: 'MTB',
			name: 'Mountain Bike',
			icon: 'icon-mtb',
			activityTitleSuffix: 'Bike Ride',
		}, {
			key: 'RUN',
			name: 'Run',
			icon: 'icon-run',
			activityTitleSuffix: 'Run',
		}, {
			key: 'WALK',
			name: 'Walk',
			icon: 'icon-hike',
			activityTitleSuffix: 'Walk',
		}, {
			key: 'HIKE',
			name: 'Walk',
			icon: 'icon-hike',
			activityTitleSuffix: 'Hike',
		}, {
			key: 'OTHER',
			name: 'Activity',
			icon: 'icon-unknown',
			activityTitleSuffix: 'Activity',
		},{
			key: 'U',
			name: 'Activity',
			icon: 'icon-unknown',
			activityTitleSuffix: 'Activity',
		},{
			key: 'SKI',
			name: 'Ski',
			icon: 'icon-unknown',
			activityTitleSuffix: 'Ski Trip',
		},{
			key: 'SKATE',
			name: 'Skate',
			icon: 'icon-unknown',
			activityTitleSuffix: 'Skate Session',
		},{
			key: 'SWIM',
			name: 'Swim',
			icon: 'icon-unknown',
			activityTitleSuffix: 'Swim',
		}

		// , {
		// 	key: 'UNKNOWN',
		// 	name: 'Unknown',
		// 	icon: 'icon-unknown',
		// 	activityTitleSuffix: 'Activity',
		// }
	];
});