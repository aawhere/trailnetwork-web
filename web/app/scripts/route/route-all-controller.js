define(['route/route-module',
	'route/completions-util',
	'explore/explore-events',
	//
	'common/header-directive',
	'route/route-completions-directive',
	'route/completions-web-api-service',
	'route/route-filter-service',
	'common/browser-service'
], function(routeModule, completionsUtil, exploreEvents) {
	'use strict';

	return routeModule.controller('tnRouteAllController', function($scope,
			CompletionsWebApiService,
			$translate,
			browserService,
			routeFilterService) {

			var VISITS_LIMIT = 5;

			$scope.visits = {};
			$scope.leaderboard = {};
			$scope.people = {};
			$scope.routeFilterService = routeFilterService;


			$scope.$watch('route.title', function(title) {
				if (title) {
					initialize();
					browserService.title(title);
				}
			});

			$scope.$on(exploreEvents.routeFilterUpdated, function() {
				$scope.configureLeaderboard().nextPage();
			});

			$scope.configureVisits = function() {
				var service = new CompletionsWebApiService()
					.limitByRouteId($scope.route.routeId)
					.orderBySocialOrder()
					.withPerson()
					.setLimit(VISITS_LIMIT);
				$scope.visits.service = service;
				return service;
			};

			$scope.configureLeaderboard = function() {
				var service = new CompletionsWebApiService()
					.limitByRouteId($scope.route.routeId)
					.orderBySocialRaceRank()
					.filteredByRouteFilter(routeFilterService)
					.withPerson()
					.setLimit(VISITS_LIMIT);
				$scope.leaderboard.service = service;
				return service;
			};

			$scope.configurePeople = function() {
				var service = new CompletionsWebApiService()
					.limitByRouteId($scope.route.routeId)
					.orderBySocialCountRank()
					.withPerson()
					.withRoute()
					.setLimit(VISITS_LIMIT);
				$scope.people.service = service;
				return service;
			};

			function initialize() {
				completionsUtil.chooseDefaultActivityTypeFilter(routeFilterService, $scope.route.route);
				$scope.activityTypeKeys = completionsUtil.activityTypeKeys($scope.route.route);
				$scope.configureLeaderboard().nextPage();
				$scope.configureVisits().nextPage();
				$scope.configurePeople().nextPage();
			}

		}
	);
});