define(['route/route-module',
		'underscore',
		'explore/explore-events',
		//
		'common/debug-service',
		'route/route-filter-service',
		'route/activity-type-filter-directive',
		'person/person-suggest-directive',
		'event/events-web-api-service',
	],
	function(routeModule, _, exploreEvents) {
		'use strict';

		/**
		 * Provides a ui for filtering routes
		 *
		 */
		routeModule.directive('tnRouteCompletionsFilter', function(debugService,
				routeFilterService,
				EventsWebApiService,
				$translate) {
				return {
					restrict: 'EA',
					transclude: true,
					templateUrl: '/views/route/route-completions-filter-directive.html',
					replace: false,
					scope: {
						//An array of activity type keys to display on the filter by activity type button.
						activityTypeKeys: '=',
						//Limit to courses only?
						courses: '=',
						//limit by people. Is an array of {id, text} where id is the person id and text is the person's name.
						people: '=',
						//limit by activity type
						activityType: '=?',
						//Don't allow activityTypes to be unselected,
						activityTypeToggleMode: '=?',
						//Flag the Events control on and off
						displayEvents: '=?',
						//limit by event
						routeEventId: '=?',
						//limit to routeId
						routeId: '=?',
					},
					link: function($scope) {

						var EVENTS_LIMIT = 100;

						if (_.isUndefined($scope.activityTypeToggleMode)) {
							$scope.activityTypeToggleMode = false; //The default
						}

						$scope.debug = debugService.enabled;
						if (_.isUndefined($scope.courses)) {
							$scope.courses = false;
						}
						$scope.routeFilterService = routeFilterService;
						if (_.isUndefined($scope.activityTypeKeys)) {
							$scope.activityTypeKeys = [];
						}

						$scope.$on(exploreEvents.routeFilterUpdated, function() {
							$scope.activityType = routeFilterService.activityType();
						});

						$scope.filterByCourses = function() {
							$scope.courses = $scope.courses ? false : true;
						};

						$scope.clearEvents = function() {
							console.log('clearevents');
							$scope.routeEventId = undefined;
						};

						$scope.events = {
							dropdown: [{
								text: '<i class="icon-cancel"></i>&nbsp;Clear Events',
								id: 0,
								click: $scope.clearEvents,
							}],
						};

						$scope.filterByEvent = function(eventId) {
							console.log('filterByEvent ' + eventId);
							$scope.routeEventId = eventId;
							//$scope.events.title = title;
						};

						$scope.addToDropdown = function(event) {
							var title = $scope.eventTitle(event);
							$scope.events.dropdown.push({
								text: '<i class="icon-event"></i>&nbsp;' + title,
								id: event.compositeId,
								click: $scope.filterByEvent,
							});
						};

						$scope.$watch('routeEventId', function() {
							$scope.updateTitle();
						});

						$scope.eventTitle = function(event) {
							return event.startTime.display.mediumDate + ' (' + event.completionsCount.display.formattedValue + ')';
						};

						$scope.updateTitle = function() {
							if ($scope.routeEventId && $scope.events.service) {
								_.each($scope.events.service.entities, function(routeEvent) {
									if (routeEvent.compositeId === $scope.routeEventId) {
										$scope.events.title = $scope.eventTitle(routeEvent);
									}
								});
							} else {
								$scope.defaultEventsTitle();
							}
						};

						$scope.defaultEventsTitle = function() {
							$translate('ROUTE_COMPLETION_EVENTS_LABEL').then(function(msg) {
								var title;
								var eventCount = $scope.events.dropdown.length - 1;
								if (eventCount === 0) {
									title = msg;
								} else {
									title = eventCount.toString() + ' ' + msg;
								}

								$scope.events.title = title;
							});
						};

						$scope.retrieveEvents = function() {
							var service = new EventsWebApiService()
								.setLimit(EVENTS_LIMIT);
							if ($scope.routeId) {
								service.limitToRouteId($scope.routeId);
							}
							if ($scope.activityType) {
								service.limitToActivityTypeKey(routeFilterService.activityType());
							}
							service.orderByScoreDesc();
							$scope.events.service = service;
							service.nextPage().then(function(events) {
								_.each(events, function(event) {
									$scope.addToDropdown(event);
								});
								$scope.updateTitle();
							});
						};

						$scope.$watch('activityType', function() {
							if ($scope.displayEvents && $scope.activityType) {
								$scope.retrieveEvents();
							}
						});
					}
				};
			}
		);
	});