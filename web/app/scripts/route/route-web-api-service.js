/**
 * 	Responsible for retreiving a route. May implement caching transparently to the consumer of the route.
 *
 * TODO: this should be called route-service.js, however that name is taken by the soon-to-be depricated route-service.js.
 */
define(['angular',
		'route/route-module',
		'common/common-tn-api-constants',
		'route/route-tn-api-constants',
		'route/route-util',
		'common/tn-api-constants',
		//
		'restangular',
		'common/entity-web-api-service',
		'route/route-repository',
		'route/route-gpolyline-service',
	],
	function(angular, routeModule, commonTnApiConstants, routeTnApiConstants, routeUtil, apiConstants) {
		'use strict';

		/**
		 * Responsible for retreiving a route.
		 *
		 */
		routeModule.factory('RouteWebApiService', ['Restangular',
			'$q',
			'$translate',
			'tnRouteRepository',
			'tnRouteGpolylineService',
			'EntityWebApiService',
			function(Restangular, $q, $translate, routeRepository, RouteGpolylineService, EntityWebApiService) {

				var Route = function(routeId) {
					var parsedRouteId = routeUtil.routeIdAsNumber(routeId);
					EntityWebApiService.call(this, {
						entityName: 'route',
						id: parsedRouteId,
						repo: routeRepository
					});
					this.routeGpolylineService = new RouteGpolylineService(parsedRouteId);

					this.addRequestOptions([
						apiConstants.options.routeGpolyline
					]);

					this.expandFields([
						apiConstants.fields.routeStartTrailheadId
					]);
					this.withCompletionStats();
				    this.withSocialCompletionStats();
				}; 

				Route.inheritsFrom(EntityWebApiService);


				Route.prototype.withBounds = function() {
					this.expandFields(apiConstants.fields.routeBoundingBox);
					return this;
				};

				Route.prototype.withCompletionStats = function() {
					this.expandFields(apiConstants.fields.routeCompletionStats);
					return this;
				};

				Route.prototype.withSocialCompletionStats = function() {
					this.expandFields(apiConstants.fields.routeSocialCompletionStats);
					return this;
				};

				/**
				 * Get the route from the server if not already retreived.
				 *
				 * @return {[Promise]} A promise for an tn.route.Route.
				 */
				Route.prototype.getFromWebApi = function() {
					var deferred = $q.defer();
					var context = this;

					Restangular.one(apiConstants.paths.routes, this.id)
						.get(context.requestParameters).then(function(route) {
							context.routeGpolylineService.get().then(function(points) {
								route = routeUtil.movePointsIntoRoute(route, {
									points: points
								});
								deferred.resolve(route);
							}, function() {
								deferred.reject();
							});
						}, function(err) {
							deferred.reject(err);
						});
					return deferred.promise;
				};

				return Route;
			}

		]);
	});