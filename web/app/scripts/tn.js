require(['tn-config'], function() {
	'use strict';

	/**
	 * RequireJS's concatenator requires only one level of 'require' statements. We are first
	 * bringing in the config 'tn-config', then when that is ready, requiring the main module 'tn-bootstrap', which has the single
	 * level require statement.
	 *
	 */
	require(['vendor', 'angular'],
		function() {
			require(['tn-bootstrap'], function() {});
		});
});