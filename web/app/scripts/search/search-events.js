define(function() {
	'use strict';

	/**
	 * Events for the map page.
	 */
	return {
		selected: 'search-result-selected',
	};
});