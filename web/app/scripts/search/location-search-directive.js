define(
	['map/map-module',
		'map/map-events',
		'jquery',
		//
		'map/map-util',
		'map/map-service',
		'common/notification-service',
		'common/reset-field-directive',
	],
	function(mapModule, mapEvents, jquery) {
		'use strict';

		/**
		 * Provides a search input overlayed on the map.
		 *
		 */
		mapModule.directive('tnLocationSearch', function(mapUtil,
			$timeout,
			$rootScope
			//$log,
			//notificationService
		) {
			return {
				restrict: 'ECMA',
				scope: {
					map: '=',
				},
				replace: false,
				transclude: true,
				templateUrl: '/views/search/location-search-directive.html',
				link: function(scope) {
					var googleMap, geocoder; //, marker;
					var MIN_ZOOM = 14;

					var mapWatcher = scope.$watch('map', function() {
						if (scope.map) {
							scope.map.onInitialized().then(function() {
								googleMap = scope.map.map();
								initialize();
							});
							//unbind so this is not called over and over.
							mapWatcher();
						}
					});

					scope.focus = function() {
						//console.log('broadcast ' + mapEvents.mapSearchFocus);
						$rootScope.$broadcast(mapEvents.mapSearchFocus);
					};

					scope.blur = function() {
						//console.log('broadcast ' + mapEvents.mapSearchBlur);
						$rootScope.$broadcast(mapEvents.mapSearchBlur);
					};

					scope.mouseOver = function() {
						//console.log('broadcast ' + mapEvents.mapSearchMouseOver);
						$rootScope.$broadcast(mapEvents.mapSearchMouseOver);
					};

					scope.mouseLeave = function() {
						//console.log('broadcast ' + mapEvents.mapSearchMouseLeave);
						$rootScope.$broadcast(mapEvents.mapSearchMouseLeave);
					};

					scope.clear = function() {
						scope.query = undefined;
					};

					scope.clearAndClose = function() {
						scope.clear();
						scope.close();
					};

					scope.close = function() {
						scope.searchOpen = false;
					};

					scope.focus = function() {
						if (scope.input) {
							scope.input.focus();
						}
					};

					function initialize() {
						geocoder = new google.maps.Geocoder();
						createSearchBox(googleMap, scope);
					}

					function createSearchBox(googleMap, scope) {
						var input, searchBox, place, marker, markers = [],
							image, placesService;
						placesService = new google.maps.places.PlacesService(googleMap);
						/**
						 * Create the search box and link it to the UI element.
						 * @type {HTMLInputElement}
						 */
						input = jquery("#search-input").get()[0];
						scope.input = input;

						/**
						 * @param {HTMLInputElement}
						 */
						searchBox = new google.maps.places.SearchBox(
							(input));

						// Listen for the event fired when the user selects an item from the
						// pick list. Retrieve the matching places for that item.
						google.maps.event.addListener(searchBox, 'places_changed', function() {
							search();
						});

						function search() {
							var places;
							places = searchBox.getPlaces();

							for (var i = 0; i < markers.length; i++) {
								marker = markers[i];
								marker.setMap(null);
							}

							handlePlaceResult(places);

							//A little time for it to display while the map updates, then clear.
							$timeout(function() {
								scope.clearAndClose();
							}, 300);
						}

						function handlePlaceResult(places) {
							// For each place, get the icon, place name, and location.
							var markers = [],
								i;

							var bounds = new google.maps.LatLngBounds();
							for (i = 0; i < places.length; i++) {
								place = places[i];
								window.maps = place;
								image = {
									url: place.icon,
									size: new google.maps.Size(71, 71),
									origin: new google.maps.Point(0, 0),
									anchor: new google.maps.Point(17, 34),
									scaledSize: new google.maps.Size(25, 25)
								};

								// Create a marker for each place.
								marker = new google.maps.Marker({
									map: googleMap,
									icon: image,
									title: place.name,
									position: place.geometry.location
								});

								markers.push(marker);
								if (typeof place.geometry.viewport === 'undefined') {
									bounds.extend(place.geometry.location);
								} else {
									bounds = place.geometry.viewport;
								}
								googleMap.fitBounds(bounds);
								if (scope.map.zoom() > MIN_ZOOM) {
									scope.map.zoom(MIN_ZOOM);
								}
							}
						}

						scope.search = function() {
							search();
							// var query, inputVal;
							// inputVal = jquery(input).val();
							// if (inputVal) {
							// 	//console.log('click search');
							// 	query = {};
							// 	query.bounds = googleMap.getBounds();
							// 	query.query = inputVal;
							// 	//console.log('searching for ', query);
							// 	$rootScope.$broadcast(mapEvents.mapSearchBefore, query);
							// 	placesService.textSearch(query, function(result, status) {
							// 		if (status !== google.maps.places.PlacesServiceStatus.OK) {
							// 			$log.debug('Error searching google places', query, result);
							// 			notificationService.networkingError();
							// 		}
							// 		handlePlaceResult(result);
							// 		$rootScope.$broadcast(mapEvents.mapSearch, query);
							// 	});
							// }
						};

						// Bias the SearchBox results towards places that are within the bounds of the
						// current map's viewport.
						google.maps.event.addListener(googleMap, 'bounds_changed', function() {
							var bounds = googleMap.getBounds();
							searchBox.setBounds(bounds);
						});
					}

					// function geocodeQuery(query) {
					// 	geocoder.geocode({
					// 		address: query,
					// 		bounds: map.getBounds()
					// 	}, function(results, status) {
					// 		if (status === google.maps.GeocoderStatus.OK) {
					// 			var result = results[0];
					// 			console.log('geocode result ', result);
					// 			if (result.geometry.viewport) {
					// 				map.fitBounds(result.geometry.viewport);
					// 			} else {
					// 				map.setCenter(result.geometry.location);
					// 			}
					// 			if (marker) {
					// 				marker.setMap(null);
					// 			}
					// 			marker = new google.maps.Marker({
					// 				map: map,
					// 				position: result.geometry.location
					// 			});
					// 		} else {
					// 			console.log('Geocode was not successful for the following reason: ' + status);
					// 		}
					// 	});
					// }

					// scope.search = function() {
					// 	var query;
					// 	query = scope.query;
					// 	if (query) {
					// 		console.log('searching for ', query);
					// 		$rootScope.$broadcast(mapEvents.mapSearchBefore, query);
					// 		geocodeQuery(query);
					// 		$rootScope.$broadcast(mapEvents.mapSearch, query);
					// 	}
					// };
				}

			};
		});
	});