define(
	['search/search-module',
		'common/search-tn-api-constants',
		'search/search-utils',
		//
		'search/search-web-api-service',
		'common/app-route-service',
	],
	function(searchModule,
		searchTnApiConstants,
		searchUtils) {
		'use strict';

		var DEFAULT_SEARCH_LIMIT = 5;

		/**
		 * service for searching against the web-api.
		 *
		 * @param  {[type]} SearchWebApiService [description]
		 * @param  {[type]} appRouteService     [description]
		 * @param  {Object} $translate)         {			var      searchService [description]
		 * @return {[type]}                     [description]
		 */
		searchModule.service('searchService', function(
			SearchWebApiService,
			appRouteService,
			$translate) {
			var searchService = {};

			searchService.SYSTEM_DOMAIN = 'system';

			/**
			 * Prepares the SearchWebApiService with the query, routeFilter, and desired indexes.
			 *
			 * @param  {[type]} query       [description]
			 * @param  {[type]} indexes     [description]
			 * @param  {[type]} routeFilter [description]
			 * @return {[type]}             [description]
			 */
			searchService.prepareWebApiService = function(query, indexes, routeFilter) {
				var service = new SearchWebApiService(query).withIndexes(indexes)
					.setLimit(DEFAULT_SEARCH_LIMIT)
					.withField(searchTnApiConstants.fields.route.id)
					.withField(searchTnApiConstants.fields.route.name)
					.withField(searchTnApiConstants.fields.route.trailheadName)
					.withField(searchTnApiConstants.fields.route.trailheadId);
				if (routeFilter && routeFilter.hasActivityType()) {
					service.limitByActivityType(routeFilter.activityType());
				}
				if (routeFilter && routeFilter.hasDistance()) {
					service.limitByDistance(routeFilter.distance());
				}
				if (routeFilter && routeFilter.hasPersons()) {
					service.limitByPersons(_.pluck(routeFilter.persons(), 'id'));
				}

				return service;
			};

			searchService.processWebApiServiceResults = function(documents) {
				documents.forEach(function(document) {
					var titleKey = searchTnApiConstants.fields.route.name;
					document.title = searchUtils.findValue(titleKey, document);
					var thName = searchUtils.findValue(searchTnApiConstants.fields.route.trailheadName, document);
					var thId = searchUtils.findValue(searchTnApiConstants.fields.route.trailheadId, document);
					if (thName) {
						$translate('ROUTE_TITLE_TRAILHEAD', {
							trailheadName: thName,
						}).then(function(name) {
							document.subtitle = name;
							document.subtitleUrl = appRouteService.explore.trailheadUrl(thId);
						});
					}
					//document.id = searchUtils.findValue(searchTnApiConstants.fields.route.id, document);
				});
				if (!documents || _.isEmpty(documents)) {
					documents = documents || [];
					documents.push({
						id: -1,
						domain: searchService.SYSTEM_DOMAIN,
						title: 'No Results',
					});
				}
				return documents;
			};

			return searchService;
		});
	});