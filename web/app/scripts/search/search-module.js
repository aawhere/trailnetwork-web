define([
	'angular',
	//
	'angular-bootstrap',
], function(angular) {
	'use strict';


	var searchModule = angular.module('tn.search', ['tn.common', 'ui.bootstrap']);

searchModule.run(["$templateCache", function($templateCache) {
		$templateCache.put("template/typeahead/typeahead-match.html",
			'<a tabindex="-1" bind-html-unsafe="match.label | typeaheadHighlight:query"></a>');
	}]);

	searchModule.run(["$templateCache", function($templateCache) {
		$templateCache.put("template/typeahead/typeahead-popup.html",
		    '<md-content role="select" class="md-whiteframe-z2 typeahead" ng-show="isOpen()" aria-hidden="{{!isOpen()}}">' +
		    '  <md-divider></md-divider>' +
		    '  <md-list>' +
			'    <md-item ng-repeat="match in matches track by $index" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index)" role="option" id="{{match.id}}">' +
			'      <div typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></div>\n' +
			'    </md-item>' +
			'    <md-divider ng-if="!$last"></md-divider>' +
			'    <div class="md-ripple-container"></div>' +
			'  </md-list>' +
			'</md-content>');
	}]);

	return searchModule;
});