define(['angular',
		'search/search-module',
		'common/search-tn-api-constants',
		'underscore',
		'common/common-util',
		'common/filter-condition-operators',
		//
		'restangular',
		'common/notification-service',
		'common/entities-web-api-service',
	],
	function(angular, searchModule, searchTnApiConstants, _, commonUtil, operators) {
		'use strict';

		/**
		 * Responsible for retreiving a list of people from completions.
		 *
		 */
		searchModule.factory('SearchWebApiService', function(Restangular, $q, EntitiesWebApiService) {

				var Search = function(query) {
					this.query = query;
					this.indexes = [];
					this.fields = [];
					EntitiesWebApiService.call(this, {
						entitiesPropertyName: 'searchDocuments',
					});
				};

				Search.inheritsFrom(EntitiesWebApiService);

				Search.prototype.withPersons = function() {
					this.indexes.push(searchTnApiConstants.indexes.person);
					this.url = searchTnApiConstants.paths.search;
					return this;
				};

				Search.prototype.withRoutes = function() {
					this.indexes.push(searchTnApiConstants.indexes.route);
					this.url = searchTnApiConstants.paths.routeSearch;
					return this;
				};

				Search.prototype.withTrialheads = function() {
					this.indexes.push(searchTnApiConstants.indexes.trailhead);
					this.url = searchTnApiConstants.paths.search;
					return this;
				};

				Search.prototype.withIndexes = function(indexes) {
					var dis = this;
					_.each(indexes, function(index) {
						if (index === searchTnApiConstants.indexes.route) {
							dis.withRoutes();
						}
						if (index === searchTnApiConstants.indexes.person) {
							dis.withPersons();
						}
						if (index === searchTnApiConstants.indexes.trailhead) {
							dis.withTrailheads();
						}
					});
					return this;
				};

				Search.prototype.where = function(key) {
					var dis = this;
					return {
						equals: function(value) {
							dis.addFilterCondition(key, operators.equals, value);
							return dis;
						},
						in: function(value) {
							dis.addFilterCondition(key, operators.in, value);
							return dis;
						},
					};
				};

				Search.prototype.limitByActivityType = function(activityType) {
					this.where(searchTnApiConstants.conditions.route.activityType).equals(activityType);
					return this;
				};

				Search.prototype.limitByPersons = function(personsIds) {
					var personIdsParsed = commonUtil.arrayToApiString(commonUtil.toArray(personsIds));
					this.where(searchTnApiConstants.conditions.route.personId).in(personIdsParsed);
					return this;
				};

				Search.prototype.limitByDistance = function(distanceCategory) {
					this.where(searchTnApiConstants.conditions.route.distanceCategory).equals(distanceCategory);
					return this;
				};

				Search.prototype.withField = function(field) {
					this.fields.push(field);
					return this;
				};

				/**
				 * Get the next page of people. If this is the first request, retreives the first page of results.
				 *
				 * @return {[Promise]} A promise for an array of tn.person.Person.
				 */
				Search.prototype.makeRequest = function() {
					var deferred = $q.defer();
					var restBuilder = getRestBuilder(this);

					restBuilder.get(buildRequestParameters(this, this.requestParameters)).then(function(persons) {
						deferred.resolve(persons);
					}, function(err) {
						deferred.reject(err);
					});
					return deferred.promise;
				};

				function getRestBuilder(context) {
					return Restangular.one(context.url);
				}

				function buildRequestParameters(context, requestParameters) {
					var params = {};
					params[searchTnApiConstants.options.query] = context.query;
					if (context.indexes) {
						params[searchTnApiConstants.options.indexes] = commonUtil.arrayToApiString(context.indexes);
					}
					if (context.fields && context.fields.length > 0) {
						params[searchTnApiConstants.options.fields] = commonUtil.arrayToApiString(context.fields);
					}
					return _.extend(requestParameters, params);
				}

				return Search;
			}

		);
	});