define(
	['search/search-module',
		'search/search-events',
		'underscore',
		'common/common-util',
		//
		'search/search-service',
	],
	function(searchModule, searchEvents, _, commonUtils) {
		'use strict';

		/**
		 * Provides a search input with auto-complete
		 *
		 */
		searchModule.directive('tnRouteSearch', function(
			searchService,
			$rootScope
		) {
			return {
				restrict: 'ECMA',
				scope: {
					routeFilter: '=',
					indexes: '=',
				},
				replace: false,
				templateUrl: '/views/search/search-directive.html',
				link: function($scope) {

					if (_.isUndefined($scope.indexes)) {
						throw "Indexes attribute required.";
					} else {
						$scope.indexes = commonUtils.toArray($scope.indexes);
					}

					$scope.suggest = function(query) {
						var service = searchService.prepareWebApiService(query, $scope.indexes, $scope.routeFilter);
						$scope.service = service;
						return service.get().then(function(documents) {
							return searchService.processWebApiServiceResults(documents);
						});

					};

					$scope.select = function(item, model /*, label*/ ) {
						if (model.domain === searchService.SYSTEM_DOMAIN) {
							$scope.query = '';
						} else if (model) {
							$rootScope.$broadcast(searchEvents.selected, model.id, model.domain);
						}
					};

				}

			};
		});



	});