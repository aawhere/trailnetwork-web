define(['underscore'], function(_) {
	'use strict';

	var util = {};

	util.find = function(key, document) {
		return _.find(document.fields, function(field) {
			if(field.key === key) {
				return true;
			} else {
				return false;
			}
		});
	};

	util.findValue = function(key, document) {
		var field = util.find(key, document);
		return field ? field.value : undefined;
	};

	return util;

});