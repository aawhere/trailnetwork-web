define(
	[
		'app-route-constants',
		'tn-module',
		'tn-messages',
		'common/common-util',
		'message-format',
		//
		'angular',
		'angular-uirouter',
		'angular-translate-static-loader',
		//'angular-translate-localstorage-provider',
		'angular-translate-interpolation-messageformat',
		'activity/activity-controller',
		'common/browser-service',
		'common/app-route-service',
		'common/spinner-directive',
		// 'common/unsafe',
		'explore/explore-controller',
		'explore/explore-routes-cards-controller',
		'explore/explore-routes-leaflets-controller',
		'explore/explore-route-cards-controller',
		'explore/explore-route-leaflets-controller',
		'explore/explore-route-chart-controller',
		'explore/explore-trailhead-cards-controller',
		'explore/explore-trailhead-leaflets-controller',
		'person/person-controller',
		'person/person-all-controller',
		'person/person-content-controller',
		'account/account-alias-controller',
		'playground/playground-controller',
		'restangular',
		'route/route-controller',
		'route/route-all-controller',
		'route/route-events-controller',
		'route/route-people-controller',
		'route/route-visits-controller',
		'route/route-leaderboard-controller',
		'tn-controller',
		'tn-templates',
		'user/user',
		'user/current-user-service',
	],
	function(routeConstants, tnModule, tnMessages, commonUtils) {
		'use strict';

		tnModule.config(function($stateProvider,
			$urlRouterProvider,
			RestangularProvider,
			$locationProvider,
			$translateProvider,
			$mdThemingProvider,
			matchmediaProvider
		) {
			var rc = routeConstants;

			//Enable HTML5 style urls with fall back for old browsers.
			$locationProvider.html5Mode({
				enabled: true,
				requireBase: false
			});

			// For any unmatched url, redirect to /state1
			$urlRouterProvider.otherwise('/explore');

			//match /route/:routeId exactly (not /route/:routeId/all) and redirect to a default state.
			$urlRouterProvider.when(/^(?!\/route\/(.*)\/.*)\/route\/(.*)/, function($state, $match) {
				$state.go(rc.states.routeAll, {
					routeId: $match[2]
				});
			});
			//match /person/:personId exactly (not /person/:personId/all) and redirect to a default state.
			$urlRouterProvider.when(/^(?!\/person\/(.*)\/.*)\/person\/(.*)/, function($state, $match) {
				$state.go(rc.states.personAll, {
					personId: $match[2]
				});
			});

			$stateProvider
				.state(rc.states.explore, {
					url: '?' + rc.routeFilterQueryParameterName + rc.params.paramsMore + rc.params.admin + rc.params.paramsMore + rc.params.boxes + rc.params.paramsMore + rc.params.allRoutes,
					templateUrl: '/views/explore/explore.html',
					controller: 'tnExploreController',
					reloadOnSearch: false,
					abstract: true,
					resolve: {
						currentUser: function(currentUserService, $q) {
							var deferred = $q.defer();
							currentUserService.currentUser().then(function(user) {
								deferred.resolve(user);
							}, function() {
								deferred.resolve({error: '401'});
							});
							deferred.resolve();
							return deferred.promise;
						}
					}
				})
				.state(rc.states.exploreRoutes, {
					url: rc.paths.seperator + rc.paths.explore + rc.params.paramsStart + rc.locationQueryParameterName,
					templateUrl: '/views/explore/explore-routes.html',
					views: {
						cardTray: {
							templateUrl: '/views/explore/explore-routes-cards.html',
							controller: 'tnExploreRoutesCardsController',
						},
						leaflets: {
							templateUrl: '/views/explore/explore-routes-leaflets.html',
							controller: 'tnExploreRoutesLeafletsController',
						}
					},
					reloadOnSearch: false,
				})
				.state(rc.states.exploreTrailhead, {
					url: rc.paths.seperator + rc.paths.explore + rc.paths.seperator + rc.paths.trailhead + rc.paths.seperator + ':trailheadId',
					reloadOnSearch: false,
					views: {
						cardTray: {
							templateUrl: '/views/explore/explore-trailhead-cards.html',
							controller: 'tnExploreTrailheadCardsController',
						},
						leaflets: {
							templateUrl: '/views/explore/explore-trailhead-leaflets.html',
							controller: 'tnExploreTrailheadLeafletsController',
						}
					},
				})
				.state(rc.states.exploreRoute, {
					url: rc.paths.seperator + rc.paths.explore + rc.paths.seperator + rc.paths.route + rc.paths.seperator + ':routeId',
					reloadOnSearch: false,
					views: {
						"leaflets": {
							templateUrl: '/views/explore/explore-route-leaflets.html',
							controller: 'tnExploreRouteLeafletsController',
						},
						cardTray: {
							templateUrl: '/views/explore/explore-route-cards.html',
							controller: 'tnExploreRouteCardsController',
						}
					},
					resolve: {
						routeId: function($stateParams) {
							return commonUtils.idAsNumber($stateParams.routeId);
						}
					}
				})
				.state(rc.states.route, {
					abstract: true,
					url: rc.paths.seperator + rc.paths.route + rc.paths.seperator + ':routeId',
					templateUrl: '/views/route/route.html',
					controller: 'tnRouteController',
					data: {
						// permissions: {
						// only: ['admin']
						// }
					},
				})
				.state(rc.states.routeAll, {
					url: rc.paths.seperator + rc.paths.all,
					templateUrl: '/views/route/route-all.html',
					controller: 'tnRouteAllController',
				})
				.state(rc.states.routePeople, {
					url: rc.paths.seperator + rc.paths.people,
					templateUrl: '/views/route/route-people.html',
					controller: 'tnRoutePeopleController',
				})
				.state(rc.states.routeCompletions, {
					url: rc.paths.seperator + rc.paths.recent + '?' + rc.routeFilterQueryParameterName + '&' + rc.routeCompletionQueryParameterName,
					templateUrl: '/views/route/route-completions.html',
					controller: 'tnRouteVisitsController',
					reloadOnSearch: false,
				})
				.state(rc.states.routeLeaderboard, {
					url: rc.paths.seperator + rc.paths.leaderboard + '?' + rc.routeFilterQueryParameterName + '&' + rc.routeCompletionQueryParameterName,
					templateUrl: '/views/route/route-leaderboard.html',
					controller: 'tnRouteLeaderboardController',
					reloadOnSearch: false,
				})
				.state(rc.states.routeEvents, {
					url: rc.paths.seperator + rc.paths.events + '?' + rc.routeFilterQueryParameterName + '&' + rc.routeCompletionQueryParameterName,
					templateUrl: '/views/route/route-events.html',
					controller: 'tnRouteEventsController',
					reloadOnSearch: false,
				})
				.state(rc.states.account, {
					url: rc.paths.seperator + rc.paths.account + rc.paths.seperator + ':accountId',
					template: '<div></div>',
					controller: 'tnAccountAliasController',
					data: {
						// permissions: {
						// only: ['admin']
						// }
					},
				})
				.state(rc.states.person, {
					abstract: true,
					url: rc.paths.seperator + rc.paths.person + rc.paths.seperator + ':personId',
					templateUrl: '/views/person/person.html',
					controller: 'tnPersonController',
					data: {
						// permissions: {
						// only: ['admin']
						// }
					},
				})
				.state(rc.states.personAll, {
					url: rc.paths.seperator + rc.paths.all,
					templateUrl: '/views/person/person-all.html',
					controller: 'tnPersonAllController',
				})
				.state(rc.states.personActivities, {
					url: rc.paths.seperator + rc.paths.activities,
					templateUrl: '/views/person/person-activities.html',
					controller: 'tnPersonContentController',
				})
				.state(rc.states.personPopularRoutes, {
					url: rc.paths.seperator + rc.paths.routes + rc.paths.seperator + rc.paths.popular,
					templateUrl: '/views/person/person-popular-routes.html',
					controller: 'tnPersonContentController',
				})
				.state(rc.states.personCompletions, {
					url: rc.paths.seperator + rc.paths.routes,
					templateUrl: '/views/person/person-completions.html',
					controller: 'tnPersonContentController',
				})
				.state(rc.states.personLeaderboard, {
					url: rc.paths.seperator + rc.paths.leaderboard,
					templateUrl: '/views/person/person-leaderboard.html',
					controller: 'tnPersonContentController',
				})
				.state(rc.states.activity, {
					url: rc.paths.seperator + rc.paths.activity + rc.paths.seperator + ':activityId',
					templateUrl: '/views/activity/activity.html',
					controller: 'tnActivityController',
					data: {
						// permissions: {
						// only: ['admin']
						// }
					},
				})
				.state('terms', {
					url: '/terms',
					templateUrl: '/views-nopreprocess/terms/conditions.html',
					//controller: 'tnPlaygroundController',
				})
				.state('privacy', {
					url: '/privacy',
					templateUrl: '/views-nopreprocess/terms/privacy.html',
					//controller: 'tnPlaygroundController',
				})
				.state('notFound', {
					templateUrl: '/views/not-found.html',
				})
				.state('playground', {
					url: '/playground',
					templateUrl: '/views/playground.html',
					controller: 'tnPlaygroundController',
				});

			RestangularProvider.setBaseUrl(rc.proxyServerBaseRestUrl);
			RestangularProvider.setRestangularFields({
				route: 'restangularRoute'
			});

			RestangularProvider.setDefaultHeaders({
				"Accept-Localization": "true",
			});

			//TODO: this caches every request. Should we be requesting every request?
			RestangularProvider.setDefaultHttpFields({
				cache: false
			});
			RestangularProvider.setRequestSuffix('.json');

			$translateProvider.translations('en', tnMessages);
			$translateProvider.registerAvailableLanguageKeys(['en'], {
				'en_*': 'en',
			});

			$translateProvider.useStaticFilesLoader({
				prefix: '/locale/',
				suffix: '.json'
			});

			$translateProvider.addInterpolation('$translateMessageFormatInterpolation');
			//$translateProvider.preferredLanguage('en');

			$translateProvider.determinePreferredLanguage();
			//$translateProvider.useLocalStorage();

			//$translateProvider.fallbackLanguage(['en']);


			$mdThemingProvider.theme('red')
				.primaryColor('red');
			$mdThemingProvider.theme('orange')
				.primaryColor('orange');

			matchmediaProvider.rules.phone = "(max-width: 600px)";
			matchmediaProvider.rules.tablet = "(min-width: 601px) and (max-width: 960px)";
			matchmediaProvider.rules.desktop = "(min-width: 961px)";
		});

		tnModule.run(function(Permission, user, $q, $rootScope, $window) {
			var verifyUserHasRole = function(role) {
				console.log('verify user in role ' + role);
				var deferred = $q.defer();

				user.role().then(function(data) {
					if (data === 'admin') {
						console.log('hasUserRole: ' + role + true);
						deferred.resolve(true);
					} else if (data === 'user') {
						if (role === 'admin') {
							deferred.reject();
						} else {
							console.log('hasUserRole: ' + role + true);
							deferred.resolve(true);
						}
					} else { //anonymous
						if (role === 'anonymous') {
							console.log('hasUserRole: ' + role + true);
							deferred.resolve(true);
						} else {
							//we are authenticated, not anonymous
							deferred.reject();
						}
					}
				}, function() {
					// Error with request
					deferred.reject();
				});

				return deferred.promise;
			};

			Permission.defineRole('user', function( /*stateParams*/ ) {
					return verifyUserHasRole('user');
				})
				.defineRole('admin', function( /*stateParams*/ ) {
					return verifyUserHasRole('admin');
				})
				.defineRole('anonymous', function( /*stateParams*/ ) {
					return verifyUserHasRole('anonymous');
				});

			$rootScope.$on('$stateChangePermissionDenied', function() {
				user.get().then(function(u) {
					if (u.links.login) {
						$window.location.href = u.links.login;
					}
				});
			});
		});

		return tnModule;
	});