/**
	 * RequireJS's concatenator requires only one level of 'require' statements. We are first
	 * bringing in the config 'tn-config', then when that is ready, requiring the main module 'tn-bootstrap', which has the single
	 * level require statement.
	 *
*/
require(['angular', 'domReady!', 'common/common-module', 'angular-material', 'common/header-directive', 'common/compact-footer-directive'],
	function(angular, document) {
		'use strict';

		angular.module('tn-auth', [
			'tn.common',
			'ngMaterial',
		]);

		angular.bootstrap(document, ['tn-auth']);
	});