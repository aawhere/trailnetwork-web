define([], function() {
	'use strict';

	var activityUtil = {}, ID_DELIMINATOR = '-';

	activityUtil.applicationKey = function(activityId) {
		return activityId.split(ID_DELIMINATOR)[0];
	};

	activityUtil.remoteId = function(activityId) {
		return activityId.split(ID_DELIMINATOR)[1];
	};

	return activityUtil;
});