define(['angular',
		'activity/activity-module',
		'common/tn-api-constants',
		'common/common-util',
		'underscore',
		//
		'restangular',
		'common/notification-service',
		'common/entities-web-api-service',
	],
	function(angular, activityModule, apiConstants, commonUtil, _) {
		'use strict';

		/**
		 * Responsible for retreiving route completions.
		 *
		 */
		activityModule.factory('ActivitiesWebApiService', function(Restangular, $q, notificationService, $translate, EntitiesWebApiService) {

				var Activities = function() {
					EntitiesWebApiService.call(this, {
						entitiesPropertyName: 'activity',
					});
					this.personIds = [];
				};

				Activities.inheritsFrom(EntitiesWebApiService);

				/**
				 * Limit results to the provided people.
				 *
				 * @param  {String or Array[String]} people
				 * @return {this}
				 */
				Activities.prototype.limitByPersonId = function(personId) {
					if(personId && _.isArray(personId) && personId.length > 0) {
						this.personId = personId[0];
					}
					if (personId) {
						this.personId = personId;
					}
					return this;
				};

				/**
				 * Include the account with the response.
				 * @return {[type]} [description]
				 */
				Activities.prototype.withAccount = function() {
					this.expandFields(apiConstants.fields.activityAccountId);
					return this;
				};

				Activities.prototype.orderByStartTimeDesc = function() {
					this.orderBy(apiConstants.fields.activityStartTime, apiConstants.orderBy.descending);
					return this;
				};

				/**
				 * Get the next page of Activities. If this is the first request, retreives the first page of results.
				 *
				 * @return {[Promise]} A promise for an array of tn.activity.Activity
				 */
				Activities.prototype.makeRequest = function() {
					var deferred = $q.defer();
					var restBuilder = getRestBuilder(this);

					restBuilder.get(this.requestParameters).then(function(activities) {
						deferred.resolve(activities);
					}, function(err) {
						deferred.reject(err);
					});
					return deferred.promise;
				};

				function getRestBuilder(context) {
					var builder = Restangular;
					if (context.personId) {
						builder = builder.one(apiConstants.paths.persons, context.personId)
						.one(apiConstants.paths.activities);
					} else {
						builder = builder.one(apiConstants.paths.activities);
					}
					return builder;
				}

				return Activities;
			}

		);
	});