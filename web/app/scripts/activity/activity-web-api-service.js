define(['angular',
		'activity/activity-module',
		'common/common-tn-api-constants',
		'activity/activity-tn-api-constants',
		'common/tn-api-constants',
		//
		'restangular',
		'common/notification-service',
		'common/entity-web-api-service',
	],
	function(angular, activityModule, commonTnApiConstants, activityTnApiConstants, apiConstants) {
		'use strict';

		/**
		 * Responsible for retreiving an activity.
		 *
		 */
		activityModule.factory('ActivityWebApiService', function(Restangular, $q, notificationService, $translate, EntityWebApiService) {

				var Activity = function(activityId) {
					EntityWebApiService.call(this, {
						entityName: 'activity',
						id: activityId,
					});
					this.requestOptions = [];
				};

				Activity.inheritsFrom(EntityWebApiService);

				/**
				 * Include the account with the response.
				 * @return {[type]} [description]
				 */
				Activity.prototype.withAccount = function() {
					this.requestOptions.push(activityTnApiConstants.options.account);
					return this;
				};

				Activity.prototype.withGPolyline = function() {
					this.requestOptions.push(apiConstants.options.gpolyline);
					return this;
				};

				/**
				 * Get the activity fronm the server if not already retreived.
				 *
				 * @return {[Promise]} A promise for an tn.activity.Activity.
				 */
				Activity.prototype.getFromWebApi = function() {
					var deferred = $q.defer();

					Restangular.one(activityTnApiConstants.paths.activities, this.id).get(requestParameters(this))
						.then(function(activity) {
							//The map thinks we are providing a route, which has points in this location.
							if (activity.track && activity.track.points) {
								activity.points = activity.track.points.details;
							}
							deferred.resolve(activity);
						}, function() {
							deferred.reject();
						});
					return deferred.promise;
				};

				function requestParameters(context) {
					var params = {};
					params[commonTnApiConstants.options] = context.requestOptions;
					return params;
				}

				return Activity;
			}

		);
	});