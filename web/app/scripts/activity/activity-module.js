define(['angular'], function(angular) {
	'use strict';

	var activityModule;

	activityModule = angular.module('tn.activity', []);

	return activityModule;
});