define(['activity/activity-module',
	'app-route-constants',
	'route/activityTypes',
	//
	'activity/activity-web-api-service',
	'route/completions-web-api-service',
	'activity/activity-map-controller',
	'common/list-more-directive',
	'route/route-completions-directive',
	'person/persons-web-api-service',
], function(activityModule, appRouteConstants, activityTypes) {
	'use strict';

	return activityModule.controller('tnActivityController', function($scope,
		$stateParams,
		ActivityWebApiService,
		CompletionsWebApiService,
		PersonsWebApiService,
		appRouteService) {

		var VISITS_LIMIT = 1000;
		var UNKNOWN_ACTIVITY = 'UNKNOWN';

		$scope.appRouteService = appRouteService;
		$scope.activityId = $stateParams.activityId;
		$scope.appRouteConstants = appRouteConstants;


		new ActivityWebApiService($scope.activityId).withGPolyline().get().then(function(activity) {
			if(!_.isObject(activity)) {
				$scope.tn.displayErrorPage({
					status: 404,
					data: activity,
				});
			}
			$scope.activity = activity;
			new PersonsWebApiService()
				.limitToAccounts([activity.accountId.value])
				.nextPage()
				.then(function(people) {
					$scope.person = people[0];
					$scope.setActivityTitle();
				}, function(err) {
					$scope.tn.displayErrorPage(err);
				});

		}, function(err) {
			$scope.tn.displayErrorPage(err);
		});

		$scope.setActivityTitle = function() {
			var title;
			var activityTitle = $scope.activity.name.$;
			var activityType = _.find(activityTypes, function(at) {
				return $scope.activity.activityType.key === at.key;
			});
			var activityTypeSuffix;
			if (activityType) {
				activityTypeSuffix = activityType.activityTitleSuffix;
			} else {
				activityTypeSuffix = 'Activity';
			}
			if ($scope.activity.name.key === UNKNOWN_ACTIVITY) {
				title = $scope.person.name + '\'s ' + $scope.activity.track.start.timestamp.display.mediumDate;
			} else {
				title = activityTitle;
			}
			$scope.activityTitle = title + ' ' + activityTypeSuffix;
		};



		$scope.completions = {};

		$scope.configureCompletions = function() {
			var service = new CompletionsWebApiService()
				.limitByActivityId($scope.activityId)
				.orderByStartTimeDesc()
				.withRoute()
				.setLimit(VISITS_LIMIT);
			$scope.completions.service = service;
			return service;
		};

		$scope.configureCompletions().nextPage();
	});
});