define(['activity/activity-module',
	//
	'application/application-link-directive',
	'common/app-route-service'
], function(activityModule) {
	'use strict';

	/**
	 * Provides a list of completions to be displayed in either a table or list.
	 *
	 * @param {Array(Routes)} The routes to <display></display>
	 *
	 */
	activityModule.directive('tnActivities', function(usSpinnerService,
		         appRouteService) {
			return {
				restrict: 'EA',
				template: '<div ng-include="template.url"></div>',
				replace: false,
				scope: {
					//Array[String] or personIds
					activitiesWebApiService: '=',
					//template url
					templateUrl: '@',
					//alternate context. Useful for providing custom vaiables for a template.
					context: '=',
				},
				link: {
					pre: function($scope, el, attr) {
						$scope.template = {};
						if (attr.templateUrl) {
							$scope.template.url = attr.templateUrl;
						} else {
							$scope.template.url = '/views/activities/activities-table-directive.html';
						}
					},
					post: function($scope) {

						$scope.appRouteService = appRouteService;

						var spinnerKey = 'activitiesSpinner';

						$scope.$watch('activitiesWebApiService.busy', function(isBusy) {
							if (isBusy) {
								if ($scope.activitiesWebApiService.busy) {
									usSpinnerService.spin(spinnerKey);
								}
							} else {
								usSpinnerService.stop(spinnerKey);
							}
						});
					}
				}
			};
		}
	);
});