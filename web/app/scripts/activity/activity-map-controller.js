define(['underscore',
	'activity/activity-module',
	'common/entities',
	'map/map-events',
	'explore/explore-events',
	'map/location',
	'map/bounding-box',
	//
	'map/map-service',
	'route/route-detail-directive',
	'trailhead/trailhead-web-api-service',
], function(_, activityModule, Entities, mapEvents, exploreEvents, Location, BoundingBox) {
	'use strict';

	return activityModule
		.controller('tnActivityMapController', function($scope,
			$rootScope,
			$timeout,
			Map) {

			$scope.map = {
				visible: true,
			};
			$scope.map.activities = {
				entities: new Entities(),
				selectable: false,
				highlight: false,
			};

			$scope.map.trailheads = {
				entities: new Entities(),
				options: {
					icon: '/images/thMarker-16-red.png'
				},
				selectedOptions: {
					icon: '/images/thMarker-16-red.png'
				},
			};

			$scope.$watch('activity', function(activity) {
				if (activity) {
					//The creation will happen on the next digest.
					$scope.mapService = new Map({
						center: BoundingBox.createFromBoundingBox(activity.track.boundingBox).googleLatLngBounds().getCenter(),
						zoom: 14,
					});
					$scope.mapService.onInitialized().then(function() {
						$scope.mapService.bounds(activity.track.boundingBox);
						$scope.map.activities.entities.add(activity);
						$rootScope.$broadcast(exploreEvents.routeSelected, activity.id);
					});
					//Activities do not have trailheads, but we need to display the start of the trailhead.
					//This accomplishes that.
					var startTh = {
						id: Location.createFromLocation(activity.track.start.location).toString(),
						location: activity.track.start.location,
					};
					$scope.map.trailheads.entities.add(startTh);
					var finishTh = {
						id: Location.createFromLocation(activity.track.finish.location).toString(),
						location: activity.track.finish.location,
					};
					$scope.map.trailheads.entities.add(finishTh);
				}
			});

			$scope.$on(mapEvents.mapBoundsChanged, function() {
				transitionMapType();
			});

			/**
			 * Convienence method for e2e testing.
			 *
			 * @param {[type]} mapTypeId [description]
			 */
			$scope.setMapTypeId = function(mapTypeId) {
				$scope.mapService.mapTypeId(mapTypeId);
			};

			//TODO: this is repeated in route-map-controller and explore-controller. Move to common location.
			function transitionMapType() {
				if ($scope.mapService.zoom() >= $scope.mapService.maxZoomForMapType($scope.mapService.mapTypeId())) {
					var currentMapType = $scope.mapService.mapTypeId();
					if (currentMapType === 'terrain') {
						$scope.mapService.mapTypeId('roadmap');
						$scope.map.transitionMapTypeId = currentMapType;
					}
				} else if ($scope.map.transitionMapTypeId && $scope.mapService.zoom() < $scope.mapService.maxZoomForMapType($scope.map.transitionMapTypeId)) {
					if ($scope.map.transitionMapTypeId === 'terrain' && $scope.mapService.mapTypeId() === 'roadmap') {
						$scope.mapService.mapTypeId($scope.map.transitionMapTypeId);
					}
					$scope.map.transitionMapTypeId = undefined;
				}
			}

		});
});