define(['user/user-module'], function(userModule) {
	'use strict';

	userModule.service('user', function(Restangular, $q) {

		var currentUser;

		var _getUser = function() {
			var deferred = $q.defer();

			if (currentUser) {
				deferred.resolve(currentUser);
			} else {

				Restangular.withConfig(function(RestagularConfigurer) {
					RestagularConfigurer.setRequestSuffix('');
					RestagularConfigurer.setBaseUrl('/');
				}).one('users').one('current').get().then(function(user) {
					deferred.resolve(user);
				}, function(err) {
					deferred.reject(err);
				});
			}

			return deferred.promise;
		};

		var User = function() {

		};

		User.prototype.isLoggedIn = function() {
			_getUser().then(function(user) {
				if(user) {
					return true;
				} else {
					return false;
				}
			});
		};

		User.prototype.get = function() {
			return _getUser();
		};

		// User.prototype.role = function() {
		// 	var deferred = $q.defer();

		// 	_getUser().then(function(user) {
		// 		if (user.isLoggedIn && user.isAdmin) {
		// 			deferred.resolve('admin');
		// 		} else if (user.isLoggedIn) {
		// 			deferred.resolve('user');
		// 		} else {
		// 			deferred.resolve('anonymous');
		// 		}
		// 	});

		// 	return deferred.promise;
		// };

		return new User();
	});
});