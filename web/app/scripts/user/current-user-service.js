define(['user/user-module'], function(userModule) {
	'use strict';

	userModule.service('currentUserService', function(CurrentUserWebApiService, $q) {

		var personPromise;
		var apiService = new CurrentUserWebApiService();

		var getCurrentUser = function() {
			var defered = $q.defer();

			apiService.get().then(function(person) {
				defered.resolve(person);
			}, function(error) {
				if (error.status === 401) {
					defered.reject();
				} else {
					defered.reject();
					console.error("Error retreiving current user:", error);
				}
			});
			personPromise = defered.promise;
			return personPromise;
		};

		return {
			currentUser: function() {
				if(!personPromise) {
					return getCurrentUser();
				} else {
					return personPromise;
				}
			}
		};
	});
});