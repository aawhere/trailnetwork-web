/**
 * 	Responsible for retreiving the Person for the currently logged in user. May implement caching transparently
 * 	to the consumer of the route.
 *
 */
define(['user/user-module',
		'common/tn-api-constants',
		'app-route-constants',
		//
		'common/entity-web-api-service',
		'restangular',
	],
	function(userModule, apiConstants, rc) {
		'use strict';

		/**
		 * Responsible for retreiving a route.
		 *
		 */
		userModule.factory('CurrentUserWebApiService', function(
				Restangular,
				EntityWebApiService) {

			var baseRestangular = Restangular.withConfig(function(RestangularConfigurer) {
				RestangularConfigurer.setBaseUrl(rc.proxyServerBaseUrl);
				RestangularConfigurer.setRequestSuffix(undefined);
			});

				var CurrentUser = function() {
					EntityWebApiService.call(this, {
						entityName: 'Person',
						id: undefined,
					});
				};

				CurrentUser.inheritsFrom(EntityWebApiService);

				/**
				 * Get the route from the server if not already retreived.
				 *
				 * @return {[Promise]} A promise for an tn.route.Route.
				 */
				CurrentUser.prototype.getFromWebApi = function() {

					return baseRestangular.one(apiConstants.paths.currentUser)
						.get();
				};

				return CurrentUser;
			}

		);
	});