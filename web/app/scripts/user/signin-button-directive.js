define(['user/user-module',
		'common/tn-api-constants',
		'underscore',
		'person/preference-constants',
		'common/dropdown-menu-directive',
		//
		'common/click-outside-directive',
	],
	function(userModule,
		tnApiConstants,
		_,
		preferenceConstants) {
		'use strict';

		/**
		 * Provides a formatted link to a provider
		 *
		 * @param {String} The url of the provider
		 *
		 */
		userModule.directive('tnSigninButton', function(appRouteService, ipCookie, $translate) {
			return {
				restrict: 'EA',
				templateUrl: '/views/user/signin-button-directive.html',
				require: '^tnMapControl',
				replace: false,
				scope: {
					user: '=',
					preferences: '=',
				},
				link: function($scope, el, attr, tnMapControl) {
					$scope.tnApiConstants = tnApiConstants;
					$scope.preferenceConstants = preferenceConstants;
					$scope.mapCtrl = tnMapControl;

					$scope.$watch('user', function(newUser) {
						if (newUser) {
							$scope.profileUrl = appRouteService.person.personAllUrl(newUser.id);
							var accountId = _.find($scope.user.accountId, function(accountId) {
								return (accountId.app === 'gi');
							});
							$scope.account = _.find($scope.user.accounts.account, function(account) {
								return (account.id === accountId.value);
							});
						}
					});

					$scope.signout = function() {
						ipCookie.remove('gtoken', {
							path: '/',
						});
						$scope.user = null;
					};

					$scope.$watch('preferences.unitsOfMeasure', function(selected) {
						if (selected) {
							var uoms = _.values(preferenceConstants.unitsOfMeasure);
							$scope.selectedUnitsOfMeasureConstant = _.find(uoms, function(uom) {
								if (uom.key === $scope.preferences.unitsOfMeasure) {
									return true;
								}
								return false;
							});
							$translate($scope.selectedUnitsOfMeasureConstant.translationKey).then(function(name) {
								$scope.selectedUnitsOfMeasureName = name;
							});
						}
					});

				}
			};
		});
	});