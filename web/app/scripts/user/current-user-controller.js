define(['user/user-module',
	'person/preference-constants',
	'user/user-events',
	//
	'user/current-user-web-api-service',
], function(
	userModule, preferenceConstatants, userEvents) {
	'use strict';

	userModule.controller('tnCurrentUserController', function($scope,
		currentUserService,
		ipCookie,
		$rootScope,
		$timeout
	) {

		$scope.initialized = false;
		$scope.notSignedIn = true;
		$scope.preferences = {
			//default
			unitsOfMeasure: undefined,
		};

		currentUserService.currentUser().then(function(person) {
			$scope.initialized = true;
			$scope.notSignedIn = false;
			$scope.currentUser = person;

		}, function() {
			$scope.initialized = true;
		});

		$scope.$watch('preferences.unitsOfMeasure', function() {
			var uom = $scope.preferences.unitsOfMeasure;
			console.log("unitsOfMeasure changed: ", uom);
			ipCookie('unitOfMeasure', uom, {
				expires: 10*365,
				path: '/',
			});
			$timeout(function() {
				$rootScope.$broadcast(userEvents.unitOfMeasurePreferenceChanged, uom);
			});
		});

		function setUnitOfMeasureFromCookie() {
			var uom = ipCookie('unitOfMeasure');
			if (uom) {
				$scope.preferences.unitsOfMeasure = uom;
			}
		}

		setUnitOfMeasureFromCookie();
	});
});