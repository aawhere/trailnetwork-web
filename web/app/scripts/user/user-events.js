define(function() {
	'use strict';

	/**
	 * Events for the map page.
	 */
	return {
		unitOfMeasurePreferenceChanged: 'unit-of-measure-preference-changed',
	};
});