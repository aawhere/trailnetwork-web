define(['angular'], function(angular) {
	'use strict';

	var eventModule;

	eventModule = angular.module('tn.event', []);

	return eventModule;
});