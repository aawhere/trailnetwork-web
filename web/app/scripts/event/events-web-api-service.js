define(['angular',
		'event/event-module',
		'common/tn-api-constants',
		'common/filter-condition-operators',
		//
		'restangular',
		'common/entities-web-api-service',
	],
	function(angular, eventModule, apiConstants, operators) {
		'use strict';

		/**
		 * Responsible for retreiving route completions.
		 *
		 */
		eventModule.factory('EventsWebApiService', function(Restangular, $q, EntitiesWebApiService) {

				var Events = function() {
					EntitiesWebApiService.call(this, {
						entitiesPropertyName: 'routeEvent',
					});
				};

				Events.inheritsFrom(EntitiesWebApiService);

				/**
				 * Limit results to the provided routeId.
				 *
				 * @param  {String} routeId
				 * @return {this}
				 */
				Events.prototype.limitToRouteId = function(routeId) {
					if (routeId) {
						this.routeId = routeId;
					}
					return this;
				};

				/**
				 * Limit results to the provided activityType.
				 *
				 * @param  {tn.route.ActivityType} activityType
				 * @return {this}
				 */
				Events.prototype.limitToActivityTypeKey = function(activityTypeKey) {
					if (activityTypeKey) {
						this.addFilterCondition(apiConstants.fields.routeEventActivityType, operators.equals, activityTypeKey);
					}
					return this;
				};

				Events.prototype.orderByScoreDesc = function() {
					this.orderBy(apiConstants.fields.eventScore, apiConstants.orderBy.descending);
				};

				/**
				 * Get the next page of Events. If this is the first request, retreives the first page of results.
				 *
				 * @return {[Promise]} A promise for an array of tn.event.Event
				 */
				Events.prototype.makeRequest = function() {
					var deferred = $q.defer();
					var restBuilder = getRestBuilder(this);

					restBuilder.get(this.requestParameters).then(function(events) {
						deferred.resolve(events);
					}, function(err) {
						deferred.reject(err);
					});
					return deferred.promise;
				};

				function getRestBuilder(context) {
					var builder = Restangular;
					if (context.routeId) {
						builder = builder.one(apiConstants.paths.routes, context.routeId);
					} else {
						throw 'Must provide a routeId';
					}
					builder = builder.one(apiConstants.paths.events);
					return builder;
				}

				return Events;
			}

		);
	});