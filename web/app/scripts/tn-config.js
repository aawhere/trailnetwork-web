require.config({
	//By default load any module IDs from js/lib
	baseUrl: '/scripts',
	//except, if the module ID starts with "app",
	//load it from the js/app directory. paths
	//config is relative to the baseUrl, and
	//never includes a ".js" extension since
	//the paths config could be for a directory.
	paths: {
		'jquery': '../vendor/jquery/1.10.2/jquery.min',
		'underscore': '../vendor/underscore/1.5.2/underscore',
		'domReady': '../vendor/requirejs/2.0.1/domReady',
		'angular': '../vendor/bower/angular/angular.min',
		'angular-aria': '../vendor/bower/angular-aria/angular-aria',
		'angular-animate': '../vendor/bower/angular-animate/angular-animate',
		'angular-clearable': '../vendor/bower/angular-clearable/js/angular-clearable',
		'angular-cookies': '../vendor/bower/angular-cookies/angular-cookies',
		'angular-cookie': '../vendor/bower/angular-cookie/angular-cookie.min',
		'angular-material': '../vendor/bower/angular-material/angular-material',
		'angular-sanitize': '../vendor/bower/angular-sanitize/angular-sanitize',
		'angular-uirouter': '../vendor/ui-router/0.2.10/angular-ui-router.min',
		'angular-translate': '../vendor/bower/angular-translate/angular-translate.min',
		'angular-translate-static-loader': '../vendor/bower/angular-translate-loader-static-files/angular-translate-loader-static-files.min',
		'angular-translate-localstorage-provider': '../vendor/bower/angular-translate-storage-local/angular-translate-storage-local.min',
		'angular-translate-interpolation-messageformat': '../vendor/bower/angular-translate-interpolation-messageformat/angular-translate-interpolation-messageformat.min',
		// 'angular-strap': '../vendor/bower/angular-strap/dist/angular-strap.min',
		// 'angular-strap-tpl': '../vendor/bower/angular-strap/dist/angular-strap.tpl.min',
		// 'angular-strap-dimensions': '../vendor/bower/angular-strap/dist/modules/dimensions.min',
		// 'angular-strap-tooltip': '../vendor/bower/angular-strap/dist/modules/tooltip',
		// 'angular-strap-tooltip-tpl': '../vendor/bower/angular-strap/dist/modules/tooltip.tpl.min',
		// 'angular-strap-popover': '../vendor/bower/angular-strap/dist/modules/popover.min',
		// 'angular-strap-popover-tpl': '../vendor/bower/angular-strap/dist/modules/popover.tpl.min',
		// 'angular-strap-dropdown': '../vendor/bower/angular-strap/dist/modules/dropdown.min',
		// 'angular-strap-dropdown-tpl': '../vendor/bower/angular-strap/dist/modules/dropdown.tpl.min',
		'angular-bootstrap': '../vendor/bower/angular-bootstrap/ui-bootstrap-tpls',
		// 'angular-strap-typeahead': '../vendor/bower/angular-strap/dist/modules/typeahead',
		// 'angular-strap-typeahead-tpl': '../vendor/bower/angular-strap/dist/modules/typeahead.tpl.min',
		// 'angular-strap-parseoptions': '../vendor/bower/angular-strap/dist/modules/parse-options.min',
		'hammerjs': '../vendor/bower/hammerjs/hammer',
		'hammerjs-shim': '../vendor/hammerjs-shim/hammerjs-shim',
		'jqueryMousewheel': '../vendor/bower/jquery-mousewheel/jquery.mousewheel.min',
		'infinite-scroll': '../vendor/bower/ngInfiniteScroll/build/ng-infinite-scroll.min',
		'matchmedia': '../vendor/bower/matchmedia-ng/matchmedia-ng',
		'message-format': '../vendor/messageformat/message-format-shim',
		'ng-tags-input': '../vendor/bower/ng-tags-input/ng-tags-input.min',
		'permission': '../vendor/bower/angular-permission/dist/angular-permission',
		'restangular': '../vendor/restangular/1.2.0/restangular.min',
		//Note: geomodel modified to use requirejs and not place global variables.
		//Also non-used logging and inspect functions removed.
		//https://github.com/danieldkim/geomodel/pull/7
		'geomodel': '../vendor/geomodel/20131222/geomodel',
		'marker-with-label': '../vendor/google-maps-utility-library/markerwithlabel-packed',
		'spin': '../vendor/spin/spin.min',
		//Modified to accept requirejs spin dependency.
		'spinner': '../vendor/angular-spinner/angular-spinner',
		//'highcharts': 'http://code.highcharts.com/adapters/standalone-framework.js',
		'tn-templates': 'tn-templates', //We'll define this later during production builds.
	},
	config: {
		buildTime: '@@build-time',
	},
	shim: {
		'angular': {
			deps: ['jquery'],
			exports: 'angular'
		},
		'angular-animate': {
			deps: ['angular']
		},
		'angular-aria': {
			deps: ['angular']
		},
		'angular-cookies': {
			deps: ['angular']
		},
		'angular-cookie': {
			deps: ['angular']
		},
		'angular-material': {
			deps: ['angular', 'hammerjs-shim', 'angular-animate', 'angular-aria']
		},
		'angular-uirouter': {
			deps: ['angular']
		},
		'angular-sanitize': {
			deps: ['angular']
		},
		'angular-translate': {
			deps: ['angular']
		},
		'angular-bootstrap': {
			deps: ['angular'],
		},
		'angular-translate-static-loader': {
			deps: ['angular', 'angular-translate']
		},
		'angular-translate-localstorage-provider': {
			deps: ['angular', 'angular-translate']
		},
		'matchmedia': {
			deps: ['angular']
		},
		'ng-tags-input': {
			deps: ['angular'],
		},
		// 'message-format': {
		// 	exports: 'MessageFormat'
		// },
		// 'angular-strap': {
		// 	deps: ['angular']
		// },
		// 'angular-strap-tpl': {
		// 	deps: ['angular', 'angular-strap']
		// },
		// 'angular-strap-dimensions': {
		// 	deps: ['angular']
		// },
		// 'angular-strap-tooltip': {
		// 	deps: ['angular', 'angular-strap-dimensions']
		// },
		// 'angular-strap-tooltip-tpl': {
		// 	deps: ['angular', 'angular-strap-tooltip']
		// },
		// 'angular-strap-popover': {
		// 	deps: ['angular', 'angular-strap-tooltip']
		// },
		// 'angular-strap-popover-tpl': {
		// 	deps: ['angular', 'angular-strap-popover']
		// },
		// 'angular-strap-dropdown': {
		// 	deps: ['angular']
		// },
		// 'angular-strap-dropdown-tpl': {
		// 	deps: ['angular', 'angular-strap-dropdown']
		// },
		// 'angular-strap-typeahead': {
		// 	deps: ['angular', 'angular-strap-parseoptions', 'angular-strap-tooltip'],
		// },
		// 'angular-strap-typeahead-tpl': {
		// 	deps: ['angular', 'angular-strap-typeahead'],
		// },
		'angular-translate-interpolation-messageformat': {
			deps: ['angular', 'angular-translate', 'message-format']
		},
		'infinite-scroll': {
			deps: ['angular'],
		},
		'underscore': {
			exports: '_'
		},
		'restangular': {
			deps: ['angular']
		},
		'spinner': {
			deps: ['angular', 'spin']
		},
		'marker-with-label': {
			exports: 'MarkerWithLabel'
		},
	}
});