define(['tn-module',
	'common/http-utils',
	//
	'common/all-routes-service',
	'common/browser-service',
	'common/notification-directive',
	'common/debug-service',
	'common/continuous-scroll',
], function(tnModule, httpUtils) {
	'use strict';

	var ErrorStatus = function(err) {
		if (_.isObject(err)) {
			this.status = err.status;
			this.message = err.data;
		} else if (_.isNumber(err)) {
			this.status = err;
		}

		if (httpUtils.isClientError(this.status)) {
			this.status = 404;
		} else if (httpUtils.isAcceptedSuccess(this.status)) {
			this.status = 202;
		} else {
			//Catch all assume the error is 500.
			this.status = 500;
		}

	};

	return tnModule
		.controller('tnController', function($scope, browserService, appRouteService, debugService, allRoutesService, $log) {

			//Provide a little namespace for us since this is inherited by all other controllers.
			$scope.tn = {};
			$scope.tn.browserService = browserService;
			$scope.tn.appRouteService = appRouteService;
			$scope.tn.debug = debugService.enabled;
			$scope.tn.allRoutes = allRoutesService.enabled;
			$scope.tn.isMobile = browserService.isMobile();

			/**
			 * Display the appropriate error page
			 *
			 * @augments {Object} err The error response from Restagular or
			 * the http status code.
			 */
			$scope.tn.displayErrorPage = function(err) {
				$log.warn(err);
				$scope.tn.error = new ErrorStatus(err);
			};

			$scope.$on('$stateChangeSuccess', function() {
				//reset
				$scope.tn.error = undefined;
			});
		});
});