
/**
 * Javascript client for accessing the CommonTrack Rest Api using Angularjs framework.
 * The client defines a module ('aawhere.web.api.client') which contains
 * angular services for each major rest endpoint.
 *
 * The client can be configured by setting or extending the default options in 'aawhere.web.api.client.configuration'
 * or by passing the options argument with the http call. The options object may contain the following properties:
 *
 *   + beforeRequest: Called before the http request.
 *   + success : callback for successful http request
 *   + error : callback for http request error
 *   + contextRoot : The root context prefixed to each request. Defaults to (/api/v1)
 *   + host : The hostname. Defaults to (INSERT MAVEN PROP OF HOST NAME HERE)
 *   + httpConfig : angularjs http configuration, merged with the method's configuration.
 *   + payload : data for post and put
 *
 */
angular.module('aawhere.web.api.client', []);

/**
 * Used to configure the client module. It contains two methods
 *
 *   - extendOptions : extends the default options configuration object with the one provided. (see main documentation for client).
 *                     properties set in the provided options object overwrite the defaults.
 *   - setOptions : sets the options configuration object to the one provided. (see main documentation for client).
 *
 */
angular.module('aawhere.web.api.client').provider('aawhere.web.api.client.configuration', function() {
    'use strict';

    var provider = {};
    provider.options = {
        contextRoot : 'http://tn-api-dev.appspot.com/rest'
    };

    provider.beforeRequest = [];
    provider.success = [];
    provider.error = [];

    this.$get = function() {
        return {
            options : provider.options,
            setOptions : provider.setOptions,
            extendOptions : provider.extendOptions,
            beforeRequestCallbacks : provider.beforeRequest,
            pushBeforeRequest : provider.pushBeforeRequest,
            successCallbacks : provider.success,
            pushSuccess : provider.pushSuccess,
            errorCallbacks : provider.error,
            pushError : provider.pushError
        };
    };

    provider.extendOptions = function(opts) {
        provider.options = angular.extend(provider.options, opts);
    };

    provider.setOptions = function(opts) {
        provider.options = opts;
    };

    provider.pushBeforeRequest = function(callback) {
        provider.beforeRequest.push(callback);
    };

    provider.pushSuccess = function(callback) {
        provider.success.push(callback);
    };

    provider.pushError = function(callback) {
        provider.error.push(callback);
    };

});
        
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.systemWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.system = {};

    definition.system.url = function(  ) {
        var url;
        url =  'system';
        
        return url;
    };
        
    definition.messages = {};

    definition.messages.url = function(  ) {
        var url;
        url =  'system/messages';
        
        return url;
    };
        
    definition.messages.GET = function(options) {
        var url, requestOptions;
        url = definition.messages.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.dictionary = {};

    definition.dictionary.url = function(  ) {
        var url;
        url =  'system/dictionary';
        
        return url;
    };
        
    definition.dictionary.GET = function(options) {
        var url, requestOptions;
        url = definition.dictionary.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.adminProcessWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.adminProcess = {};

    definition.adminProcess.url = function(  ) {
        var url;
        url =  'admin/process';
        
        return url;
    };
        
    definition.appGcFilter = {};

    definition.appGcFilter.url = function(  ) {
        var url;
        url =  'admin/process/app/gc/filter';
        
        return url;
    };
        
    definition.appGcFilter.GET = function(options) {
        var url, requestOptions;
        url = definition.appGcFilter.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.appGcGeocellGeocell = {};

    definition.appGcGeocellGeocell.url = function( geocell ) {
        var url;
        url =  'admin/process/app/gc/geocell/{geocell}';
        
        url = url.replace(/\{geocell\}/, encodeURIComponent(geocell));
    
        return url;
    };
        
    definition.appGcGeocellGeocell.GET = function(geocell, options) {
        var url, requestOptions;
        url = definition.appGcGeocellGeocell.url( geocell );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.appGcCrawl = {};

    definition.appGcCrawl.url = function(  ) {
        var url;
        url =  'admin/process/app/gc/crawl';
        
        return url;
    };
        
    definition.appGcCrawl.GET = function(options) {
        var url, requestOptions;
        url = definition.appGcCrawl.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.ignoremeGenerateActivityImportHistory = {};

    definition.ignoremeGenerateActivityImportHistory.url = function(  ) {
        var url;
        url =  'admin/process/ignoreme/generateActivityImportHistory';
        
        return url;
    };
        
    definition.ignoremeGenerateActivityImportHistory.GET = function(options) {
        var url, requestOptions;
        url = definition.ignoremeGenerateActivityImportHistory.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.activitiesWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.activities = {};

    definition.activities.url = function(  ) {
        var url;
        url =  'activities';
        
        return url;
    };
        
    definition.activities.GET = function(options) {
        var url, requestOptions;
        url = definition.activities.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activities.GET = function(options) {
        var url, requestOptions;
        url = definition.activities.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activities.POST = function(options) {
        var url, requestOptions;
        url = definition.activities.url(  );

        requestOptions = {
            method: 'POST',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.fields = {};

    definition.fields.url = function(  ) {
        var url;
        url =  'activities/fields';
        
        return url;
    };
        
    definition.fields.GET = function(options) {
        var url, requestOptions;
        url = definition.fields.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityIdDocuments = {};

    definition.activityIdDocuments.url = function( activityId ) {
        var url;
        url =  'activities/{activityId}/documents';
        
        url = url.replace(/\{activityId\}/, encodeURIComponent(activityId));
    
        return url;
    };
        
    definition.activityIdDocuments.GET = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityIdDocuments.url( activityId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityId.Ext = {};

    definition.activityId.Ext.url = function( activityId, ext ) {
        var url;
        url =  'activities/{activityId}.{ext}';
        
        url = url.replace(/\{activityId\}/, encodeURIComponent(activityId));
    
        url = url.replace(/\{ext\}/, encodeURIComponent(ext));
    
        return url;
    };
        
    definition.activityId.Ext.GET = function(activityId, ext, options) {
        var url, requestOptions;
        url = definition.activityId.Ext.url( activityId, ext );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityId = {};

    definition.activityId.url = function( activityId ) {
        var url;
        url =  'activities/{activityId}';
        
        url = url.replace(/\{activityId\}/, encodeURIComponent(activityId));
    
        return url;
    };
        
    definition.activityId.GET = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityId.url( activityId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityId.PUT = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityId.url( activityId );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityId.GET = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityId.url( activityId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.dictionary = {};

    definition.dictionary.url = function(  ) {
        var url;
        url =  'activities/dictionary';
        
        return url;
    };
        
    definition.dictionary.GET = function(options) {
        var url, requestOptions;
        url = definition.dictionary.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityIdImports = {};

    definition.activityIdImports.url = function( activityId ) {
        var url;
        url =  'activities/{activityId}/imports';
        
        url = url.replace(/\{activityId\}/, encodeURIComponent(activityId));
    
        return url;
    };
        
    definition.activityIdImports.GET = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityIdImports.url( activityId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityIdImportsHistory = {};

    definition.activityIdImportsHistory.url = function( activityId ) {
        var url;
        url =  'activities/{activityId}/imports/history';
        
        url = url.replace(/\{activityId\}/, encodeURIComponent(activityId));
    
        return url;
    };
        
    definition.activityIdImportsHistory.GET = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityIdImportsHistory.url( activityId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.cellsCells = {};

    definition.cellsCells.url = function( cells ) {
        var url;
        url =  'activities/cells/{cells}';
        
        url = url.replace(/\{cells\}/, encodeURIComponent(cells));
    
        return url;
    };
        
    definition.cellsCells.GET = function(cells, options) {
        var url, requestOptions;
        url = definition.cellsCells.url( cells );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.cellsCells.GET = function(cells, options) {
        var url, requestOptions;
        url = definition.cellsCells.url( cells );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.cellsCells.PUT = function(cells, options) {
        var url, requestOptions;
        url = definition.cellsCells.url( cells );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.cellsCellsTimings = {};

    definition.cellsCellsTimings.url = function( cells ) {
        var url;
        url =  'activities/cells/{cells}/timings';
        
        url = url.replace(/\{cells\}/, encodeURIComponent(cells));
    
        return url;
    };
        
    definition.cellsCellsTimings.PUT = function(cells, options) {
        var url, requestOptions;
        url = definition.cellsCellsTimings.url( cells );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.search = {};

    definition.search.url = function(  ) {
        var url;
        url =  'activities/search';
        
        return url;
    };
        
    definition.search.GET = function(options) {
        var url, requestOptions;
        url = definition.search.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.search.POST = function(options) {
        var url, requestOptions;
        url = definition.search.url(  );

        requestOptions = {
            method: 'POST',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.courseAttemptTimings = {};

    definition.courseAttemptTimings.url = function( attempt, course ) {
        var url;
        url =  'activities/{course}/{attempt}/timings';
        
        url = url.replace(/\{attempt\}/, encodeURIComponent(attempt));
    
        url = url.replace(/\{course\}/, encodeURIComponent(course));
    
        return url;
    };
        
    definition.courseAttemptTimings.GET = function(attempt, course, options) {
        var url, requestOptions;
        url = definition.courseAttemptTimings.url( attempt, course );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.courseAttemptTimings.GET = function(attempt, course, options) {
        var url, requestOptions;
        url = definition.courseAttemptTimings.url( attempt, course );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityIdTimings = {};

    definition.activityIdTimings.url = function( activityId ) {
        var url;
        url =  'activities/{activityId}/timings';
        
        url = url.replace(/\{activityId\}/, encodeURIComponent(activityId));
    
        return url;
    };
        
    definition.activityIdTimings.GET = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityIdTimings.url( activityId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityIdTimings.PUT = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityIdTimings.url( activityId );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityIdCourseTimings = {};

    definition.activityIdCourseTimings.url = function( activityId ) {
        var url;
        url =  'activities/{activityId}/course/timings';
        
        url = url.replace(/\{activityId\}/, encodeURIComponent(activityId));
    
        return url;
    };
        
    definition.activityIdCourseTimings.GET = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityIdCourseTimings.url( activityId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityIdAttemptTimings = {};

    definition.activityIdAttemptTimings.url = function( activityId ) {
        var url;
        url =  'activities/{activityId}/attempt/timings';
        
        url = url.replace(/\{activityId\}/, encodeURIComponent(activityId));
    
        return url;
    };
        
    definition.activityIdAttemptTimings.GET = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityIdAttemptTimings.url( activityId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.courseLeaderboards = {};

    definition.courseLeaderboards.url = function( course ) {
        var url;
        url =  'activities/{course}/leaderboards';
        
        url = url.replace(/\{course\}/, encodeURIComponent(course));
    
        return url;
    };
        
    definition.courseLeaderboards.GET = function(course, options) {
        var url, requestOptions;
        url = definition.courseLeaderboards.url( course );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityIdSuggestNames = {};

    definition.activityIdSuggestNames.url = function( activityId ) {
        var url;
        url =  'activities/{activityId}/SuggestNames';
        
        url = url.replace(/\{activityId\}/, encodeURIComponent(activityId));
    
        return url;
    };
        
    definition.activityIdSuggestNames.GET = function(activityId, options) {
        var url, requestOptions;
        url = definition.activityIdSuggestNames.url( activityId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.activitiesLeaderboardsWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.activitiesLeaderboards = {};

    definition.activitiesLeaderboards.url = function(  ) {
        var url;
        url =  'activities/leaderboards';
        
        return url;
    };
        
    definition.activitiesLeaderboards.GET = function(options) {
        var url, requestOptions;
        url = definition.activitiesLeaderboards.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activitiesLeaderboards.GET = function(options) {
        var url, requestOptions;
        url = definition.activitiesLeaderboards.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activitiesLeaderboards.GET = function(options) {
        var url, requestOptions;
        url = definition.activitiesLeaderboards.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activitiesLeaderboards.PUT = function(options) {
        var url, requestOptions;
        url = definition.activitiesLeaderboards.url(  );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.leaderboardId = {};

    definition.leaderboardId.url = function( leaderboardId ) {
        var url;
        url =  'activities/leaderboards/{leaderboardId}';
        
        url = url.replace(/\{leaderboardId\}/, encodeURIComponent(leaderboardId));
    
        return url;
    };
        
    definition.leaderboardId.GET = function(leaderboardId, options) {
        var url, requestOptions;
        url = definition.leaderboardId.url( leaderboardId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.leaderboardId.GET = function(leaderboardId, options) {
        var url, requestOptions;
        url = definition.leaderboardId.url( leaderboardId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.leaderboardId.GET = function(leaderboardId, options) {
        var url, requestOptions;
        url = definition.leaderboardId.url( leaderboardId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.leaderboardId.PUT = function(leaderboardId, options) {
        var url, requestOptions;
        url = definition.leaderboardId.url( leaderboardId );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.leaderboardIdReciprocals = {};

    definition.leaderboardIdReciprocals.url = function( leaderboardId ) {
        var url;
        url =  'activities/leaderboards/{leaderboardId}/reciprocals';
        
        url = url.replace(/\{leaderboardId\}/, encodeURIComponent(leaderboardId));
    
        return url;
    };
        
    definition.leaderboardIdReciprocals.GET = function(leaderboardId, options) {
        var url, requestOptions;
        url = definition.leaderboardIdReciprocals.url( leaderboardId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.leaderboardIdReciprocals.GET = function(leaderboardId, options) {
        var url, requestOptions;
        url = definition.leaderboardIdReciprocals.url( leaderboardId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.leaderboardIdRoutes = {};

    definition.leaderboardIdRoutes.url = function( leaderboardId ) {
        var url;
        url =  'activities/leaderboards/{leaderboardId}/routes';
        
        url = url.replace(/\{leaderboardId\}/, encodeURIComponent(leaderboardId));
    
        return url;
    };
        
    definition.leaderboardIdRoutes.GET = function(leaderboardId, options) {
        var url, requestOptions;
        url = definition.leaderboardIdRoutes.url( leaderboardId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.geoWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.geo = {};

    definition.geo.url = function(  ) {
        var url;
        url =  'geo';
        
        return url;
    };
        
    definition.boundsBounds = {};

    definition.boundsBounds.url = function( bounds ) {
        var url;
        url =  'geo/bounds/{bounds}';
        
        url = url.replace(/\{bounds\}/, encodeURIComponent(bounds));
    
        return url;
    };
        
    definition.boundsBounds.GET = function(bounds, options) {
        var url, requestOptions;
        url = definition.boundsBounds.url( bounds );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.boundsBounds.POST = function(bounds, options) {
        var url, requestOptions;
        url = definition.boundsBounds.url( bounds );

        requestOptions = {
            method: 'POST',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.boundsBoundsBoxes = {};

    definition.boundsBoundsBoxes.url = function( bounds ) {
        var url;
        url =  'geo/bounds/{bounds}/boxes';
        
        url = url.replace(/\{bounds\}/, encodeURIComponent(bounds));
    
        return url;
    };
        
    definition.boundsBoundsBoxes.GET = function(bounds, options) {
        var url, requestOptions;
        url = definition.boundsBoundsBoxes.url( bounds );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.boundsBoundsBoxes.GET = function(bounds, options) {
        var url, requestOptions;
        url = definition.boundsBoundsBoxes.url( bounds );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.boxes = {};

    definition.boxes.url = function(  ) {
        var url;
        url =  'geo/boxes';
        
        return url;
    };
        
    definition.boxes.GET = function(options) {
        var url, requestOptions;
        url = definition.boxes.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.cellsBoundsBounds = {};

    definition.cellsBoundsBounds.url = function( bounds ) {
        var url;
        url =  'geo/cells/bounds/{bounds}';
        
        url = url.replace(/\{bounds\}/, encodeURIComponent(bounds));
    
        return url;
    };
        
    definition.cellsBoundsBounds.GET = function(bounds, options) {
        var url, requestOptions;
        url = definition.cellsBoundsBounds.url( bounds );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.boundsCellsCells = {};

    definition.boundsCellsCells.url = function( cells ) {
        var url;
        url =  'geo/bounds/cells/{cells}';
        
        url = url.replace(/\{cells\}/, encodeURIComponent(cells));
    
        return url;
    };
        
    definition.boundsCellsCells.GET = function(cells, options) {
        var url, requestOptions;
        url = definition.boundsCellsCells.url( cells );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.activitiesTimingsWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.activitiesTimings = {};

    definition.activitiesTimings.url = function(  ) {
        var url;
        url =  'activities/timings';
        
        return url;
    };
        
    definition.activitiesTimings.GET = function(options) {
        var url, requestOptions;
        url = definition.activitiesTimings.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activitiesTimings.POST = function(options) {
        var url, requestOptions;
        url = definition.activitiesTimings.url(  );

        requestOptions = {
            method: 'POST',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activitiesTimings.PUT = function(options) {
        var url, requestOptions;
        url = definition.activitiesTimings.url(  );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.fields = {};

    definition.fields.url = function(  ) {
        var url;
        url =  'activities/timings/fields';
        
        return url;
    };
        
    definition.fields.GET = function(options) {
        var url, requestOptions;
        url = definition.fields.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityTimingIdCourseAlert = {};

    definition.activityTimingIdCourseAlert.url = function( activityTimingId ) {
        var url;
        url =  'activities/timings/{activityTimingId}/course/alert';
        
        url = url.replace(/\{activityTimingId\}/, encodeURIComponent(activityTimingId));
    
        return url;
    };
        
    definition.activityTimingIdCourseAlert.GET = function(activityTimingId, options) {
        var url, requestOptions;
        url = definition.activityTimingIdCourseAlert.url( activityTimingId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityTimingId = {};

    definition.activityTimingId.url = function( activityTimingId ) {
        var url;
        url =  'activities/timings/{activityTimingId}';
        
        url = url.replace(/\{activityTimingId\}/, encodeURIComponent(activityTimingId));
    
        return url;
    };
        
    definition.activityTimingId.GET = function(activityTimingId, options) {
        var url, requestOptions;
        url = definition.activityTimingId.url( activityTimingId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityTimingId.PUT = function(activityTimingId, options) {
        var url, requestOptions;
        url = definition.activityTimingId.url( activityTimingId );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityTimingId.GET = function(activityTimingId, options) {
        var url, requestOptions;
        url = definition.activityTimingId.url( activityTimingId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.kmlActivityTimingId = {};

    definition.kmlActivityTimingId.url = function( activityTimingId ) {
        var url;
        url =  'activities/timings/kml/{activityTimingId}';
        
        url = url.replace(/\{activityTimingId\}/, encodeURIComponent(activityTimingId));
    
        return url;
    };
        
    definition.kmlActivityTimingId.GET = function(activityTimingId, options) {
        var url, requestOptions;
        url = definition.kmlActivityTimingId.url( activityTimingId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.activityTimingIdAttemptAlert = {};

    definition.activityTimingIdAttemptAlert.url = function( activityTimingId ) {
        var url;
        url =  'activities/timings/{activityTimingId}/attempt/alert';
        
        url = url.replace(/\{activityTimingId\}/, encodeURIComponent(activityTimingId));
    
        return url;
    };
        
    definition.activityTimingIdAttemptAlert.GET = function(activityTimingId, options) {
        var url, requestOptions;
        url = definition.activityTimingIdAttemptAlert.url( activityTimingId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.dictionary = {};

    definition.dictionary.url = function(  ) {
        var url;
        url =  'activities/timings/dictionary';
        
        return url;
    };
        
    definition.dictionary.GET = function(options) {
        var url, requestOptions;
        url = definition.dictionary.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.update = {};

    definition.update.url = function(  ) {
        var url;
        url =  'activities/timings/update';
        
        return url;
    };
        
    definition.update.PUT = function(options) {
        var url, requestOptions;
        url = definition.update.url(  );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.importsWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.imports = {};

    definition.imports.url = function(  ) {
        var url;
        url =  'imports';
        
        return url;
    };
        
    definition.imports.GET = function(options) {
        var url, requestOptions;
        url = definition.imports.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.imports.POST = function(options) {
        var url, requestOptions;
        url = definition.imports.url(  );

        requestOptions = {
            method: 'POST',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.trackWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.track = {};

    definition.track.url = function(  ) {
        var url;
        url =  'track';
        
        return url;
    };
        
    definition.dictionary = {};

    definition.dictionary.url = function(  ) {
        var url;
        url =  'track/dictionary';
        
        return url;
    };
        
    definition.dictionary.GET = function(options) {
        var url, requestOptions;
        url = definition.dictionary.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.trailheadsWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.trailheads = {};

    definition.trailheads.url = function(  ) {
        var url;
        url =  'trailheads';
        
        return url;
    };
        
    definition.trailheads.GET = function(options) {
        var url, requestOptions;
        url = definition.trailheads.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.trailheads.GET = function(options) {
        var url, requestOptions;
        url = definition.trailheads.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.trailheads.PUT = function(options) {
        var url, requestOptions;
        url = definition.trailheads.url(  );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.dictionary = {};

    definition.dictionary.url = function(  ) {
        var url;
        url =  'trailheads/dictionary';
        
        return url;
    };
        
    definition.dictionary.GET = function(options) {
        var url, requestOptions;
        url = definition.dictionary.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.trailheadIdRoutes = {};

    definition.trailheadIdRoutes.url = function( trailheadId ) {
        var url;
        url =  'trailheads/{trailheadId}/routes';
        
        url = url.replace(/\{trailheadId\}/, encodeURIComponent(trailheadId));
    
        return url;
    };
        
    definition.trailheadIdRoutes.GET = function(trailheadId, options) {
        var url, requestOptions;
        url = definition.trailheadIdRoutes.url( trailheadId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.trailheadIdRoutes.GET = function(trailheadId, options) {
        var url, requestOptions;
        url = definition.trailheadIdRoutes.url( trailheadId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.trailheadId = {};

    definition.trailheadId.url = function( trailheadId ) {
        var url;
        url =  'trailheads/{trailheadId}';
        
        url = url.replace(/\{trailheadId\}/, encodeURIComponent(trailheadId));
    
        return url;
    };
        
    definition.trailheadId.GET = function(trailheadId, options) {
        var url, requestOptions;
        url = definition.trailheadId.url( trailheadId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.trailheadIdRoutesGroups = {};

    definition.trailheadIdRoutesGroups.url = function( trailheadId ) {
        var url;
        url =  'trailheads/{trailheadId}/routes/groups';
        
        url = url.replace(/\{trailheadId\}/, encodeURIComponent(trailheadId));
    
        return url;
    };
        
    definition.trailheadIdRoutesGroups.GET = function(trailheadId, options) {
        var url, requestOptions;
        url = definition.trailheadIdRoutesGroups.url( trailheadId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.trailheadIdRoutesGroups.PUT = function(trailheadId, options) {
        var url, requestOptions;
        url = definition.trailheadIdRoutesGroups.url( trailheadId );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.trailheadIdRoutesMains = {};

    definition.trailheadIdRoutesMains.url = function( trailheadId ) {
        var url;
        url =  'trailheads/{trailheadId}/routes/mains';
        
        url = url.replace(/\{trailheadId\}/, encodeURIComponent(trailheadId));
    
        return url;
    };
        
    definition.trailheadIdRoutesMains.GET = function(trailheadId, options) {
        var url, requestOptions;
        url = definition.trailheadIdRoutesMains.url( trailheadId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.trailheadIdRoutesMains.GET = function(trailheadId, options) {
        var url, requestOptions;
        url = definition.trailheadIdRoutesMains.url( trailheadId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.documentsWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.documents = {};

    definition.documents.url = function(  ) {
        var url;
        url =  'documents';
        
        return url;
    };
        
    definition.activityActivityId.Ext = {};

    definition.activityActivityId.Ext.url = function( applicationKey, ext ) {
        var url;
        url =  'documents/activity/activityId.{ext}';
        
        url = url.replace(/\{applicationKey\}/, encodeURIComponent(applicationKey));
    
        url = url.replace(/\{ext\}/, encodeURIComponent(ext));
    
        return url;
    };
        
    definition.activityActivityId.Ext.GET = function(applicationKey, ext, options) {
        var url, requestOptions;
        url = definition.activityActivityId.Ext.url( applicationKey, ext );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.documentId.Ext = {};

    definition.documentId.Ext.url = function( ext, documentId ) {
        var url;
        url =  'documents/{documentId}.{ext}';
        
        url = url.replace(/\{ext\}/, encodeURIComponent(ext));
    
        url = url.replace(/\{documentId\}/, encodeURIComponent(documentId));
    
        return url;
    };
        
    definition.documentId.Ext.GET = function(ext, documentId, options) {
        var url, requestOptions;
        url = definition.documentId.Ext.url( ext, documentId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.documentId = {};

    definition.documentId.url = function( documentId ) {
        var url;
        url =  'documents/{documentId}';
        
        url = url.replace(/\{documentId\}/, encodeURIComponent(documentId));
    
        return url;
    };
        
    definition.documentId.GET = function(documentId, options) {
        var url, requestOptions;
        url = definition.documentId.url( documentId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.accountsWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.accounts = {};

    definition.accounts.url = function(  ) {
        var url;
        url =  'accounts';
        
        return url;
    };
        
    definition.accounts.GET = function(options) {
        var url, requestOptions;
        url = definition.accounts.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.accounts.POST = function(options) {
        var url, requestOptions;
        url = definition.accounts.url(  );

        requestOptions = {
            method: 'POST',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.fields = {};

    definition.fields.url = function(  ) {
        var url;
        url =  'accounts/fields';
        
        return url;
    };
        
    definition.fields.GET = function(options) {
        var url, requestOptions;
        url = definition.fields.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.dictionary = {};

    definition.dictionary.url = function(  ) {
        var url;
        url =  'accounts/dictionary';
        
        return url;
    };
        
    definition.dictionary.GET = function(options) {
        var url, requestOptions;
        url = definition.dictionary.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.accountId = {};

    definition.accountId.url = function( accountId ) {
        var url;
        url =  'accounts/{accountId}';
        
        url = url.replace(/\{accountId\}/, encodeURIComponent(accountId));
    
        return url;
    };
        
    definition.accountId.GET = function(accountId, options) {
        var url, requestOptions;
        url = definition.accountId.url( accountId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.routesWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.routes = {};

    definition.routes.url = function(  ) {
        var url;
        url =  'routes';
        
        return url;
    };
        
    definition.routes.GET = function(options) {
        var url, requestOptions;
        url = definition.routes.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.routes.GET = function(options) {
        var url, requestOptions;
        url = definition.routes.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.routes.PUT = function(options) {
        var url, requestOptions;
        url = definition.routes.url(  );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.routeIdLeaderboards = {};

    definition.routeIdLeaderboards.url = function( routeId ) {
        var url;
        url =  'routes/{routeId}/leaderboards';
        
        url = url.replace(/\{routeId\}/, encodeURIComponent(routeId));
    
        return url;
    };
        
    definition.routeIdLeaderboards.GET = function(routeId, options) {
        var url, requestOptions;
        url = definition.routeIdLeaderboards.url( routeId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.routeIdLeaderboards.GET = function(routeId, options) {
        var url, requestOptions;
        url = definition.routeIdLeaderboards.url( routeId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.routeIdGroups = {};

    definition.routeIdGroups.url = function( routeId ) {
        var url;
        url =  'routes/{routeId}/groups';
        
        url = url.replace(/\{routeId\}/, encodeURIComponent(routeId));
    
        return url;
    };
        
    definition.routeIdGroups.GET = function(routeId, options) {
        var url, requestOptions;
        url = definition.routeIdGroups.url( routeId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.routeIdGroups.GET = function(routeId, options) {
        var url, requestOptions;
        url = definition.routeIdGroups.url( routeId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.trailheadsTrailheadId = {};

    definition.trailheadsTrailheadId.url = function( trailheadId ) {
        var url;
        url =  'routes/trailheads/{trailheadId}';
        
        url = url.replace(/\{trailheadId\}/, encodeURIComponent(trailheadId));
    
        return url;
    };
        
    definition.trailheadsTrailheadId.GET = function(trailheadId, options) {
        var url, requestOptions;
        url = definition.trailheadsTrailheadId.url( trailheadId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.mains = {};

    definition.mains.url = function(  ) {
        var url;
        url =  'routes/mains';
        
        return url;
    };
        
    definition.mains.GET = function(options) {
        var url, requestOptions;
        url = definition.mains.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.mains.GET = function(options) {
        var url, requestOptions;
        url = definition.mains.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.routeId = {};

    definition.routeId.url = function( routeId ) {
        var url;
        url =  'routes/{routeId}';
        
        url = url.replace(/\{routeId\}/, encodeURIComponent(routeId));
    
        return url;
    };
        
    definition.routeId.GET = function(routeId, options) {
        var url, requestOptions;
        url = definition.routeId.url( routeId );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    
angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.adminWebApi', ['aawhere.web.api.client.configuration', 'aawhere.web.api.client.util', function(configuration, util) {
    'use strict';
    var definition = {};
        
    definition.admin = {};

    definition.admin.url = function(  ) {
        var url;
        url =  'admin';
        
        return url;
    };
        
    definition.queueEmpty = {};

    definition.queueEmpty.url = function(  ) {
        var url;
        url =  'admin/queue/empty';
        
        return url;
    };
        
    definition.queueEmpty.GET = function(options) {
        var url, requestOptions;
        url = definition.queueEmpty.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.securityWhoami = {};

    definition.securityWhoami.url = function(  ) {
        var url;
        url =  'admin/security/whoami';
        
        return url;
    };
        
    definition.securityWhoami.GET = function(options) {
        var url, requestOptions;
        url = definition.securityWhoami.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.securityPermitAll = {};

    definition.securityPermitAll.url = function(  ) {
        var url;
        url =  'admin/security/PermitAll';
        
        return url;
    };
        
    definition.securityPermitAll.GET = function(options) {
        var url, requestOptions;
        url = definition.securityPermitAll.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.securityAdminRoleOnly = {};

    definition.securityAdminRoleOnly.url = function(  ) {
        var url;
        url =  'admin/security/AdminRoleOnly';
        
        return url;
    };
        
    definition.securityAdminRoleOnly.GET = function(options) {
        var url, requestOptions;
        url = definition.securityAdminRoleOnly.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.securityDenyAccess = {};

    definition.securityDenyAccess.url = function(  ) {
        var url;
        url =  'admin/security/DenyAccess';
        
        return url;
    };
        
    definition.securityDenyAccess.GET = function(options) {
        var url, requestOptions;
        url = definition.securityDenyAccess.url(  );

        requestOptions = {
            method: 'GET',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    definition.queue = {};

    definition.queue.url = function(  ) {
        var url;
        url =  'admin/queue';
        
        return url;
    };
        
    definition.queue.PUT = function(options) {
        var url, requestOptions;
        url = definition.queue.url(  );

        requestOptions = {
            method: 'PUT',
            path: url
        };

        requestOptions = angular.extend({}, configuration.options, requestOptions, options);
        return util.callRemote(requestOptions);
    };
    
    return definition;
} ]);
    

angular.module('aawhere.web.api.client').factory('aawhere.web.api.client.util', ['$http', 'aawhere.web.api.client.configuration', function($http, configuration) {
    'use strict';

    var util = {};

    /**
    * calls the server according to the configuration
    */
    util.callRemote = function(options) {
        var httpOptions;
        options = angular.extend({}, configuration.options, options);
        httpOptions = angular.extend({
            method: (options.method || 'GET'),
            url: util.fullPath(options),
            headers : {}
        }, options.httpOptions);

        // append request body if data has to be sent (JSON only)
        if (options.payload) {
            httpOptions.headers.post = (httpOptions.headers.post || {});
            httpOptions.headers.post.Accept = 'application/json';
            httpOptions.headers.post['Content-Type'] = 'application/json';
            httpOptions.data = options.payload;
        }

        util.beforeRequest(options.beforeRequest);
        return $http(httpOptions).success(function(data, status, headers, responseOptions) {
            util.success(options.successCallback, data, status, headers, responseOptions);
        }).error(function(data, status, headers, responseOptions) {
            util.doCallback(configuration.errorCallbacks, data, status, headers, responseOptions);
        });
    };

    util.fullPath = function(options) {
        return options.contextRoot + '/' + options.path;
    };

    util.beforeRequest = function(callback) {
        var callbacks = [];
        callbacks.concat(configuration.beforeRequestCallbacks);
        if(callback) {
            callbacks.push(callback);
        }
        util.doCallback(callbacks);
    };

    util.success = function(callback, data, status, headers, responseOptions) {
        var callbacks = [];
        callbacks.concat(configuration.successCallbacks);
        if(callback) {
            callbacks.push(callback);
        }
        util.doCallback(callbacks, data, status, headers, responseOptions);
    };

    util.error = function(callback, data, status, headers, responseOptions) {
        var callbacks = [];
        callbacks.concat(configuration.errorCallbacks);
        if(callback) {
            callbacks.push(callback);
        }
        util.doCallback(callbacks, data, status, headers, responseOptions);
    };

    util.doCallback = function(callbacks, data, status, headers, responseOptions) {
        angular.forEach(callbacks, function(callback) {
            callback(data, status, headers, responseOptions);
        });
    };

    return util;
}]);

    