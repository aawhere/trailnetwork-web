define(['../vendor/bower/messageformat/messageformat'], function(MessageFormat) {
	'use strict';

	window.MessageFormat = MessageFormat;
	return MessageFormat;
});