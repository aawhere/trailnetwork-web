# Installation

This project uses node, npm, grunt, requirejs, sass+compass and maven. These tools are required to develop javascript and css as well as create a war file for distribution to appengine.

## Install Node and npm

There are installers for both Mac and Windows. However, on osx and Linux, we recommend using [NVM](https://github.com/creationix/nvm). NVM allows for easy upgrades and switching between node versions. The supported version of node on this project is 0.10.x.
	
Node package manager (NPM) is a package manager for Node.js. It is used to install packages such as Karma, concatinators, minimizers, etc. This should automatically be installed when Node.js is installed, but if not then please install it afterwards.

	# Install node version manager NVM (Requires a compiler. eg. XCode for OSX or build-essentials for ubuntu)
	curl https://raw.github.com/creationix/nvm/master/install.sh | sh
	
	# Either reopen the terminal or reload your profile or bashrc
	source ~/.profile #OR source ~/.bashrc
	
	# Verify installed correctly
	nvm --version
	
	# Install node
	nvm install 0.10
	nvm use 0.10
	nvm alias default 0.10
	
	# Verify node is installed
	node --version
	
	# Verify node package manager (NPM) is installed
	npm --version


It is recommended that you restart your terminal and verify that nvm, node, and npm are available before you continue.

Next switch to the 'web' directory and install all node dependencies:

	#from the root of the source:
	cd web
	npm install

More information:

 * [Node Version Manager](https://github.com/creationix/nvm)
 * [Node](http://nodejs.org/)
 * [Node Package Manager (npm)](https://npmjs.org/)

## Grunt
Grunt is a command line tool that automates many processes in your application. It also has a watch target that will watch your project files and when any changes will run commands as defined in Gruntfile.js.

Grunt should have been installed during the node installation. However, it is desired to have grunt in the path. To install grunt and have it available on your path:

	# Install grunt globally
	npm install -g grunt-cli
	#verify grunt works
	grunt --version

The expected output of `grunt --version` is the following (both grunt-cli and grunt should be listed)

>grunt-cli v0.1.11<br/>
>grunt v0.4.2

More information: [http://gruntjs.com/getting-started](http://gruntjs.com/getting-started)

## Install compass and sass
Sass is an extension of CSS that adds power and elegance to the basic language. It allows you to use variables, nested rules, mixins, inline imports, and more, all with a fully CSS-compatible syntax. Sass helps keep large stylesheets well-organized, and get small stylesheets up and running quickly, particularly with the help of the Compass style library.

Compass is a Sass-based Stylesheet Framework that streamlines the creation and maintainance of CSS. 

To install you need ruby. It is recommended that you use [rvm](https://rvm.io/). However if you have ruby already installed and working you can skip rvm installation and move on to "Now verify your installation" to make sure you have ruby, gem, and bundle.
	
	# Install rvm
	\curl -sSL https://get.rvm.io | bash -s stable
	# Follow the instructions if there are any Installation or Upgrade notes displayed.
	# Either reopen the terminal or reload your profile or bashrc
	source ~/.profile #OR source ~/.bashrc
	# Verify installed correctly
	rvm --version

Now that rvm is installed, we need to install a version of ruby:

	# Install the latest version of ruby
	rvm install ruby 2.0.0

Now verify your installation:

	# Check that ruby is installed 
	ruby --version
	# Check that gem is installed
	gem --version
	# Check that bundle is installed
	bundle --version 
	#If bundle is not available, use 'gem install bundler' to install

It is recommended that you restart your terminal and switch back to the 'web' directory. The .rvmrc file should automatically configure .rvm to use ruby 2.0.0.

After installing ruby, the required gems for the project can be installed:
	
	# Switch to the web directory (if not already there)
	cd web 
	bundle install

More Information: 

* [sass](http://sass-lang.com/)
* [compass](http://http://compass-style.org/)

You should now be setup and ready for development

# Development

## Updating dependencies
After checking out new code it is a good idea to make sure that all build dependencies are updated. Since we use both node and ruby, these tools will have to be involked.

	#Update node dependencies
	cd web
	npm install

	#Update ruby gems (from web dir)
	bundle install
	
## Running the app during development

To run the development server and automatically watch for changes:

	#Start development server and watch for file changes
	grunt serve

Then navigate your browser to `http://localhost:9000` to see the app running in your browser (if it didn't automatically do that for you).

Make a change. Change a template or index.html. Save. Watch the browser automatically reload with the changes.

## Running unit tests

Test are run using Karma and jasmine.

	#Run unit tests
    grunt test


## End to end testing
TODO: implement end to end tests and update me!

## Building
After development of a feature is complete, the production ready distributable should be built. This process will concatenate and minimize javascript, compile sass files, fingerprint assets, and more. The grunt command to do this is:

	#Build application
	grunt

This will output the built artifacts in the web/dist directory. However, you will likley not need to use this command except as a debug or verification tool as Maven will automate the process of building the app and packaging it into a WAR for distribution to an application server.

	#from root directory
	mvn install

## Updating appspot
To push your changes to appspot:

	#build and push changes to demo
	mvn appengine:update -Pdemo

	#build and push changes to live
	mvn appengine:update -Plive

# Coding

## RequireJs

The application depends on requireJs for module loading. If you are not familiar with requirejs, please take a look at [this primer] (http://raymondjulin.com/2012/12/19/a-practical-guide-to-amd-and-require-js/) and the [docs](http://requirejs.org/docs/api.html)
