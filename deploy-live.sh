#!/bin/bash
die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 2 ] || die "1 argument required (live build number), $# provided"
echo $1 | grep -E -q '^[0-9-]+$' || die "Numeric argument required, $1 provided"
echo $2 | grep -E -q '^[0-9-]+$' || die "Numeric argument required, $2 provided"

mvn clean appengine:update -Plive -Dgae.application.version=live-$1 -Dapiserver=$2.tn-api-live.appspot.com
