package com.trailnetwork.api;

import org.junit.Ignore;
import org.junit.Test;

import com.trailnetwork.api.Api.ApiReadException;

@Ignore("Unfortunatly these test may require authentication, even with the devserver. TODO: add authentication")
public class ApiWebTest {
	
	private String accountId = "gi-1234";
	private String name = "Lance Armstrong";
	private String username = "prick";
    private String photoUrl = "http://example.com/photo";
	
	@Test
	public void testAccountGetPerson() throws ApiReadException {
		Api api = Api.newInstance();
		api.account().putPerson(accountId, name, username, photoUrl);
	}
	
	@Test
	public void testAccountGetPersonNullPhoto() throws ApiReadException {
		Api api = Api.newInstance();
		api.account().putPerson(accountId, name, username, null);
	}

}
