package com.trailnetwork.mail;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.google.appengine.api.mail.dev.LocalMailService;
import com.google.appengine.tools.development.ApiProxyLocal;
import com.google.appengine.tools.development.testing.LocalMailServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class EmailUnitTest {

	private final LocalServiceTestHelper helper =
	        new LocalServiceTestHelper(new LocalMailServiceTestConfig());
	private LocalMailService mailService;

	@Before
	public void setup() {
		helper.setUp();
		ApiProxyLocal proxy = LocalServiceTestHelper.getApiProxyLocal();
		mailService = (LocalMailService) proxy
				.getService(LocalMailService.PACKAGE);
	}

	@Test
	public void testSendEmail() throws EmailException {
		String body = "test email";
		String subject = "Testing email from appengine";
		String to = "brian.chapman@aawhere.com";
		String toName = "Brian Chapman";
		String from = "support@trailnetwork.com";
		String fromName = "Trailnetwork Support";

		Email email = Email.create().to(to).toName(toName).from(from)
				.fromName(fromName).subject(subject).body(body).build();
		email.send();
		assertEquals(1, mailService.getSentMessages().size());
	}
}
