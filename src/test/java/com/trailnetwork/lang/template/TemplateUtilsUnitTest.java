package com.trailnetwork.lang.template;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.google.appengine.repackaged.com.google.common.collect.Maps;

import freemarker.template.TemplateException;

public class TemplateUtilsUnitTest {
	
	private String url;
	private Map<String, String> data;
	
	@Before
	public void setup() {
		url = "http://www.trailnetwork.com/users/reset";
		data = Maps.newHashMap();
		data.put("passwordResetUrl", url);
	}
	
	@Test
	public void testParseTemplate() throws IOException, TemplateException {
		String templatePath = "src/test/resources/com/trailnetwork/lang/template/password-reset-email.ftl";
		String result = TemplateUtils.processFileLoader(data, templatePath);
		assertTrue(result.contains(url));
	}
	
	@Test
	public void testParseTemplateClassLoader() throws IOException, TemplateException {
		String templatePath = "/com/trailnetwork/lang/template/password-reset-email.ftl";
		String result = TemplateUtils.processClassLoader(data, templatePath);
		assertTrue(result.contains(url));
	}

}
