package com.trailnetwork.api;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Person implements Serializable {
	private static final long serialVersionUID = 8977849602899327323L;

	public String id;
	public String name;
	public String nameWithAliases;
}
