package com.trailnetwork.api;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.UriBuilder;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;

import com.aawhere.log.LoggerFactory;
import com.aawhere.net.HttpStatusCode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;
import com.trailnetwork.web.ProxyPersonalizationFilter;

public class Api {

	public static final String ROOT = "rest";
	public static final int API_TIMEOUT = 60 * 1000;// milliseconds
	private final Boolean FOLLOW_REDIRECTS = Boolean.FALSE;

	public final static class API_FIELDS {
		public final static class ACCOUNT {
			public static final String ACCOUNTS = "accounts";
		}

		public final static class PERSON {
			public static final String PERSONS = "persons";
		}
	}

	public static Api newInstance() {
		return new Api();
	}

	private final String apiBaseUri;
	private AccountApiClient account;
	private PersonApiClient person;
	private UriBuilder rootUri;
	private URLFetchService fetchService;

	public Api() {
		Properties properties = new Properties();
		String configFile = "tn-config.properties";
		InputStream in = getClass().getClassLoader().getResourceAsStream(configFile);
		fetchService = URLFetchServiceFactory.getURLFetchService();
		try {
			properties.load(in);
		} catch (IOException e) {
			throw new RuntimeException("Property file " + configFile + " not found in the classpath.", e);
		}
		apiBaseUri = properties.getProperty("apiServer");
		rootUri = UriBuilder.fromUri(apiBaseUri).path(ROOT);
	}

	public AccountApiClient account() {
		if (account == null) {
			account = new AccountApiClient();
		}
		return account;
	}

	public PersonApiClient person() {
		if (person == null) {
			person = new PersonApiClient();
		}
		return person;
	}

	public class AccountApiClient {

		private static final String EXPAND = "expand";
		private static final String PERSON_ACCOUNT_ID = "person-accountId";
		URI accounts = UriBuilder.fromUri(rootUri.build()).path(API_FIELDS.ACCOUNT.ACCOUNTS).build();

		public Person putPerson(String accountId, String name, String email, String photoUrl) throws ApiReadException {
			UriBuilder personsBuilder = UriBuilder.fromUri(accounts).path(accountId).path(API_FIELDS.PERSON.PERSONS)
					.queryParam("name", name);
			if(photoUrl != null) {
				personsBuilder.queryParam("photoUrl", photoUrl);
			}
			if (email != null) {
				personsBuilder.queryParam("email", email);
			}
			URI persons = personsBuilder.build();
			HttpURLConnection response;
			response = Api.this.doPut(persons, MediaType.JSON_UTF_8, accountId);

			return readEntity(response, Person.class);
		}

		public Person getPerson(String accountId) throws ApiReadException {
			URI persons = UriBuilder.fromUri(accounts).path(accountId).path(API_FIELDS.PERSON.PERSONS)
					.queryParam(EXPAND, PERSON_ACCOUNT_ID).build();
			HttpURLConnection response = Api.this.doGet(persons, MediaType.JSON_UTF_8, accountId);

			return readEntity(response, Person.class);
		}

		public String getPersonAsJson(String accountId) throws ApiReadException {
			URI persons = UriBuilder.fromUri(accounts).path(accountId).path(API_FIELDS.PERSON.PERSONS)
					.queryParam(EXPAND, PERSON_ACCOUNT_ID).build();
			HttpURLConnection response = Api.this.doGet(persons, MediaType.JSON_UTF_8, accountId);

			try {
				return IOUtils.toString(response.getInputStream());
			} catch (IOException e) {
				throw new ApiReadException(HttpStatusCode.INTERNAL_ERROR, e);
			}
		}
	}

	public class PersonApiClient {

		private URI personsUri = UriBuilder.fromUri(rootUri.build()).path(API_FIELDS.PERSON.PERSONS).build();

		public Person getPerson(String personId, String accountId) throws ApiReadException {

			URI persons = UriBuilder.fromUri(personsUri).path(personId).build();
			HttpURLConnection response = Api.this.doGet(persons, MediaType.JSON_UTF_8, accountId);
			return readEntity(response, Person.class);
		}

		public Person updateEmail(String personId, String email, String accountId) throws ApiReadException {

			URI persons = UriBuilder.fromUri(personsUri).path(personId).queryParam("person-email", email).build();
			HttpURLConnection response = Api.this.doPut(persons, MediaType.JSON_UTF_8, accountId);
			return readEntity(response, Person.class);
		}
	}

	public static class ApiReadException
			extends Exception {

		private static final long serialVersionUID = 832139631099209699L;
		private HttpStatusCode statusCode;
		private HttpResponse response;
		private Logger logger = LoggerFactory.getLogger(getClass());

		public ApiReadException(HttpStatusCode statusCode, Throwable cause) {
			super("Error reading from API. Response: " + statusCode.value, cause);
			this.statusCode = statusCode;
		}

		public ApiReadException(HttpStatusCode statusCode) {
			this(statusCode, new RuntimeException());
			this.statusCode = statusCode;
		}

		public ApiReadException(HttpStatusCode status, HttpURLConnection response) {
			this(status);
			try {
				logger.log(Level.SEVERE, getClass().getSimpleName() + ", StatusCode: " + response.getResponseCode());
				logger.log(Level.SEVERE, getClass().getSimpleName() + ", URL: " + response.getURL());
				logger.log(Level.SEVERE, "Entity: " + IOUtils.toString(response.getInputStream(), "UTF-8"));
				for (Entry<String, List<String>> entry : response.getHeaderFields().entrySet()) {
					logger.log(	Level.SEVERE,
								"Header: " + entry.getKey() + " : " + StringUtils.join(entry.getValue(), ", "));
				}
			} catch (Exception e) {
				logger.log(Level.SEVERE, "", e);
			}
		}

		public HttpStatusCode getStatusCode() {
			return statusCode;
		}

		public HttpResponse getResponse() {
			return response;
		}

	}

	private HttpURLConnection doGet(URI target, MediaType mediaType, String accountId) throws ApiReadException {
		return makeRequest(target, HTTPMethod.GET, mediaType, accountId);
	}

	private HttpURLConnection doPut(URI target, MediaType mediaType, String accountId) throws ApiReadException {
		return makeRequest(target, HTTPMethod.PUT, mediaType, accountId);

		// Puts can't have empty entities by default.
		// target.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION,
		// Boolean.TRUE);

	}

	private HttpURLConnection makeRequest(URI uri, HTTPMethod method, MediaType mediaType, String accountId)
			throws ApiReadException {

		HttpURLConnection connection;
		try {
			connection = (HttpURLConnection) uri.toURL().openConnection();
			connection.setRequestMethod(method.toString());
			connection.setInstanceFollowRedirects(FOLLOW_REDIRECTS);
			connection.setReadTimeout(API_TIMEOUT);
			connection.setRequestProperty(ProxyPersonalizationFilter.USER_ID_HEADER_NAME, accountId);
			connection.setRequestProperty(HttpHeaders.ACCEPT, mediaType.toString());
			connection.connect();
			handleError(connection);
			return connection;
		} catch (IOException e1) {
			throw new ApiReadException(HttpStatusCode.INTERNAL_ERROR, e1);
		}
	}

	private void handleError(HttpURLConnection response) throws ApiReadException, IOException {
		int status = response.getResponseCode();
		if (status != 200) {
			throw new ApiReadException(HttpStatusCode.valueOf(status), response);
		}
	}

	private <EntityT> EntityT readEntity(HttpURLConnection connection, Class<EntityT> type) throws ApiReadException {

		try {
			InputStream response = connection.getInputStream();
			ObjectMapper mapper = new ObjectMapper();
			EntityT entity = mapper.readValue(response, type);
			return entity;
		} catch (IOException e) {
			throw new ApiReadException(HttpStatusCode.INTERNAL_ERROR, e);
		}
	}

}