package com.trailnetwork.mail;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * Sends an email
 * 
 * @author Brian Chapan
 *
 */
public class Email {
	
	public final static String DEFAULT_FROM_EMAIL = "support@aawhere.com";
	public final static String DEFAULT_FROM_NAME = "TrailNetwork Support";

	public static class Builder extends ObjectBuilder<Email> {

		public Builder() {
			super(new Email());
		}

		public Builder to(String email) {
			building.to = email;
			return this;
		}

		public Builder toName(String name) {
			building.toName = name;
			return this;
		}

		public Builder from(String email) {
			building.from = email;
			return this;
		}

		public Builder fromName(String name) {
			building.fromName = name;
			return this;
		}

		public Builder subject(String subject) {
			building.subject = subject;
			return this;
		}

		public Builder body(String body) {
			building.body = body;
			return this;
		}

		@Override
		protected void validate() {
			if(building.from == null) {
				building.from = DEFAULT_FROM_EMAIL;
				building.fromName = DEFAULT_FROM_NAME;
			}
			Assertion.assertNotNull("to", building.to);
			Assertion.assertNotNull("toName", building.toName);
			Assertion.assertNotNull("from", building.from);
			Assertion.assertNotNull("fromName", building.fromName);
			Assertion.assertNotNull("subject", building.subject);
			Assertion.assertNotNull("body", building.body);
		}

		@Override
		public Email build() {
			Email built = super.build();

			return built;
		}
	}

	public static Builder create() {
		return new Builder();
	}

	private Email() {
		// create using Builder;
	}

	private String to;
	private String toName;
	private String from;
	private String fromName;
	private String subject;
	private String body;

	public void send() throws EmailException {

		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		try {
			Message msg = new MimeMessage(session);

			msg.setFrom(new InternetAddress(from, fromName));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to,
					toName));
			msg.setSubject(subject);
			msg.setText(body);
			Transport.send(msg);

		} catch (MessagingException | UnsupportedEncodingException e) {
			throw new EmailException(e);
		}
	}
}
