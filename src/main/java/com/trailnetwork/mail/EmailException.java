package com.trailnetwork.mail;

import com.aawhere.lang.exception.BaseException;

public class EmailException extends BaseException {

	public EmailException(Exception e) {
		super(e);
	}

	private static final long serialVersionUID = -7914025843190003544L;

}
