package com.trailnetwork.web;

import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Set;

import javax.annotation.Nullable;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.google.appengine.repackaged.com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.trailnetwork.auth.AuthUtil;
import com.trailnetwork.auth.GitKitUtil;
import com.trailnetwork.auth.UnauthenticatedException;
import com.trailnetwork.auth.User;

/**
 * Inserts the current userId into the header, if available. Also provides the current unit of
 * measure if set in a cookie, but only if the userId is not known.
 * 
 * @author Brian Chapman
 *
 */
public class ProxyPersonalizationFilter
		implements Filter {

	public static final String USER_ID_HEADER_NAME = "tn-accountId";
	public static final String UNIT_OF_MEASURE_HEADER_NAME = "Accept-UnitOfMeasure";
	public static final String UNIT_OF_MEASURE_COOKIE_NAME = "unitOfMeasure";

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(final ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
			ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) req;
		@Nullable
		final String uom = getUnitOfMeasureFromCookie(httpRequest);

		User user = null;
		try {
			user = AuthUtil.getUser((HttpServletRequest) req);
		} catch (UnauthenticatedException e) {
			// Not logged in, send anonymous request.
		}

		final Enumeration<String> names = httpRequest.getHeaderNames();
		Set<String> nameSet = Sets.newHashSet(Collections.list(names));

		if (user != null && user.getId() != null) {
			nameSet.add(USER_ID_HEADER_NAME);
		}
		if (uom != null) {
			nameSet.add(UNIT_OF_MEASURE_HEADER_NAME);
		}
		final Enumeration<String> headerNames = Collections.enumeration(nameSet);

		final User finalUser = user;
		httpRequest = new HttpServletRequestWrapper(httpRequest) {
			@Override
			public String getHeader(String name) {
				return getHeaders(name).nextElement();
			}

			@Override
			public Enumeration<String> getHeaders(String name) {
				if (name.equals(USER_ID_HEADER_NAME) && finalUser != null) {
					return Collections.enumeration(Lists.newArrayList(GitKitUtil.accountId(finalUser.getId())));
				}
				if (name.equals(UNIT_OF_MEASURE_HEADER_NAME)) {
					return Collections.enumeration(Lists.newArrayList(uom));
				}
				return super.getHeaders(name);
			}

			@Override
			public Enumeration<String> getHeaderNames() {
				return headerNames;
			}
		};

		chain.doFilter(httpRequest, res);

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Nullable
	private String getUnitOfMeasureFromCookie(HttpServletRequest req) {
		return CookieUtils.getCookie(UNIT_OF_MEASURE_COOKIE_NAME, req.getCookies());
	}

}
