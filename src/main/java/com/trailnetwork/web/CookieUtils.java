package com.trailnetwork.web;

import javax.annotation.Nullable;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.lang.exception.BaseException;

public class CookieUtils {

	private CookieUtils() {
		// Only static methods allowed.
	}

	public static Cookie createCookie(String name, String value) {
		String encoded = new String(Base64.encodeBase64(value.getBytes()));
		Cookie c = new Cookie(name, encoded);
		return c;
	}

	public static String decodeCookie(Cookie c) {
		return new String(Base64.decodeBase64(c.getValue()));
	}

	public static String getDecodedCookieValue(String name, HttpServletRequest request) throws CookieNotFoundException {
		for (Cookie c : request.getCookies()) {
			if (c.getName().equals(name)) {
				return decodeCookie(c);
			}
		}
		throw new CookieNotFoundException();
	}

	@Nullable
	public static String getCookie(String name, @Nullable Cookie[] cookies) {
		String value = null;
		// WW-307 avoid null since cookies can be null from request
		if (ArrayUtils.isNotEmpty(cookies)) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(name)) {
					value = cookie.getValue();
				}
			}
		}
		return value;
	}

	public static class CookieNotFoundException
			extends BaseException {

		private static final long serialVersionUID = -5647760856950582776L;

	}

}
