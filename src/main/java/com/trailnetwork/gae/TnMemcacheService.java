package com.trailnetwork.gae;

import java.io.Serializable;
import java.util.logging.Level;

import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;

public class TnMemcacheService {

	private MemcacheService syncCache;

	public TnMemcacheService() {
		syncCache = MemcacheServiceFactory.getMemcacheService();
		syncCache.setErrorHandler(ErrorHandlers
				.getConsistentLogAndContinue(Level.INFO));
	}

	public <valueT extends Serializable> void put(String key, valueT value) {
		syncCache.put(key, value);
	}

	@SuppressWarnings("unchecked")
	public <valueT extends Serializable> valueT read(String key, Class<valueT> type) {
		valueT value = (valueT) syncCache.get(key);
		if(value != null) {
			put(key, value);
		}
		return value;
	}
}
