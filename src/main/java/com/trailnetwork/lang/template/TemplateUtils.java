package com.trailnetwork.lang.template;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import javax.servlet.ServletContext;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.FileTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.cache.WebappTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;

public class TemplateUtils {

	private static Configuration freemakerConfiguration;

	private TemplateUtils() {
		// static helper methods only.
	}

	private static Configuration configuration() {
		if (freemakerConfiguration == null) {
			freemakerConfiguration = new Configuration(new Version("2.3.21"));
		}
		return freemakerConfiguration;
	}

	public static String processFileLoader(Map<String, String> data, String templatePath) throws IOException, TemplateException {
		return process(data, templatePath, new FileTemplateLoader());
	}
	
	public static String processClassLoader(Map<String, String> data, String templatePath) throws IOException, TemplateException {
		return process(data, templatePath, new ClassTemplateLoader(TemplateUtils.class, "/"));
	}
	
	private static String process(Map<String, String> data, String templatePath, TemplateLoader loader) throws IOException, TemplateException {
		Configuration cfg = configuration();
		cfg.setTemplateLoader(loader);
		Template template = cfg.getTemplate(templatePath);
		StringWriter out = new StringWriter();
		template.process(data, out);
		out.flush();
		return out.toString();
	}

}
