package com.trailnetwork.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aawhere.log.LoggerFactory;
import com.aawhere.net.HttpStatusCode;
import com.trailnetwork.api.Api;
import com.trailnetwork.api.Api.ApiReadException;

/**
 * After GitKit successfully signs in a user, they are redirected here. This Servlet
 * Takes care of talking to the API and creating an Account and Person.
 * 
 * One important aspect is that after signin, we want to update the account details with the latest info.
 * 
 * @author Brian Chapman
 *
 */
public class SigninSuccessServlet extends HttpServlet {
	private static final long serialVersionUID = 7102200688512020213L;
	
	private Api api;
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
				
		try {
			User user = AuthUtil.getUser(request);
			AuthUtil.updateAccountAndPerson(api, user);
			AuthUtil.redirectToExplorePage(response);
		} catch (UnauthenticatedException | ApiReadException e) {
			logger.log(Level.SEVERE, "Error updating user: ", e);
			response.setStatus(HttpStatusCode.UNAUTHORIZED.value);
			AuthUtil.redirectToSigninPage(response);
		}
	}
	
	@Override
	public void init() throws ServletException {
		super.init();
		api = Api.newInstance();
	}
}
