package com.trailnetwork.auth;

import java.io.Serializable;
import java.net.URL;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

public class User implements Serializable {
	
	private static final long serialVersionUID = -478816489444775378L;

	public static class Builder extends ObjectBuilder<User> {
		
		public Builder() {
			super(new User());
		}
		
		public Builder email(String email) {
			building.email = email;
			return this;
		}
		
		public Builder provider(String provider) {
			building.provider = provider;
			return this;
		}
		
		public Builder id(String id) {
			building.id = id;
			return this;
		}
		
		public Builder name(String name) {
			building.name = name;
			return this;
		}
		
		public Builder photoUrl(URL url) {
			building.photoUrl = url;
			return this;
		}
		
		@Override
		protected void validate() {
			Assertion.assertNotNull("Id", building.id);
		}
	}
	
	public static Builder create() {
		return new Builder();
	}
	
	private User() {
		//create using Builder;
	}
	
	private String email;
	private String provider;
	private String id;
	private String name;
	private URL photoUrl;
	
	public String getEmail() {
		return email;
	}
	
	public String getProvider() {
		return provider;
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public URL getPhotoUrl() {
		return photoUrl;
	}
}
