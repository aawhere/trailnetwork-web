package com.trailnetwork.auth;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.io.ByteStreams;
import com.google.identitytoolkit.GitkitClient;
import com.google.identitytoolkit.GitkitClientException;
import com.google.identitytoolkit.GitkitServerException;
import com.google.identitytoolkit.GitkitUser;

public class GitKitUtil {

	// Singleton client used for all requests.
	private static final GitkitClient gitkitClient;

	static {
		ClassLoader classLoader = GitKitUtil.class.getClassLoader();
		InputStream file = classLoader.getResourceAsStream("gitkit-server-config.json");

		try {
			JSONObject configData = new JSONObject(StandardCharsets.UTF_8.decode(ByteBuffer.wrap(ByteStreams
					.toByteArray(file))).toString());

			gitkitClient = GitkitClient
					.newBuilder()
					.setGoogleClientId(configData.getString("clientId"))
					.setServiceAccountEmail(configData.getString("serviceAccountEmail"))
					.setKeyStream(classLoader.getResourceAsStream(configData.getString("serviceAccountPrivateKeyFile")))
					.setWidgetUrl(configData.getString("widgetUrl")).setCookieName(configData.getString("cookieName"))
					.build();
		} catch (JSONException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	private GitKitUtil() {
		// Static members only.
	}

	/**
	 * Create the client. Note that the shortcut method GitkitClient gitkitClient = GitkitClient
	 * .createFromJson("gitkit-server-config.json"); fails in appengine (java.nio.file.* is
	 * restricted).
	 * 
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	public static GitkitClient gitKitClient() throws JSONException, IOException {
		return gitkitClient;
	}

	public static GitkitUser getGitKitUser(HttpServletRequest request) throws UnauthenticatedException {
		try {
			GitkitClient gitkitClient = GitKitUtil.gitKitClient();
			GitkitUser u = gitkitClient.validateTokenInRequest(request);
			if (u != null) {
				return gitkitClient.getUserByLocalId(u.getLocalId());
			} else {
				throw new UnauthenticatedException();
			}
		} catch (IOException | JSONException | GitkitClientException | GitkitServerException e) {
			throw new UnauthenticatedException();
		}
	}

	public static String accountId(String gitKitId) {
		return "gi-" + gitKitId;
	}
}
