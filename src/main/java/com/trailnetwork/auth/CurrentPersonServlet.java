package com.trailnetwork.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aawhere.net.HttpStatusCode;
import com.trailnetwork.api.Api;
import com.trailnetwork.api.Api.ApiReadException;

public class CurrentPersonServlet extends HttpServlet {
	private static final long serialVersionUID = 5811564508522325229L;

	private Api api;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		try {
			User user = AuthUtil.getUser(request);
			String accountId = GitKitUtil.accountId(user.getId());
			// person = AuthUtil.getPersonFromAccountId(api, accountId);
			// com.fasterxml.jackson.databind.ObjectMapper mapper = new
			// com.fasterxml.jackson.databind.ObjectMapper();
			// mapper.writeValue(response.getWriter(), person);
			response.getWriter().append(AuthUtil.getPersonAsJsonFromAccountId(api, accountId));
		} catch (UnauthenticatedException | ApiReadException e) {
			response.setStatus(HttpStatusCode.UNAUTHORIZED.value);
			// AuthUtil.redirectToSigninPage(response);
		}
	}

	@Override
	public void init() throws ServletException {
		super.init();
		api = Api.newInstance();
	}
}
