package com.trailnetwork.auth;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aawhere.log.LoggerFactory;
import com.google.identitytoolkit.GitkitUser;
import com.trailnetwork.api.Api;
import com.trailnetwork.api.Api.ApiReadException;
import com.trailnetwork.api.Person;

public class AuthUtil {

	private static Logger logger = LoggerFactory.getLogger(AuthUtil.class);
	private static final String EXPLORE_URL = "/";
	private static final String SIGNIN_URL = "/users/signin.html";

	private AuthUtil() {
		// no initiation allowed, use static methods.
	}

	public static String signinUrl() {
		return SIGNIN_URL;
	}

	public static User getUser(HttpServletRequest request) throws UnauthenticatedException {
		GitkitUser gkUser;

		gkUser = GitKitUtil.getGitKitUser(request);

		if (gkUser != null) {
			URL photoUrl = null;
			String gkUrl = null;
			try {
				gkUrl = gkUser.getPhotoUrl();
				if (gkUrl != null && !gkUrl.isEmpty()) {
					photoUrl = new URL(gkUrl);
				}
			} catch (MalformedURLException e) {
				logger.log(Level.WARNING, "Error processing photoUrl from GitKit: " + gkUrl, e);
			}
			return User.create().id(gkUser.getLocalId()).email(gkUser.getEmail()).provider(gkUser.getCurrentProvider())
					.name(gkUser.getName()).photoUrl(photoUrl).build();

		} else {
			throw new UnauthenticatedException();
		}
	}

	public static Person updateAccountAndPerson(Api api, User user) throws ApiReadException {
		String accountId = GitKitUtil.accountId(user.getId());
		String photoString = null;
		if(user.getPhotoUrl() != null) {
			photoString = user.getPhotoUrl().toString();
		}
		return api.account().putPerson(accountId, user.getName(), user.getEmail(), photoString);
	}

	public static Person getPersonFromAccountId(Api api, String accountId) throws ApiReadException {
		return api.account().getPerson(accountId);
	}

	public static String getPersonAsJsonFromAccountId(Api api, String accountId) throws ApiReadException {
		return api.account().getPersonAsJson(accountId);
	}

	public static void redirectToSigninPage(HttpServletResponse response) throws IOException {
		response.sendRedirect(SIGNIN_URL + "?mode=select");
	}

	public static void redirectToExplorePage(HttpServletResponse response) throws IOException {
		response.sendRedirect(EXPLORE_URL);
	}
}
