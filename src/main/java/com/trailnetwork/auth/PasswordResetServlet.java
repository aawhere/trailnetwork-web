package com.trailnetwork.auth;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aawhere.log.LoggerFactory;
import com.aawhere.net.HttpStatusCode;
import com.google.common.base.Optional;
import com.google.common.collect.Maps;
import com.google.identitytoolkit.GitkitClient.OobAction;
import com.google.identitytoolkit.GitkitClient.OobResponse;
import com.google.identitytoolkit.GitkitServerException;
import com.trailnetwork.lang.template.TemplateUtils;
import com.trailnetwork.mail.Email;
import com.trailnetwork.mail.EmailException;

import freemarker.template.TemplateException;

/**
 * GitKit Posts to this servlet when a user clicks on the forgotten password
 * link. This servlet is responsible for sending an email to the user with a
 * link to change their password.
 * 
 * @see https://developers.google.com/identity-toolkit/v3/required-endpoints
 * 
 * @author Brian Chapman
 *
 */
public class PasswordResetServlet extends HttpServlet {

	private static final long serialVersionUID = 5372118957004726032L;
	private final String EMAIL_BODY_PASSWORD_RESET_TEMPLATE_PATH = "/com/trailnetwork/auth/password-reset-email.ftl";
	private final String EMAIL_BODY_CHANGE_EMAIL_TEMPLATE_PATH = "/com/trailnetwork/auth/change-email-email.ftl";
	private final String EMAIL_BODY_TEMPLATE_URL_PARAM = "oobUrl";
	private final String EMAIL_BODY_TEMPLATE_NEW_EMAIL_PARAM = "newEmail";
	private final String EMAIL_BODY_TEMPLATE_CURRENT_EMAIL_PARAM = "currentEmail";
	private final String EMAIL_SUBJECT_PASSWORD_RESET = "TrailNetwork Password Reset";
	private final String EMAIL_SUBJECT_CHANGE_EMAIL = "TrailNetwork Email Change Request";
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		try {
			OobResponse oob = GitKitUtil.gitKitClient().getOobResponse(req);
			Optional<String> url = oob.getOobUrl();
			OobAction action = oob.getOobAction();
			

			if (url.isPresent()) {
				String urlString = url.get();
				
				if(action.equals(OobAction.CHANGE_EMAIL)) {
					sendEmailReset(urlString, oob);					
				} else {
					sendPasswordReset(urlString, oob);
				}
			}

			resp.getWriter().append(oob.getResponseBody());
		} catch (GitkitServerException | EmailException | TemplateException e) {
			resp.setStatus(HttpStatusCode.INTERNAL_ERROR.value);
			logger.log(Level.WARNING, "Error sending password reset email: " + e.getMessage(), e);
		}
	}

	@Override
	public void init() throws ServletException {
		super.init();
	}
	
	private String processPasswordResetTemplate(String url) throws IOException, TemplateException {
		Map<String, String> data = Maps.newHashMap();
		data.put(EMAIL_BODY_TEMPLATE_URL_PARAM, url);
		return TemplateUtils.processClassLoader(data, EMAIL_BODY_PASSWORD_RESET_TEMPLATE_PATH);
	}
	
	private String processChangeEmailTemplate(String url, OobResponse oob) throws IOException, TemplateException {
		Map<String, String> data = Maps.newHashMap();
		data.put(EMAIL_BODY_TEMPLATE_URL_PARAM, url);
		data.put(EMAIL_BODY_TEMPLATE_NEW_EMAIL_PARAM, oob.getNewEmail());
		data.put(EMAIL_BODY_TEMPLATE_CURRENT_EMAIL_PARAM, oob.getEmail());
		return TemplateUtils.processClassLoader(data, EMAIL_BODY_CHANGE_EMAIL_TEMPLATE_PATH);
	}
	
	private void sendPasswordReset(String urlString, OobResponse oob) throws IOException, TemplateException, EmailException {
		String body = processPasswordResetTemplate(urlString);
		Email email = Email.create().to(oob.getEmail())
				.toName(oob.getRecipient()).subject(EMAIL_SUBJECT_PASSWORD_RESET)
				.body(body).build();
		email.send();
	}
	
	private void sendEmailReset(String urlString, OobResponse oob) throws IOException, TemplateException, EmailException {
		String body = processChangeEmailTemplate(urlString, oob);
		Email email = Email.create().to(oob.getEmail())
				.toName(oob.getRecipient()).subject(EMAIL_SUBJECT_CHANGE_EMAIL)
				.body(body).build();
		email.send();
	}
}
