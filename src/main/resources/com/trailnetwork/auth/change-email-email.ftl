We received a request to change the email address for the account associated with this e-mail address. If you made this request, please follow the instructions below.

Click the link below to confirm your email address change from ${currentEmail} to ${newEmail}:

${oobUrl}

If you did not request to have your email changed you can safely ignore this email. Rest assured your account is safe.

If clicking the link doesn't seem to work, you can copy and paste the link into your browser's address window, or retype it there. Once you have returned to TrailNetwork.com we will confirm your request to change your email address and ask you to sign in using your new email address.

TrailNetwork will never e-mail you and ask you to disclose or verify your TrailNetwork password. If you receive a suspicious e-mail with a link to update your account information, do not click on the link--instead, report the e-mail to TrailNetwork for investigation. Thanks for visiting TrailNetwork!
